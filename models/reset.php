<?php
//require( 'connection.php' );

class Reset
{   
    private $db;
    function __construct($conn){
        $this->db = $conn;
    }

    public function verify($email)
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE email = '$email' ");
        $query->BindParam(1,$email);
        $query->execute();
        $data   = $query->fetch();
        if(!is_array($data)){ 
            
            return array(0,"Email tidak terdaftar");

        }else{

            $key    = get_token(50);
            $update = $this->db->prepare('UPDATE customer SET reset_key = ? WHERE email=?');
            $update->BindParam(1,$key);
            $update->BindParam(2,$email);
            $update->execute();        

            return array(1,"Silahkan periksa email anda untuk melakukan penggantian password");
        }   
    }

    public function get_data($email)
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE email = '$email' ");
        $query->BindParam(1,$email);
        $query->execute();
        $data    = $query->fetch();
        return $data;
    }
}

$reset = new Reset(@$conn);
?>