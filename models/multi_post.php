<?php
//require( 'connection.php' );

class MultiPost
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($slug,$page,$item)
    {
        $cat    = $this->get_menu($slug)['id'];
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 ORDER BY insertDate DESC LIMIT $page,$item");
        $query->BindParam(1,$cat);
        $query->execute();
        $data   = $query->fetchAll();
        if(!empty($data)){
            return $data;
        }
        
    }

    public function count($slug)
    {
        $cat    = $this->get_menu($slug)['id'];
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 ");     
        $query->BindParam(1,$cat);   
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }
/*
    public function get_category($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }*/

    public function get_menu($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
/*
    public function tbl_category()
    {
        $query  = $this->db->prepare("SELECT * FROM category");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }*/
}

$multipost = new MultiPost(@$conn);
?>