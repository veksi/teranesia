<?php
//require( 'connection.php' );

class Home
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($cat,$limit)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 ORDER BY insertDate DESC LIMIT $limit");
        $query->BindParam(1,$cat);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function get_menu($id)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
/*
    public function tbl_menu()
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE publish = 1 ");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }*/

    public function tbl_banner($tipe)
    {
        $query  = $this->db->prepare("SELECT * FROM banner WHERE tipe = ? AND publish = 1  ORDER BY insertDate DESC");       
        $query->BindParam(1,$tipe); 
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function is_ebook($id)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ? ");        
        $query->BindParam(1,$id); 
        $query->execute();
        $data  = $query->fetch();        
        return $data;
    }
}

$home = new Home(@$conn);
?>