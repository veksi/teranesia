<?php
//require( 'connection.php' );

class Mandatory
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index()
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE position = 2 ");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function menu() // mobile
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE publish = 1 AND parent = 0 ORDER BY insertDate ASC");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function have_cm($id)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function top_menu() // desktop
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE position = 1 AND parent = 0 AND publish = 1 ORDER BY insertDate ASC");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function side_menu() // desktop
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE position = 2 AND parent = 0 AND publish = 1 ORDER BY insertDate ASC");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function child_sm($parent_id) // desktop
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE position = 2 AND parent = ? AND publish = 1 ORDER BY insertDate ASC");        
        $query->BindParam(1,$parent_id);
        $query->execute();
        $data  = $query->fetchAll(); 
        return $data;
    }

    public function is_menu($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE slug = ? AND publish = 1 ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data   = $query->fetch();   
        return $data;
    }
/*
    public function is_category($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE slug = ? ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data   = $query->fetch();   
        return $data;
    }

    public function tbl_category()
    {
        $query  = $this->db->prepare("SELECT * FROM category");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }
*/
    public function waiting($id)
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE customer = ? AND status_trx = 'pending' ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function profile()
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE id = ? ");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

}

$mandatory = new Mandatory(@$conn);
?>