<?php
//require( 'connection.php' );

class SinglePost
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($slug)
    {
        $cat    = $this->get_menu($slug)['id'];
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 ORDER BY insertDate DESC ");
        $query->BindParam(1,$cat);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }
/*
    public function othernews($id)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE publish = 1 AND id != ? ORDER BY RAND() LIMIT 10");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
        
    }
*/
    public function get_menu($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

}

$singlepost = new SinglePost(@$conn);
?>