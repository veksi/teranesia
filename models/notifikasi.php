<?php
//require( 'connection.php' );

class Notification
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE order_id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function pending($status,$order_id)
    {
        $update = $this->db->prepare('UPDATE trx SET status_trx=? WHERE order_id=?');
        $update->BindParam(1,$status);
        $update->BindParam(2,$order_id);
        $update->execute();        
        
    }
    
    public function settlement($status,$pay_date,$order_id)
    {
        $update = $this->db->prepare('UPDATE trx SET status_trx=?,pay_date=? WHERE order_id=?');
        $update->BindParam(1,$status);
        $update->BindParam(2,$pay_date);
        $update->BindParam(3,$order_id);
        $update->execute();        
        
    }

    public function modified($status,$pay_date,$order_id)
    {
        $update = $this->db->prepare('UPDATE trx SET status_trx=?,pay_date=? WHERE order_id=?');
        $update->BindParam(1,$status);
        $update->BindParam(2,$pay_date);
        $update->BindParam(3,$order_id);
        $update->execute();        
        
    }
}

$notification = new Notification(@$conn);
?>