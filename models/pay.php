<?php
session_name("teranesia");
session_start();

require_once dirname(__DIR__).'/config/connection.php';
require_once dirname(__DIR__).'/public/function.php';

class Pay
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function pay($trx,$order_id){
        $insert = $this->db->prepare('INSERT INTO trx (customer,order_id,transactions) VALUES (?,?,?)');        
        $insert->BindParam(1,$_SESSION['customer']);
        $insert->BindParam(2,$order_id);
        $insert->BindParam(3,$trx);
        $insert->execute();
    }
}

$pay = new Pay(@$conn);

if(validation()==true){
    $trx      = decrypt($_POST['t']);
    $order_id = $_POST['o'];
    $pay->pay($trx,$order_id);        
}else{
    header("HTTP/1.0 404 Not Found");
    die();
}
?>