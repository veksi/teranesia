<?php
//require( 'connection.php' );

class Page
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ? AND publish = 1 ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        $viewer = $this->db->prepare("UPDATE product SET viewer = viewer+1 WHERE id=?");
        $viewer->BindParam(1,$id);
        $viewer->execute();
        
        return $data;        
    }

    public function others($cat)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 ORDER BY RAND() DESC LIMIT 6");
        $query->BindParam(1,$cat);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function get_menu($id)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
    
    public function get_slug($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE slug = ? ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data  = $query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
/*
    public function category($id)
    {
        $query  = $this->db->prepare("SELECT title FROM category WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data  = $query->fetch(PDO::FETCH_COLUMN);  
        return $data;
    }*/
}

$page = new Page($conn);
?>