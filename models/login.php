<?php
//require( 'connection.php' );

class Login
{   
    private $db;
    function __construct($conn){
        $this->db = $conn;
    }

    public function auth($u,$p)
    {
        $query = $this->db->prepare("SELECT * FROM customer WHERE email = ?");
        $query->BindParam(1,$u);
        $query->execute();
        $data = $query->fetch();
        if(password_verify($p,$data['pass']) && $data['active']==1 ){
            $_SESSION['authorized_customer']  = true;
            $_SESSION['customer']             = $data['id'];
            header("Location: {$_SERVER["HTTP_REFERER"]}");
        }elseif(password_verify($p,$data['pass']) && $data['active']==0 ){
            return array(0,"Mohon verifikasi email anda");
        }else{            
            return array(0,"Email atau password tidak sesuai");
        }

        
    }

    public function store($name,$hp,$email,$password)    
    {             
        $exist          = $this->get_account($email);
        if(empty($email)||empty($password)||empty($name)){
            return array(0,"Lengkapi data dengan benar");
        }elseif($exist>=1){
            return array(0,"Email sudah terdaftar");
        }else{
            $newpass    = password_hash($password, PASSWORD_DEFAULT);
        
            $insert = $this->db->prepare('INSERT INTO customer (fullname,hp,email,pass) VALUES (?,?,?,?)');        
            $insert->BindParam(1,$name);
            $insert->BindParam(2,$hp);
            $insert->BindParam(3,$email);
            $insert->BindParam(4,$newpass);
            $insert->execute();
            
            return array(1,"Silahkan periksa email anda untuk melakukan verifikasi");            
        }

    }

    public function get_account($email)
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE email = '$email' ");
        $query->BindParam(1,$email);
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

}

$login = new Login(@$conn);
?>