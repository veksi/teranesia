<?php
//require( 'connection.php' );

class Profil
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index()
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE id = ? ");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

    public function update($nama,$password,$passDB,$hp,$born,$borndate,$alamat,$picture,$imgDB)
    {
        global $imgpath;
        $newborndate    = date("Y-m-d", strtotime($borndate));

        $query  = $this->db->prepare("SELECT * FROM customer WHERE id = ?");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'customer/'.$data['foto'])){
                unlink($imgpath.'customer/'.$data['foto']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'customer/'.$newpicture);        
            
            if(file_exists($imgpath.'customer/'.$data['foto'])){
                unlink($imgpath.'customer/'.$data['foto']);
            }
        }

        if( empty($password) ){            //password tdk diubah
            $newpass        = $passDB;
        }else{       //password diubah
            $newpass        = password_hash($password, PASSWORD_DEFAULT);
        }
    
        $update = $this->db->prepare('UPDATE customer SET fullname=?,pass=?,hp=?,bornPlace=?,bornDate=?,alamat=?,foto=? WHERE id=?');
        $update->BindParam(1,$nama);        
        $update->BindParam(2,$newpass);
        $update->BindParam(3,$hp);
        $update->BindParam(4,$born);
        $update->BindParam(5,$newborndate);
        $update->BindParam(6,$alamat);
        $update->BindParam(7,$newpicture);
        $update->BindParam(8,$_SESSION['customer']);
        $update->execute();        
        
        header('location:'.$base_url.'profil');
        //return $_SESSION['customer'];
    }

}

$profil = new Profil(@$conn);
?>