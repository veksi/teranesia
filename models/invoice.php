<?php
//require( 'connection.php' );

class Invoice
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($order_id)
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE order_id = ? ");
        $query->BindParam(1,$order_id);
        $query->execute();
        $data   = $query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

}

$invoice = new Invoice(@$conn);
?>