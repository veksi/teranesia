<?php
//require( 'connection.php' );

class Promo
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($slug,$page,$item)
    {
        $query  = $this->db->prepare("SELECT * FROM promo WHERE publish = 1 ORDER BY insertDate DESC LIMIT $page,$item");
        $query->execute();
        $data   = $query->fetchAll();
        if(!empty($data)){
            return $data;
        }
        
    }

    public function count()
    {
        $query  = $this->db->prepare("SELECT * FROM promo WHERE publish = 1 ");        
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function get_category($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

    public function tbl_category()
    {
        $query  = $this->db->prepare("SELECT * FROM category");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }
}

$promo = new Promo(@$conn);
?>