<?php
//require( 'connection.php' );

class Newspage
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM news WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function othernews($id)
    {
        $query  = $this->db->prepare("SELECT * FROM news WHERE publish = 1 AND id != ? ORDER BY RAND() LIMIT 10");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
        
    }

}

$newspage = new Newspage(@$conn);
?>