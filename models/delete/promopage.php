<?php
//require( 'connection.php' );

class Promopage
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM promo WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function othernews($id)
    {
        $query  = $this->db->prepare("SELECT * FROM promo WHERE publish = 1 AND id != ? ORDER BY RAND() LIMIT 10");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
        
    }

}

$promopage = new Promopage(@$conn);
?>