<?php
//require( 'connection.php' );

class MultiPostDetail
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        $viewer = $this->db->prepare("UPDATE product SET viewer = viewer+1 WHERE id=?");
        $viewer->BindParam(1,$id);
        $viewer->execute();
        
        return $data;
    }

    public function otherpost($slug,$id)
    {

        $cat    = $this->get_menu($slug)['id'];
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? AND publish = 1 AND id != ? ORDER BY RAND() LIMIT 10");
        $query->BindParam(1,$cat);
        $query->BindParam(2,$id);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
        
    }

    public function get_menu($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM menu WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
}

$multipostdetail = new MultiPostDetail(@$conn);
?>