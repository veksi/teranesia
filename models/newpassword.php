<?php
//require( 'connection.php' );

class NewPwd
{   
    private $db;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($key)
    {        
        $query  = $this->db->prepare("SELECT * FROM customer WHERE reset_key = ? ");
        $query->BindParam(1,$key);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }
    
    public function setNewPwd($newPwd,$key)
    {
        $newpass    = password_hash($newPwd, PASSWORD_DEFAULT);
        $update     = $this->db->prepare("UPDATE customer SET pass = ? WHERE reset_key = ? ");
        $update->BindParam(1,$newpass);
        $update->BindParam(2,$key);
        $update->execute();

        if($update->execute()){            

            $id         = $this->index($key)['id'];
            $update     = $this->db->prepare("UPDATE customer SET reset_key = '' WHERE id = ? ");
            $update->BindParam(1,$id);
            $update->execute();
            
            $_SESSION['authorized_customer']  = true;
            $_SESSION['customer']             = $id;
            
            header("Location: ..");
        }else{
            return array(0,"Maaf penggantian password gagal");
        }
        
    }

}

$newPwd = new NewPwd(@$conn);
?>