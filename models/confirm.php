<?php
//require( 'connection.php' );

class Confirm
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($email)
    {        
        $query  = $this->db->prepare("SELECT * FROM customer WHERE email = ? ");
        $query->BindParam(1,$email);
        $query->execute();
        $data   = $query->fetch();        
        return $data;
    }
    
    public function verify($email)
    {

        $update = $this->db->prepare('UPDATE customer SET active = 1 WHERE email=?');
        $update->BindParam(1,$email);
        $update->execute();        
        
    }

}

$confirm = new Confirm(@$conn);
?>