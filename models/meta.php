<?php
//require( 'connection.php' );

class Meta
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ? ");        
        $query->BindParam(1,$id);
        $query->execute();
        $data  = $query->fetch();        
        return $data;
    }

}

$meta = new Meta(@$conn);
?>