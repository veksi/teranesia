<?php
//require( 'connection.php' );

class Pembelian
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($page,$item)
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE customer = ? AND status_trx = 'pending' ORDER BY insertDate DESC LIMIT $page,$item");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data   = $query->fetchAll();
        if(!empty($data)){
            return $data;
        }else{
            return "Produk tidak ditemukan";
        }
        
    }

    public function count()
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE customer = ? AND status_trx = 'pending' ");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function get_category($slug)
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE slug = '$slug' ");
        $query->BindParam(1,$slug);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

    public function tbl_category()
    {
        $query  = $this->db->prepare("SELECT * FROM category");        
        $query->execute();
        $data  = $query->fetchAll();        
        return $data;
    }

    public function profile()
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE id = ? ");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
}

$pembelian = new Pembelian(@$conn);
?>