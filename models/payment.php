<?php
//require( 'connection.php' );

class Payment
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($order_id)
    {
        $query  = $this->db->prepare("SELECT * FROM trx WHERE order_id = ? AND customer = ? ");
        $query->BindParam(1,$order_id);
        $query->BindParam(2,$_SESSION['customer']);
        $query->execute();
        $data   = $query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function update($respon,$exp,$va,$hrg,$status,$order_id)
    {

        $update = $this->db->prepare('UPDATE trx SET midtrans_respon=?, expired_va=?,rekening=?,harga=?,status_trx=? WHERE order_id=?');
        $update->BindParam(1,$respon);
        $update->BindParam(2,$exp);
        $update->BindParam(3,$va);
        $update->BindParam(4,$hrg);
        $update->BindParam(5,$status);
        $update->BindParam(6,$order_id);
        $update->execute();        
        
    }
}

$payment = new Payment(@$conn);
?>