<?php
//require( 'connection.php' );

class Beli
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function index($id)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        if(is_array($data)){
            return $data;
        }else{
            header("Location: ".$base_url);
        }
        
    }

    public function others($cat)
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE category = ? ORDER BY RAND() DESC LIMIT 6");
        $query->BindParam(1,$cat);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function get_category($id)
    {
        $query  = $this->db->prepare("SELECT * FROM category WHERE id = ? ");
        $query->BindParam(1,$id);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }

    public function category($id)
    {
        $query  = $this->db->prepare("SELECT title FROM category WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data  = $query->fetch(PDO::FETCH_COLUMN);        
        return $data;
    }

    public function get_customer()
    {
        $query  = $this->db->prepare("SELECT * FROM customer WHERE id = ? ");
        $query->BindParam(1,$_SESSION['customer']);
        $query->execute();
        $data    = $query->fetch();   
        return $data;
    }
}

$beli = new Beli($conn);
?>