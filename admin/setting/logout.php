<?php
session_name("teranesia_admin");
session_start();
session_destroy();
header("Location: {$_SERVER["HTTP_REFERER"]}");
?>