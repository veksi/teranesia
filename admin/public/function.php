<?php 
function lastURL($URL){
    $result = end(explode('-',$URL));
    return $result;
}

function seo($string, $ext=''){     
    $replace = '-';         
    $string = strtolower($string);     
    //replace / and . with white space     
    $string = preg_replace("/[\/\.]/", " ", $string);     
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);     
    //remove multiple dashes or whitespaces     
    $string = preg_replace("/[\s-]+/", " ", $string);     
    //convert whitespaces and underscore to $replace     
    $string = preg_replace("/[\s_]/", $replace, $string);     
    //limit the slug size     
    $string = substr($string, 0, 100);     
    //text is generated     
    return ($ext) ? $string.$ext : $string; 
}   

$bulan = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

function http_request($url){
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch);      
    return $output;
}

function encrypt($str) {
  $kunci = '979a218e0632df2935317f98d47956c742c67db9b00a7ee21db54c8dab6ffc41';
  $hasil = '';
	for ($i = 0; $i < strlen($str); $i++) {
		$karakter = substr($str, $i, 1);
		$kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		$karakter = chr(ord($karakter)+ord($kuncikarakter));
		$hasil .= $karakter;
	}
	return (base64_encode($hasil));
}

function decrypt($str) {
	$str = base64_decode($str);
	$hasil = '';
	$kunci = '979a218e0632df2935317f98d47956c742c67db9b00a7ee21db54c8dab6ffc41';
	for ($i = 0; $i < strlen($str); $i++) {
		$karakter = substr($str, $i, 1);
		$kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		$karakter = chr(ord($karakter)-ord($kuncikarakter));
		$hasil .= $karakter;
	}
	return $hasil;
}

function trimString($string,$word) { 
  $result = strip_tags($string);
  $result = implode(" ",array_slice(explode(" ",$result),0,$word));

  return $result; 
} 

function trimParagraph($string,$start,$word) { 
  $result = strip_tags($string);
  $result = implode(" ",array_slice(explode(" ",$result),$start,$word));

  return $result; 
} 

function Rp($val){
  if($val==0){
    return "--";
  }else{
    return "RP ".$val;
  }
}

function num($val){
  return str_replace(",",".",number_format((float)$val));
}

function tgl_indo($tanggal, $cetak_hari = true, $jam = false){
  $hari = array ( 1 =>    'Senin',
              'Selasa',
              'Rabu',
              'Kamis',
              'Jumat',
              'Sabtu',
              'Minggu'
          );
          
  $bulan = array (1 =>   'Januari',
              'Februari',
              'Maret',
              'April',
              'Mei',
              'Juni',
              'Juli',
              'Agustus',
              'September',
              'Oktober',
              'November',
              'Desember'
          );
  $partDate = explode(' ', $tanggal);
  $split 	  = explode('-', $partDate[0]);
  $split2   = explode(':', $partDate[1]);

  $tgl_indo = $split[2].' '.$bulan[(int)$split[1]].' '.$split[0];

  if($jam){        
      return $tgl_indo.', '.$split2[0].':'.$split2[1].' WIB';
  }
  
  if ($cetak_hari) {
      $num = date('N', strtotime($tanggal));
      return $hari[$num].' '.$tgl_indo;
  }

  return $tgl_indo;
}

function otp(){
  $randomNum = substr(str_shuffle("0123456789"), 0, 5);
  return $randomNum;
}

function month_fwd($date,$exp){
  $tgl1 = date("Y-m-d", strtotime($date));
  $tgl2 = date('Y-m-d', strtotime('+'.$exp.' days', strtotime($tgl1)));
  return formatDayTime($tgl2.' 00:00:00');
}

function formatDayTime($date){
  $date     = date("Y-m-d", strtotime($date));
  $bulan    = array("January","February","March","April","May","June","July","August","September","October","November","December");
  $hari     = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  $month    = intval(date('m',strtotime($date))) - 1;
  $days     = date('w',strtotime($date));
  $tg_angka = date('d',strtotime($date));
  $year     = date('Y',strtotime($date));
  return $hari[$days].', '.$bulan[$month].' '.$tg_angka.', '.$year;
}

function getDay($data){
  $booking    = new DateTime($data);
  $today      = new DateTime();
  $diff       = $today->diff($booking);
  $days       = $diff->d;
  if($days==1){
    return '<i class="fa fa-clock-o" aria-hidden="true"></i> '.$days.' Day Remaining';
  }else{
    return '<i class="fa fa-clock-o" aria-hidden="true"></i> '.$days.' Days Remaining';
  }
}



function getUrlID($url)
{
    $result = explode('-',$url);
    $result = end($result);
    return $result;
}

function unique($length){
    $karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
    $string = '';
    for ($i = 0; $i < $length; $i++) {
      $pos = rand(0, strlen($karakter)-1);
      $string .= $karakter{$pos};
    }
    return $string;
}

function get_token($panjang){
  $token = array(
   range(1,9),
   range('a','z'),
   range('A','Z')
  );

  $karakter = array();
  foreach($token as $key=>$val){
   foreach($val as $k=>$v){
    $karakter[] = $v;
   }
  }

  $token = null;
  for($i=1; $i<=$panjang; $i++){
   // mengambil array secara acak
   $token .= $karakter[rand($i, count($karakter) - 1)];
  }

  return $token;
}

function paginate($item_per_page, $current_page, $total_records, $total_pages, $page_url){
  $pagination = '';
  if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
    $pagination .= '<ul class="pagination">';
    
    $right_links    = $current_page + 3; 
    //$previous       = $current_page - 3; //previous link 
    $previous       = $current_page - 1; //previous link 
    $next           = $current_page + 1; //next link
    $first_link     = true; //boolean var to decide our first link
    
    if($current_page > 1){
      $previous_link = ($previous==0)?1:$previous;
      //$pagination .= '<li class="first"><a href="'.$page_url.'?page=1" title="First">«</a></li>'; //first link
      $pagination .= '<li><a href="'.queryBuilder('page='.$previous).'" title="Previous"> &nbsp;<i class="fa fa-chevron-left"></i> </a></li>'; //previous link
      for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
        if($i > 0){
          $pagination .= '<li><a href="'.queryBuilder('page='.$i).'">'.$i.'</a></li>';
        }
      }   
      $first_link = false; //set first link to false
    }else{
      $pagination .= '<li class="disabled"><a href=# onclick="return false"> &nbsp;<i class="fa fa-chevron-left"></i> </a></li>'; 
    }
      
    if($first_link){ //if current active page is first link
      //$pagination .= '<li class="active">'.$current_page.'</li>';
      $pagination .= '<li class="active"><a href="#">'.$current_page.' <span class="sr-only">(current)</span></a></li>';
    }elseif($current_page == $total_pages){ //if it's the last active link
      //$pagination .= '<li class="last active">'.$current_page.'</li>';
      $pagination .= '<li class="active"><a href="#">'.$current_page.' <span class="sr-only">(current)</span></a></li>';
    }else{ //regular current link
      //$pagination .= '<li class="active">'.$current_page.'</li>';
      $pagination .= '<li class="active"><a href="#">'.$current_page.' <span class="sr-only">(current)</span></a></li>';
    }
              
    for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
      if($i<=$total_pages){
        $pagination .= '<li><a href="'.queryBuilder('page='.$i).'">'.$i.'</a></li>';
      }
    }
    if($current_page < $total_pages){ 
      $next_link = ($i > $total_pages)? $total_pages : $i;
      $pagination .= '<li><a href="'.queryBuilder('page='.$next).'" > &nbsp;<i class="fa fa-chevron-right fa-1x"></i> </a></li>'; //next link
      //$pagination .= '<li class="last"><a href="'.$page_url.'?page='.$total_pages.'" title="Last">»</a></li>'; //last link
    }else{
      $next_link = ($i > $total_pages)? $total_pages : $i;
      $pagination .= '<li class="disabled"><a href="#" onclick="return false"> &nbsp;<i class="fa fa-chevron-right fa-1x"></i> </a></li>'; //next link
      //$pagination .= '<li class="last"><a href="'.$page_url.'?page='.$total_pages.'" title="Last">»</a></li>'; //last link
    }
      
    $pagination .= '</ul>'; 
  }
  return $pagination; //return pagination links
}

function queryBuilder($var){  
  parse_str($_SERVER['REDIRECT_QUERY_STRING'],$arrQueryURL);
  parse_str($var,$arrPage);
  $arrURL = array_merge(array_unique($arrQueryURL),array_filter(array_unique($arrPage)));
  return $_SERVER['SCRIPT_URI'].'?'.http_build_query($arrURL);
}

function queryRemove($key){ 
  parse_str($_SERVER['REDIRECT_QUERY_STRING'],$arrQueryURL);
  unset($arrQueryURL[$key]);
  return $_SERVER['SCRIPT_URI'].'?'.http_build_query($arrQueryURL);
}

function compress($source, $destination) {
  ini_set('memory_limit', '-1');
  $info = getimagesize($source);

  if ($info['mime'] == 'image/jpeg') 
      $image = imagecreatefromjpeg($source);

  elseif ($info['mime'] == 'image/gif') 
      $image = imagecreatefromgif($source);

  elseif ($info['mime'] == 'image/png') 
      
      $image = imagecreatefrompng($source);

  imagejpeg(@$image, @$destination, 60);
      
  return $destination;
}
?>