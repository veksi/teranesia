<div class="container-fluid">
    <div class="form-group">
        <a href="<?=$base_url?>transaction/add" class="btn btn-success float-right addBtn"><i class="fa fa-plus"></i> Add</a>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="box-body table-responsive">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Invoice</th>
                    <th style="border-bottom:none;font-weight:600">Customer</th>
                    <th style="border-bottom:none;font-weight:600">Produk</th>
                    <th style="border-bottom:none;font-weight:600">Status</th>
                    <th style="border-bottom:none;font-weight:600">Tanggal</th>
                    <th style="border-bottom:none;font-weight:600"><i class="fa fa-cog" aria-hidden="true"></i></th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="<?=$base_url;?>plugin/dataTable/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">

   $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({        
        "processing": true,
        "oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": {
            'type': 'POST',
            'url': '<?=$base_url;?>models/transaction.php',
            'data': {
                csrf: '<?=get_token(50)?>',
                mode: 'list',
            },
        },
        "order": [[ 5, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": actionCol,
                "targets": 6
            }
        ],
    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function actionCol(data, type, full) {

        view    = '<form style="display:inline" method="post" action="<?=$base_url?>transaction/view"><input type="hidden" name="id" value="'+full[0]+'"><li><input class="dropdown-item" type="submit" value="View" style="outline:none"></li></form>';
        del     = '<a class="dropdown-item" href=# onclick=deleteRecord("'+full[0]+'","<?=$imgpath?>transaction/")>Delete</a>';
        edit    = '<form style="display:inline" method="post" action="<?=$base_url?>transaction/edit"><input type="hidden" name="id" value="'+full[0]+'"><li><input class="dropdown-item" type="submit" value="Edit" style="outline:none"></li></form>';

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li>'+view+'</li><li>'+edit+'</li><li>'+del+'</li></ul></div>';
    }

    function deleteRecord(id,path){
        swal({
            title: "Data akan dihapus?",
            text: "Setelah dihapus, data tidak bisa dikembalikan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type:'POST',
                    url: '<?=$base_url;?>models/transaction.php',
                    data: {
                        csrf: '<?=get_token(50)?>',
                        mode:'del',
                        id:id,
                        path:path,
                    },
                    success: function(res)
                    {
                        reload();
                        success("Data telah dihapus!");
                    }       
                });
                
            } else {
                //swal("Your data is safe!");
            }
        });
    }

    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload();
    }

</script>