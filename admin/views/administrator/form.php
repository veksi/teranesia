<?php 
if(isset($_POST['submit'])){
    
    if(empty($_POST['id'])){        
        $administrator->store($_POST['first'],$_POST['last'],$_POST['email'],$_POST['password'],$_POST['hp'],$_POST['born'],$_POST['Dates'],$_POST['alamat'],$_FILES['photo1']);
        require __DIR__.'/new_admin_email.php';
    }else{
        $administrator->update($_POST['first'],$_POST['last'],$_POST['email'],$_POST['password'],$_POST['hp'],$_POST['born'],$_POST['Dates'],$_POST['alamat'],$_FILES['photo1'],$_POST['imgDB1'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $administrator->edit($_POST['id']);
}

?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar img-thumbnail rounded-circle" alt="avatar" style="width:250px;height:250px;" src="<?=(!empty($edit['photo'])) ? $imgurl.'administrator/'.$edit['photo'] : $imgadmin."noprofile.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['foto']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <small id="emailHelp" class="pull-left form-text text-muted">Nama Depan</small>
                                <input type="text" id="first" name="first" value="<?=htmlentities(@$edit['first_name'])?>" class="form-control" placeholder="Nama Depan" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="form-group">
                                <small id="emailHelp" class="pull-left form-text text-muted">Nama Belakang</small>
                                <input type="text" id="last" name="last" value="<?=htmlentities(@$edit['last_name'])?>" class="form-control" placeholder="Nama Belakang" autocomplete="off" >
                            </div>
                        </div>
                    </div>       
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Email</small>
                        <input type="email" id="email" name="email" value="<?=@$edit['email']?>" class="form-control" placeholder="Email" autocomplete="off" >
                    </div>                 
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Password</small>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Handphone *</small>
                        <input type="text" id="hp" name="hp" value="<?=@$edit['phone']?>" class="form-control" placeholder="Handphone" autocomplete="off" >
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Tempat Lahir *</label>
                                <input type="text" id="born" name="born" value="<?=htmlentities(@$edit['bornPlace'])?>" class="form-control" placeholder="Tempat Lahir" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Tanggal Lahir</label>
                                <div class="input-group date form-group" id="datepicker">
                                    <input type="text" class="form-control" id="Dates" name="Dates" value="<?=date("d-m-Y", strtotime(@$edit['bornDate']))?>" placeholder="Pilih Tanggal" autocomplete="off"  />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <span class="count"></span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <small id="emailHelp" class="form-text text-muted">Alamat *</small>                   
                        <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Alamat"><?=@$edit['alamat'];?></textarea>
                    </div>

                    <div class="float-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>administrator" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>noprofile.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}
</script>

<!--multi datepicker -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function() {
    $('#datepicker').datepicker({
        numberOfMonths: 4,
        startDate: -Infinity,
        multidate: false,
        //multidateSeparator:'|',
        format: "dd-mm-yyyy",
        daysOfWeekHighlighted: "5,6",
        //datesDisabled: ['31/08/2017'],
        language: 'en',
        clearBtn:true,
        toggleActive:true,
        forceParse:true,
        todayHighlight:true,
    });
});
</script>