<div class="container-fluid">
    <div class="row">
        <div class="col-6 col-sm-3">
            <div class="card bg-white text-muted mb-4">
                <div class="card-body text-center"><h2><?=$dashboard->customer();?></h2></div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-muted stretched-link" href="<?=$base_url.'customer'?>">Customer</a>
                    <div class="small text-muted"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="card bg-white text-muted mb-4">
                <div class="card-body text-center"><h2><?=$dashboard->product();?></h2></div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-muted stretched-link" href="<?=$base_url.'product'?>">Product</a>
                    <div class="small text-muted"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="card bg-white text-muted mb-4">
                <div class="card-body text-center"><h2><?=$dashboard->postingan();?></h2></div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-muted stretched-link" href="<?=$base_url.'post'?>">Artikel</a>
                    <div class="small text-muted"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="card bg-white text-muted mb-4">
                <div class="card-body text-center"><h2><?=$dashboard->transactions();?></h2></div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-muted stretched-link" href="<?=$base_url.'transaction'?>">Transactions</a>
                    <div class="small text-muted"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mb-5">
    <div class="row">
        <div class="col-12">
            <div class="card bg-white text-muted mb-4">
                <div class="card-body text-center"><h2><?=Rp(num($dashboard->trx_success()));?></h2></div>
                <div class="card-footer text-center">
                    <div class="small text-muted">Total Transaksi</div>                    
                </div>
            </div>
        </div>
        <div class="col-12">
            <div id="graph" style="width:99%; height: auto; margin: 0;padding:0"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    Highcharts.chart('graph', {
        chart: {
            type: 'spline',
            style: {
                    fontFamily: ''
                }
        },
        title: {
            text: '<b>Transaksi 1 Bulan Terakhir</b>'
        },
        xAxis: {
            title: {
                text: 'Tanggal',
            },            
            categories: [<?= join($dashboard->graphData()[0],',') ?>],
        },
        yAxis: {
            title: {
                text: 'Valuasi'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
        },
        series: [
        {   
            name: 'Pembayaran',            
            data: [<?=join($dashboard->graphData()[1],',') ?>],
            color: '#F8CB00'
        }        
        ]
    });

</script>