<nav class="sb-topnav navbar navbar-expand navbar-white bg-white shadow">
    <a class="navbar-brand text-center text-muted logo" href="<?=$base_url?>">
        <img src="<?=$imgurl?>logo-desktop.png" height="40px">
    </a>
    <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fa fa-bars text-muted"></i></button>
    <!-- Navbar-->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-muted" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  class="avatar rounded-circle img-thumbnail" alt="avatar" style="width:40px;height:40px;"  ></a>
            <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?=$base_url?>profil">Profil</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void(0)" onclick="window.location.href='<?=$base_url;?>/setting/logout.php'"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp; Keluar</a>
            </div>
        </li>
    </ul>
</nav>