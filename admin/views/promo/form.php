<script src="<?=$base_url?>vendor/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea#konten',
        branding: false,
        height: 400,
        theme: 'modern',
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help responsivefilemanager'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager | print preview media | forecolor backcolor emoticons | codesample ',
        image_advtab: true,
            external_filemanager_path:"<?=$base_url?>vendor/filemanager/",
        filemanager_title:"Responsive Filemanager",
        external_plugins: { "filemanager" : "<?=$base_url?>vendor/filemanager/plugin.min.js"},
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [ '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i' ]
    });

</script>

<?php 
if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $promo->store($_POST['title'],$_POST['konten'],$_FILES['photo1']); 
    }else{
        $promo->update($_POST['title'],$_POST['konten'],$_FILES['photo1'],$_POST['imgDB1'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $promo->edit($_POST['id']);
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar img-thumbnail" alt="avatar" style="width:250px;height:250px;" src="<?=(!empty($edit['foto'])) ? $imgurl.'promo/'.$edit['foto'] : $imgadmin."nofoto.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['foto']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Judul *</small>
                        <input type="text" id="title" name="title" value="<?=htmlentities(@$edit['title'])?>" class="form-control" placeholder="Judul" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Konten *</small>      
                        <textarea id="konten" name="konten" class="form-control" rows="3" placeholder="Konten"><?=@$edit['content']?></textarea>
                    </div>

                    <div class="float-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>promo" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}
</script>