<?php 
if(isset($_POST['login'])){
 
    $resLog = $login->auth($_POST['u'],$_POST['p']);        
    if(!is_array($resLog)){ ?>
        <script type="text/javascript">swal("Gagal","Email atau password tidak sesuai","warning",).then(function(){ window.history.back(); });</script>
    <?php }else{ 
        header("Location: {$_SERVER["HTTP_REFERER"]}"); 
    }
   
}else{ ?>

    <body class="bg-white">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Administrator</h3></div>
                                    <div class="card-body">
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="login">
                                            <div class="form-group">
                                                <label class="small mb-1" for="u">Email</label>
                                                <input class="form-control py-4" id="u" name="u" type="text" placeholder="Email" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="p">Kata Sandi</label>
                                                <input class="form-control py-4" id="p" name="p" type="password" placeholder="Kata Sandi" />
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                                                    <label class="custom-control-label" for="rememberPasswordCheck">Ingat saya</label>
                                                </div>
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <button type="submit" value="Masuk" class="btn btn-primary loadingBtn" >Masuk</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?=$instrukturPath?>reset">Lupa kata sandi?</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <?php //require "modul/copy.php"; ?>
            </div>
        </div>
    </body>
    
    <script>
    $("#u").focus();
    function validasi(){

        var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email       = $("#u").val();
        var password    = $("#p").val();
        
        if (email==""){
            swal("","Email tidak terdaftar","warning",).then(function(){ $("#u").focus();$(".loadingBtn").html('Masuk'); });
            return false;

        }else if (!filter.test(email)) {

            swal("","Email anda tidak valid","warning",).then(function(){ $("#u").focus();$(".loadingBtn").html('Masuk'); });
            return false;

        }else if (password==""){
            swal("","Password salah","warning",).then(function(){ $("#p").focus();$(".loadingBtn").html('Masuk'); });
            return false;

        }else{

            return true;

        }

    }
    </script>

<?php } ?>