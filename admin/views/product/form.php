<script src="<?=$base_url?>vendor/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea#konten',
        branding: false,
        height: 400,
        theme: 'modern',
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak ',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help responsivefilemanager'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager | print preview media | forecolor backcolor emoticons | codesample ',
        image_advtab: true,
            external_filemanager_path:"<?=$base_url?>vendor/filemanager/",
        filemanager_title:"Responsive Filemanager",
        external_plugins: { "filemanager" : "<?=$base_url?>vendor/filemanager/plugin.min.js"},
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [ '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i' ]
    });

</script>

<?php 
if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $product->store($_POST['category'],$_POST['title'],$_POST['konten'],$_POST['price'],$_FILES['photo1'],$_FILES['lampiran'],$_POST['insertDate']); 
    }else{
        $product->update($_POST['category'],$_POST['title'],$_POST['konten'],$_POST['price'],$_FILES['photo1'],$_POST['imgDB1'],$_FILES['lampiran'],$_POST['insertDate'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $product->edit($_POST['id']);
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar img-thumbnail" alt="avatar" style="width:250px;height:250px;" src="<?=(!empty($edit['picture'])) ? $imgurl.'product/'.$edit['picture'] : $imgadmin."nofoto.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['picture']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Nama Produk *</small>
                        <input type="text" id="title" name="title" value="<?=htmlentities(@$edit['title'])?>" class="form-control" placeholder="Nama Produk" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Kategori *</small>
                        <select id="category" name="category"  class="form-control" >
                            <option value="" disabled selected hidden>Pilih Kategori</option>                            
                            <?php foreach($product->menu() as $rootmenu){ ?>
                                <option value="<?=$rootmenu['id'];?>" <?=$rootmenu['id']==@$edit['category'] ? 'selected' : ''; ?> ><?=$rootmenu['title'];?></option>
                                <?php foreach($product->childmenu($rootmenu['id']) as $childmenu){ ?>                             
                                    <option value="<?=$childmenu['id'];?>" <?=$childmenu['id']==@$edit['category'] ? 'selected' : ''; ?>>--<?=$childmenu['title'];?></option>
                                <?php }?>
                            <?php }?>
                        </select>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Harga</small>
                        <input type="text" name="price" id="price" class="form-control" value="<?=@$edit['price']?>" placeholder="Rp. 200.000.000" onkeypress="return hanyaAngka(event)" autocomplete="off">
                    </div>

                    <div class="form-group"> 
                        <small id="emailHelp" class="form-text text-muted">Tanggal</small>
                        <div class="input-group mb-3" id="datetimepicker1">
                            <input type="text" name="insertDate"  value="<?=@$edit['insertDate']?>" class="form-control datetimepicker1" placeholder="Password" aria-label="Recipient's username" aria-describedby="basic-addon2">                                
                            <div class="input-group-append">
                                <span class="input-group-text datetimepicker1" id="basic-addon2"><span class='fa fa-calendar fa-fw'></span></span>
                            </div>
                        </div>                         
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Deskripsi *</small>      
                        <textarea id="konten" name="konten" class="form-control" rows="3" placeholder="Deskripsi"><?=@$edit['descriptions']?></textarea>
                    </div>                    

                    <div class="row">
                        <div class="col">
                            <small id="emailHelp" class="pull-left form-text text-muted">Lampiran</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">                                
                                <div class="custom-file">
                                    <input type="file" name="lampiran" class="custom-file-input customFile" id="customFile">                                    
                                    <label class="custom-file-label filename" for="customFile" style="height:38px;overflow:hidden;">Choose file</label>
                                    <div class="valid-tooltip">Upload complete</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <div class="reload">
                                <?php if(empty($edit['attachment'])){ ?>
                                    <button class="btn btn-light btn-block" disabled>
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Tidak ada file
                                    </button>
                                <?php }else{ ?>
                                    
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="javascript:void(0)" onclick="deleteFile('<?=@$edit['id']?>','<?=$filepath?>e-book/')" class="btn btn-primary btn-lg">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <a href="<?=$fileurl.'e-book/'.@$edit['attachment']?>" target="_blank" class="btn btn-primary btn-lg">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="float-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>product" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

var dengan_rupiah = document.getElementById('price');
dengan_rupiah.addEventListener('keyup', function(e)
{
    dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');    
});

dengan_rupiah.addEventListener('keydown', function(event)
{
    //limitCharacter(event);
});

function formatRupiah(bilangan, prefix)
{
    var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function limitCharacter(event)
{
    key = event.which || event.keyCode;
    if ( key != 188 // Comma
            && key != 8 // Backspace
            && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
            && (key < 48 || key > 57) // Non digit
            // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
        ) 
    {
        event.preventDefault();
        return false;
    }
}

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
}

$('.filename').html('<?=(@$edit['attachment']!='')?@$edit['attachment']:'Lampiran'?>');

function deleteFile(id,path){
    swal({
        title: "Data akan dihapus?",
        text: "Setelah dihapus, data tidak bisa dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type:'POST',
                url: '<?=$base_url;?>models/product.php',
                data: {
                    csrf: '<?=get_token(50)?>',
                    mode:'attachment',
                    id:id,
                    path:path,
                },
                success: function(respon){
                    $(".reload").load(window.location.href + " .reload" );
                    $('.filename').addClass("selected").html('Lampiran');
                }
            });
            
        } else {
           
        }
    });
}

$('.customFile').on('change',function(){
    var fileName = document.getElementById("customFile").files[0].name;
    $(this).next('.filename').addClass("selected").html(fileName);
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?=$base_url?>vendor/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow,
        });
        
    });
</script>