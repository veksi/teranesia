<?php 
if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $menu->store($_POST['title'],$_POST['parent'],$_POST['position'],$_POST['model'],$_FILES['photo1'],$_POST['insertDate']); 
    }else{
        $menu->update($_POST['title'],$_POST['parent'],$_POST['position'],$_POST['model'],$_FILES['photo1'],$_POST['imgDB1'],$_POST['insertDate'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $menu->edit($_POST['id']);
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar img-thumbnail" alt="avatar" style="width:250px;height:250px;" src="<?=(!empty($edit['icon'])) ? $imgurl.$edit['icon'] : $imgadmin."nofoto.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['icon']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik icon untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Judul *</small>
                        <input type="text" id="title" name="title" value="<?=htmlentities(@$edit['title'])?>" class="form-control" placeholder="Judul" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Level Menu *</small>
                        <select id="parent" name="parent"  class="form-control" >
                            <option value="" disabled selected hidden>Pilih Level Menu</option>
                            <option value="0" <?=@$edit['parent']==0 ? 'selected' : ''; ?> >Root</option>
                            <?php foreach($menu->menu() as $rootmenu){ ?>
                                <option value="<?=$rootmenu['id'];?>" <?=$rootmenu['id']==@$edit['parent'] ? 'selected' : ''; ?> >-<?=$rootmenu['title'];?></option>
                                <?php foreach($menu->childmenu($rootmenu['id']) as $childmenu){ ?>                            
                                    <option value="<?=$childmenu['id'];?>" >--<?=$childmenu['title'];?></option>
                                <?php }?>
                            <?php }?>
                        </select>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Posisi *</small>
                        <select id="position" name="position"  class="form-control" >
                            <option value="" disabled selected hidden>Pilih Posisi</option>
                            <option value="1" <?=@$edit['position']==1 ? 'selected' : ''; ?> >Top Menu</option>
                            <option value="2" <?=@$edit['position']==2 ? 'selected' : ''; ?> >Side Menu</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Model *</small>
                        <select id="model" name="model"  class="form-control" >
                            <option value="" disabled selected hidden>Pilih Model</option>
                            <option value="1" <?=@$edit['model']==1 ? 'selected' : ''; ?> >Single Post</option>
                            <option value="2" <?=@$edit['model']==2 ? 'selected' : ''; ?> >Multi Post</option>
                            <option value="3" <?=@$edit['model']==3 ? 'selected' : ''; ?> >Email Form</option>
                            <option value="4" <?=@$edit['model']==4 ? 'selected' : ''; ?> >Testimoni</option>
                            <option value="5" <?=@$edit['model']==5 ? 'selected' : ''; ?> >Home</option>
                        </select>
                    </div>

                    <div class="form-group"> 
                        <small id="emailHelp" class="form-text text-muted">Tanggal</small>
                        <div class="input-group mb-3" id="datetimepicker1">
                            <input type="text" name="insertDate"  value="<?=@$edit['insertDate']?>" class="form-control datetimepicker1" placeholder="Password" aria-label="Recipient's username" aria-describedby="basic-addon2">                                
                            <div class="input-group-append">
                                <span class="input-group-text datetimepicker1" id="basic-addon2"><span class='fa fa-calendar fa-fw'></span></span>
                            </div>
                        </div>                         
                    </div>

                    <div class="float-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>menu" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?=$base_url?>vendor/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow,
        });
        
    });
</script>