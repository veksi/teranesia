<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-white bg-white" id="sidenavAccordion">
        <div class="sb-sidenav-menu border-right">
            <div class="nav pt-3">
                <a class="nav-link text-muted px-4" href="<?=$base_url?>dashboard">
                    <div class="sb-nav-link-icon"><i class="fa fa-th-large"></i></div>
                    Dashboard
                </a>
                <a class="nav-link text-muted px-4" href="<?=$base_url?>customer">
                    <div class="sb-nav-link-icon"><i class="fa fa-users"></i></div>
                    Customer
                </a>
                <a class="nav-link text-muted px-4" href="<?=$base_url?>product">
                    <div class="sb-nav-link-icon"><i class="fa fa-dropbox" aria-hidden="true"></i></div>
                    Product
                </a>
                <a class="nav-link text-muted px-4" href="<?=$base_url?>transaction">
                    <div class="sb-nav-link-icon"><i class="fa fa-line-chart"></i></div>
                    Transaction
                </a>                                
<!--                <a class="nav-link text-muted px-4" href="<?=$base_url?>category">
                    <div class="sb-nav-link-icon"><i class="fa fa-list-ul"></i></div>
                    Category
                </a>   -->
                <a class="nav-link text-muted px-4" href="<?=$base_url?>banner">
                    <div class="sb-nav-link-icon"><i class="fa fa-picture-o" aria-hidden="true"></i></i></div>
                    Banner & Ads
                </a>  
                 
                <a class="nav-link text-muted px-4" href="<?=$base_url?>post">
                    <div class="sb-nav-link-icon"><i class="fa fa-newspaper-o" aria-hidden="true"></i></div>
                    Postingan
                </a> 
                <!--
               <a class="nav-link text-muted px-4" href="<?=$base_url?>news">
                    <div class="sb-nav-link-icon"><i class="fa fa-newspaper-o" aria-hidden="true"></i></div>
                    News
                </a>                  
                <a class="nav-link text-muted px-4" href="<?=$base_url?>promo">
                    <div class="sb-nav-link-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></div>
                    Promo
                </a>  
                <a class="nav-link text-muted px-4" href="<?=$base_url?>partnership">
                    <div class="sb-nav-link-icon"><i class="fa fa-handshake-o" aria-hidden="true"></i></div>
                    Partnership
                </a>   
                <a class="nav-link text-muted px-4" href="<?=$base_url?>testimoni">
                    <div class="sb-nav-link-icon"><i class="fa fa-handshake-o" aria-hidden="true"></i></div>
                    Testimoni
                </a>           -->
                <a class="nav-link text-muted px-4" href="<?=$base_url?>menu">
                    <div class="sb-nav-link-icon"><i class="fa fa-list-ul"></i></div>
                    Menu
                </a>   
                <a class="nav-link text-muted px-4" href="<?=$base_url?>administrator">
                    <div class="sb-nav-link-icon"><i class="fa fa-user-circle-o"></i></div>
                    Administrator
                </a>                
            </div>
        </div>
       
    </nav>
</div>
