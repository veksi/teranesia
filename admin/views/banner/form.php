<?php 
if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $banner->store($_POST['link'],$_FILES['photo1'],$_POST['tipe']); 
    }else{
        $banner->update($_POST['link'],$_FILES['photo1'],$_POST['tipe'],$_POST['imgDB1'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $banner->edit($_POST['id']);
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-8">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar img-fluid img-thumbnail" alt="avatar" style="max-height:250px;" src="<?=(!empty($edit['picture'])) ? $imgurl.'banner/'.$edit['picture'] : $imgadmin."nofoto.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['picture']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                    <br>
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Link</small>
                        <input type="text" id="link" name="link" value="<?=@$edit['link']?>" class="form-control" placeholder="Link" autocomplete="off" >
                    </div>       
                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Tipe *</small>
                        <select id="tipe" name="tipe" class="form-control" >
                            <option value="" disabled selected hidden>Pilih Tipe</option>
                            <option value="1">Slider</option>
                            <option value="2">Ads</option>
                        </select>
                    </div>
                    <div class="text-center">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>banner" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}
</script>