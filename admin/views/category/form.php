<?php 
if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $category->store(stripslashes($_POST['title']),$_POST['icon']); 
    }else{
        $category->update(stripslashes($_POST['title']),$_POST['icon'],$_POST['id']); 
    }
}

if(isset($_POST['id'])){
    $edit = $category->edit($_POST['id']);
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <input type=hidden name='id' value='<?=@$_POST['id']?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-8">
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Kategori *</small>
                        <input type="text" id="title" name="title" value="<?=htmlentities(@$edit['title']); ?>" class="form-control" placeholder="Kategori" autocomplete="off" >
                    </div>
                    <small id="emailHelp" class="pull-left form-text text-muted">Icon *</small>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="icon" name="icon" value="<?=htmlentities(@$edit['icon'])?>" placeholder="example : fa fa-spa" autocomplete="off" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank" class="btn border"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="float-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <a href="<?=$base_url?>category" type="button" class="btn btn-info">Cancel</a>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}
</script>