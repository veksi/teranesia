<div class="container-fluid">
    <div class="jumbotron text-center">
        <h1 class="display-1"><b>404</b></h1>
        <p class="lead">Halaman tidak ditemukan !!!</p>
        <hr class="my-4">
        <p class="lead">
            <a href="<?=$instrukturPath?>">
                <i class="fa fa-arrow-left mr-1"></i>
                Return to Dashboard
            </a>
        </p>
    </div>
</div>