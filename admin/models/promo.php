<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Promo
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;        
        
        $table      = 'promo';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
                        array( 'db' => '`a`.`publish`', 'dt' => 2,'field' => 'publish','formatter' => function ($publish) { return ($publish==1) ? "Live" : "Draft"; }),                                                     
                        array( 'db' => '`a`.`insertDate`', 'dt' => 3,'field' => 'insertDate'),
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($title,$content,$picture)
    {   
        global $imgpath;
        $slug       = seo($title);
        $price      = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);
        $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.'promo/'.$newpicture);
        
        $insert = $this->db->prepare('INSERT INTO promo (title,content,slug,foto) VALUES (?, ?, ?, ?)');        
        $insert->BindParam(1,$title);
        $insert->BindParam(2,$content);
        $insert->BindParam(3,$slug);
        $insert->BindParam(4,$newpicture);
        $insert->execute();

        header('location:'.$base_url.'promo');
    }

    public function update($title,$content,$picture,$imgDB,$id)
    {
        global $imgpath;
        $slug   = seo($title);
        $price  = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);

        $query  = $this->db->prepare("SELECT * FROM promo WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'promo/'.$data['foto'])){
                unlink($imgpath.'promo/'.$data['foto']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'promo/'.$newpicture);        
            
            if(file_exists($imgpath.'promo/'.$data['foto'])){
                unlink($imgpath.'promo/'.$data['foto']);
            }
        }  

        $update = $this->db->prepare('UPDATE promo SET title=?,content=?,slug=?,foto=? WHERE id=?');        
        $update->BindParam(1,$title);
        $update->BindParam(2,$content);
        $update->BindParam(3,$slug);
        $update->BindParam(4,$newpicture);
        $update->BindParam(5,$id);
        $update->execute();        
        
        header('location:'.$base_url.'promo');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM promo WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['foto'])){
            unlink($path.$data['foto']);
        }

        $delete = $this->db->prepare('DELETE FROM promo WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM promo WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function publish($id,$val)
    {
        $update = $this->db->prepare('UPDATE promo SET publish=? WHERE id=?');
        $update->BindParam(1,$val);
        $update->BindParam(2,$id);
        $update->execute();        
        
        header('location:'.$base_url.'promo');
    }

}

$promo = new Promo(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $promo->index();
    }else if($_POST['mode']=='del'){
        $promo->delete($_POST['id'],$_POST['path']);        
    }else if($_POST['mode']=='publish'){
        $promo->publish($_POST['id'],$_POST['val']);        
    }
}


?>
