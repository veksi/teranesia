<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Banner
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;
        global $imgpath;
        
        $table      = 'banner';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`picture`', 'dt' => 1,'field' => 'picture'),
                        array( 'db' => '`a`.`link`', 'dt' => 2,'field' => 'link'),
                        array( 'db' => '`a`.`tipe`', 'dt' => 3,'field' => 'tipe'),
                        array( 'db' => '`a`.`publish`', 'dt' => 4,'field' => 'publish','formatter' => function ($publish) { return ($publish==1) ? "Live" : "Draft"; }),  
                        array( 'db' => '`a`.`insertDate`', 'dt' => 5,'field' => 'insertDate'),                      
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($link,$picture,$tipe)
    {   
        global $imgpath;
        $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.'banner/'.$newpicture);
        
        $insert = $this->db->prepare('INSERT INTO banner (picture,link,tipe) VALUES (?,?,?)');                
        $insert->BindParam(1,$newpicture);
        $insert->BindParam(2,$link);
        $insert->BindParam(3,$tipe);
        $insert->execute();

        header('location:'.$base_url.'banner');
    }

    public function update($link,$picture,$tipe,$imgDB,$id)
    {
        global $imgpath;

        $banner = $this->get_row($id);

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'banner/'.$banner['picture'])){
                unlink($imgpath.'banner/'.$banner['picture']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'banner/'.$newpicture);        
            
            if(file_exists($imgpath.'banner/'.$banner['picture'])){
                unlink($imgpath.'banner/'.$banner['picture']);
            }
        }  
    
        $update = $this->db->prepare('UPDATE banner SET link=?,picture=?,tipe=? WHERE id=?');
        $update->BindParam(1,$link);
        $update->BindParam(2,$newpicture);
        $update->BindParam(3,$tipe);
        $update->BindParam(4,$id);
        $update->execute();        
        
        header('location:'.$base_url.'banner');
    }

    public function delete($id,$path)
    {   
        $banner = $this->get_row($id);
                      
        if(file_exists($path.$banner['picture'])){
            unlink($path.$banner['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM banner WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM banner WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function publish($id,$val)
    {
        $update = $this->db->prepare('UPDATE banner SET publish=? WHERE id=?');
        $update->BindParam(1,$val);
        $update->BindParam(2,$id);
        $update->execute();        
        
        header('location:'.$base_url.'banner');
    }

    public function get_row($id){
        $query  = $this->db->prepare("SELECT * FROM banner WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

}

$banner = new Banner(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $banner->index();
    }else if($_POST['mode']=='del'){
        $banner->delete($_POST['id'],$_POST['path']);        
    }else if($_POST['mode']=='publish'){
        $banner->publish($_POST['id'],$_POST['val']);        
    }
}
?>
