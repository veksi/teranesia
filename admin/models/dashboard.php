<?php
require( 'connection.php' );

class Dashboard
{   
    protected $conn;
    function __construct($conn){
        $this->db = $conn;
    }

    public function customer()
    {
        $query  = $this->db->prepare("SELECT * FROM customer");
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function product()
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE model = 1");
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function postingan()
    {
        $query  = $this->db->prepare("SELECT * FROM product WHERE model = 2");
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function transactions()
    {
        $query  = $this->db->prepare("SELECT * FROM trx");
        $query->execute();
        $data   = $query->rowCount();
        return $data;
    }

    public function trx_success()
    {
        $query  = $this->db->prepare("SELECT SUM(harga) AS total FROM trx WHERE status_trx = 'settlement' ");
        $query->execute();
        $data   = $query->fetch();
        return $data['total'];
    }

    public function graphData(){        
        $d = strtotime("-1 Months");
        $dateBefore = date("Y-m-d", $d);
        
        $dateNow = date("Y-m-d");
        while (strtotime($dateBefore) <= strtotime($dateNow)) {
            $arrMonth[] = '"'.str_replace("-","/",substr(date('d-m-Y',strtotime($dateBefore)),0,5)).'"';
            $arrTotal[] = $this->sum($dateBefore);
            $dateBefore = date ("Y-m-d", strtotime("+1 days", strtotime($dateBefore)));
        }
    
        $result = array($arrMonth,$arrTotal);
        return $result;
    }

    
    public function sum($date){     
        $query  = $this->db->prepare("SELECT SUM(harga) AS total FROM trx WHERE status_trx = 'settlement' AND DATE(insertDate) = ? ");
        $query->BindParam(1,$date);
        $query->execute();
        $data   = $query->fetch();
        if(!empty($data['total'])){
            return $data['total'];
        }else{
            return 0;
        }
        
    }

}

$dashboard = new Dashboard($conn);
?>