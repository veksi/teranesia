<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Category
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;
        
        $table      = 'category';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($title,$icon)
    {   
        $slug   = seo($title);
        $insert = $this->db->prepare('INSERT INTO category (title,icon,slug) VALUES (?,?,?)');        
        $insert->BindParam(1,$title);
        $insert->BindParam(2,$icon);
        $insert->BindParam(3,$slug);        
        $insert->execute();

        header('location:'.$base_url.'category');
    }

    public function update($title,$icon,$id)
    {
        $slug   = seo($title);
        $update = $this->db->prepare('UPDATE category SET title=?,icon=?,slug=? WHERE id=?');
        $update->BindParam(1,$title);    
        $update->BindParam(2,$icon);
        $update->BindParam(3,$slug);    
        $update->BindParam(4,$id);
        $update->execute();        
        
        header('location:'.$base_url.'category');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM category WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['picture'])){
            unlink($path.$data['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM category WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM category WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

}

$category = new Category(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $category->index();
    }else if($_POST['mode']=='del'){
        $category->delete($_POST['id'],$_POST['path']);        
    }
}


?>
