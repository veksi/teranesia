<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Product
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;        
        
        $table      = 'product';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
                        array( 'db' => '`a`.`category`', 'dt' => 2,'field' => 'category','formatter' => function ($cat) {return $this->get_menu($cat)['title'];}),     
                        array( 'db' => '`a`.`price`', 'dt' => 3,'field' => 'price','formatter' => function ($price) {return "Rp. ".number_format($price);}),     
                        array( 'db' => '`a`.`publish`', 'dt' => 4,'field' => 'publish','formatter' => function ($publish) { return ($publish==1) ? '<i class="fa fa-check-circle text-success" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle text-muted" aria-hidden="true"></i>'; }),                                                  
                        array( 'db' => '`a`.`insertDate`', 'dt' => 5,'field' => 'insertDate'),
                        array( 'db' => '`a`.`publish`', 'dt' => 6,'field' => 'publish'),
                    );
        $extraWhere = " model = 1 ";
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, $extraWhere));

    }

    public function store($category,$title,$descriptions,$price,$picture,$lampiran,$tgl)
    {   
        global $imgpath;
        $price      = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);
        (empty($picture['name'])) ? $newpicture = '' : $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.'product/'.$newpicture);

        global $filepath;
        (empty($lampiran['name'])) ? $newfiles = '' : $newfiles = date("dmYHis").$lampiran['name'];        
        move_uploaded_file($lampiran["tmp_name"], $filepath.'e-book/'.$newfiles);
        
        $insert = $this->db->prepare('INSERT INTO product (category,title,descriptions,price,picture,attachment,insertDate,model) VALUES (?,?,?,?,?,?,?,1)');        
        $insert->BindParam(1,$category);
        $insert->BindParam(2,$title);
        $insert->BindParam(3,$descriptions);
        $insert->BindParam(4,$price);
        $insert->BindParam(5,$newpicture);
        $insert->BindParam(6,$newfiles);        
        $insert->BindParam(7,$tgl);        
        $insert->execute();

        header('location:'.$base_url.'product');
    }

    public function update($category,$title,$descriptions,$price,$picture,$imgDB,$lampiran,$tgl,$id)
    {
        global $imgpath;
        global $filepath;
        $price  = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);

        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'product/'.$data['picture'])){
                unlink($imgpath.'product/'.$data['picture']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'product/'.$newpicture);        
            
            if(file_exists($imgpath.'product/'.$data['picture'])){
                unlink($imgpath.'product/'.$data['picture']);
            }
        }  

        if( empty($lampiran['name']) ){            //gambar tdk diubah
            $newfiles       = $data['attachment']; 
        }elseif( empty($lampiran['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newfiles       = "";
            if(file_exists($filepath.'e-book/'.$data['attachment'])){
                unlink($filepath.'e-book/'.$data['attachment']);
            }
        }else{                                                     //gambar baru
            $newfiles        = date("dmYHis").$lampiran['name'];
            move_uploaded_file($lampiran["tmp_name"], $filepath.'e-book/'.$newfiles);        
            
            if(file_exists($filepath.'e-book/'.$data['attachment'])){
                unlink($filepath.'e-book/'.$data['attachment']);
            }
        }

        $update = $this->db->prepare('UPDATE product SET category=?,title=?,descriptions=?,price=?,picture=?,attachment=?,insertDate=? WHERE id=?');
        $update->BindParam(1,$category);
        $update->BindParam(2,$title);
        $update->BindParam(3,$descriptions);
        $update->BindParam(4,$price);
        $update->BindParam(5,$newpicture);
        $update->BindParam(6,$newfiles);
        $update->BindParam(7,$tgl);
        $update->BindParam(8,$id);
        $update->execute();        
        
        header('location:'.$base_url.'product');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['picture'])){
            unlink($path.$data['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM product WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function delete_attach($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['attachment'])){
            unlink($path.$data['attachment']);
        }

        $update = $this->db->prepare('UPDATE product SET attachment ="" WHERE id=?');
        $update->BindParam(1,$id);
        $update->execute();
        
    }

    public function get_menu($id){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }
/*
    public function category(){
        $query  = $this->db->prepare("SELECT * FROM category");
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }
*/
    public function menu(){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = 0 ORDER BY insertDate ASC");
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function childmenu($parent){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = ? ORDER BY insertDate ASC");
        $query->BindParam(1,$parent);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function publish($id,$val)
    {
        $update = $this->db->prepare('UPDATE product SET publish=? WHERE id=?');
        $update->BindParam(1,$val);
        $update->BindParam(2,$id);
        $update->execute();        
        
        header('location:'.$base_url.'product');
    }

}

$product = new Product(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $product->index();
    }else if($_POST['mode']=='del'){
        $product->delete($_POST['id'],$_POST['path']);        
    }else if($_POST['mode']=='attachment'){
        $product->delete_attach($_POST['id'],$_POST['path']);        
    }else if($_POST['mode']=='publish'){
        $product->publish($_POST['id'],$_POST['val']);        
    }
}


?>
