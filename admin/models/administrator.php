
<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Administrator
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;
        
        $table      = 'administrator';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`first_name`', 'dt' => 1,'field' => 'first_name'),
                        array( 'db' => '`a`.`email`', 'dt' => 2,'field' => 'email'),
                        array( 'db' => '`a`.`insertDate`', 'dt' => 3,'field' => 'insertDate'),
                        array( 'db' => '`a`.`photo`', 'dt' => 4,'field' => 'photo'),
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($first,$last,$email,$password,$hp,$born,$borndate,$alamat,$picture)    
    {             
            global $imgadmin;
            $newpass        = password_hash($password, PASSWORD_DEFAULT);
            $newborndate    = date("Y-m-d", strtotime($borndate));
            $newpicture     = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgadmin.'administrator/'.$newpicture);
            
            $insert = $this->db->prepare('INSERT INTO administrator (first_name,last_name,email,pass,phone,bornPlace,bornDate,alamat,photo) VALUES (?,?,?,?,?,?,?,?,?)');        
            $insert->BindParam(1,$first);
            $insert->BindParam(2,$last);
            $insert->BindParam(3,$email);
            $insert->BindParam(4,$newpass);
            $insert->BindParam(5,$hp);
            $insert->BindParam(6,$born);
            $insert->BindParam(7,$newborndate);
            $insert->BindParam(8,$alamat);
            $insert->BindParam(9,$newpicture);
            $insert->execute();
    
            header('location:'.$base_url.'administrator');
     

    }

    public function update($first,$last,$email,$password,$hp,$born,$borndate,$alamat,$picture,$imgDB,$id)
    {
        global $imgadmin;
        $newpass        = password_hash($password, PASSWORD_DEFAULT);
        $newborndate    = date("Y-m-d", strtotime($borndate));

        $query  = $this->db->prepare("SELECT * FROM administrator WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgadmin.'administrator/'.$data['photo'])){
                unlink($imgadmin.'administrator/'.$data['photo']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgadmin.'administrator/'.$newpicture);        
            
            if(file_exists($imgadmin.'administrator/'.$data['photo'])){
                unlink($imgadmin.'administrator/'.$data['photo']);
            }
        }  
    
        $update = $this->db->prepare('UPDATE administrator SET first_name=?,last_name=?,email=?,pass=?,phone=?,bornPlace=?,bornDate=?,alamat=?,photo=? WHERE id=?');
        $update->BindParam(1,$first);
        $update->BindParam(2,$last);
        $update->BindParam(3,$email);
        $update->BindParam(4,$newpass);
        $update->BindParam(5,$hp);
        $update->BindParam(6,$born);
        $update->BindParam(7,$newborndate);
        $update->BindParam(8,$alamat);
        $update->BindParam(9,$newpicture);
        $update->BindParam(10,$id);
        $update->execute();        
        
        header('location:'.$base_url.'administrator');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM administrator WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['picture'])){
            unlink($path.$data['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM administrator WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM administrator WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

}

$administrator = new Administrator(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $administrator->index();
    }else if($_POST['mode']=='del'){
        $administrator->delete($_POST['id'],$_POST['path']);        
    }
}


?>
