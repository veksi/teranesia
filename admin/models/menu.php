<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Menu
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;        
        
        $table      = 'menu';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
                        array( 'db' => '`a`.`parent`', 'dt' => 2,'field' => 'parent','formatter' => function ($parent) {return ($parent==0) ? 'Root' :  $this->get_parent($parent)['title']; }),     
                        array( 'db' => '`a`.`position`', 'dt' => 3,'field' => 'position','formatter' => function ($position) {return $this->position($position);}),     
                        array( 'db' => '`a`.`model`', 'dt' => 4,'field' => 'model','formatter' => function ($model) {return $this->model($model);}),     
                        array( 'db' => '`a`.`publish`', 'dt' => 5,'field' => 'publish','formatter' => function ($publish) { return ($publish==1) ? '<i class="fa fa-check-circle text-success" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle text-muted" aria-hidden="true"></i>'; }),
                        array( 'db' => '`a`.`insertDate`', 'dt' => 6,'field' => 'insertDate'),                        
                        array( 'db' => '`a`.`publish`', 'dt' => 7,'field' => 'publish'),                        
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($title,$parent,$position,$model,$picture,$tgl)
    {   
        global $imgpath;
        $slug       = seo($title);        
        (empty($picture['name'])) ? $newpicture = '' : $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.$newpicture);
        
        $insert = $this->db->prepare('INSERT INTO menu (title,parent,position,model,icon,insertDate,slug) VALUES (?,?,?,?,?,?,?)');                
        $insert->BindParam(1,$title);
        $insert->BindParam(2,$parent);
        $insert->BindParam(3,$position);
        $insert->BindParam(4,$model);
        $insert->BindParam(5,$newpicture);        
        $insert->BindParam(6,$tgl);
        $insert->BindParam(7,$slug);
        $insert->execute();

        if($insert){
            header('location:'.$base_url.'menu');
        }else{
            
        }
        
    }

    public function update($title,$parent,$position,$model,$picture,$imgDB,$tgl,$id)
    {
        global $imgpath;        
        $slug   = seo($title);
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.$data['icon'])){
                unlink($imgpath.$data['icon']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.$newpicture);        
            
            if(file_exists($imgpath.$data['icon'])){
                unlink($imgpath.$data['icon']);
            }
        }  

        $update = $this->db->prepare('UPDATE menu SET title=?,parent=?,position=?,model=?,icon=?,insertDate=?,slug=? WHERE id=?');        
        $update->BindParam(1,$title);
        $update->BindParam(2,$parent);
        $update->BindParam(3,$position);
        $update->BindParam(4,$model);
        $update->BindParam(5,$newpicture);        
        $update->BindParam(6,$tgl);
        $update->BindParam(7,$slug);
        $update->BindParam(8,$id);
        $update->execute();        
        
        header('location:'.$base_url.'menu');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['icon'])){
            unlink($path.$data['icon']);
        }

        $delete = $this->db->prepare('DELETE FROM menu WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function menu(){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = 0 ORDER BY insertDate ASC");
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function childmenu($parent){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = ? ORDER BY insertDate ASC");
        $query->BindParam(1,$parent);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function get_parent($id){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function publish($id,$val)
    {
        $update = $this->db->prepare('UPDATE menu SET publish=? WHERE id=?');
        $update->BindParam(1,$val);
        $update->BindParam(2,$id);
        $update->execute();        
        
        header('location:'.$base_url.'menu');
    }

    
    public function position($var){
        if($var==1){
            $result = 'Top Menu';
        }else{
            $result = 'Side Menu';
        }
    
        return $result;
    }
  
    public function model($var){
        if($var==1){
            $result = 'Single Post';
        }else if($var==2){
            $result = 'Multi Post';
        }else if($var==3){
            $result = 'Email Form';
        }else if($var==4){
            $result = 'Testimoni';
        }else{
            $result = 'Home';
        }
    
        return $result;
    }

}

$menu = new Menu(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $menu->index();
    }else if($_POST['mode']=='del'){
        $menu->delete($_POST['id'],$_POST['path']);              
    }else if($_POST['mode']=='publish'){
        $menu->publish($_POST['id'],$_POST['val']);        
    }
}


?>
