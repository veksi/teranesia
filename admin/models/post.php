<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Post
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;        
        
        $table      = 'product';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
                        array( 'db' => '`a`.`category`', 'dt' => 2,'field' => 'category','formatter' => function ($cat) {return $this->get_menu($cat)['title'];}),     
                        array( 'db' => '`a`.`publish`', 'dt' => 3,'field' => 'publish','formatter' => function ($publish) { return ($publish==1) ? '<i class="fa fa-check-circle text-success" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle text-muted" aria-hidden="true"></i>'; }),
                        array( 'db' => '`a`.`insertDate`', 'dt' => 4,'field' => 'insertDate'),
                        array( 'db' => '`a`.`publish`', 'dt' => 5,'field' => 'publish'),
                    );
        $extraWhere = " model = 2 ";
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, $extraWhere));

    }

    public function store($title,$category,$descriptions,$picture)
    {   
        global $imgpath;                
        (empty($picture['name'])) ? $newpicture = '' : $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.'post/'.$newpicture);
        
        $insert = $this->db->prepare('INSERT INTO product (title,category,descriptions,picture,model) VALUES (?,?,?,?,2)');        
        $insert->BindParam(1,$title);
        $insert->BindParam(2,$category);
        $insert->BindParam(3,$descriptions);        
        $insert->BindParam(4,$newpicture);
        $insert->execute();

        header('location:'.$base_url.'post');
    }

    public function update($title,$menu,$content,$picture,$imgDB,$id)
    {
        global $imgpath;        
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'post/'.$data['picture'])){
                unlink($imgpath.'post/'.$data['picture']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'post/'.$newpicture);        
            
            if(file_exists($imgpath.'post/'.$data['picture'])){
                unlink($imgpath.'post/'.$data['picture']);
            }
        }  

        $update = $this->db->prepare('UPDATE product SET title=?,category=?,descriptions=?,picture=? WHERE id=?');        
        $update->BindParam(1,$title);
        $update->BindParam(2,$menu);
        $update->BindParam(3,$content);        
        $update->BindParam(4,$newpicture);
        $update->BindParam(5,$id);
        $update->execute();        
        
        header('location:'.$base_url.'post');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['picture'])){
            unlink($path.$data['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM product WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function get_menu($id){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function publish($id,$val)
    {
        $update = $this->db->prepare('UPDATE product SET publish=? WHERE id=?');
        $update->BindParam(1,$val);
        $update->BindParam(2,$id);
        $update->execute();        
        
        header('location:'.$base_url.'post');
    }

    public function menu(){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = 0 ORDER BY insertDate ASC");
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function childmenu($parent){
        $query  = $this->db->prepare("SELECT * FROM menu WHERE parent = ? ORDER BY insertDate ASC");
        $query->BindParam(1,$parent);
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

}

$post = new Post(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $post->index();
    }else if($_POST['mode']=='del'){
        $post->delete($_POST['id'],$_POST['path']);        
    }else if($_POST['mode']=='publish'){
        $post->publish($_POST['id'],$_POST['val']);        
    }
}


?>
