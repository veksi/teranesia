<?php
require( 'connection.php' );
require( 'ssp.class.php' );

class Product
{   
    private $db;
    private $ssp;

    function __construct($conn){
        $this->db = $conn;   
    }

    public function index()
    {
        global $sql_details;        
        
        $table      = 'trx';
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns    = array(
                        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
                        array( 'db' => '`a`.`order_id`', 'dt' => 1,'field' => 'order_id','formatter' => function ($order_id) {
                            return "#".$order_id;
                        }),                             
                        array( 'db' => '`a`.`transactions`', 'dt' => 2,'field' => 'transactions','formatter' => function ($trx) {
                            $data = unserialize($trx);
                            return $data['customer_details']['first_name'];
                        }),   
                        array( 'db' => '`a`.`transactions`', 'dt' => 3,'field' => 'transactions','formatter' => function ($trx) {
                            $data = unserialize($trx);
                            return $data['item_details'][0]['name'];
                        }),                             
                        array( 'db' => '`a`.`status_trx`', 'dt' => 4,'field' => 'status_trx'),                                                
                        array( 'db' => '`a`.`insertDate`', 'dt' => 5,'field' => 'insertDate'),                                                
                    );
        
        $this->ssp = new SSP();
        return json_encode($this->ssp->simple($_POST, $sql_details, $table, $primaryKey, @$columns, $joinQuery, ''));

    }

    public function store($category,$title,$descriptions,$price,$picture)
    {   
        global $imgpath;
        $price      = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);
        $newpicture = date("dmYHis").$picture['name'];
        move_uploaded_file($picture["tmp_name"], $imgpath.'product/'.$newpicture);
        
        $insert = $this->db->prepare('INSERT INTO product (category,title,descriptions,price,picture) VALUES (?, ?, ?, ?, ?)');        
        $insert->BindParam(1,$category);
        $insert->BindParam(2,$title);
        $insert->BindParam(3,$descriptions);
        $insert->BindParam(4,$price);
        $insert->BindParam(5,$newpicture);
        $insert->execute();

        header('location:'.$base_url.'product');
    }

    public function update($category,$title,$descriptions,$price,$picture,$imgDB,$id)
    {
        global $imgpath;
        $price  = (int) filter_var($price, FILTER_SANITIZE_NUMBER_INT);

        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();

        if( empty($picture['name']) && !empty($imgDB) ){            //gambar tdk diubah
            $newpicture       = $imgDB; 
        }elseif( empty($picture['name']) && empty($imgDB) ){       //gambar dikosongkan
            $newpicture       = "";
            
            if(file_exists($imgpath.'product/'.$data['picture'])){
                unlink($imgpath.'product/'.$data['picture']);
            }
        }else{                                                     //gambar baru
            $newpicture        = date("dmYHis").$picture['name'];
            move_uploaded_file($picture["tmp_name"], $imgpath.'product/'.$newpicture);        
            
            if(file_exists($imgpath.'product/'.$data['picture'])){
                unlink($imgpath.'product/'.$data['picture']);
            }
        }  

        $update = $this->db->prepare('UPDATE product SET category=?,title=?,descriptions=?,price=?,picture=? WHERE id=?');
        $update->BindParam(1,$category);
        $update->BindParam(2,$title);
        $update->BindParam(3,$descriptions);
        $update->BindParam(4,$price);
        $update->BindParam(5,$newpicture);
        $update->BindParam(6,$id);
        $update->execute();        
        
        header('location:'.$base_url.'product');
    }

    public function delete($id,$path)
    {   
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
                      
        if(file_exists($path.$data['picture'])){
            unlink($path.$data['picture']);
        }

        $delete = $this->db->prepare('DELETE FROM product WHERE id = ?');
        $delete->BindParam(1,$id);
        $delete->execute();
        
    }

    public function get_category($id){
        $query  = $this->db->prepare("SELECT * FROM category WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

    public function category(){
        $query  = $this->db->prepare("SELECT * FROM category");
        $query->execute();
        $data   = $query->fetchAll();
        return $data;
    }

    public function edit($id){
        $query  = $this->db->prepare("SELECT * FROM product WHERE id = ?");
        $query->BindParam(1,$id);
        $query->execute();
        $data   = $query->fetch();
        return $data;
    }

}

$product = new Product(@$conn);

if(isset($_POST['csrf'])){
    if($_POST['mode']=='list'){
        echo $product->index();
    }else if($_POST['mode']=='del'){
        $product->delete($_POST['id'],$_POST['path']);        
    }
}


?>
