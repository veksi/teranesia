<?php
require( 'connection.php' );

class Login
{   
    private $db;
    function __construct($conn){
        $this->db = $conn;
    }

    public function auth($u,$p)
    {
        $query = $this->db->prepare("SELECT * FROM administrator WHERE email = ? AND active = 1");
        $query->BindParam(1,$u);
        $query->execute();
        $data = $query->fetch();
        if(password_verify($p,$data['pass'])){
            $_SESSION['authorized_admin']  = true;
            $_SESSION['admin']             = $data['id'];
        }
        return $data;
    }

}

$login = new Login(@$conn);
?>