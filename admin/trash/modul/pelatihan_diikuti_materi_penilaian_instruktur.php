<?php

$userExist  = dbGetRow('evaluasi_ujian',"user = '".$_SESSION['member']."' AND kegiatan_materi = '".$id."'",'','');
$soal       = dbGetAll('soal','soal_group = '.$kegiatan['group_soal'],'','','');
$totSoal    = dbGetNum('soal','soal_group = '.$kegiatan['group_soal'],'','');

if(isset($_POST['mulai'])){

  if($userExist['expired']==1){ include "404.php"; exit;}

  if(empty($userExist['id'])){
    $startTime    = date("Y-m-d H:i:s");
    $endTime      = date('Y-m-d H:i:s',strtotime('+'.$_POST['durasi'].' minutes',strtotime($startTime)));
    $wkt_selesai  = mulaiTes($id,$_SESSION['member'],$startTime,$endTime);
  }else{
    $wkt_selesai = $userExist['waktu_selesai'];
  }?>

  <script>
    var countDownDate = new Date("<?=$wkt_selesai?>").getTime();
    var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if(hours<10){ hours_time = "0"+hours; }else{ hours_time = hours; }
    if(minutes<10){ minutes_time = "0"+minutes; }else{ minutes_time = minutes; }
    if(seconds<10){ second_time = "0"+seconds; }else{ second_time = seconds; }
    
    //document.getElementById("demo").innerHTML = days + "0:" + hours + "0:"+ minutes + ":" + seconds + "";
    document.getElementById("demo").innerHTML = "<i class='fa fa-clock-o'></i> "+ hours_time + ":" +minutes_time + ":" + second_time + "";
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";

        $.ajax({
          type: "POST",
          url: '<?=$domain;?>modul/ajax/data_pelatihan_diikuti_materi_ujian.php',
          data: $('form').serialize() // serializes the form's elements.
        }).done(function(data) {
          window.location.href="<?=$domain?>pelatihan-diikuti/materi/<?=encrypt($id)?>"; 
        }).fail(function(data) {
            
        });

      }
    }, 1000);
  </script>

<form id="form" action="<?=$domain?>modul/ajax/data_pelatihan_diikuti_materi_ujian.php" method="post">
  <input type="hidden" name="jumlah" value="<?=$totSoal?>">
  <input type="hidden" name="kegiatan_materi" value="<?=$kegiatan['id']?>">

  <div class="container-fluid">
    <div class="jumbotron p-4 mb-4 bg-white">
      <span class="pull-right" style="position:fixed;right:20;bottom:20;z-index:99">
        <span id="demo" class="btn btn-secondary btn-lg disabled" ></span>
      </span>

      <?php 
        $no = 1;
        if(is_array($soal)){

          foreach($soal as $soalVal){
        ?>
          <input type="hidden" name="id[]" value=<?=$soalVal['id']?>>
          <input type="hidden" name="upd" value="upd">

          <ul class="list-group mb-2" >
            <li class="list-group-item p-2 border-0">
              <table>
                <tr><td class="align-top font-weight-bold"><span class="badge badge-dark"><?=$no?></span>&nbsp;&nbsp;</td><td class="font-weight-bold"> <?=$soalVal['soal'];?></td></tr>
              </table>
            </li>

            <li class="list-group-item py-1 px-2 border-0">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="pilihan[<?=$soalVal['id']?>]" id="a<?=$soalVal['id']?>" value="a">
                <label class="form-check-label" for="a<?=$soalVal['id']?>">
                  <?=$soalVal['ans_a'];?>
                </label>
              </div>
            </li>

            <li class="list-group-item py-1 px-2 border-0">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="pilihan[<?=$soalVal['id']?>]" id="b<?=$soalVal['id']?>" value="b">
                <label class="form-check-label" for="b<?=$soalVal['id']?>">
                <?=$soalVal['ans_b'];?>
                </label>
              </div>
            </li>

            <li class="list-group-item py-1 px-2 border-0">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="pilihan[<?=$soalVal['id']?>]" id="c<?=$soalVal['id']?>" value="c">
                <label class="form-check-label" for="c<?=$soalVal['id']?>">
                <?=$soalVal['ans_c'];?>
                </label>
              </div>
            </li>

            <li class="list-group-item py-1 px-2 border-0">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="pilihan[<?=$soalVal['id']?>]" id="d<?=$soalVal['id']?>" value="d">
                <label class="form-check-label" for="d<?=$soalVal['id']?>">
                  <?=$soalVal['ans_d'];?>
                </label>
              </div>
            </li>

          </ul>
          
          <hr>

        <?php $no++; } ?>

        <div class="text-center">
          <input type="submit" name="submit" value="Kirim" class="btn btn-primary">
        </div>

      <?php } ?>

    </form>

  </div>
</div>

<?php }else{ ?>

  <?php $soal = dbGetAll('soal','soal_group = '.$kegiatan['group_soal'],'','',''); ?>

  <div class="container-fluid">
    <div class="jumbotron p-4 mb-4 bg-white">
      <div class="row justify-content-md-center">
        <div class="col-sm-8">
          
          <h1 class="display-4"><?=$kegiatan['title']?></h1>
          <p class="lead <?php if($userExist['expired']==1){ echo "d-none"; } ?>">Pastikan anda siap untuk mengikuti tes ini</p>
          Jumlah Soal : <?=$totSoal?><br>
          Waktu Pengerjaan : <?=$kegiatan['durasi']?> Menit
          <?php if($userExist['expired']==1){?><h1 class="display-4 text-center"><?=$userExist['nilai']?></h1><?php } ?>
          <hr class="my-4">

          <div class="<?php if($userExist['expired']==1){ echo "d-none"; } ?>">
            <p>
              <ol type="1">
                <li>Bacalah dengan teliti setiap soal sebelum menjawab</li>
                <li>Pengerjaan soal ujian akan diberikan batas waktu. Apabila waktu habis anda tidak dapat lagi mengisi/mengoreksi kembali jawaban dari soal yang tersedia. Begitupun sebaliknya apabila waktu masih tersisa anda dapat secara bebas mengoreksi kembali jawaban anda.</li>
              </ol>
            </p>
            <div class="lead text-center py-4">
              <form action="#" method="post" enctype="multipart/form-data">   
                <input type="hidden" name="mulai">
                <input type="hidden" name="durasi" value="<?=$kegiatan['durasi']?>">
                <?php if($totSoal>0){ ?>
                  <input type="submit" id="loginBtn" value="Mulai" class="btn btn-primary btn-lg"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Mulai">
                <?php } ?>
              </form>
            </div>
          </div>

        </div>
      </div>          
    </div>        
  </div>

<?php } ?>