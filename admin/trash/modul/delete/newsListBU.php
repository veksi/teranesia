<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Blog</b></h4>
    <a href="?news&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Title</th>
                <th>Category</th>
                <th>Date</th>
                <th>Editor</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('article');
            $no=1;
            foreach($list as $data){

                if($data['publish']==1){
                    $status = "<a href=library/qNews.php?news&on&id=".$data['idArticle'].">Draft</a>";
                }else{
                    $status = "<a href=library/qNews.php?news&off&id=".$data['idArticle'].">Publish</a>";
                }
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td style="text-align: center;">
                    <img height="50px" width="100px" src="<?php echo $imgNewsFolder.$data['pict']; ?>"/>      
                </td>
                <td>
                    <?php echo $data['title']; ?>
                    <div class="clearfix"></div>
                    <?php if($data['publish']=='1'){ ?>
                        <span class="label label-success">Publish</span>
                    <?php }else{ ?>
                        <span class="label label-info">Draft</span>
                    <?php } ?>
                </td>
                <td>
                    <?php if($data['category']=='1'){ ?>
                        News
                    <?php }elseif($data['category']=='3'){ ?>
                        Inspiration
                    <?php }elseif($data['category']=='4'){ ?>
                        Announcement
                    <?php } ?>
                </td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getUser($data['editor']); ?></td>
                <td style="text-align: center;">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><?php echo $status; ?></li>
                            <li><a href='#'
                                data-toggle='modal'
                                data-target='#myModal'
                                data-title='<?php echo $data['title'];?>' 
                                data-content='<?php echo $data['content'];?>' 
                                data-id='<?php echo $data['idArticle'];?>'
                                data-pict='<?php echo $imgNewsFolder.$data['pict']; ?>'
                                >
                                View</a></li>
                            <li><a href="?news&mode=ins&id=<?php echo $data['idArticle']; ?>">Edit</a></li>
                            <li><a href="#" onclick="deleteRecord('article','<?php echo $data['idArticle']; ?>','<?php echo urlencode($data['pict']); ?>')" >Delete</a></li>
                            <li>
                                <a href='#'
                                data-toggle='modal'
                                data-target='#myModal'
                                data-title='<?php echo $data['title'];?>' 
                                data-content='<?php echo $data['content'];?>' 
                                data-id='<?php echo $data['idArticle'];?>'
                                data-pict='<?php echo $imgNewsFolder.$data['pict']; ?>'>
                                Send as push</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b><i class="fa fa-bell-o"></i> Push Notification</b></h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
                <input type="hidden" id="title" placeholder="email" name="title"/>
                <input type="hidden" id="content" placeholder="email" name="content"/>
                <input type="hidden" id="id" placeholder="email" name="id"/>
                <img id="pict" src="" style="width:100%;height:30%;"/>   
                <div class="clear10"></div>
                <b><span id="modal-title"></span></b>
                <div class="clear10"></div>
                <span id="modal-content"></span>
            </div>
            <div class="modal-footer">
                <button type="button" id="send" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Send</button>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?')){
            window.location.href='library/qNews.php?news&del&id='+id+'&photo='+photo;
        }
    }

    // data-* attributes to scan when populating modal values
    
    var ATTRIBUTES = ['title', 'content', 'id', 'pict'];

    $('[data-toggle="modal"]').on('click', function (e) {
        // convert target (e.g. the button) to jquery object
        var $target = $(e.target);
        // modal targeted by the button
        var modalSelector = $target.data('target');
        
        // iterate over each possible data-* attribute
        ATTRIBUTES.forEach(function (attributeName) {
            // retrieve the dom element corresponding to current attribute
            var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
            var dataValue = $target.data(attributeName);

            $('#title').val($target.data('title'));
            $('#content').val($target.data('content'));            
            $('#id').val($target.data('id'));
            $("#pict").attr("src", $target.data('pict'));
            
            // if the attribute value is empty, $target.data() will return undefined.
            // In JS boolean expressions return operands and are not coerced into
            // booleans. That way is dataValue is undefined, the left part of the following
            // Boolean expression evaluate to false and the empty string will be returned
            $modalAttribute.text(dataValue || '');
        });
    });




    $(function(){
        $('#send').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: "library/pushLib.php?news",
                type: "post",
                data: $('form').serialize(),
                success: function(data)
                {   
                    success();                      
                }           
            });
        });
    });

    function success(){
        swal("Berhasil", "Push notification terkirim", "success");
    }
    
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

<style>
.myAlert-top{
    position: absolute;
    top: 5px; 
    left:2%;
    width: 96%;
    z-index:10
}


.alert{
    display: none;
}
</style>