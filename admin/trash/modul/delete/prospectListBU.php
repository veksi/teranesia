<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Prospect List</b></h4>
    <a href="?prospect&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Finder</th>
                <th>Prospect Name</th>
                <th>Phone</th>
                <th>Date</th>
                <th>Editor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('prospect');
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>                
                <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName']; ?></td>
                <td><?php echo getProspect($data['idProspect']); ?></td>
                <td><?php echo $data['phone']; ?></td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getUser($data['editor']); ?></td>
                <td style="text-align: center;">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="?prospect&mode=ins&id=<?php echo $data['idProspect']; ?>">Edit</a></li>
                            <li><a href="#" onclick="deleteRecord('prospect','<?php echo $data['idProspect']; ?>')" >Delete</a></li>
                            <li><a href="?trx&mode=ins&idSales=<?php echo $data['idSales']; ?>&idProspect=<?php echo $data['idProspect']; ?>" target="_blank">Add Prospect Activation</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qProspect.php?prospect&del&id='+id;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

