<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=prospect_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>ID</th>
        <th>Nama</th>
        <th>Telepon</th>
        <th>Email</th>
        <th>Profesi</th>
        <th>Tgl Lahir</th>
        <th>Kebutuhan</th>
        <th>Catatan</th>
        <th>Status</th>
        <th>Finder</th>
        <th>Tgl Input</th>    
        <th>Tgl Aktivasi</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getList('prospect');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>            
            <td>P<?php echo $data['idProspect']; ?></td>
            <td><?php echo $data['name']; ?></td>
            <td><?php echo $data['phone']; ?></td>
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['profession']; ?></td>            
            <td><?php echo $data['bornDate']; ?></td>
            <td><?php echo $data['need']; ?></td>
            <td><?php echo $data["note"]; ?></td>
            <td><?php if($data['activate']=='1'){echo "Active";}else{echo "Non Active";}; ?></td>
            <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName']; ?></td>
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php echo $data['activationDate']; ?></td>            
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
