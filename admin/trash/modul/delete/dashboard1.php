<script type="text/javascript">

    $(document).ready(function() {

        $("#divSales").load("https://www.starhome.co.id/admin/modul/record_count.php?sales");
        $("#divProject").load("https://www.starhome.co.id/admin/modul/record_count.php?project");
        $("#divProspect").load("https://www.starhome.co.id/admin/modul/record_count.php?prospect");
        
        var refreshId = setInterval(function() {
            $("#divSales").load("https://www.starhome.co.id/admin/modul/record_count.php?sales").fadeIn("slow");
            $("#divProject").load("https://www.starhome.co.id/admin/modul/record_count.php?project").fadeIn("fadeToggle");
            $("#divProspect").load("https://www.starhome.co.id/admin/modul/record_count.php?prospect").fadeIn("fadeToggle");

        }, 10000);
    //$.ajaxSetup({ cache: false });
    });

</script>

<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Dashboard</b></h4>
</div>

<div class="clear10"></div>

<ul class="column">
    <li class="fourColumn alert color1">
        <i class="fa fa-user text-white fa-2x"></i>
        <h5 class="card-title">Sales</h5>
        <h1><div id="divSales"></div></h1>
    </li>
    <li class="fourColumn alert color2">
        <i class="fa fa-building fa-2x"></i>
        <h5 class="card-title">Project</h5>
        <h1><div id="divProject"></div></h1>
    </li>
    <li class="fourColumn alert color3">
        <i class="fa fa-handshake-o fa-2x"></i>
        <h5 class="card-title">Prospect </h5>
        <h1><div id="divProspect"></div></h1>
    </li>
    <li class="fourColumn alert color4">
        <i class="fa fa-bell text-white fa-2x"></i>
        <h5 class="card-title">Anonymous</h5>
        <h1>0</h1>
    </li>
</ul>


<div class="clear10"></div>

<!--
    
<span class="label label-info">Top 10 Views</span>
<div class="alert">
    <ul class="list-group">
        <?php  
        $top10 = getTop10();
        foreach($top10 as $data){?>
            <li class="list-group-item"><span class="badge"><?php echo $data['viewer']; ?></span>
                <?php echo $data['title']; ?>
            </li>
        <?php } ?>
    </ul>
</div>


<?php $data = getTraffic(); ?>

<script src="chart/highcharts.js"></script>


<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script type="text/javascript">

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Monthly Traffic'
    },
    
    xAxis: {
        categories: [
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu',
            'Minggu'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Visitor'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'I',
        data: [<?= join($data, ',') ?>]

    }, {
        name: 'II',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.4]

    }, {
        name: 'III',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.4]

    }, {
        name: 'IV',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.4]

    }]
});
        </script>
        

        -->