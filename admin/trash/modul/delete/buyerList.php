<?php 
if($gadget=="hp"){
    $url = "https://wa.me/";
}else{
    $url = "https://web.whatsapp.com/send?phone=";
}
?>

<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Buyer</b></h4>
    <!--<a href="?secondary&mode=ins" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah listing</a>-->
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Kebutuhan</th>
                <th>Budget</th>
                <th>Lokasi</th>
                <th>Tanggal</th>
                <th style="text-align:center">Action</th>
                <th>Code</th>
            </tr>
        </thead>                                            
    </table>
</div>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
//$(document).ready(function() {

    var session = <?=$_SESSION['level']?>;

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({

        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataBuyer.php?list",
        "order": [[ 6, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
        // table.order = [[ 5, "desc" ]];
        },
        "deferRender": true,
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
           
            { 
                "render": nameCol,
                "targets"  : 1,
                "searchable": true
            },
            { 
                "data": 2,
                "targets": 2,
            },
            { 
                "data": 3,
                "targets": 3,
            },
            { 
                "data": 4,
                "targets"  : 4,
            },
            { 
                "data": 5,
                "targets": 5,
            },
            { 
                "data": 6,
                "targets": 6,
            },
            { 
                "render": actionCol,
                "targets": 7,
            },
            { 
                "data": 11,
                "targets": 8,
                "visible":false
            }
           
            
        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  

    function nameCol(data, type, full) {

        if(full[7]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[7]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[7]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        phone = "<a target=_blank href=<?php echo $url; ?>"+full[9]+">"+full[9]+"</a>";

        return ''+full[1]+' - B'+full[0]+' <div class="clearfix"></div> '+status+' <br> '+full[8]+' ('+phone+')';

    }

    function actionCol(data, type, full) {

        if(full[7]==1){
            status = "<a href=# onclick=suspend("+full[0]+")>Suspend</a>";
        }else{
            status = "<a href=# onclick=approve("+full[0]+")>Approve</a>";
        }
        
        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="#" onclick=view("'+full[0]+'"); data-toggle="modal" data-target="#myModal">View</a></li><li>'+status+'</li>  </ul></div>';

    }

    function view(id){

        $.ajax({
            url: "modul/ajax/dataBuyer.php?view&id="+id,
            success: function(respon)
            {
                $('.result').html(respon);
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function approve(id){

        $.ajax({
            url: "modul/ajax/dataBuyer.php?approve&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing approved");      
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function suspend(id){
    
        $.ajax({
            url: "modul/ajax/dataBuyer.php?suspend&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing suspended"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload( null, false );
    }


    //});

    </script>

    <!-- Modal  -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b><i class='fa fa-eye'></i> View</b></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
    </div>


    <style>
    .tooltipx {
    position: relative;
    display: inline-block;
    }

    .tooltipx .tooltiptextx {
    visibility: hidden;
    width: 140px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -75px;
    opacity: 0;
    transition: opacity 0.3s;
    }

    .tooltipx .tooltiptextx::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
    }

    .tooltipx:hover .tooltiptextx {
    visibility: visible;
    opacity: 1;

    }
    </style>

    <script type="text/javascript">

    function myFunctionx() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        document.execCommand("copy");

    var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied! ";
    }

</script>


