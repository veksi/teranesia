<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=sales_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>ID</th>
        <th>Nama</th>
        <th>Posisi</th>
        <th>Level</th>
        <th>Tgl Lahir</th>
        <th>Email</th>
        <th>Telepon</th>
        <th>Domisili</th>
        <th>KTP</th>
        <th>NPWP</th>
        <th>Bank</th>
        <th>Rekening</th>
        <th>Refferal</th>
        <th>Status</th>
        <th>Tgl Bergabung</th>
        <th>Tgl Set Posisi</th>
        <th>Tgl Aktivasi</th>        
        <th>Editor</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getReport('sales');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>            
            <td><?php echo $data['idSales']; ?></td>
            <td><?php echo $data['fullName']; ?></td>
            <td><?php echo stsPosSales($data['position']); ?></td>
            <td><?php echo $data['level']; ?></td>
            <td><?php echo $data['bornDate']; ?></td>            
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['phone']; ?></td>
            <td><?php echo getDomicile($data['domicile']); ?></td>
            <td><?php echo $data['ktp_number']; ?></td>
            <td><?php echo $data['npwp']; ?></td>
            <td><?php echo getBank($data['bankCode']); ?></td>
            <td><?php echo $data['account']; ?></td>
            <td><?php echo $data['refferal']; ?></td>
            <td><?php echo stsRegSales($data['active']); ?></td>            
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php echo $data['joinPositionDate']; ?></td>
            <td><?php echo $data['activationDate']; ?></td>
            <td><?php echo getUser($data['activatedBy']); ?></td>
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
