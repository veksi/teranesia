<?php
if($gadget=="hp"){
    $url = "https://wa.me/";
}else{
    $url = "https://web.whatsapp.com/send?phone=";
}
?>
<div class="clear10"></div>
<div class="form-group">
    <h4 class="pull-left">Rekapitulasi</h4>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Kehadiran</th>
                <th>Status</th>
                <!--<th style="text-align:center">Action</th>-->
                <th>email</th>
                <th>phone</th>
                <th>nama</th>
                <th>tgl</th>
            </tr>
        </thead>                                            
    </table>
</div>

<script>
$('a').click( function(e) { 
//$('#click_advance').click(function() {
    $("i", this).toggleClass("fa-chevron-up fa-chevron-down");
    //$("#arrow").toggleClass("fa fa-chevron-up fa fa-chevron-down");
});

$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });
});
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

 
//$(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({
        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataRekap.php?list",
        "order": [[ 7, "desc" ]],
        "language": { "infoFiltered": ""},
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
        },
        //"bInfo" : false,
        //"sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>', //set rigth paging
        //"lengthMenu": [2, 10, 20, 50, 100, 200, 500],
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            { 
                "render": nameCol,
                "targets": 1,
            },
            { 
                "data": 4,
                "targets": 2,
            },
            { 
                "data": 7,
                "targets": 3,
            },
            /*{ 
                "render": actionCol,
                "targets": 4,
            },*/
            { 
                "data": 9,
                "targets": 4,
                "visible":false
            },
            { 
                "data": 4,
                "targets": 5,
                "visible":false
            },
            { 
                "data": 2,
                "targets": 6,
                "visible":false
            },
            { 
                "data": 5,
                "targets": 7,
                "visible":false
            },
           

        ],

    });

    $('#closer').on('click', function() {                
        otable.ajax.url( 'modul/ajax/dataEventView.php?list&sts=closer' ).load();
    });
    $('#agent').on('click', function() {
        otable.ajax.url( 'modul/ajax/dataEventView.php?list&sts=agent' ).load();
    });
    $('#finder').on('click', function() {
        otable.ajax.url( 'modul/ajax/dataEventView.php?list&sts=finder' ).load();
    });
    $('#coordinator').on('click', function() {
        otable.ajax.url( 'modul/ajax/dataEventView.php?list&sts=coordinator' ).load();
    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function cekCol(data, type, full) {
        return '<input type=checkbox class=check-item name=idSales[] value='+full[0]+' >';
    }

    function nameCol(data, type, full) {

        if(full[8]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[8]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[8]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }
        phone = "<a href=<?=$url;?>"+full[6]+">"+full[6]+"</a>";

        return ''+full[2]+' - '+phone+' ';

    }

    function actionCol(data, type, full) {
        //return '<a class="btn btn-default" data-toggle="modal" data-target="#myModal" href=# onclick=setId("\'' + full[2] + '\'")><i class="fa fa-check-square-o" aria-hidden="true"></i></a>';
        return '<div class="text-center"><a class="btn btn-default" data-toggle="modal" data-target="#myModal" href=# onClick="setId(\'' + full[0] + '\',\''+full[2]+'\')" /><i class="fa fa-qrcode" aria-hidden="true"></i></a><a class="btn btn-default" href=# onclick=present("'+full[0]+'","'+idEvent+'")><i class="fa fa-check" aria-hidden="true"></i></a></div>';
    }
/*
    function phoneCol(data, type, full) {
        phone = "<a href=<?php echo $url; ?>"+full[4]+">"+full[4]+"</a>";
        return ''+phone+'';
    }*/

    function setId(id,nama){
        document.getElementById("webcodecam-canvas").style.display = "block";
        $("#idSales").val(id);
        $("#namaSales").val(nama);
        $("#myModalLabel").html(nama);
        $("#scanned-img").attr('src','');
    }


    function nameCol2(data, type, full) {

        if(full[8]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[8]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[8]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        if(full[10]==''){
            note = '';
        }else{
            note = '<a href="#" class="note_btn" data-toggle="modal" data-target="#modal2" data-id="'+full[0]+'" data-note="'+full[10]+'" ><i class="fa fa-sticky-note-o"></i></a>';            
        }

        return ''+full[2]+' <small>('+full[0]+')</small> <div class="clearfix"></div> '+status+' <div class="clearfix"></div> '+full[9]+ ' '+note;

    }

    function actionCol2(data, type, full2) {
        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href=# onclick=notPresent("'+full2[1]+'","'+idEvent+'")>Delete</a></li><li><a href="#" class="note_btn" data-toggle="modal" data-target="#modal2" data-id="'+full2[0]+'" data-note="'+full2[10]+'" >Note</a></li></ul></div>';
    }

    function notPresent(id,idEvent){
        $.ajax({
            url: 'modul/ajax/dataPresent.php?del&id='+id+"&idEvent="+idEvent,
            success: function(respon)
            {
                reload();
                success("Berhasil dihapus");      
            },
            error: function(err) {
                console.log(err);
            }          
        });
    }

    $(function(){
        $('#saveNote').on('click', function(e){
            e.preventDefault();            
            $.ajax({
                url: "modul/ajax/dataPresent.php?note",
                type: "post",
                data: $('#contoh-form').serialize(),
                success: function(data)
                {   
                    $("#note").val('');
                    $('#modal2').modal('toggle');
                    reload();
                    success("Note saved");
                }           
            });
        });
    });

    function reload(){
        otable.ajax.reload( null, false );
        otable2.ajax.reload( null, false );
    }

    function success(msg){
        swal("Done", msg , "success");
    }   

    $(document).on( "click", '.note_btn',function(e) {
        var id = $(this).data('id');
        $("#id").val(id);
        var note = $(this).data('note');
        $("#note").val(note);
    });

    function present(id,idEvent){
  
        $.ajax({
            url: "modul/ajax/dataEventView.php?present&id="+id+"&idEvent="+idEvent,
            success: function(respon)
            {
                reload();
                success("Sales hadir");      
                
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }
    
</script>

<!-- Modal  -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2Label">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelNote"><b><i class="fa fa-sticky-note-o"></i> Note</b></h4>
            </div>
            <div class="modal-body">
                <form id="contoh-form">
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id"/>
                        <textarea id="note" name="note" class="form-control" rows="3" placeholder="Note"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="saveNote" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="stop"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="modal-body">
                <script type="text/javascript">                 
                $('#myModal').on('shown.bs.modal', function () {                                        
                    $('#idSales').focus();     
                })
                </script> 
                
                <div class="navbar-form navbar-right" style="display:none">
                    <div class="form-group">
                        <input id="image-url" type="text" class="form-control" placeholder="Image url">
                        <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                        <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                        <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
                        <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                        <!--<button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>-->
                    </div>
                    <select class="form-control" id="camera-select"></select>
                    <input type="text" id="scanned">
                    <input type="text" id="idEvent" value="<?=@$_GET['idEvent'];?>">
                </div>
                <input type="text" id="idSales" value="" style="pointer-event:none;position:absolute;z-index:-100" readonly>
                <input type="hidden" id="namaSales" value="" class="form-control" readonly>
                    
                <img id="scanned-img" src="" style="width:100%;height:320px;display:none;">
                <canvas width="100%" height="320" id="webcodecam-canvas" style="display:none;width:100%;height:320px"></canvas>
                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>

                <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/filereader.js"></script>
                <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/qrcodelib.js"></script>
                <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/webcodecamjs.js"></script>
                <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/main.js"></script>
                <br>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>