<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Activity List</b></h4>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Prospect</th>
                <th>Project</th>
                <th>Finder</th>
                <th>Closer</th>
                <th>Date</th>
                <th>Progress</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('trx');
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo getProspect($data['idProspect']); ?></td>
                <td><?php echo getProduct($data['idProduct']); ?></td>
                <td><?php $finder = getSales($data['idSales']); echo $finder['fullName']; ?></td>
                <td><?php $closer = getSales($data['closer']); echo $closer['fullName']; ?></td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getProgressTrx($data['progress']); ?></td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qTrx.php?trx&del&id='+id;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

