<?php if(isset($_GET['id'])){
    $data = editProspect('prospect',$_GET['id']);
?>

    <script>

        $(document).ready(function() { 
            document.getElementsByName("mode")[0].value = 'upd';
            document.getElementById("model").innerHTML = 'Edit Prospect';
            document.getElementsByName("id")[0].value = '<?php echo $data['idProspect']; ?>';
            document.getElementsByName("namaSales")[0].value = '<?=getSales($data['idSales'])['fullName'];?>';
            document.getElementsByName("idSales")[0].value = '<?php echo $data['idSales']; ?>';
            document.getElementsByName("name")[0].value = '<?php echo $data['name']; ?>';
            document.getElementsByName("profession")[0].value = '<?php echo $data['profession']; ?>';
            document.getElementsByName("phone")[0].value = '<?php echo $data['phone']; ?>';
            document.getElementsByName("email")[0].value = '<?php echo $data['email']; ?>';
            document.getElementsByName("need")[0].value = '<?php echo $data['need']; ?>';
            document.getElementsByName("note")[0].value = '<?php echo $data['note']; ?>';
            document.getElementsByName("borndate")[0].value = '<?php echo $data['bornDate']; ?>';
            document.getElementsByName("insertDate")[0].value = '<?php echo $data['insertDate']; ?>';
        });

    </script>

<?php } ?>

<form action="library/qProspect.php" method="post" enctype="multipart/form-data">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Prospect</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Sales</label>
        <input type="text" id="namaSales" class="form-control" name="namaSales" placeholder="Sales name" value="">
        <input type="hidden" id="idSales" name="idSales">
    </div>

    <div class="form-group">
        <label>Prospect name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Prospect name">
    </div>

    <div class="form-group">
        <label>Profession</label>
        <input type="text" name="profession" class="form-control" placeholder="Profession">
    </div>

    <div class="form-group">
        <label>Phone</label>
        <input type="text" name="phone" class="form-control" placeholder="Phone">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control" placeholder="Email">
    </div>

    <div class="form-group">
        <label>Need</label>
        <textarea name="need" class="form-control"  rows="3" placeholder="Need"></textarea>
    </div>

    <div class="form-group">
        <label>Note</label>
        <textarea name="note" class="form-control"  rows="3" placeholder="Note"></textarea>
    </div>

    <div class="form-group">
        <label>Born date</label>
        <div class='input-group date' id='borndate'>                            
            <input type='text' name="borndate" class="form-control" placeholder="Born date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>  

    <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker'>                            
            <input type='text' name="insertDate" class="form-control" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>  

    <div class="form-group">
        <button name=submit type="submit" class="btn btn-success">Save</button>
        <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
    </div>

</form>


<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    var dateNow = new Date();
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });
});

$(function() {
    $('#borndate').datetimepicker({
        format: 'YYYY-MM-DD'
    });
});
</script>



<style>
    .autocomplete-suggestions {
        border: 1px solid #999;
        background: #FFF;
        overflow: auto;
    }
    .autocomplete-suggestion {
        padding: 2px 5px;
        white-space: nowrap;
        overflow: hidden;
    }
    .autocomplete-selected {
        background: #F0F0F0;
    }
    .autocomplete-suggestions strong {
        font-weight: normal;
        color: #3399FF;
    }
    .autocomplete-group {
        padding: 2px 5px;
    }
    .autocomplete-group strong {
        display: block;
        border-bottom: 1px solid #000;
    }
</style>



<!-- Memanggil Autocomplete.js -->
<script src="js/jquery.autocomplete.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Selector input yang akan menampilkan autocomplete.
        $( "#namaSales" ).autocomplete({
            serviceUrl: "modul/ajax/autocompleteSales.php",   // Kode php untuk prosesing data.
            dataType: "JSON",           // Tipe data JSON.
            onSelect: function (suggestion) {
                $( "#namaSales" ).val("" + suggestion.value);
                $( "#idSales" ).val("" + suggestion.id);
            }
        });

    })
</script>
