

<?php 

$now=date('Y');
$m = date("m");

if(empty($_POST['year'])){
    $year = $now; 
}else{ 
    $year = $_POST['year']; 
};

if(empty($_POST['month'])){
    $month = "All"; 
}else{ 
    $month = $_POST['month']; 
};

$monthName = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

if($month=="All"){
    
    for($i=0;$i<count($monthName);$i++){
        
        $arrMonth[] = '"'.$monthName[$i].'"';

        $actSS = getActivateProjectSS($i+1,$year);
        $arrActivateSS[] = $actSS;

        $actSH = getActivateProjectSH($i+1,$year);
        $arrActivateSH[] = $actSH;

    }

}else{

    for($i=0;$i<count($monthName);$i++){
        
        $arrMonth[] = '"'.$monthName[$i].'"';

        if(($month-1)==$i){

            $actSS = getActivateProjectSS($month,$year);
            $arrActivateSS[] = $actSS;

            $actSH = getActivateProjectSH($month,$year);
            $arrActivateSH[] = $actSH;

        }else{

            $actSS = getActivateProjectSS($month,$year);
            $arrActivateSS[] = 0;

            $actSH = getActivateProjectSH($month,$year);
            $arrActivateSH[] = 0;

        }

    }

}

?>

<form action="?sum_project" method="post" enctype="multipart/form-data">

    <div class="form-group">

        <h4 class="pull-left"><b>Summary <?php echo @$monthName[$month-1]; ?> <?php echo $year; ?></b></h4>

        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success pull-right">Submit</button>
        </div>

        <div class="pull-right">

            <select name="month" class="form-control pull-right">
                <!--<option value="" disabled selected hidden>Month</option>-->
                <option value="All" selected>All</option>
                <?php
                $monthName=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                for($c=1; $c<=12; $c++){ 
                    $monthNumber = $c-1; 
                    if($c==$month){
                        echo"<option value=$c selected> $monthName[$monthNumber] </option>";
                    }else{
                        echo"<option value=$c> $monthName[$monthNumber] </option>";
                    }
                }?>
            </select>
        
        </div>

        <div class="pull-right">
        
            <select name="year" class="form-control">
                <?php
                
                for ($a=2018;$a<=$now;$a++){ 
                    if($a==$year){
                        echo"<option value='$a' selected> $a </option>";
                    }else{
                        echo "<option value='$a'>$a</option>";
                    }
                }?>
            </select>

        </div>
    
    </div>

</form>

<div class="clear10"></div>

<script src="js/highcharts.js"></script>

<div id="containerSS" style="width:100%; height: auto; margin: 0 auto;"></div>
<script type="text/javascript">
    Highcharts.chart('containerSS', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'STARSALES'
        },
        xAxis: {
            categories: [<?= join($arrMonth, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Activation',
            data: [<?= join($arrActivateSS, ',') ?>]
        }]
    });
</script>

<div id="containerSH" style="width:100%; height: auto; margin: 0 auto;"></div>
<script type="text/javascript">
    Highcharts.chart('containerSH', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'STARHOME'
        },
        xAxis: {
            categories: [<?= join($arrMonth, ',') ?>]
        },
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Activation',
            data: [<?= join($arrActivateSH, ',') ?>]
        }]
    });
</script>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Project</th>
                <th>Total Activation</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getListGroupProject('trx',$year,$month);
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>                
                <td><?php echo getProduct($data['idProduct']); ?></td>
                <td><?php echo getCountProduct('trx',$data['idProduct'],$year,$month); ?></td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {
            window.location.href='library/qProspect.php?prospect&del&id='+id;
        }
    }
</script>


<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>