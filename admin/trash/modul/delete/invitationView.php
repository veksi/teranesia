<?php 
if($gadget=="hp"){
    $url = "https://wa.me/";
    $sep = "?";
    $target = "";
}else{
    //$url = "https://web.whatsapp.com/send?phone=";
    //$sep = "&";
    //$target = "target=_blank";
    $url = "https://wa.me/";
    $sep = "?";
    $target = "";
}
?>
<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left">Kirim Undangan</h4>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>                 
                <th>Name</th>
                <th>Phone</th>
                <th>Status</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>                                            
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {
        var idEvent = '<?=$_GET['idEvent'];?>';
        var area = '<?=$_GET['area'];?>';
        
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var otable = $('#tabelAuthor').DataTable({

            "processing": true,
            responsive: true,
            "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
            "serverSide": true,
            "ajax": "modul/ajax/dataInvitationView.php?list&idEvent="+idEvent+"&area="+area,
            "order": [[ 1, "asc" ]],
            "stateSave": false, //tetap pada current page apapun yg terjadi
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // table.order = [[ 5, "desc" ]];
            },
            "language": { "infoFiltered": ""},
            "lengthMenu": [10, 25, 50, 100, 500, 1000],
            "fnCreatedRow": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            },

            "columnDefs": [
                
                { 
                    "data": 1,
                    "targets": 1 
                },
                { 
                    "render": phoneCol,
                    "targets": 2 
                },
                { 
                    "render": stsCol,
                    "targets": 3 
                },
                { 
                    "render": actionCol,
                    "targets": 4 
                },

            ],

        });

        $('#tabelAuthor_filter input').unbind();
        $('#tabelAuthor_filter input').bind('keyup', function(e) {
            if(e.keyCode === 13) {
                otable.search( this.value ).draw();
            }
        });  

        function phoneCol(data, type, full) {
            phone = "<a href=<?=$url;?>"+full[2]+">"+full[2]+"</a>";
            return ''+phone+'';
        }

        function stsCol(data, type, full) {
            if(full[3]=='1'){
                sts = 'Terkirim'
            }else{
                sts = '';
            }
            
            return ''+sts+'';
        }

        function actionCol(data, type, full) {

            let myStr = full[4];
            myStr = myStr.replace('[nama]', full[1]);

            undang = "<div class='text-center'><a <?=$target;?> onclick=send("+full[0]+") href=<?=$url?>"+full[2]+"<?=$sep?>text="+encodeURIComponent(myStr)+" class='btn btn-primary btn-xs'>Undang</a></div>";
            return ''+undang+'';
        }

        function send(idSales){

            $.ajax({
                url: "modul/ajax/dataInvitationView.php?send&idSales="+idSales,
                success: function(respon)
                {
                    reload();
                    //success();      
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

        function success(msg){
            swal("Done", msg, "success");
        }

        function reload(){
            otable.ajax.reload( null, false );
        }
</script>


