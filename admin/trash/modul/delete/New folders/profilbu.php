<?php if(isset($id)){ 
    $data = dbGetRow('user','id='.$id,'','');
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("name_d").value = '<?=$data['first_name'];?>';
        document.getElementById("name_b").value = '<?=$data['last_name'];?>';
        document.getElementById("phone").value = '<?=$data['telp'];?>';
        document.getElementById("email").value = '<?=$data['email'];?>';
        document.getElementsByName("imgDB1")[0].value = '<?=$data['foto'];?>';
    });

</script>

<?php  } 

if(!empty(@$data['foto'])){ 
    $pp = "src=".$domain."images/admin/".$data['foto']; 
}else{ 
    $pp="src=".$domain."images/nofoto.png";
}

?>

<form action="<?=$adminPath?>modul/ajax/data_profil.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id' value='<?=$id?>'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar rounded-circle img-thumbnail" alt="avatar" style="width:250px;height:250px;" <?=$pp;?> >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1'>
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Nama Depan</label>
                                <input class="form-control" id="name_d" name="name_d" type="text" placeholder="Nama depan" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Nama Belakang</label>
                                <input class="form-control" id="name_b" name="name_b" type="text" placeholder="Nama belakang" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Email</small>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Password</small>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Telp *</small>
                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Telp" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Tempat Lahir *</small>
                        <input type="text" id="born" name="born" class="form-control" placeholder="Tempat Lahir" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="form-text text-muted">Jadwal Pelatihan *</small>      
                        <div class="input-group date form-group" id="datepicker">
                            <input type="text" class="form-control" id="Dates" name="Dates" placeholder="Pilih Tanggal" autocomplete="off" required />
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <span class="count"></span>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>              
                    
                    <div class="pull-right">
                        <button name=submit type="submit" class="btn btn-success">Save</button>
                        <button type="button" onclick="window.location.href='<?=$domain;?>dashboard'" class="btn btn-info">Cancel</button>
                    </div>

                    <div class="clearfix"></div>
                
                </div>   
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

function HitungTitle(){
    var Teks = document.forminput.title.value.length;
    var total = document.getElementById('hasil');
    total.innerHTML = 200 - Teks;
}


function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$domain?>images/nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var angka = "0123456789";
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    var title       = document.getElementById('title').value;
    var title       = title.toLowerCase();
    
    var category    = document.getElementById('category').value;
    var interior    = document.getElementById('interior').value;
    var price       = document.getElementById('price').value;
    var idArea      = document.getElementById('idArea').value;
    
    var description = tinyMCE.activeEditor.getContent();
    var descriptionLength = (tinyMCE.activeEditor.getContent({format : 'text'}).length);

    var kata_jual = title.search("jual");
    var kata_beli = title.search("beli");
    var kata_sewa = title.search("sewa");

    if (title==""){
        swal("","Lengkapi data judul","warning",).then(function(){ document.getElementById("title").focus(); });
        return false;
    }else if (title.length>200){
        swal("","Judul terlalu panjang, max 100 karakter","warning",).then(function(){ document.getElementById("title").focus(); });
        return false;
    }else if ((kata_jual>=0)||(kata_beli>=0)||(kata_sewa>=0)){
        swal("","Maaf, judul tidak boleh mengandung kata JUAL, SEWA, BELI","warning",).then(function(){ document.getElementById("title").focus(); });
        return false;
    }else if (category==""){
        swal("","Lengkapi data kategori","warning",).then(function(){ document.getElementById("category").focus(); });
        return false;
    }else if (interior==""){
        swal("","Lengkapi data interior","warning",).then(function(){ document.getElementById("interior").focus(); });
        return false;
    }else if (price==""){
        swal("","Lengkapi data harga","warning",).then(function(){ document.getElementById("price").focus(); });
        return false; 
    }else if (description==""){
        swal("","Lengkapi data informasi tambahan","warning",).then(function(){ tinyMCE.get('description').focus(); });
        tinyMCE.get('description').focus();
        return false;
    }else if (descriptionLength<50){
        swal("","Informasi terlalu pendek, min 50 karakter","warning",).then(function(){ tinyMCE.get('description').focus(); });
        return false;           
    }else if (idArea==""){
        swal("","Lengkapi data kecamatan atau kelurahan","warning",).then(function(){ document.getElementById("area").focus(); });
        return false;         
    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}

</script>

<!-- multi datepicker -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

<script>
$(document).ready(function() {
    $('#datepicker').datepicker({
        numberOfMonths: 4,
        startDate: -Infinity,
        multidate: false,
        multidateSeparator:'|',
        format: "dd-mm-yyyy",
        daysOfWeekHighlighted: "5,6",
        //datesDisabled: ['31/08/2017'],
        language: 'en',
        clearBtn:true,
        toggleActive:true,
        forceParse:true,
        todayHighlight:true,
    }).on('changeDate', function(e) {
        // `e` here contains the extra attributes
        $(this).find('.input-group-text .count').text(' ' + e.dates.length);
    });
});