<?php 
if(isset($_POST['upd'])){ 

    $reg = dbGetNum('user',"email = '".$_POST['email']."'",'','');
    if($reg==1){ ?>
        <script>swal("Gagal","Email sudah terdaftar","warning",).then(function(){ window.history.back(); });</script>
    <?php }else{
        
        reg($_POST['name_d'],$_POST['name_b'],$_POST['email'],$_POST['password']); 
        //include "library/mail/register.php";
    ?>

        <body class="bg-primary">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                        <div class="container text-center">
                            <div class="row justify-content-center">
                                <div class="col-lg-7">
                                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                                        <div class="card-header"><h3 class="text-center font-weight-light my-4">Terima Kasih</h3></div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <i class="fa fa-check-square-o fa-5x" style="color: #DCC000"></i>
                                            </div>
                                            <div class="form-group">
                                                Akun anda akan aktif setelah diverifikasi oleh tim kami maksimal 1 x 24 Jam.
                                            </div>
                                            <div class="form-group">
                                                <a href="<?=$domain?>" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Oke">Oke</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <br>
                <br>
                <div id="layoutAuthentication_footer">
                    <?php require "modul/copy.php"; ?>
                </div>
            </div>
        </body>
        
    <?php } ?>

<?php }else if(isset($_POST['new_pwd'])){

    $otp    = $_POST['one'].$_POST['two'].$_POST['three'].$_POST['four'].$_POST['five'];
    $row    = dbGetRow('user',"email = '".$_POST['email']."' AND otp = '".$otp."'",'','');
    if(empty($row['email'])){ ?>
        <script>swal("Gagal","Kode OTP tidak terdaftar","warning",);</script>
    <?php }else{
    ?>

        <body class="bg-primary">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                    
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5">
                                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                                        <div class="card-header"><h3 class="text-center font-weight-light my-4">Kode OTP</h3></div>
                                        <div class="card-body">
                                            <div class="small mb-3 text-muted">Masukkan kode OTP yang anda terima.
                                            </div>
                                            <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                                <input type="hidden" name="upd">
                                                <input type="hidden" name="email" value="<?=$_POST['email']?>">
                                                <div class="form-group">
                                                    <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                    <input class="form-control py-4" id="inputEmailAddress" name="email" type="email" aria-describedby="emailHelp" placeholder="Email " />
                                                </div>
                                                
                                                <div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Reset" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Reset"></div>
                                            </form>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <br>
                <br>
                <div id="layoutAuthentication_footer">
                    <?php require "modul/copy.php"; ?>
                </div>
            </div>
        </body>
        
    <?php } ?>

<?php }else if(isset($_POST['otp_cek'])){

    $otp    = $_POST['one'].$_POST['two'].$_POST['three'].$_POST['four'].$_POST['five'];
    $row    = dbGetRow('user',"email = '".$_POST['email']."' AND otp = '".$otp."'",'','');
    if(empty($row['otp'])){ ?>
        <script>swal("Gagal","Kode OTP tidak valid","warning",).then(function(){ window.history.back(); });</script>
    <?php  header("Location: {$_SERVER["HTTP_REFERER"]}"); 
    }else{
    ?>
        <body class="bg-primary">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                    
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5">
                                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                                        <div class="card-header"><h3 class="text-center font-weight-light my-4">Kata Sandi Baru</h3></div>
                                        <div class="card-body">
                                            <div class="small mb-3 text-muted">Silahkan atur ulang kata sandi anda yang baru.
                                            </div>
                                            <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi_otp();">
                                                <input type="hidden" name="new_pwd">
                                                <input type="hidden" name="email" value="<?=$_POST['email']?>">
                                                <div class="form-group">
                                                    <label class="small mb-1" for="inputPassword">Kata Sandi</label>
                                                    <input class="form-control py-4" id="inputPassword" name="password" type="password" placeholder="Kata sandi" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="small mb-1" for="inputPassword">Ulangi Kata Sandi</label>
                                                    <input class="form-control py-4" id="inputPassword" name="password" type="password" placeholder="Ulangi Kata sandi" />
                                                </div>
                                                
                                                <div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Reset" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Reset"></div>
                                            </form>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <br>
                <br>
                <div id="layoutAuthentication_footer">
                    <?php require "modul/copy.php"; ?>
                </div>
            </div>
        </body>

    <?php } ?>
    
<?php }else if(isset($_POST['email_cek'])){

    $row    = dbGetRow('user',"email = '".$_POST['email']."'",'','');
    if(empty($row['email'])){ ?>
        <script>swal("Gagal","Email tidak terdaftar","warning",).then(function(){ window.history.back(); });</script>
    <?php  
    }else{
        $otp    = otp();
        $conn   = db_conn_sepro();
        $upd    = mysqli_query($conn,"UPDATE user SET otp = '$otp' WHERE email = '$_POST[email]' ");  
        //include "library/mail/otp.php";
    ?>
        <body class="bg-primary">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                    
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5">
                                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                                        <div class="card-header"><h3 class="text-center font-weight-light my-4">Kode OTP</h3></div>
                                        <div class="card-body">
                                            <div class="small mb-3 text-muted">Masukkan kode OTP yang anda terima.
                                            </div>
                                            <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                                <input type="hidden" name="otp_cek">
                                                <input type="hidden" name="email" value="<?=$_POST['email']?>">                                                
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col p-2">
                                                            <input class="form-control py-4 text-center" id="one" name="one" type="text" />
                                                        </div>
                                                        <div class="col p-2">
                                                            <input class="form-control py-4 text-center" id="two" name="two" type="text" />
                                                        </div>
                                                        <div class="col p-2">
                                                            <input class="form-control py-4 text-center" id="three" name="three" type="text" />
                                                        </div>
                                                        <div class="col p-2">
                                                            <input class="form-control py-4 text-center" id="four" name="four" type="text" />
                                                        </div>
                                                        <div class="col p-2">
                                                            <input class="form-control py-4 text-center" id="five" name="five" type="text" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim"></div>
                                            </form>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <br>
                <br>
                <div id="layoutAuthentication_footer">
                    <?php require "modul/copy.php"; ?>
                </div>
            </div>
        </body>
        
    <?php } ?>

<?php }else{ ?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Lupa Kata Sandi</h3></div>
                                    <div class="card-body">
                                        <div class="small mb-3 text-muted">Masukkan e-mail yang terdaftar. Kami akan mengirimkan kode verifikasi untuk atur ulang kata sandi.
                                        </div>
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="email_cek">
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" id="email" name="email" type="email" aria-describedby="emailHelp" placeholder="Email " />
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Reset" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Reset"></div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?=$domain?>">Sudah punya akun? Masuk</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

<?php } ?>