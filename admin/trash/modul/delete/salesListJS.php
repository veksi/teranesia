



<div class="clear10"></div>

<form action="library/ajaxSales.php" method="post">

    <div class="form-group">
        <h4 class="pull-left"><b>Sales</b></h4>
        <div class="pull-right">
            <a href="modul/excel.php" class="btn btn-primary">Export to excel</a>
            <button name=activate type="submit" id="btn-activate" class="btn btn-success">Activate</button>
            <button name=deactivate type="submit" class="btn btn-danger">Suspend</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="box-body table-responsive">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="no-sort" name="prop_ref_no" style="width:30px"><input type="checkbox" id="check-all"></th>
                    <th style="vertical-align:middle;text-align:center">No</th>
                    <!--<th style="vertical-align:middle;text-align:center">KTP Number</th>-->
                    <th style="vertical-align:middle;text-align:center">Name</th>
                    <th style="vertical-align:middle;text-align:center">Domicile</th>
                    <th style="vertical-align:middle;text-align:center">Phone</th>
                    <th style="vertical-align:middle;text-align:center">Join Date</th>
                    <th style="vertical-align:middle;text-align:center">Editor</th>
                    <th style="vertical-align:middle;text-align:center">Position</th>
                    <th style="vertical-align:middle;text-align:center">Action</th>
                </tr>
            </thead>
            <!--
            <tbody>
                <?php
                $list = getList('sales');
                $no=1;
                foreach($list as $data){

                if($data['active']==1){
                    $status = "<a href=library/qSales.php?sales&on&id=".$data['idSales'].">Suspend</a>";
                }else{
                    $status = "<a href=library/qSales.php?sales&off&id=".$data['idSales'].">Activate</a>";
                }

                ?>
                <tr>
                    <td>
                        <input type="checkbox" class="check-item" name="idSales[]" value="<?php echo $data['idSales']; ?>" >
                    </td>
                    <td><?php echo $no; ?></td>
                    <td>
                        <?php echo $data['fullName']; ?>
                        <div class="clearfix"></div>
                        <?php if($data['active']=='0'){ ?>
                            <span class="label label-info">Pending</span>
                        <?php }elseif($data['active']=='1'){ ?>
                            <span class="label label-success">Active</span>
                        <?php }elseif($data['active']=='2'){ ?>
                            <span class="label label-danger">Suspend</span>    
                        <?php } ?>
                        <div class="clearfix"></div>
                        <small><?php echo $data['email']; ?></small>
                    </td>
                    <td><?php echo getDomicile($data['domicile']); ?></td>
                    <td><?php echo $data['phone']; ?></td>
                    <td><?php echo $data['joinDate']; ?></td> 
                    <td><?php echo getUser($data['activatedBy']); ?></td>   
                    <td style="text-align: center;">
                        <select name="position" class="form-control" onchange="javascript:location.href='library/qSales.php?sales&position&position='+(this.value)+'&id=<?=$data['idSales']?>'" >
                            <option value="" disabled selected >Not set</option>
                            <option value="1" <?php if($data['position']==1){ echo "selected";} ?> >Finder</option>
                            <option value="2" <?php if($data['position']==2){ echo "selected";} ?>>Closer</option>
                            <option value="3" <?php if($data['position']==3){ echo "selected";} ?>>Coordinator</option>
                        </select>
                    </td>            
                    <td style="text-align: center;">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">View</a></li>
                                <li><a href="?sales&mode=ins&id=<?php echo $data['idSales']; ?>">Edit</a></li>
                                <li><a href="#">Delete</a></li>
                                <li><?php echo $status; ?></li>
                                <li><a href="library/qSales.php?sales&reset&id=<?=$data['idSales']?>">Reset Password</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <?php $no++;} ?>                                    
            </tbody>-->
        </table>
    </div><!-- /.box-body -->

</form>

<script>

    $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
        $("#check-all").click(function(){ // Ketika user men-cek checkbox all
            if($(this).is(":checked")){
                $(".check-item").prop("checked", true);
            }else{ // Jika checkbox all tidak diceklis
                $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
            }
        });
    });

</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script>/*
    $(document).ready(function () {
        var t = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": 'https://www.starhome.co.id/admin/modul/action/dataTables.php',
            "columns": [
                {"data": "fullName"},
                {"data": "position"}
            ],
            "order": [[1, 'asc']]
        });

        $('#example1 tbody').on( 'click', '.tblEdit', function () {
            var data = table.row( $(this).parents('tr') ).data();
            window.location.href = "edit.php?id="+ data[4];
        } );
    });*/
</script>

<script type="text/javascript">
 $(document).ready(function() {
     var table = $('#example1').DataTable( { 
         "processing": true,
         "serverSide": true,
         "ajax": 'https://www.starhome.co.id/admin/modul/action/dataTables.php',
         "columnDefs": [ {
             "targets": 8,
             "data": null,
             "defaultContent": "<div class='btn-group pull-right'><button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Action <span class='caret'></span></button><ul class='dropdown-menu'><li><a href='#'>View</a></li><li><a class='tblEdit' href='#'>Edit</a></li><li><a href='#'>Delete</a></li><li><?php echo $status; ?></li><li><a href='library/qSales.php?sales&reset&id=<?=$data['idSales']?>'>Reset Password</a></li></ul></div>"
         }],
         "columnDefs": [ {
            
            "targets": 0, 
            "data": null,
            "orderable": false,
           
         }]
         
         /*
         "columns": [
                {"data": "fullName"},
                {"data": "position"}
         ],
         "columnDefs": [ {
             "targets": -1,
             "data": null,

             "defaultContent": "<div class='btn-group pull-right'><button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Action <span class='caret'></span></button><ul class='dropdown-menu'><li><a href='#'>View</a></li><li><a class='tblEdit' href='#'>Edit</a></li><li><a href='#'>Delete</a></li><li><?php echo $status; ?></li><li><a href='library/qSales.php?sales&reset&id=<?=$data['idSales']?>'>Reset Password</a></li></ul></div>"
         }]*/
     });

     $('#example1 tbody').on( 'click', '.tblEdit', function () {
         var data = table.row( $(this).parents('tr') ).data();
         window.location.href = "?sales&mode=ins&id="+ data[8];
     } );
/*
     $('#example1').dataTable( {
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    });*/

 });
</script>