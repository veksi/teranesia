<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Invitation</b></h4>
    <a href="?invitation&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Title</th>
                <th style="text-align:center">Action</th>                                                                                
            </tr>
        </thead>                                            
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var otable = $('#tabelAuthor').DataTable({

            "processing": true,
            responsive: true,
            "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
            "serverSide": true,
            "ajax": "modul/ajax/dataInvitation.php?list",
            "order": [[ 3, "desc" ]],
            "stateSave": false, //tetap pada current page apapun yg terjadi
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // table.order = [[ 5, "desc" ]];
            },
            "fnCreatedRow": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            },

            "columnDefs": [
                
                { 
                    "render": imgCol,
                    "targets": 1 
                },
                { 
                    "render": titleCol,
                    "targets": 2 
                },
                { 
                    "render": actionCol,
                    "targets": 3 
                },

            ],

        });

        $('#tabelAuthor_filter input').unbind();
        $('#tabelAuthor_filter input').bind('keyup', function(e) {
            if(e.keyCode === 13) {
                otable.search( this.value ).draw();
            }
        });  

        function imgCol(data, type, full) {

            return '<img height=50px width=100px src=<?php echo $imgNewsFolder; ?>'+full[1]+'>';

        }

        function titleCol(data, type, full) {

            if(full[6]==1){
                status = "<span class='label label-success'>Publish</span>";
            }else{
                status = "<span class='label label-info'>Draft</span>";
            }

            return ''+full[2]+' <div class="clearfix"></div> '+status+'';

        }

        function actionCol(data, type, full) {

            viewBtn = "<a href=?event&mode=view&idEvent="+full[0]+">View</a>";

            return '<div class="btn-group pull-right"><button  type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>'+viewBtn+'</li></ul></div>';

        }

        function success(msg){
            swal("Done", msg, "success");
        }

        function reload(){
            otable.ajax.reload( null, false );
        }
</script>


