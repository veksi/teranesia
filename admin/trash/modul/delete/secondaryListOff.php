<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Listing Secondary</b></h4>
    <!--<a href="?secondary&mode=ins" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah listing</a>-->
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Gambar</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Harga</th>
                <th>Lokasi</th>
                <th>Tanggal</th>
                <th style="text-align:center">Action</th>                                                                        
            </tr>
        </thead>                                            
    </table>
</div>

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qSecondary.php?secondary&del&id='+id;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

$(document).ready(function() {

    var session = <?=$_SESSION['level']?>;

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({

        "processing": true,
        "language": {
            processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataSecondary.php",
        "order": [[ 6, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
           // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            
            { 
                "render": imgCol,
                "targets"  : 1,
            },
            { 
                "render": nameCol,
                "targets"  : 2,
            },
            { 
                "render": actionCol,
                "targets": 7,
            },
            
        ],

    });

    function imgCol(data, type, full) {
        return '<img height=50px width=100px src=<?php echo $imgSecondaryFolder; ?>'+full[1]+'>';
    }

    function nameCol(data, type, full) {

        if(full[7]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[7]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[7]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        if(session==2){
            if(full[8]=='1'){
                flag = '<i class="fa fa-flag"></i>';
            }else{
                flag = '';
            }
        }else{
            flag = '';
        }

        return ''+full[2]+' SH-'+full[0]+' <div class="clearfix"></div> '+status+' '+flag+' <br> '+full[9];

    }

    function actionCol(data, type, full) {

        if(full[7]==1){
            status = "<a href=library/qSecondary.php?on&id="+full[0]+">Suspend</a>";
        }else{
            status = "<a href=library/qSecondary.php?off&id="+full[0]+">Approved</a>";
        }
        
        if(full[8]==1){
            imgValid = "<a href=library/qSecondary.php?done&id="+full[0]+">Undone</a>";
        }else{
            imgValid = "<a href=library/qSecondary.php?undone&id="+full[0]+">Done</a>";
        }

        if(session==2){

            return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="?secondary&mode=view&id='+full[0]+'">View</a></li><li>'+imgValid+'</li>  </ul></div>';

        }else{

            return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="?secondary&mode=view&id='+full[0]+'">View</a></li><li>'+status+'</li>  </ul></div>';

        }

    }

});

</script>

