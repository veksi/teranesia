<?php if(isset($_GET['id'])){ 
    $data = editEvent('event',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Blog';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
        document.getElementsByName("keyword")[0].value = '<?php echo $data['keyword']; ?>';
        document.getElementsByName("category")[0].value = '<?php echo $data['category']; ?>';
        document.getElementsByName("audience")[0].value = '<?php echo $data['audience']; ?>';
        document.getElementsByName("imgDB")[0].value = '<?php echo $data['pict']; ?>';
    });

</script>

<?php } ?>

<form action="library/qEvent.php" method="post" enctype="multipart/form-data">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><div id="model">Add Event</div></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>
    
    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" class="form-control" placeholder="Title">
    </div>

    <div class="form-group">
        <label>Event Date</label>
        <div class='input-group date' id='eventDate'>
            <input type='text' name="borndate" class="form-control" placeholder="Event date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-6 col-md-3">
            <label>Start Date</label>
            <div class='input-group date' id='startDate'>
            <input type='text' name="startDate" class="form-control" placeholder="Start date"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="col-lg-6">
            <label>End Date</label>
            <div class='input-group date' id='endDate'>
            <input type='text' name="endDate" class="form-control" placeholder="End date"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>            
        </div>
    </div> 

    <div class="form-group">
        <label>Content</label> 
        <div class="clearfix"></div>
        <textarea name=content placeholder=" Write something..." style="max-width:350px;max-height:400px;padding:20;height:100%;width:100%"><?php if(isset($_GET['id'])){ echo $data['content']; }?></textarea>
    </div>
</form>

<script>

    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }
    
    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };

    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
        });
    }));

</script>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    var dateNow = new Date();
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });

    $('#eventDate').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#startDate').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#endDate').datetimepicker({
        format: 'YYYY-MM-DD'
    });
});
</script>

