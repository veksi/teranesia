<?php if(isset($_GET['id'])){ 
    $data = edit('invitation',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Invitation';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementById("title").value = '<?=$data['title'];?>';
        document.getElementsByName("startDate")[0].value = '<?php echo $data['start_date']; ?>';
        document.getElementsByName("endDate")[0].value = '<?php echo $data['end_date']; ?>';

        var area = '<?php echo $data['area']; ?>';
        $.each(area.split(", "), function(i,e){
            $("#area option[value='" + e + "']").prop("selected", true);
        });

        var target = '<?php echo $data['target']; ?>';
        $.each(target.split(", "), function(i,e){
            $("#target option[value='" + e + "']").prop("selected", true);
        });
    });

</script>

<?php } ?>

<form action="library/qInvitation.php" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><div id="model">Add Invitation</div></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Title</label>
        <input type="text" id="title" name="title" class="form-control" placeholder="Title">
    </div>
    
    <div class="form-group">
        <label>Area</label>
        <select id="area" name="area[]" class="selectpicker form-control"  multiple data-live-search="true" data-placeholder="Yours Placeholder">
            <!--<option disabled selected style="display:none">Select Area</option>-->
            <option value="all">All Area</option>
            <option value="nonjabodetabek">Luar Jabodetabek</option>
            <option value="jabodetabek">Jabodetabek</option>
            <option value="yogjakarta">Yogyakarta</option>
            <option value="surabaya">Surabaya</option>
            <option value="makassar" disabled>Makassar</option>
            <option value="bandung">Bandung</option>
        </select>
        
        <!--<select id="area" name="area" class="form-control">
            <option value="" disabled selected>Select area</option>
            <option value="all">All Area</option>
            <option value="nonjabodetabek">Luar Jabodetabek</option>
            <option value="jabodetabek">Jabodetabek</option>    
            <option value="jogjakarta">Yogyakarta</option>
            <option value="surabaya">Surabaya</option>
            <option value="makassar" disabled>Makassar</option>
            <option value="bandung">Bandung</option>
        </select>-->
    </div>

    <div class="form-group">
        <label>Target</label>
        <select id="target" name="target[]" class="selectpicker form-control"  multiple data-live-search="true" data-placeholder="Yours Placeholder">
            <!--<option disabled selected style="display:none">Select Area</option>-->
            <option value="all">All Sales</option>
            <option value="finder">Finder</option>
            <option value="closer">Closer</option>
            <option value="manager">Manager</option>
        </select>
    </div>

    <div class="form-group row">
        <div class="col-lg-6 col-md-3">
            <label>Start Date</label>
            <div class='input-group date' id='startDate'>
                <input type='text' class="form-control" name="startDate" placeholder="Start Date"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <div class="col-lg-6">
            <label>End Date</label>
            <div class='input-group date' id='endDate'>
                <input type='text' class="form-control" name="endDate" placeholder="End Date"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
<!--
    <div class="form-group">
        <label>Event</label>
        <select id="event" name="event" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select project name</option>
            <?php  
           // $combo = getCombo2("article"," WHERE category ='4' "," ORDER BY insertDate DESC ");
            //foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idArticle]"; ?>"><?php echo $data['title']; ?></option>
            <?php //} ?>
        </select>
    </div>
-->
    <div class="form-group">
        <label>Text</label> 
        <div class="clearfix"></div>
        <textarea id="msg" name="msg" placeholder=" Write something..." style="max-width:350px;max-height:400px;padding:20;height:100%;width:100%"><?php if(isset($_GET['id'])){ echo $data['msg']; }else{echo "[nama]";}?></textarea>
    </div>
</form>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {

    var dateNow = new Date();

    var mStart = dateNow.getMonth()+1;
    var yStart = dateNow.getFullYear();
    var dateStart = yStart+'-'+mStart+'-01';

    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });

    $('#eventDate').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#startDate').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: dateStart
    });

    $('#endDate').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: dateNow
    });
});

function validasi(){

    var title       = document.getElementById('title').value;
    var area        = document.getElementById('area').value;
    var target      = document.getElementById('target').value;
    var msg         = document.getElementById('msg').value;

    if (title==""){
        swal("","Lengkapi data judul","warning",).then(function(){ document.getElementById("title").focus(); });
        return false;
    }else if (area==""){
        swal("","Lengkapi data area","warning",).then(function(){ document.getElementById("area").focus(); });
        return false;
    }else if (target==""){
        swal("","Lengkapi data target","warning",).then(function(){ document.getElementById("target").focus(); });
        return false;
    }else if (msg==""){
        swal("","Lengkapi data text","warning",).then(function(){ document.getElementById("msg").focus(); });
        return false; 
    }else{
        return true;
    }

}

</script>