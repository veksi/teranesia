<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=project_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>ID</th>
        <th>Judul</th>
        <th>Kategori</th>
        <th>Harga</th>
        <th>Komisi</th>
        <th>Lokasi</th>
        <th>Tipe Unit</th>
        <th>Tipe</th>
        <th>Harga Per (m)</th>
        <th>Deskripsi</th>
        <th>Alamat</th>
        <th>Tgl Input</th>
        <th>Koordinator</th>
        <th>Editor</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getList('product');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $data['idProduct']; ?></td>
            <td><?php echo $data['title']; ?></td>
            <td><?php if($data['category']=='1'){echo "Apartement";}elseif($data['category']=='2'){echo "House";}else{echo "Other";}; ?></td>
            <td><?php echo "Rp. ".number_format($data['price_num']); ?></td>
            <td><?php echo $data['commision']; ?></td>
            <td><?php echo $data['location']; ?></td>            
            <td><?php echo $data['typeUnit']; ?></td>
            <td><?php echo $data['type']; ?></td>
            <td><?php echo $data["priceMeter"]; ?></td>
            <td><?php echo $data['description']; ?></td>
            <td><?php echo $data['address']; ?></td>
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php $dataSales = getSales($data['koordinator']); echo $dataSales['fullName']; ?></td>
            <td><?php echo getUser($data['editor']); ?></td>            
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
