<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Project Unit</b></h4>
    <a href="?unit&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Project Name</th>
                <th>Unit</th>
                <th>Image</th>
                <th>Date</th>
                <th>Editor</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('productUnit');
            $no=1;
            foreach($list as $data){

                if($data['publish']==1){
                    $status = "<a href=library/qUnit.php?unit&on&id=".$data['idUnit'].">Draft</a>";
                }else{
                    $status = "<a href=library/qUnit.php?unit&off&id=".$data['idUnit'].">Publish</a>";
                }
            ?>
            <tr>
                <td><?php echo $no; ?></td>              
                <td>
                    <?php echo getProduct($data['idProduct']); ?>
                    <div class="clearfix"></div>
                    <?php if($data['publish']=='1'){ ?>
                        <span class="label label-success">Publish</span>
                    <?php }else{ ?>
                        <span class="label label-info">Draft</span>
                    <?php } ?>
                </td>
                <td><?php echo $data['type']; ?></td>
                <td style="text-align: center;">
                    <img height="50px" width="100px" src="<?php echo $imgProjectFolder.$data['pict']; ?>"/>      
                </td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getUser($data['editor']); ?></td>
                <td style="text-align: center;">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><?php echo $status; ?></li>
                            <li><a href="#">View</a></li>
                            <li><a href="?unit&mode=ins&id=<?php echo $data['idUnit']; ?>">Edit</a></li>
                            <!--<li><a href="#" onclick=deleteRecord('product','<?php echo $data['idUnit']; ?>','<?php echo urlencode($data['pict']); ?>'); >Delete</a></li>-->
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qUnit.php?unit&del&id='+id+'&photo='+photo;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

