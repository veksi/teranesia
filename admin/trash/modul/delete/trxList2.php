<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Prospect</b></h4>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Sales</th>
                <th>Prospect</th>
                <th>Project</th>
                <th>Closer</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('trx');
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo getSales($data['idSales']); ?></td>
                <td><?php echo getSales($data['idProspect']); ?></td>
                <td><?php echo getProduct($data['idProduct']); ?></td>
                <td><?php echo getCloser($data['allowed']); ?></td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

