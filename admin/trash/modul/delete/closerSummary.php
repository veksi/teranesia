<script>
$(document).ready(function() { 
    $('#all').addClass('active');
});
</script>

<?php 
$monthName = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
$now       = date('Y');
$m         = date('m');

if(empty($_POST['year'])){
    $year = $now; 
}else{ 
    $year = $_POST['year']; 
};

if(empty($_POST['month'])){
    $month = $m-0;
}else{ 
    $month = $_POST['month']; 
};

if($month=="All"){
    
    for($i=1;$i<=count($monthName);$i++){
        
        $arrMonth[] = '"'.$monthName[$i].'"';

        $regWeb = getRegCloser($i,$year);
        $arrRegWeb[] = $regWeb;

        $regApp = getRegCloserPaid($i,$year);
        $arrRegApp[] = $regApp;

    }

}else{

    for($i=1;$i<=31;$i++){

        $arrMonth[] = '"'.$i.'"';

        $regWeb = getRegCloserDay($i,$month,$year);
        $arrRegWeb[] = $regWeb;

        $regApp = getRegCloserPaidDay($i,$month,$year);
        $arrRegApp[] = $regApp;        

    }

}

?>

<form action="?sum_closer" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <h4 class="pull-left"><b>Summary <?php echo $monthName[$month]; ?> <?php echo $year; ?></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success pull-right">Submit</button>
        </div>
        <div class="pull-right">
            <select name="month" class="form-control pull-right">
                <option value="All">All</option>
                <?php
                for($c=1; $c<=12; $c++){ 
                    if($c==$month){
                        echo"<option value=$c selected> $monthName[$c] </option>";
                    }else{
                        echo"<option value=$c> $monthName[$c] </option>";
                    }
                }?>
            </select>
        </div>
        <div class="pull-right">
            <select name="year" class="form-control">
                <?php
                for ($a=2018;$a<=$now;$a++){ 
                    if($a==$year){
                        echo"<option value='$a' selected> $a </option>";
                    }else{
                        echo "<option value='$a'>$a</option>";
                    }
                }?>
            </select>
        </div>
    </div>
</form>

<div class="clear10"></div>

<script src="js/highcharts.js"></script>

<div id="containerSS" style="width:100%; height: auto; margin: 0 auto;"></div>
<script type="text/javascript">
    Highcharts.chart('containerSS', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Closer'
        },
        xAxis: {
            title: {
                text: '<?=$monthName[$month];?>',
            },
            categories: [<?= join($arrMonth, ',') ?>],
        },
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
        },
        series: [{
            name: 'Register',
            data: [<?= join($arrRegWeb, ',') ?>]
        }, {
            name: 'Paid',
            data: [<?= join($arrRegApp, ',') ?>]
        }]
    });
</script>