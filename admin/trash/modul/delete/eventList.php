<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left">Absensi</h4>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <!--<th>Image</th>-->
                <th>Title</th>
                <th style="text-align:center"></th>                                                                                
                <th>Insert Date</th>
            </tr>
        </thead>                                            
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({

        "processing": true,
        responsive: true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataEvent.php?list",
        "order": [[ 3, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            /* 
            { 
                "render": imgCol,
                "targets": 1 
            },*/
            { 
                "render": titleCol,
                "targets": 1
            },
            { 
                "render": actionCol,
                "targets": 2
            },
            { 
                "data": 4,
                "targets": 3,
                "visible" : false
            },

        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  
/*
    function imgCol(data, type, full) {

        return '<img height=50px width=100px src=<?php echo $imgNewsFolder; ?>'+full[1]+'>';

    }*/

    function titleCol(data, type, full) {

        /*if(full[6]==1){
            status = "<span class='label label-success'>Publish</span>";
        }else{
            status = "<span class='label label-info'>Draft</span>";
        }*/

        if(full[7]==1){
            status = "<span class='label label-warning'><i class='fa fa-video-camera' aria-hidden='true'></i> Live</span>";
        }else{
            status = "<span class='label label-info'>Closed</span>";
        }

        return ''+full[2]+' '+status+'';

    }

    function statusCol(data, type, full) {
        
        if(full[7]==1){
            status = "<i class='fa fa-video-camera' aria-hidden='true'></i> Live";
        }else{
            status = "Closed";
        }

        return ''+status+'';

    }

    function actionCol(data, type, full) {

        if(full[7]==1){
            status = "<a href=# onclick=closed("+full[0]+")>Closed</a>";                
            viewBtn = "<a href='#' class='setManajer' data-toggle='modal' data-target='#myModal3' data-id="+full[0]+" data-nama="+full[1]+">View</a>";
        }else{
            status = "<a href=# onclick=lived("+full[0]+")>Live</a>";
            viewBtn = "";
        }

        //viewBtn = "<a href=?event&mode=view&idEvent="+full[0]+">View</a>";        
        

        return '<div class="btn-group btn-group-xs pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-cog"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li>'+viewBtn+'</li><li>'+status+'</li><li><a href="#" onclick=running("view","'+full[0]+'"); data-toggle="modal" data-target="#myModal">Detail</a></li></ul></div>';

    }

    function lived(id){

        $.ajax({
            url: "modul/ajax/dataEvent.php?lived&id="+id,
            success: function(respon)
            {
                reload();
                success("Event akan live");       
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function closed(id){

        $.ajax({
            url: "modul/ajax/dataEvent.php?closed&id="+id,
            success: function(respon)
            {
                reload();
                success("Event telah berakhir"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });
    }

    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload( null, false );
    }

    $(document).on("click", ".setManajer", function () {
        var idEvent = $(this).data('id');
        $(".modal-body #idEvent").val( idEvent );
        $(".modal-body #area").val('');
    });

    function validasi(){
        var area        = document.getElementById('area').value;
        if (area==""){
            swal("","Pilih lokasi acara","warning",).then(function(){ document.getElementById("area").focus(); });
            return false;
        }else{
            return true;
        }

    }

    function running(par,id){
        var src = '<?=$imgNewsFolder;?>';
        $.ajax({
            url: "modul/ajax/dataNews.php?view&id="+id,
            success: function(respon)
            {
                //$('.result').html(respon);
                data = JSON.parse(respon);
                document.getElementById('id').value = id;
                document.getElementById('title').value = data.title;
                document.getElementById('content').value = data.content;
                document.getElementById('pict').src = src+data.pict;
                document.getElementById('modal-title').innerHTML = data.title;
                document.getElementById('modal-content').innerHTML = data.content;
                document.getElementsByClassName('modal-title')[0].innerHTML = "<b><i class='fa fa-eye'></i> View</b>";
              
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }
</script>

<!-- Modal  -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><i class="fa fa-map-marker"></i> Pilih Lokasi Acara</h5>
      </div>
      <div class="modal-body">
 
        <form>
            <div class="modal-body">
            <input type="hidden" name="idEvent" id="idEvent"/>
            <div class="form-group">
                <select id="area" name="area" class="form-control" class="defaultSelect">
                    <option value="" disabled selected hidden>Pilih Lokasi Acara</option>
                    <?php  
                    $combo = getCombo2('event_area','','');
                    foreach($combo as $data){
                    ?>
                        <option value="<?=$data['id'];?>"><?=$data['area'];?> - <?=implode(" ",array_slice(explode(" ",$data['address']),0,8));?>...</option>
                    <?php } ?>
                </select>
            </div>
            </div>
            <div class="modal-footer">
                <a href="#" id="lanjut" class="btn btn-primary" onclick="return validasi();">Lanjut</a>                
            </div>
        </form>

        <script type="text/javascript">
            document.getElementById("area").onclick = function() {
                var idEvent = document.getElementById("idEvent").value;
                var area = document.getElementById("area").value;
                document.getElementById("lanjut").href="https://www.starhome.co.id/admin/?event&mode=view&idEvent="+idEvent+"&area="+area;
            return false;
        };
        </script>

      </div>
    </div>
  </div>
</div>

<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class='fa fa-eye'></i> View</h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
                <input type="hidden" id="title" placeholder="title" name="title"/>
                <input type="hidden" id="content" placeholder="content" name="content"/>
                <input type="hidden" id="id" placeholder="id" name="id"/>
                <img id="pict" src="" style="width:100%;height:30%;"/>   
                <div class="clear10"></div>
                <b><span id="modal-title"></span></b>
                <div class="clear10"></div>
                <pre><span id="modal-content"></span></pre>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>