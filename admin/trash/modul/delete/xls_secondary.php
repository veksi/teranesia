<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=secondary_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>        
        <th>ID</th>
        <th>Judul</th>
        <th>Kategori</th>
        <th>Kebutuhan</th>
        <th>Harga</th>
        <th>Alamat</th>
        <th>Sertifikat</th>
        <th>Kamar Tidur</th>
        <th>Kamar Mandi</th>
        <th>Kamar Pembantu</th>
        <th>Garasi</th>
        <th>Car Port</th>
        <th>Luas Tanah</th>
        <th>Luas Bangunan</th>
        <th>Jumlah Lantai</th>
        <th>Bangunan Menghadap</th>
        <th>Informasi</th>
        <th>Sales</th>
        <th>Tgl Input</th>
        <th>Status</th>
        <th>Editor</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getList('secondary');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $data['id']; ?></td>
            <td><?php echo $data['title']; ?></td>
            <td><?php echo getCategory($data['category']); ?></td>
            <td><?php echo getNeed($data['types']); ?></td>
            <td><?php echo "Rp. ".number_format($data['price']); ?></td>
            <td><?php 

            $lokasi = explode('.',$data['wilayah_indonesia']);
            $prop = $lokasi[0];
            $kab = $lokasi[1];
            $kec = $lokasi[2];
            $kel = $lokasi[3];
  
            echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));
            echo ",";
            echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab)));
            echo ",";
            echo ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec)));
            echo ",";
            echo ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));
            
            ?></td>            
            <td><?php echo getSertifikat($data['certified']); ?></td>
            <td><?php echo getRoom($data['bedroom']); ?></td>
            <td><?php echo getRoom($data['bathroom']); ?></td>
            <td><?php echo getRoom($data['maidsroom']); ?></td>
            <td><?php echo getRoom($data['garage']); ?></td>
            <td><?php echo getRoom($data['carPort']); ?></td>
            <td><?php echo $data['surface']." m2"; ?></td>
            <td><?php echo $data['building']." m2"; ?></td>
            <td><?php echo $data['flor']; ?></td>
            <td><?php echo getFacing($data['facing']); ?></td>
            <td><?php echo $data['descript']; ?></td>
            <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName']; ?></td>
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php if($data['approved']=='1'){echo "Live";}else{ echo"Pending";}; ?></td>
            <td><?php echo getUser($data['activatedBy']); ?></td>
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
