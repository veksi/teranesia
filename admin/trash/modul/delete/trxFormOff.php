<?php if(isset($_GET['id'])){ 
    $data = editTrx('trx',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Transaction';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("idSales")[0].value = '<?php echo $data['idSales']; ?>';
        document.getElementsByName("idProspect")[0].value = '<?php echo $data['idProspect']; ?>';
        document.getElementsByName("idProduct")[0].value = '<?php echo $data['idProduct']; ?>';
        document.getElementsByName("bookingDate")[0].value = '<?php echo $data['bookingDate']; ?>';
        document.getElementsByName("stsTrx")[0].value = '<?php echo $data['stsTrx']; ?>';
        document.getElementsByName("information")[0].value = '<?php echo $data['information']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
    
    });

</script>

<?php } ?>

<form action="library/qTrx.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Transaction</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Sales</label>
        <select id="idSales" name="idSales" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select sales name</option>
            <?php  
            $combo = getComboSales('sales');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idSales]"; ?>"><?php echo $data['fullName']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Prospect</label>
        <select id="idProspect" name="idProspect" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select prospect name</option>
            <?php  
            $combo = getComboProspect('prospect');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idProspect]"; ?>"><?php echo $data['name']; ?></option>
            <?php } ?>
        </select>
    </div>
    
    <div class="form-group">
        <label>Project name</label>
        <select id="idProduct" name="idProduct" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select project name</option>
            <?php  
            $combo = getComboProject('product');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idProduct]"; ?>"><?php echo $data['title']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Booking date</label>
        <div class='input-group date' id='datetimepicker'>
            <input type='text' class="form-control" name="bookingDate" placeholder="Booking date"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label>Status</label>
        <select name="stsTrx" class="form-control">
            <option value="" disabled selected>Select status</option>
            <option value="1">Booking Fee</option>
            <option value="2">DP1</option>
            <option value="3">DP2</option>
            <option value="4">DP3</option>
            <option value="5">DP4</option>
            <option value="6">DP5</option>
            <option value="7">DP6</option>
            <option value="8">DP7</option>
            <option value="9">DP8</option>
            <option value="10">DP9</option>
            <option value="11">DP10</option>
            <option value="12">DP11</option>
            <option value="13">DP12</option>
            <option value="14">Akad</option>
            <option value="15">Lunas</option>
        </select>
    </div>

    <div class="form-group">
        <label>Information</label>
        <div class="clearfix"></div>
        <textarea name=information placeholder="Information" class="form-control" style="height:200px"><?php if(isset($_GET['id'])){ echo $data['information']; }?></textarea>
    </div>

    <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control" name="dateAdd" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    
</form>

<script>

function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }
    
    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
        });
    }));



</script>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

        var dateNow = new Date();
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>