<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Prospect Activation List</b></h4>
    <div class="pull-right">
        <a href="?trx&mode=ins" class="btn btn-success pull-right">Add</a>
    </div>    
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <th>No</th>
            <th>Sales</th>
            <th>Prospect</th>
            <th>Project</th>
            <th>Status sales</th>                
            <th>Nominal</th>
            <th>Closing progress</th>
            <th>Date</th>
            <th style="text-align:center">Action</th>
            <th>Phone</th>
        </thead>                                            
    </table>
</div>

<script type="text/javascript">
    function deleteRecord(tabel,id,idSales){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qTrx.php?trx&del&id='+id+'&idSales='+idSales;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

$(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({

        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataTrx.php?list",
        "order": [[ 7, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            
            { 
                "render": nameCol,
                "targets": 1
            },
            { 
                "render": stsSales,
                "targets": 4
            },
            { 
                "render": actionCol,
                "targets": 8 
            },
            { 
                "data": 11,
                "targets": 9 
            },

        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  

    function nameCol(data, type, full) {

        if(full[10]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[10]==1){
            status = "<span class='label label-success'>Active</span>";
        }

        return ''+full[1]+' <div class="clearfix"></div> '+status+'';

    }

    function stsSales(data, type, full) {

        if(full[4]==1){
            res = "Finder";
        }else if(full[4]==2){
            res = "Closer";
        }else if(full[4]==3){
            res = "Finder/Closer";
        }else{
            res = "-";
        }

        return ''+res+'';

    }

    function actionCol(data, type, full) {

        if(full[8]==1){
            status = "<a href=library/qTrx.php?unit&on&id="+full[0]+"&idSales="+full[9]+">Draft</a>";
        }else{
            status = "<a href=library/qTrx.php?unit&off&id="+full[0]+"&idSales="+full[9]+">Publish</a>";
        }

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>' + status + '</li><li><a href="#">View</a></li><li><a href="?trx&mode=ins&id='+full[0]+'">Edit</a></li><li><a href="#" onclick=deleteRecord("trx",'+full[0]+','+full[9]+'); >Delete</a></li></ul></div>';
    }

});
</script>

