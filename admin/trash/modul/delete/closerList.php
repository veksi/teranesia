<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Closer</b></h4>    
</div>

<div class="clear10">
<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Manajer</th>
                <th>Join Date</th>                
                <th style="text-align:center">Action</th>                                                                                
                <th>paid</th>                
            </tr>
        </thead>
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({

        "processing": true,
        responsive: true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataCloser.php?list",
        "order": [[ 4, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            { 
                "data": 8,
                "targets": 1 
            },
            { 
                "render": titleCol,
                "targets": 2 
            },
            { 
                "render": managerCol,
                "targets": 3
            },
            { 
                "data": 3,
                "targets": 4 
            },
            { 
                "render": actionCol,
                "targets":5 
            },
            { 
                "data": 9,
                "targets": 6,
                "visible":false
            },

        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  

    function titleCol(data, type, full) {

        if(full[4]==1){
            byr = "<span class='label label-success'>Paid</span>";
        }else{
            byr = "<span class='label label-info'>Unpaid</span>";
        }

        if(full[7]==1){
            cer = "<i class='fa fa-certificate' style='color:#06D755'></i>";
        }else{
            cer = "";
        }

        return ''+full[1]+' <small>('+full[6]+')</small><div class="clearfix"></div>'+full[5]+' '+byr+' '+cer;

    }

    function managerCol(data, type, full) {
        if(full[2]!=null){
            manager = ''+full[10]+' <small>('+full[2]+')</small>';
        }else{
            manager = "";
        }

        return manager;

    }

    function actionCol(data, type, full) {

        if(full[4]==1){
            bayar = "<a href=# onclick=unpaid("+full[0]+","+full[6]+")>Unpaid</a>";
            tbl     = '<li><a href="#" class="setManajer" data-toggle="modal" data-target="#myModal3" data-id="'+full[6]+'" data-nama="'+full[1]+'">Choose Manager</a></li>';
        }else{
            //bayar = "<a href=# onclick=paid("+full[0]+","+full[6]+")>Paid</a>";
            bayar = '<li><a href="#" class="payDateBtn" data-toggle="modal" data-target="#modalPay" data-id="'+full[6]+'" data-nama="'+full[1]+'">Paid</a></li>';
            tbl     = '';
        }

        if(full[7]==1){
            cert = "<a href=# onclick=notcertified("+full[0]+")>Not Certified</a>";
        }else{
            cert = "<a href=# onclick=certified("+full[0]+")>Certified</a>";
        }

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>' + bayar + '</li><li>' + cert + '</li>' + tbl + '</ul></div>';

    }

    function paid(id,idSales){
        $.ajax({
            url: "modul/ajax/dataCloser.php?paid&id="+id+"&idSales="+idSales,
            success: function(respon)
            {
                reload();
                success('Closer sudah membayar');      
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function unpaid(id,idSales){

        $.ajax({
            url: "modul/ajax/dataCloser.php?unpaid&id="+id+"&idSales="+idSales,
            success: function(respon)
            {
                reload();
                success('Closer belum membayar'); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function certified(id){

        $.ajax({
            url: "modul/ajax/dataCloser.php?certified&id="+id,
            success: function(respon)
            {
                reload();
                success('Closer sudah tersertifkasi');
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function notcertified(id){

        $.ajax({
            url: "modul/ajax/dataCloser.php?notcertified&id="+id,
            success: function(respon)
            {
                reload();
                success('Closer belum tersertifkasi');
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function success(msg){
        swal("Berhasil", msg, "success");
    }

    function error(msg){
        swal("Gagal", msg , "error");
    }

    function reload(){
        otable.ajax.reload( null, false );
    }

    $(document).on("click", ".setManajer", function () {
        var idSales = $(this).data('id');
        var nama = $(this).data('nama');
        $(".modal-body #idSales").val( idSales );
        $(".modal-body #nama").val( nama );
    });

    $(function(){
        $('#setManajer').on('click', function(e){           
            var coord = document.getElementById('coordinator').value;
            e.preventDefault();
            if(coord==""){
                error("Lengkapi data manajer");
                document.getElementById("coordinator").focus();
                return false;
            }else{
                $.ajax({
                    url: "modul/ajax/dataCloser.php",
                    type: "post",
                    data: $('#formSetManajer').serialize(),
                    success: function(data)
                    {   
                        reload();
                        success("Manajer dipilih"); 
                    }           
                });
            }
        });
    });


    $(document).on("click", ".payDateBtn", function () {
        var idSales = $(this).data('id');
        var nama = $(this).data('nama');
        $(".modal-body #idSales").val( idSales );
        $(".modal-body #nama").val( nama );
    });

    $(function(){
        $('#payDateBtn').on('click', function(e){           
            e.preventDefault();
            $.ajax({
                url: "modul/ajax/dataCloser.php",
                type: "post",
                data: $('#formPayCloser').serialize(),
                success: function(data)
                {   
                    reload();
                    success("Closer sudah membayar");                        
                }           
            });
        });
    });

   // });

</script>

<!-- Modal  -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b><i class="fa fa-user" aria-hidden="true"></i> Pilih Manajer</b></h4>
      </div>
      <div class="modal-body">

        <form id="formSetManajer">
            <div class="modal-body">
            <input type="hidden" class="form-control" name="setManajer" value="setManajer"/>
            <input type="hidden" class="form-control" name="idSales" id="idSales"/>
            <input type="text" class="form-control" name="nama" id="nama" value="" disabled/>
            <br>
            <div class="form-group">
                <select id="coordinator" name="coordinator" class="form-control" class="defaultSelect">
                    <option value="" disabled selected hidden>Pilih Manajer</option>
                    <?php  
                    $combo = getCombo('sales');
                    foreach($combo as $data){
                    ?>
                    <option value="<?php echo"$data[idSales]"; ?>"><?php echo $data['fullName']." (".$data['phone']." - ".$data['email'].")"; ?></option>
                    <?php } ?>
                </select>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="setManajer" class="btn btn-primary"> Simpan</button>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal  -->
<div class="modal fade" id="modalPay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b><i class="fa fa-user" aria-hidden="true"></i> Pilih Tanggal Bayar</b></h4>
      </div>
      <div class="modal-body">

      <form id="formPayCloser">
            <div class="modal-body">
            <input type="hidden" class="form-control" name="paid" value="paid"/>
            <input type="hidden" class="form-control" name="idSales" id="idSales"/>
            <div class="form-group">
                <label>Closer Name</label>
                <input type="text" class="form-control" name="nama" id="nama" value="" disabled/>
            </div>
            <div class="form-group">
                <label>Pay Date</label>
                <div class='input-group date' id='payDate'>
                    <input type='text' name="payDate" class="form-control" placeholder="Event date"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="payDateBtn" class="btn btn-primary"> Simpan</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    var dateNow = new Date();
    $('#payDate').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });
});
</script>