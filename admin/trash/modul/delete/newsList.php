<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Blog</b></h4>
    <a href="?news&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Title</th>
                <th>Category</th>
                <th>Date</th>
                <th>Editor</th>
                <th style="text-align:center">Action</th>                                                                                
        </tr>
        </thead>                                            
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {

    var sesi = '<?=$_SESSION['idUser'];?>';

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var otable = $('#tabelAuthor').DataTable({

            "processing": true,
            responsive: true,
            "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
            "serverSide": true,
            "ajax": "modul/ajax/dataNews.php?list",
            "order": [[ 4, "desc" ]],
            "stateSave": false, //tetap pada current page apapun yg terjadi
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // table.order = [[ 5, "desc" ]];
            },
            "fnCreatedRow": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            },

            "columnDefs": [
                
                { 
                    "render": imgCol,
                    "targets": 1 
                },
                { 
                    "render": titleCol,
                    "targets": 2 
                },
                {
                    "data": 3,
                    "targets": 3
                },
                { 
                    "render": actionCol,
                    "targets": 6 
                },

            ],

        });

        $('#tabelAuthor_filter input').unbind();
        $('#tabelAuthor_filter input').bind('keyup', function(e) {
            if(e.keyCode === 13) {
                otable.search( this.value ).draw();
            }
        });  

        function imgCol(data, type, full) {

            return '<img height=50px width=100px src=<?php echo $imgNewsFolder; ?>'+full[1]+'>';

        }

        function titleCol(data, type, full) {

            if(full[6]==1){
                status = "<span class='label label-success'>Publish</span>";
            }else{
                status = "<span class='label label-info'>Draft</span>";
            }

            return ''+full[2]+' <div class="clearfix"></div> '+status+'';

        }
/*
        function categoryCol(data, type, full) {
            
            if(full[3]==1){
                status = "News";
            }else if(full[3]==3){
                status = "Inspiration";
            }else if(full[3]==4){
                status = "Announcement";
            }else{
                status = "Unknown";
            }

            return ''+status+'';

        }*/

        function actionCol(data, type, full) {

            if(full[6]==1){
                status = "<a href=# onclick=off("+full[0]+")>Draft</a>";
            }else{
                status = "<a href=# onclick=on("+full[0]+")>Publish</a>";
            }

            if(sesi==8){
                return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>' + status + '</li><li><a href="#" onclick=running("view","'+full[0]+'"); data-toggle="modal" data-target="#myModal">View</a></li><li><a href="?news&mode=ins&id='+full[0]+'">Edit</a></li><li><a href="#" onclick=deleteRecord("article","'+full[0]+'")>Delete</a></li><li><a href="#" onclick=running("push","'+full[0]+'"); data-toggle="modal" data-target="#myModal">Send as push</a></li></ul></div>';
            }else{
                return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>' + status + '</li><li><a href="#" onclick=running("view","'+full[0]+'"); data-toggle="modal" data-target="#myModal">View</a></li><li><a href="?news&mode=ins&id='+full[0]+'">Edit</a><li><a href="#" onclick=running("push","'+full[0]+'"); data-toggle="modal" data-target="#myModal">Send as push</a></li></ul></div>';
            }

        }


        function deleteRecord(tabel,id){

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    
                    $.ajax({
                        url: 'modul/ajax/dataNews.php?del&id='+id,
                        success: function(respon)
                        {
                            reload();
                            swal("Your data has been deleted!", {
                                icon: "success",
                            });
                            
                        }       
                    });
                    
                } else {
                    //swal("Your data is safe!");
                }
            });
        }

        function running(par,id){
            document.getElementById("audience").value = '';
            var src = '<?=$imgNewsFolder;?>';

            $.ajax({
                url: "modul/ajax/dataNews.php?view&id="+id,
                success: function(respon)
                {
                    //$('.result').html(respon);
                    data = JSON.parse(respon);
                    document.getElementById('id').value = id;
                    document.getElementById('title').value = data.title;
                    document.getElementById('content').value = data.content;
                    document.getElementById('pict').src = src+data.pict;
                    document.getElementById('modal-title').innerHTML = data.title;
                    document.getElementById('modal-content').innerHTML = data.content;
                    if(par=='view'){
                        document.getElementsByClassName('modal-title')[0].innerHTML = "<b><i class='fa fa-eye'></i> View</b>";
                        document.getElementsByClassName('modal-footer')[0].style.display = "none";                    
                    }else{
                        document.getElementsByClassName('modal-title')[0].innerHTML = "<b><i class='fa fa-bell-o'></i> Push Notification</b>";
                        document.getElementsByClassName('modal-footer')[0].style.display = "block";   
                    }
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

        function on(id){

            $.ajax({
                url: "modul/ajax/dataNews.php?on&id="+id,
                success: function(respon)
                {
                    reload();
                    success();      
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

        function off(id){

            $.ajax({
                url: "modul/ajax/dataNews.php?off&id="+id,
                success: function(respon)
                {
                    reload();
                    success(); 
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

        $(function(){
            $('#send').on('click', function(e){
            
                var that = this;
                $(this).attr("disabled", true);
                setTimeout(function() { enableSubmit(that) }, 10000);

                e.preventDefault();
                $.ajax({
                    url: "library/pushLibNews.php?news",
                    type: "post",
                    data: $('form').serialize(),
                    success: function(data)
                    {   
                        success();
                    }           
                });
            });
        });

        function success(){
            swal("Berhasil", "Push notification terkirim", "success");
        }

        function reload(){
            otable.ajax.reload( null, false );
        }

   // });

</script>

<!-- Modal 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
</div>
-->

<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
                <input type="hidden" id="title" placeholder="title" name="title"/>
                <input type="hidden" id="content" placeholder="content" name="content"/>
                <input type="hidden" id="id" placeholder="id" name="id"/>
                <img id="pict" src="" style="width:100%;height:30%;"/>   
                <div class="clear10"></div>
                <b><span id="modal-title"></span></b>
                <div class="clear10"></div>
                <pre><span id="modal-content"></span></pre>
            </div>
            <div class="modal-footer">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-3">
                        <select name="audience" id="audience" class="form-control" onchange="enable(this.value)">
                            <option value="" disabled selected>Select audience</option>
                            <option value="1">All</option>
                            <option value="2">Starsales</option>
                            <option value="3">Leads Agent</option>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <button disabled type="button" id="send" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Send</button>
                    </div>
                </div>  
            </div>
        </form>

      </div>
    </div>
  </div>
</div>

<!--
<script>
$('#loader_img').show();

// main image loaded ?
$('#main_img').on('load', function(){
  // hide/remove the loading image
  $('#loader_img').hide();
});
</script>

<img id="loader_img" src="http://thinkfuture.com/wp-content/uploads/2013/10/loading_spinner.gif">
<img id="main_img" src="https://www.starhome.co.id/images/noImg.png">
-->

<style>
.loader { 
left: 900px; 
position: fixed; 
text-align: center; 
top: 100px;  
z-index: 99999;}


</style>
<script type="text/javascript">
$conv = jQuery;
$conv(document).ready(function(){
$conv('img').each(function(){
$conv(this).after('<img class="loader" src="http://thinkfuture.com/wp-content/uploads/2013/10/loading_spinner.gif" style="opacity:0.7"/>') // some preloaded "loading" image
                 .animate({  opacity: 0.25})
                 .attr('src',this.src)
                 .one('load', function() {
                    $conv(this).animate({opacity: 1}).fadeIn().next().remove();
                 });
});
});
</script>

<script type="text/javascript">
    
var btn = document.getElementById("send");
function enable(id) {
    btn.disabled = false;
}

var enableSubmit = function(ele) {
    $(ele).removeAttr("disabled");
}

</script>