<?php 
if($gadget=="hp"){
    $url = "https://wa.me/";
    $sep = "?";
}else{
    $url = "https://web.whatsapp.com/send?phone=";
    $sep = "&";
}

if(isset($_GET['pending'])){ $act1 = "class=active"; }
elseif(isset($_GET['suspend'])){ $act2 = "class=active"; }
elseif(isset($_GET['sold'])){ $act3 = "class=active"; }
elseif(isset($_GET['rented'])){ $act4 = "class=active"; }
elseif(isset($_GET['deleted'])){ $act6 = "class=active"; }
else{ $act5 = "class=active"; }
?>

<div class="form-group">
    <h4 class="pull-left"><b>Listing Secondary</b></h4>
    <!--<a href="?secondary&mode=ins" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah listing</a>-->
</div>

<div class="clear10"></div>

<ul class="nav nav-tabs">
    <li role="presentation" <?=@$act5;?> ><a href="https://www.starhome.co.id/admin/?secondary&active">Active <span class="label label-success"><?=number_format(getSecondary('approved = 1'));?></span></a></li>
    <li role="presentation" <?=@$act1;?> ><a href="https://www.starhome.co.id/admin/?secondary&pending">Pending <span class="label label-info"><?=number_format(getSecondary('approved = 0'));?></span></a></li>
    <li role="presentation" <?=@$act2;?> ><a href="https://www.starhome.co.id/admin/?secondary&suspend">Suspend <span class="label label-danger"><?=number_format(getSecondary('approved = 2'));?></span></a></li>
    <li role="presentation" <?=@$act3;?> ><a href="https://www.starhome.co.id/admin/?secondary&sold">Sold <span class="label label-primary"><?=number_format(getSecondary('approved = 1 AND sts_trx = 2'));?></span></a></li>
    <li role="presentation" <?=@$act4;?> ><a href="https://www.starhome.co.id/admin/?secondary&rented">Rented <span class="label label-warning"><?=number_format(getSecondary('approved = 1 AND sts_trx = 1'));?></span></a></li>
    <li role="presentation" <?=@$act6;?> ><a href="https://www.starhome.co.id/admin/?secondary&deleted"><i class="fa fa-trash-o" aria-hidden="true"></i> <span class="label label-danger"><?=number_format(getSecondary('deleted = 1'));?></span></a></li>
</ul>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Gambar</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Harga</th>
                <th>Lokasi</th>
                <th>Tanggal</th>
                <th style="text-align:center">Action</th>  
                <th>Sales</th>                                                      
                <th>Phone</th>                                                      
                <th>Area</th>
                <th>Code</th>   
            </tr>
        </thead>                                            
    </table>
</div>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

//$(document).ready(function() {

    var session = <?=$_SESSION['level']?>;
    var url = document.URL.split("&");

    if(url[1]=='pending'){
        var dest = "modul/ajax/dataSecondary.php?list&pending";
    }else if(url[1]=='suspend'){
        var dest = "modul/ajax/dataSecondary.php?list&suspend";
    }else if(url[1]=='sold'){
        var dest = "modul/ajax/dataSecondary.php?list&sold";
    }else if(url[1]=='rented'){
        var dest = "modul/ajax/dataSecondary.php?list&rented";
    }else if(url[1]=='deleted'){
        var dest = "modul/ajax/dataSecondary.php?list&deleted";
    }else{
        var dest = "modul/ajax/dataSecondary.php?list";
    }

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({

        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        //"ajax": "modul/ajax/dataSecondary.php?list",
        "ajax": dest,
        "order": [[ 6, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
           // table.order = [[ 5, "desc" ]];
        },
        "deferRender": true,
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            
            { 
                "render": imgCol,
                "targets"  : 1,
            },
            { 
                "render": nameCol,
                "targets"  : 2,
                "searchable": true
            },
            { 
                "render": priceCol,
                "targets"  : 4,
            },
            { 
                "render": actionCol,
                "targets": 7,
            },
            { 
                "data": 6,
                "targets": 6,
            },
            { 
                "data": 9,
                "targets": 8,
                "visible":false
            },
            { 
                "data": 10,
                "targets": 9,
                "visible":false
            },
            { 
                "data": 11,
                "targets": 10,
                "visible":false
            },
            { 
                "data": 12,
                "targets": 11,
                "visible":false
            }
            
        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  

    function imgCol(data, type, full) {
        if(full[15]==1){
            sts_trx = "<img height=60px width=60px src=../images/icon/rented.png class=sts_trx_tbl>";
        }else if(full[15]==2){
            sts_trx = "<img height=60px width=60px src=../images/icon/sold.png class=sts_trx_tbl>";
        }else{
            sts_trx = "";
        }

        //return ''+sts_trx+'<img style="height:60px;width:100px" src="https://www.starhome.co.id/admin/systemImg/loader.gif" class="img-responsive" data-echo=<?php echo $imgSecondaryFolder; ?>'+full[1]+'>';
        return ''+sts_trx+'<img style="height:60px;width:100px" src=<?php echo $imgSecondaryFolder; ?>'+full[1]+'>';
    }

    function priceCol(data, type, full) {
        return ''+full[4]+' '+full[13];
    }

    function nameCol(data, type, full) {

        if(full[7]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[7]==1){
            
            if(full[15]==1){
                status = "<span class='label label-warning'>Active</span>";
            }else if(full[15]==2){
                status = "<span class='label label-primary'>Active</span>";
            }else{
                status = "<span class='label label-success'>Active</span>";
            }

        }else if(full[7]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }else{
            status = "";
        }

        if(session==2){
            if(full[8]=='1'){
                flag = '<i class="fa fa-flag"></i>';
            }else{
                flag = '';
            }
        }else{
            flag = '';
        }

        if(full[14]>0){
            note = "<a href=# onclick=viewNote("+full[0]+"); data-toggle=modal data-target=#myModal><i class='fa fa-sticky-note-o'></i></a>";
        }else{
            note = '';
        }

        phone = "<a target=_blank href=<?php echo $url; ?>"+full[10]+">"+full[10]+"</a>";

        return ''+full[2]+' - SH'+full[0]+' <div class="clearfix"></div> '+status+' '+note+' '+flag+' <br> <small><i class="fa fa-user-circle"></i> '+full[9]+' ('+phone+')</small>';

    }

    function actionCol(data, type, full) {

        if(full[7]==1){
            status = "<a href=# onclick=suspend("+full[0]+")>Suspend</a>";
        }else{
            status = "<a href=# onclick=approve("+full[0]+")>Approve</a>";
        }
        
        if(full[8]==1){
            imgValid = "<a href=library/qSecondary.php?done&id="+full[0]+">Undone</a>";
        }else{
            imgValid = "<a href=library/qSecondary.php?undone&id="+full[0]+">Done</a>";
        }

        if(full[16]==1){
            type = "dijual";
        }else if(full[16]==2){
            type = "disewa";
        }else{
            type = "";
        }

        if(session==2){

            return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="?secondary&mode=view&id='+full[0]+'">View</a></li><li>'+imgValid+'</li>  </ul></div>';

        }else{

            listing = "<a target=_blank href=<?=$url?>"+full[10]+"<?=$sep?>text=https://www.starhome.co.id/properti/"+full[3].toLowerCase()+"/"+type+"/"+full[17]+"-sh"+full[0]+" >Send Url</a>";

            if(full[7]==1){
                tbl1 = "<li>"+listing+"</li>";
                tbl2 = "<div class='btn-group pull-right'><button type=button class=btn btn-default dropdown-toggle data-toggle=dropdown aria-haspopup=true aria-expanded=false>Status <span class=caret></span></button><ul class=dropdown-menu><li><a href=# onclick=rented("+full[0]+")>Rented</a></li><li><a href=# onclick=sold("+full[0]+")>Sold</a></li><li><a href=# onclick=available("+full[0]+")>Available</a></li>  </ul></div>";
            }else{
                tbl1 = "";
                tbl2 = "";
            }

            return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="#" onclick=view("'+full[0]+'"); data-toggle="modal" data-target="#myModal">View</a></li><li><a href=# onclick=downloadImg("'+full[0]+'");>Download</a></li><li>'+status+'</li><li><a href="#" class="note_btn" data-toggle="modal" data-target="#modal2" data-id="'+full[0]+'" data-note="'+full[14]+'" >Note</a></li>'+tbl1+'  </ul></div>'+tbl2+'';

        }
     
    }

    function downloadImg(id){

        $.ajax({
            url: "modul/ajax/dataSecondary.php?downloadImg&id="+id,
            success: function(respon)
            {
                
                var obj = JSON.parse(respon);
                len = Object.keys(obj).length;
                
                for (var i = 0; i < len; ++i) {
                    var link = document.createElement('a');
                    link.setAttribute('download',''+obj[i]+'');
                    link.style.display = 'none';
                    document.body.appendChild(link);
                    link.setAttribute('href', 'https://www.starhome.co.id/api/images/secondary/'+obj[i]+'');
                    link.click();
                }
                
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function view(id){

        $.ajax({
            url: "modul/ajax/dataSecondary.php?view&id="+id,
            success: function(respon)
            {
                $('.result').html(respon);
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function viewNote(id){

        $.ajax({
            url: "modul/ajax/dataSecondary.php?viewNote&id="+id,
            success: function(respon)
            {
                $('.result').html(respon);
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function approve(id){

        $.ajax({
            url: "modul/ajax/dataSecondary.php?approve&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing approved");      
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function suspend(id){
      
        $.ajax({
            url: "modul/ajax/dataSecondary.php?suspend&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing suspended"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function rented(id){
      
        $.ajax({
            url: "modul/ajax/dataSecondary.php?rented&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing rented"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function sold(id){
        
        $.ajax({
            url: "modul/ajax/dataSecondary.php?sold&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing sold"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function available(id){
        
        $.ajax({
            url: "modul/ajax/dataSecondary.php?available&id="+id,
            success: function(respon)
            {
                reload();
                success("Listing available"); 
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    $(function(){
        $('#saveNote').on('click', function(e){
            e.preventDefault();            
            $.ajax({
                url: "modul/ajax/dataSecondary.php?note",
                type: "post",
                data: $('#contoh-form').serialize(),
                success: function(data)
                {   
                    $("#note").val('');
                    $('#modal2').modal('toggle');
                    reload();
                    success("Note saved");     
                }           
            });
        });
    });


    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload( null, false );
    }

    $(document).on( "click", '.note_btn',function(e) {
        var id = $(this).data('id');
        $("#id").val(id);
    });


//});

</script>

<!-- Modal  -->
<div class="modal fade sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
                <h4 class="modal-title" id="modal2Label"><b><i class='fa fa-eye'></i> View</b></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
</div>


<!-- Modal  -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2Label">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b><i class="fa fa-sticky-note-o"></i> Note</b></h4>
            </div>
            <div class="modal-body">
                <form id="contoh-form">
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id"/>
                        <textarea id="note" name="note" class="form-control" rows="3" placeholder="Note"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="saveNote" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<style>
.tooltipx {
    position: relative;
    display: inline-block;
}

.tooltipx .tooltiptextx {
    visibility: hidden;
    width: 140px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -75px;
    opacity: 0;
    transition: opacity 0.3s;
}

.tooltipx .tooltiptextx::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.tooltipx:hover .tooltiptextx {
    visibility: visible;
    opacity: 1;
    
}
</style>

<script type="text/javascript">

    function myFunctionx() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        document.execCommand("copy");
    
    var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied! ";
    }

</script>

<script>
function copyClipboard() {
  var elm = document.getElementById("divClipboard");
  // for Internet Explorer

  if(document.body.createTextRange) {
    var range = document.body.createTextRange();
    range.moveToElementText(elm);
    range.select();
    document.execCommand("Copy");
    //alert("Copied div content to clipboard");
  }
  else if(window.getSelection) {
    // other browsers

    var selection = window.getSelection();
    var range = document.createRange();
    range.selectNodeContents(elm);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand("Copy");
    //alert("Copied div content to clipboard");
  }
}
</script>