<?php if(isset($_GET['id'])){ 
    $data = editNews('article',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Blog';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
        document.getElementsByName("keyword")[0].value = '<?php echo $data['keyword']; ?>';
        document.getElementsByName("category")[0].value = '<?php echo $data['category']; ?>';
        document.getElementsByName("imgDB")[0].value = '<?php echo $data['pict']; ?>';
    });

</script>

<?php } ?>

<form action="library/qNews.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>
    <input type=hidden name='imgDB'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Principal</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>
    <div class="form-group">
    <img id="companyLogo2" data-type="editable1" <?php if(!empty(@$data['pict'])){?> src="<?php echo $imgNewsFolder.$data['pict']; ?> <?php }else{?> src="/api_dev/images/addimg.png" <?php } ?>"/>    
    </div>
    
    <div class="form-group">
        <label>Office Name</label>
        <input type="text" name="office" class="form-control" placeholder="Office Name">
    </div>

    <div class="form-group">
        <label>Principal</label>
        <input type="text" name="principal" class="form-control" placeholder="Principal">
    </div>

    <div class="form-group">
        <label>Customer Care</label>
        <input type="text" name="phone" class="form-control" placeholder="Customer Care">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control" placeholder="Email">
    </div>

    <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="address" class="form-control" placeholder="Alamat">
    </div>

    <div class="form-group">
        <label>Kategori</label>
        <select name="category" class="form-control">
            <option value="" disabled selected>Pilih kategori</option>
            <option value="1">By Area</option>
            <option value="2">By Office</option>          
        </select>
    </div>

</form>

<script>

    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }
    
    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
        });
    }));



</script>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>

