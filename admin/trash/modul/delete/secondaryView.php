<?php 

$data = getDetail('secondary',$_GET['id']); 
$dataSales = getSales($data['idSales']);

?>

<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Detail Properti</b></h4>
    <div class="pull-right">
          <button type="button" onclick="goBack()" class="btn btn-info">Back</button>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-sm-5">
    <div class="card text-center">
      <div class="card-block">
        <h3 class="card-title"></h3>

        <img id="companyLogo1" data-type="editable1" <?php if(!empty(@$data['photo1'])){?> src="<?php echo $imgSecondaryFolder.$data['photo1']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />

        <h3>
          <img id="companyLogo2" data-type="editable2" <?php if(!empty(@$data['photo2'])){?> src="<?php echo $imgSecondaryFolder.$data['photo2']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo3" data-type="editable3" <?php if(!empty(@$data['photo3'])){?> src="<?php echo $imgSecondaryFolder.$data['photo3']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo4" data-type="editable4" <?php if(!empty(@$data['photo4'])){?> src="<?php echo $imgSecondaryFolder.$data['photo4']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo5" data-type="editable5" <?php if(!empty(@$data['photo5'])){?> src="<?php echo $imgSecondaryFolder.$data['photo5']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo6" data-type="editable6" <?php if(!empty(@$data['photo6'])){?> src="<?php echo $imgSecondaryFolder.$data['photo6']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo7" data-type="editable7" <?php if(!empty(@$data['photo7'])){?> src="<?php echo $imgSecondaryFolder.$data['photo7']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
          <img id="companyLogo8" data-type="editable8" <?php if(!empty(@$data['photo8'])){?> src="<?php echo $imgSecondaryFolder.$data['photo8']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
        </h3>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-block">
        <h3 class="card-title"><?php echo $data['title']; ?></h3>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Sales</label>
          <?php echo $dataSales['fullName']; ?> - <?php echo $dataSales['phone']; ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Kategori properti</label>
          <?php echo getCategory($data['category']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Kebutuhan</label>
          <?php echo getNeed($data['types']); ?>
        </div>

        <div class="form-group row">
          <label class="col-sm-5 col-form-label">Lokasi</label>
          <?php 
          $lokasi = explode('.',$data['wilayah_indonesia']);
          $prop = $lokasi[0];
          $kab = $lokasi[1];
          $kec = $lokasi[2];
          $kel = $lokasi[3];

          echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));
          echo ",";
          echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab)));
          echo ",";
          echo ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec)));
          echo ",";
          echo ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));
          ?>
        </div>

        

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Harga</label>
            <?php echo "Rp. ".number_format($data['price']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Alamat</label>
            <?php echo $data['addres']; ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Sertifikat</label>
            <?php echo getSertifikat($data['certified']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Kamar Tidur</label>
            <?php echo getRoom($data['bedroom']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Kamar Mandi</label>
            <?php echo getRoom($data['bathroom']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Kamar Pembantu</label>
            <?php echo getRoom($data['maidsroom']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Garasi</label>
            <?php echo getRoom($data['garage']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Car Port</label>
            <?php echo getRoom($data['carPort']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Luas Tanah</label>
            <?php echo $data['surface']." m2"; ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Luas Bangunan</label>
            <?php echo $data['building']." m2"; ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Lantai</label>
            <?php echo $data['flor']; ?>
        </div>
        
        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Bangunan Menghadap</label>
            <?php echo getFacing($data['facing']); ?>
        </div>

        <div class="form-group row">
          <label  class="col-sm-5 col-form-label">Informasi Tambahan</label>
            <?php echo $data['descript']; ?>
        </div>

      </div>
    </div>
  </div>
</div>

