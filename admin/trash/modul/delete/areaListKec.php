<?php 
$prop = $_GET['prop']; 
$kab = $_GET['kab']; 
?>

<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Wilayah Indonesia</b></h4>
    <a href="#" data-toggle='modal' data-target='#myModal' class="btn btn-success pull-right">Add Kecamatan</a>
</div>

<div class="clear10"></div>

<div class="alert alert-info" role="alert">
  Propinsi <?php echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));?>
  <i class="fa fa-long-arrow-right"></i>
  <?php echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab))); ?>
</div>

<div class="clearfix"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Kecamatan</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getListAreaKec('wilayah_indonesia', $prop, $kab );
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $data['lokasi_kode']; ?></td>
                <td><?php echo ucwords(strtolower($data['lokasi_nama'])); ?> <?php if($data['valid']==1){echo "<i class='fa fa-check'></i>";} ?></td>
                <td style="text-align: center;">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><?php echo $status; ?></li>
                            <li><a href="?area&listKel&mode=view&prop=<?php echo $data['lokasi_propinsi']; ?>&kab=<?php echo $data['lokasi_kabupatenkota']; ?>&kec=<?php echo $data['lokasi_kecamatan']; ?>">View Kelurahan</a></li>
                            <li><a href="#" data-toggle='modal' data-target='#myModal' data-title="<?php echo $data['lokasi_nama'];?>" data-id='<?php echo $data['lokasi_ID'];?>' >Edit</a></li>
                            <li><a href="#" onclick="deleteRecord('<?php echo $data['lokasi_propinsi']; ?>','<?php echo $data['lokasi_kabupatenkota']; ?>','<?php echo $data['lokasi_kecamatan']; ?>','<?php echo $data['lokasi_kelurahan']; ?>');" >Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->


<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Add Kecamatan</b></h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
                <div class="form-group">
                    <label>Kecamatan Name</label>
                    <input type="hidden" name="kode_propinsi" class="form-control" value="<?=$_GET['prop']?>">
                    <input type="hidden" name="kode_kabupaten" class="form-control" value="<?=$_GET['kab']?>">
                    <input type="hidden" name="lokasi_ID" class="form-control" id="lokasi_ID">
                    <input type="text" name="nama_kecamatan" class="form-control" id="nama_kecamatan" placeholder="Kecamatan name">
                </div>
            </div>
            <div class="modal-footer">
                <button name=submit type="submit" class="btn btn-success" data-dismiss="modal" id="save">Save</button>
                <button type="button" data-dismiss="modal" class="btn btn-info">Cancel</button>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

    function deleteRecord(kode_prop,kode_kab,kode_kec,kode_kel){

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "library/ajaxQuery.php?deleteWilayah&kode_prop="+kode_prop+"&kode_kab="+kode_kab+"&kode_kec="+kode_kec+"&kode_kel="+kode_kel,
                    success: function(data)
                    {   
                        swal("Your data has been deleted!", {
                            icon: "success",
                        });     
                        location.reload();             
                    }           
                });


            }
        });
    }

    $(function(){
        $('#save').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: "library/ajaxQuery.php?kecamatan",
                type: "post",
                data: $('form').serialize(),
                success: function(data)
                {   
                    swal("Berhasil", "Kecamatan berhasil ditambahkan", "success");       
                    location.reload();             
                }           
            });
        });
    });

    var ATTRIBUTES = ['title', 'id'];
    $('[data-toggle="modal"]').on('click', function (e) {
        var $target = $(e.target);
        var modalSelector = $target.data('target');
        ATTRIBUTES.forEach(function (attributeName) {
            var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
            var dataValue = $target.data(attributeName);

            $('#nama_kecamatan').val($target.data('title'));
            $('#lokasi_ID').val($target.data('id'));       

            $modalAttribute.text(dataValue || '');
        });
    });

</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

