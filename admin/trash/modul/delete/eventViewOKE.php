<?php
if($gadget=="hp"){
    $url = "https://wa.me/";
}else{
    $url = "https://web.whatsapp.com/send?phone=";
}
$data = editNews('article',$_GET['idEvent']);
?>

<div class="form-group">
    <h4 class="pull-left"><b>Event</b></h4>
</div>

<div class="clearfix"></div>
<div class="form-group row">
    <div class="col-lg-2 col-md-3">
        <img src="<?php echo $imgNewsFolder.$data['pict']; ?>" width="100%"/>
    </div>
    <div class="col-lg-6">
        <h3><?=$data['title'];?></h3>
    </div>
</div>

<input type="hidden" id="browser" value="<?=$browser;?>">

<?php if(isset($_GET['on'])){ ?>

    <div class="navbar-form navbar-right">
        <div class="form-group">
            <input id="image-url" type="text" class="form-control" placeholder="Image url">
            <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
            <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
            <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span></button>
            <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
            <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
        </div>
        <select class="form-control" id="camera-select"></select>
        <input type="text" id="scanned">
        <input type="text" id="idSales" value="<?=$_GET['idSales'];?>">
        <input type="text" id="namaSales" value="<?=getSales($_GET['idSales'])['fullName'];?>">
        <input type="text" id="idEvent" value="<?=$_GET['idEvent'];?>">
        <img width="320" height="240" id="scanned-img" src="">
    </div>

    <canvas width="auto" height="320" id="webcodecam-canvas"></canvas>
    <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
    <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
    <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
    <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>

    <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/filereader.js"></script>
    <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/qrcodelib.js"></script>
    <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/webcodecamjs.js"></script>
    <script type="text/javascript" src="https://www.starhome.co.id/admin/scanner/js/main.js"></script>
    
<?php } ?>



<ul class="list-group">
    <li class="list-group-item">
        <a data-toggle="collapse" href="#collapse1"><h4 class="panel-title"><span class="fa fa-star" aria-hidden="true"></span><span class="textTab"> Starsales</span><div class="pull-right"><i id="arrow" class="fa fa-chevron-down"></i></div></h4></a>
        <div id="collapse1" class="panel-collapse collapse">
            <br>
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th style="text-align:center">Action</th>               
                            <th>email</th>
                            <th>phone</th>
                            <th>nama</th>
                        </tr>
                    </thead>                                            
                </table>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <a data-toggle="collapse" href="#collapse2"><h4 class="panel-title"><span class="fa fa-check-square-o" aria-hidden="true"></span><span class="textTab"> Kehadiran</span><div class="pull-right"><i id="arrow" class="fa fa-chevron-up"></i></div></h4></a>
        <div id="collapse2" class="panel-collapse collapse in">
        <br>
            <div class="box-body table-responsive">
                <table id="present" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Date</th>
                            <th style="text-align:center">Action</th>                                                                                
                            <th>email</th>
                            <th>phone</th>
                            <th>nama</th>
                        </tr>
                    </thead>                                            
                </table>
            </div>
        </div>
    </li>
</ul>

<script>
$('a').click( function(e) { 
//$('#click_advance').click(function() {
    $("i", this).toggleClass("fa-chevron-up fa-chevron-down");
    //$("#arrow").toggleClass("fa fa-chevron-up fa fa-chevron-down");
});

$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });
});

</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

    var idEvent = '<?=$_GET['idEvent'];?>';
//$(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({
        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataEventView.php?list&idEvent="+idEvent,
        "order": [[ 6, "asc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
        },
        "bInfo" : false,
        "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        //"lengthMenu": [2, 10, 20, 50, 100, 200, 500],
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            { 
                "render": nameCol,
                "targets": 1,
            },
            { 
                "render": phoneCol,
                "targets": 2,
            },
            { 
                "render": actionCol,
                "targets": 3,
            },
            { 
                "data": 9,
                "targets": 4,
                "visible":false
            },
            { 
                "data": 4,
                "targets": 5,
                "visible":false
            },
            { 
                "data": 2,
                "targets": 6,
                "visible":false
            },
           

        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function cekCol(data, type, full) {
        return '<input type=checkbox class=check-item name=idSales[] value='+full[0]+' >';
    }

    function nameCol(data, type, full) {

        if(full[8]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[8]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[8]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        return ''+full[2]+' <small>('+full[0]+')</small> <div class="clearfix"></div> '+status+' <div class="clearfix"></div> '+full[9];

    }

    function actionCol(data, type, full) {
        return '<a href=<?=$currentUrl;?>&idSales='+full[1]+'&on class="btn btn-default">Hadir</a>';
    }

    function phoneCol(data, type, full) {
        phone = "<a href=<?php echo $url; ?>"+full[4]+">"+full[4]+"</a>";
        return ''+phone+'';
    }
//});
/*
    function present(id,idEvent){
  
        $.ajax({
            url: "modul/ajax/dataEventView.php?present&id="+id+"&idEvent="+idEvent,
            success: function(respon)
            {
                reload();
                success("Sales hadir");      
                
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function presentPlan(id,idEvent){

        $.ajax({
            url: "modul/ajax/dataEventView.php?presentPlan&id="+id+"&idEvent="+idEvent,
            success: function(respon)
            {
                reload();
                success("Sales berencana hadir");      
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }*/

    // present table
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable2 = $('#present').DataTable({
        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataPresent.php?list&idEvent="+idEvent,
        "order": [[ 3, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
        },
        "bInfo" : false,
        "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        //"lengthMenu": [2, 10, 20, 50, 100, 200, 500],
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            { 
                "render": nameCol2,
                "targets": 1,
            },
            { 
                "render": phoneCol,
                "targets": 2,
            },
            { 
                "data": 5,
                "targets": 3,
            },
            { 
                "render": actionCol2,
                "targets": 4,
            },
            { 
                "data": 9,
                "targets": 5,
                "visible":false
            },
            { 
                "data": 4,
                "targets": 6,
                "visible":false
            },
            { 
                "data": 2,
                "targets": 7,
                "visible":false
            },
           

        ],

    });

    $('#present_filter input').unbind();
    $('#present_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable2.search( this.value ).draw();
        }
    });

    function nameCol2(data, type, full) {

        if(full[8]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[8]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[8]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        return ''+full[2]+' <small>('+full[0]+')</small> <div class="clearfix"></div> '+status+' <div class="clearfix"></div> '+full[9];

    }

    function actionCol2(data, type, full2) {
        return '<a class="btn btn-default" href=# onclick=notPresent("'+full2[1]+'","'+idEvent+'")><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
    }

    function notPresent(id,idEvent){
        $.ajax({
            url: 'modul/ajax/dataPresent.php?del&id='+id+"&idEvent="+idEvent,
            success: function(respon)
            {
                reload();
                success("Sales tidak hadir");      
            },
            error: function(err) {
                console.log(err);
            }          
        });
    }

    function reload(){
        otable.ajax.reload( null, false );
        otable2.ajax.reload( null, false );
    }

    function success(msg){
        swal("Done", msg , "success");
    }

</script>