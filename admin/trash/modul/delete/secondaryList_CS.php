<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Listing Secodary</b></h4>
    <!--<a href="?secondary&mode=ins" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah listing</a>-->
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Gambar</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Harga</th>
                <th>Lokasi</th>
                <th>Tanggal</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('secondary');
            $no=1;
            if(!empty($list)){

            foreach($list as $data){

                $dataSales = getSales($data['idSales']);

                if($data['approved']==1){
                    $status = "<a href=library/qSecondary.php?on&id=".$data['id'].">Suspend</a>";
                }else{
                    $status = "<a href=library/qSecondary.php?off&id=".$data['id'].">Approved</a>";
                }
                
                if($data['imgValid']==1){
                    $imgValid = "<a href=library/qSecondary.php?done&id=".$data['id'].">Undone</a>";
                }else{
                    $imgValid = "<a href=library/qSecondary.php?undone&id=".$data['id'].">Done</a>";
                }
                
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td style="text-align: center;">
                    <img height="50px" width="100px" <?php if(!empty(@$data['photo1'])){?> src="<?php echo $imgSecondaryFolder.$data['photo1']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?>/>      
                </td>
                <td>
                    <?php echo $data['title']; ?><?php echo " - SH".$data['id']; ?>
                    
                    <?php if($data['approved']=='0'){ ?>
                        <span class="label label-info">Pending</span>
                    <?php }elseif($data['approved']=='1'){ ?>
                        <span class="label label-success">Active</span>
                    <?php }elseif($data['approved']=='2'){ ?>
                        <span class="label label-danger">Suspend</span>    
                    <?php } ?>

                    <?php if($_SESSION['level']=="2"){ ?>
                        <?php if($data['imgValid']=='1'){ ?>
                            <i class="fa fa-flag"></i>
                        <?php } ?>
                    <?php } ?>
                    <div class="clear10"></div>
                    <small><i class="fa fa-user-circle"></i> <?php echo $dataSales['fullName']; ?> (<?php echo $dataSales['phone']; ?>)</small>
                </td>
                <td>
                    <?php echo getCategory($data['category']) ?>
                </td>
                <td>
                    <?php echo "Rp. ".number_format($data['price']) ?>
                </td>
                <td>
                    <?php 
                    $lokasi = explode('.',$data['wilayah_indonesia']);
                    $prop = $lokasi[0];
                    $kab = $lokasi[1];
                    $kec = $lokasi[2];
                    $kel = $lokasi[3];

                    echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));
                    echo ",";
                    echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab)));
                    echo ",";
                    echo ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec)));
                    echo ",";
                    echo ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));
                    
                    ?>
                </td>
                <td><?php echo $data['insertDate']; ?></td>
                <td style="text-align: center;">
                
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <?php if($_SESSION['level']=="2"){ ?>
                            <ul class="dropdown-menu">   
                                <li><a href="?secondary&mode=view&id=<?php echo $data['id']; ?>">View</a></li>                             
                                <li><?php echo $imgValid; ?></li>
                            </ul>
                        <?php }else{ ?>
                            <ul class="dropdown-menu">
                                <li><a href="?secondary&mode=view&id=<?php echo $data['id']; ?>">View</a></li>
                                <li><?php echo $status; ?></li>
                            </ul>
                        <?php } ?>
                        
                    </div>

                </td>
            </tr>
            <?php $no++; } } ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qSecondary.php?secondary&del&id='+id;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

