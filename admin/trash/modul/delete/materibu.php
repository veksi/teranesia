<div class="container-fluid">
    <div class="box-body table-responsive px-2">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Pelatihan</th>
                    <th style="border-bottom:none;font-weight:600">Materi</th>
                    <th style="border-bottom:none;font-weight:600;width:80px">Waktu</th>
                    <th style="border-bottom:none;font-weight:600">Jadwal</th>
                    <th style="border-bottom:none;font-weight:600"></th>
                    <th style="border-bottom:none;font-weight:600"></th>
                    <th style="border-bottom:none;font-weight:600"></th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="<?=$adminPath;?>plugin/dataTable/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
   
   $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({
        //"processing": true,
        //"language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        //"oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax":{
            url: '<?=$instrukturPath;?>modul/ajax/data_materi.php',
            headers: { Authorization: '<?=$_SESSION['token_instruktur']?>' },
        },
        "order": [[ 6, "desc" ], [ 7, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": waktuCol,
                "targets": 3
            },            
            {
                "data": 5,
                "targets": 4
            },
            {
                "render": actionCol,
                "targets": 5
            },
            {
                "data": 6,
                "targets": 6,
                "visible" :false
            },
            {
                "data": 7,
                "targets": 7,
                "visible" :false
            },
        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function waktuCol(data, type, full) {
        return ''+full[3]+' - '+full[4];
    }

    function sortCol(data, type, full) {
        return ''+full[6]+' '+full[7];
    }

    function actionCol(data, type, full) {

        if(full[2]==1){
            link = '<a class="dropdown-item" href=<?=$instrukturPath?>pelatihan-saya/'+full[4]+' >Detail</a>';
        }else{
            link = '';
        }

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li>'+link+'</li><li><a class="dropdown-item" href=<?=$instrukturPath?>materi-saya/absensi-'+full[0]+' >Absensi</a></li><li><a class="dropdown-item" href=<?=$instrukturPath?>materi-saya/peserta-'+full[0]+' >Peserta</a></li><li><a class="dropdown-item" href=<?=$instrukturPath?>materi-saya/essay-'+full[0]+' >Essay</a></li></ul></div>';

    }

    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload();
    }

</script>