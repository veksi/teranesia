<?php if(isset($_GET['id'])){
    $data = edit('product',$_GET['id']);
?>

    <script>

        $(document).ready(function() { 
            document.getElementsByName("mode")[0].value = 'upd';
            document.getElementById("model").innerHTML = 'Edit Property';
            document.getElementsByName("id")[0].value = '<?php echo $data['id']; ?>';

            document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
            document.getElementsByName("dateAdd")[0].value = '<?php echo $data['dateAdd']; ?>';
            //document.getElementsByName("keyword")[0].value = '<?php echo $data['keyword']; ?>';
            document.getElementsByName("category")[0].value = '<?php echo $data['category']; ?>';
            document.getElementsByName("types")[0].value = '<?php echo $data['types']; ?>';
            document.getElementsByName("price")[0].value = '<?php echo $data['price']; ?>';
            document.getElementsByName("city")[0].value = '<?php echo $data['city']; ?>';
            document.getElementsByName("addres")[0].value = '<?php echo $data['addres']; ?>';
            document.getElementsByName("description")[0].value = '<?php echo $data['description']; ?>';
            document.getElementsByName("certificate")[0].value = '<?php echo $data['certificate']; ?>';
            document.getElementsByName("bedroom")[0].value = '<?php echo $data['bedroom']; ?>';
            document.getElementsByName("bathroom")[0].value = '<?php echo $data['bathroom']; ?>';
            document.getElementsByName("maidsroom")[0].value = '<?php echo $data['maidsroom']; ?>';
            document.getElementsByName("garage")[0].value = '<?php echo $data['garage']; ?>';
            document.getElementsByName("carPort")[0].value = '<?php echo $data['carPort']; ?>';
            document.getElementsByName("surface")[0].value = '<?php echo $data['surface']; ?>';
            document.getElementsByName("building")[0].value = '<?php echo $data['building']; ?>';
            document.getElementsByName("floor")[0].value = '<?php echo $data['floor']; ?>';
            document.getElementsByName("facing")[0].value = '<?php echo $data['facing']; ?>';
            document.getElementsByName("power")[0].value = '<?php echo $data['power']; ?>';
            document.getElementsByName("phone")[0].value = '<?php echo $data['phone']; ?>';
            document.getElementsByName("genset")[0].value = '<?php echo $data['genset']; ?>';
            document.getElementsByName("water")[0].value = '<?php echo $data['water']; ?>';
            document.getElementsByName("ac")[0].value = '<?php echo $data['ac']; ?>';
            document.getElementsByName("heater")[0].value = '<?php echo $data['heater']; ?>';
            document.getElementsByName("swimming")[0].value = '<?php echo $data['swimming']; ?>';
            document.getElementsByName("internet")[0].value = '<?php echo $data['internet']; ?>';
            document.getElementsByName("carIn")[0].value = '<?php echo $data['carIn']; ?>';
            document.getElementsByName("freeFlood")[0].value = '<?php echo $data['freeFlood']; ?>';
        });

    </script>

<?php } ?>


<h2 class="text-center"><div id="model">Form Listing Secondary</div></h2>
<form action="query/qProduct.php" method="post" enctype="multipart/form-data">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <label>Judul</label>
        <input type="text" name="title" class="form-control" placeholder="Judul">
    </div>

    <div class="form-group">
        <label>Tanggal Input</label>
        <div class='input-group date' id='datetimepicker'>                            
            <input type='text' name="dateAdd" class="form-control" placeholder="Tanggal Input"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>  
<!--
    <div class="form-group">
        <label>Keyword</label>
        <input type="text" name="keyword" class="form-control" placeholder="Keyword : separate with commas (,)">
    </div>-->

    <div class="form-group">
        <label>Kategori</label>
        <select name="category" class="form-control">
            <option value="" disabled selected>Pilih kategori</option>
            <option value="1">Rumah</option>
            <option value="2">Tanah</option>
            <option value="3">Apartemen</option>
            <option value="4">Kantor</option>
            <option value="5">Vila</option>
            <option value="6">Pabrik</option>
            <option value="7">Bangunan</option>
            <option value="8">Gudang</option>
        </select>
    </div>

    <div class="form-group">
        <label>Tipe</label>
        <select name="types" class="form-control">
            <option value="" disabled selected>Pilih tipe</option>
            <option value="1">Jual</option>
            <option value="2">Sewa</option>
        </select>
    </div>

    <div class="form-group">
        <label>Gambar</label>
        <div class="clear5"></div>
        <img id="companyLogo1" data-type="editable1" height="250px" width="250px" src="../images/product/<?php echo $data['photo1']; ?>"/>
        <img id="companyLogo2" data-type="editable2" height="250px" width="250px" src="../images/product/<?php echo $data['photo2']; ?>"/>
        <img id="companyLogo3" data-type="editable3" height="250px" width="250px" src="../images/product/<?php echo $data['photo3']; ?>"/>
        <img id="companyLogo4" data-type="editable4" height="250px" width="250px" src="../images/product/<?php echo $data['photo4']; ?>"/>
        <img id="companyLogo5" data-type="editable5" height="250px" width="250px" src="../images/product/<?php echo $data['photo5']; ?>"/>
        <img id="companyLogo6" data-type="editable6" height="250px" width="250px" src="../images/product/<?php echo $data['photo6']; ?>"/>
        <img id="companyLogo7" data-type="editable7" height="250px" width="250px" src="../images/product/<?php echo $data['photo7']; ?>"/>
        <img id="companyLogo8" data-type="editable8" height="250px" width="250px" src="../images/product/<?php echo $data['photo8']; ?>"/>
        
    </div>
    <div class="clear10"></div>

    <div class="form-group">
        <label>Harga</label>
        <input type="text" name="price" id="price" class="form-control" placeholder="Harga" onChange="convert(this.value)" maxlength="30">
    </div>

    <div class="form-group">
        <label>Kota</label>
        <select name="city" class="form-control">
            <option value="" disabled selected>Pilih kota</option>
            <option value="1">Bekasi</option>
            <option value="2">Depok</option>
            <option value="3">Jakarta Pusat</option>
            <option value="4">Jakarta Timur</option>
            <option value="5">Jakarta Barat</option>
            <option value="6">Jakarta Utara</option>
            <option value="7">Jakarta Selatan</option>
            <option value="8">Tangerang</option>
        </select>
    </div>

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="addres" class="form-control"  rows="3" placeholder="Alamat"></textarea>
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="description" class="form-control"  rows="3" placeholder="Deskripsi"></textarea>
    </div>

    <div class="form-group">
        <label>Sertifikat</label>
        <select name="certificate" class="form-control">
            <option value="" disabled selected>Pilih sertifikat</option>
            <option value="1">Hak Milik</option>
            <option value="2">Hak Guna Bangunan</option>
            <option value="3">Hak Guna Usaha</option>
            <option value="4">Akta Jual Beli (AJB)</option>
            <option value="5">Perjanjian Pengikatan Jual Beli</option>
        </select>
    </div>
<!--
    <div class="form-group">
        <label>Bedroom</label>
        <select name="bedroom" class="form-control">
            <option value="" disabled selected>Number of bedroom</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="5">5</option>
        </select>
    </div>-->

    <div class="form-group">
        <label>Kamar Tidur</label>
        <input type="text" name="bedroom" id="bedroom" class="form-control" placeholder="Kamar Tidur" onChange="convert(this.value)" maxlength="30">
    </div>
<!--
    <div class="form-group">
        <label>Bathroom</label>
        <select name="bathroom" class="form-control" >
            <option value="" disabled selected>Number of bathroom</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
    </div>-->

    <div class="form-group">
        <label>Kamar Mandi</label>
        <input type="text" name="bathroom" id="bathroom" class="form-control" placeholder="Kamar Mandi" onChange="convert(this.value)" maxlength="30">
    </div>

    <div class="form-group">
        <label>Kamar Pembantu</label>
        <select name="maidsroom" class="form-control" >
            <option value="" disabled selected>Kamar Pembantu</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">More</option>
        </select>
    </div>

    <div class="form-group">
        <label>Garasi</label>
        <select name="garage" class="form-control" >
            <option value="" disabled selected>Garasi</option>
            <option value="1">Ada</option>
            <option value="2">Tidak Ada</option>
        </select>
    </div>

    <div class="form-group">
        <label>Carport</label>
        <select name="carPort" class="form-control" >
            <option value="" disabled selected>Carport</option>
            <option value="1">Ada</option>
            <option value="2">Tidak Ada</option>
        </select>
    </div>

    <div class="form-group">
        <label>Luas Tanah</label>
        <input type="text" name="surface" class="form-control" placeholder="Luas Tanah">
    </div>

    <div class="form-group">
        <label>Luas Bangunan</label>
        <input type="text" name="building" class="form-control" placeholder="Luas Bangunan">
    </div>
<!--
    <div class="form-group">
        <label>Floor</label>
        <select name="floor" class="form-control" >
            <option value="" disabled selected>Number of floor</option>
            <?php for($i=1;$i<=100;$i++){ ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
        </select>
    </div>-->

    <div class="form-group">
        <label>Jumlah Lantai</label>
        <input type="text" name="floor" class="form-control" placeholder="Jumlah Lantai">
    </div>

    <div class="form-group">
        <label>Bangunan Menghadap</label>
        <select name="facing" class="form-control" >
            <option value="" disabled selected>Bangunan Menghadap</option>
            <option value="1">Utara</option>
            <option value="2">Timur Laut</option>
            <option value="3">Timur</option>
            <option value="4">Tenggara</option>
            <option value="5">Selatan</option>
            <option value="6">Barat Daya</option>
            <option value="7">Barat</option>
            <option value="8">Barat Laut</option>
        </select>
    </div>
<!--
    <div class="form-group">
        <label >Phone line</label>
        <select name="phone" class="form-control" >
            <option value="" disabled selected>Number of phone line</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
    </div>

    <div class="form-group">
        <label>Power</label>
        <input type="text" name="power" class="form-control" placeholder="Power (watt)">
    </div>

    <div class="form-group">
        <label>Genset</label>
        <select name="genset" class="form-control" >
            <option value="" disabled selected>Genset</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Water sources</label>
        <select name="water" class="form-control" >
            <option value="" disabled selected>Water sources</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">AC</label>
        <select name="ac" class="form-control" >
            <option value="" disabled selected>Air Conditioner (AC)</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Water heater</label>
        <select name="heater" class="form-control" >
            <option value="" disabled selected>Water heater</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Swimming pool</label>
        <select name="swimming" class="form-control" >
            <option value="" disabled selected>Swimming pool</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Internet connection</label>
        <select name="internet" class="form-control"  >
            <option value="" disabled selected>Internet connection</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Does the car come in?</label>
        <select name="carIn" class="form-control" >
            <option value="" disabled selected>Does the car come in?</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>

    <div class="form-group">
        <label>Free flood?</label>
        <select name="freeFlood" class="form-control" >
            <option value="" disabled selected>Free flood?</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
        </select>
    </div>-->

    <div class="form-group">
        <button name=submit type="submit" class="btn btn-success">Save</button>
        <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
    </div>

</form>


<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    var dateNow = new Date();
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });
});
</script>

<script type="text/javascript">
	var dengan_rupiah = document.getElementById('price');
	dengan_rupiah.addEventListener('keyup', function(e)
	{
		dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
	});

	dengan_rupiah.addEventListener('keydown', function(event)
	{
		limitCharacter(event);
	});

	function formatRupiah(bilangan, prefix)
	{
		var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function limitCharacter(event)
	{
		key = event.which || event.keyCode;
		if ( key != 188 // Comma
			 && key != 8 // Backspace
			 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
			 && (key < 48 || key > 57) // Non digit
			 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
			) 
		{
			event.preventDefault();
			return false;
		}
	}

    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 




    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init2() {
        $("img[data-type=editable2]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo2')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init3() {
        $("img[data-type=editable3]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo3')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init4() {
        $("img[data-type=editable4]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo4')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init5() {
        $("img[data-type=editable5]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo5')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init6() {
        $("img[data-type=editable6]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo6')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init7() {
        $("img[data-type=editable7]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo7')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init8() {
        $("img[data-type=editable8]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo8')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    

    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
            init2();
            init3();
            init4();
            init5();
            init6();
            init7();
            init8();
        });
    }));

</script>


<!--

<script language="javascript" type="text/javascript">
window.onload = function () {
    var fileUpload = document.getElementById("fileupload");
    fileUpload.onchange = function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = document.getElementById("dvPreview");
            dvPreview.innerHTML = "";
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            for (var i = 0; i < fileUpload.files.length; i++) {
                var file = fileUpload.files[i];
                if (regex.test(file.name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("IMG");
                        img.height = "100";
                        img.width = "100";
                        img.src = e.target.result;
                        dvPreview.appendChild(img);
                    }
                    reader.readAsDataURL(file);
                } else {
                    alert(file.name + " is not a valid image file.");
                    dvPreview.innerHTML = "";
                    return false;
                }
            }
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    }
};
</script>

<input id="fileupload" type="file" multiple="multiple" />
<hr />
<b>Live Preview</b>
<br />
<br />
<div id="dvPreview">
</div>-->