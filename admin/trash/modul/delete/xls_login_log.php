<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=login_log_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>ID Sales</th>
        <th>Nama</th>
        <th>Date</th>
        <th>Start Time</th>
        <th>End Time</th>
        <th>Est Click</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getList('login_app_log');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>            
            <td><?php echo $data['idSales']; ?></td>
            <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName'] ?></td>
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php echo $data['insertTime']; ?></td>            
            <td><?php echo $data['lastTime']; ?></td>
            <td><?php echo $data['click']; ?></td>
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
