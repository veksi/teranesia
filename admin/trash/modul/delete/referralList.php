<?php 
if($gadget=="hp"){
    $url = "https://wa.me/";
}else{
    $url = "https://web.whatsapp.com/send?phone=";
}

if(isset($_GET['agent'])){ 
    $act2 = "class=active"; 
    if(isset($_GET['suspend'])){ $act8 = "class=active"; }
    elseif(isset($_GET['pending'])){ $act7 = "class=active"; }
    else{ $act6 = "class=active"; }
}else{ 
    $act1 = "class=active"; 
    if(isset($_GET['suspend'])){ $act5 = "class=active"; }
    elseif(isset($_GET['pending'])){ $act4 = "class=active"; }
    else{ $act3 = "class=active"; }
}

?>

<div class="clear10"></div>

<form action="library/ajaxSales.php" method="post">

    <div class="form-group">
        <h4 class="pull-left"><b>Referral</b></h4>
        <div class="pull-right">
            <!--<button name=activate type="submit" id="btn-activate" class="btn btn-success">Activate</button>
            <button name=deactivate type="submit" class="btn btn-danger">Suspend</button>-->
        </div>
    </div>

    <div class="clear10"></div>

    <ul class="nav nav-tabs">
        <li role="presentation" <?=@$act1;?> >
            <a href="https://www.starhome.co.id/admin/?sales"><i class="fa fa-star "></i> Starsales <span class="label label-success"><?=number_format(dbGetSales("office = ''"));?></span></a>
        </li>
        <li role="presentation" <?=@$act2;?> >
            <a href="https://www.starhome.co.id/admin/?sales&agent"><i class="fa fa-user-secret"></i> Agent <span class="label label-info"><?=number_format(dbGetSales("office != ''"));?></span></a>
        </li>
    </ul>
    <?php if(isset($_GET['agent'])){ ?>
        <ul class="nav nav-tabs">
            <li role="presentation" <?=@$act6;?> ><a href="https://www.starhome.co.id/admin/?sales&agent&active">Active <span class="label label-success"><?=number_format(dbGetSales(" office != '' AND active = 1 "));?></span></a></li>
            <li role="presentation" <?=@$act7;?> ><a href="https://www.starhome.co.id/admin/?sales&agent&pending">Pending <span class="label label-info"><?=number_format(dbGetSales(" office != '' AND active = 0 "));?></span></a></li>
            <li role="presentation" <?=@$act8;?> ><a href="https://www.starhome.co.id/admin/?sales&agent&suspend">Suspend <span class="label label-danger"><?=number_format(dbGetSales(" office != '' AND active = 2 "));?></span></a></li>
        </ul>
    <?php }else{ ?>
        <ul class="nav nav-tabs">
            <li role="presentation" <?=@$act3;?> ><a href="https://www.starhome.co.id/admin/?sales&active">Active <span class="label label-success"><?=number_format(dbGetSales(" office = '' AND active = 1 "));?></span></a></li>
            <li role="presentation" <?=@$act4;?> ><a href="https://www.starhome.co.id/admin/?sales&pending">Pending <span class="label label-info"><?=number_format(dbGetSales(" office = '' AND active = 0 "));?></span></a></li>
            <li role="presentation" <?=@$act5;?> ><a href="https://www.starhome.co.id/admin/?sales&suspend">Suspend <span class="label label-danger"><?=number_format(dbGetSales(" office = '' AND active = 2 "));?></span></a></li>
        </ul>
    <?php } ?>

    <div class="clear10"></div>

    <div class="box-body table-responsive">
        <table id="example1" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th class="no-sort" name="prop_ref_no" style="width:30px"><input type="checkbox" id="check-all"></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Domicile</th>
                    <th>Phone</th>
                    <th>Join Date</th>
                    <th>Last Activity</th>
                    <th>Position</th>
                    <!--<th>KTP</th>-->
                    <th style="text-align:center">Action</th>                                                                                
                    <th>email</th>
                    <th>city</th>                    
                </tr>
            </thead>                                            
        </table>
    </div>

</form>

<script>

    $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
        $("#check-all").click(function(){ // Ketika user men-cek checkbox all
            if($(this).is(":checked")){
                $(".check-item").prop("checked", true);
            }else{ // Jika checkbox all tidak diceklis
                $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
            }
        });
    });

    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
    });

</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

//$(document).ready(function() {
    
    var url = document.URL.split("&");
    if(url[1]=='agent'){
        if(url[2]=='pending'){
            var dest = "modul/ajax/dataSales.php?list&agent&pending";
        }else if(url[2]=='suspend'){
            var dest = "modul/ajax/dataSales.php?list&agent&suspend";    
        }else{
            var dest = "modul/ajax/dataSales.php?list&agent";
        }
    }else{
        if(url[1]=='pending'){
            var dest = "modul/ajax/dataSales.php?list&pending";
        }else if(url[1]=='suspend'){
            var dest = "modul/ajax/dataSales.php?list&suspend";    
        }else{
            var dest = "modul/ajax/dataSales.php?list";
        }
    }
    
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings){
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({
        
        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": dest,
        "order": [[ 5, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
           // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(1)', row).html(index);
        },
        "columnDefs": [
            { 
                "render": cekCol,
                "targets"  : 0,
                "orderable": false,
            },
            { 
                "render": nameCol,
                "targets": 2,
            },
            { 
                "render": cityCol,
                "targets": 3,
            },
            { 
                "render": phoneCol,
                "targets": 4,
            },
            { 
                "targets": 5 
            },
            { 
                "targets": 6 
            },
            { 
                "render": positionCol,
                "targets": 7 
            },
            /*{ 
                "data": 12,
                "targets": 8 
            },*/
            { 
                "render": actionCol,
                //"data": null,         
                //"targets": [8], 
                "targets": 8,
                //"defaultContent": "<button>click</button>"
            },
            { 
                "data": 9,
                "targets": 9,
                "visible":false
            },
            { 
                "data": 10,
                "targets": 10,
                "visible":false
            },

        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function cekCol(data, type, full) {
        return '<input type=checkbox class=check-item name=idSales[] value='+full[0]+' >';
    }

    function nameCol(data, type, full) {

        if(full[8]==0){
            status = "<span class='label label-info'>Pending</span>";
        }else if(full[8]==1){
            status = "<span class='label label-success'>Active</span>";
        }else if(full[8]==2){
            status = "<span class='label label-danger'>Suspend</span>";
        }

        return ''+full[2]+' <small>('+full[0]+')</small> <div class="clearfix"></div> '+status+' <div class="clearfix"></div> '+full[9];

    }

    function cityCol(data, type, full) {
        if(full[3]!=null){
            city = full[3];
        }else{
            city = full[11];
        }
        return ''+city+'';
    }

    function positionCol(data, type, full) {
        if(full[7]==1){
            selected1 = "selected";
        }else{
            selected1 = "";
        }
        
        if(full[7]==2){
            selected2 = "selected";
        }else{
            selected2 = "";
        }

        if(full[7]==3){
            selected3 = "selected";
        }else{
            selected3 = "";
        }

        return '<select name=position class=form-control onchange=updPst(this.value,'+full[0]+')><option value="" selected '+selected2+' >Not set</option><option value="1" '+selected1+'>Finder</option><option value="2" '+selected2+'>Closer</option><option value="3" '+selected3+'>Coordinator</option></select>';
    }

    function actionCol(data, type, full){
        if(full[8]==1){
            status = "<a href=# onclick=off("+full[0]+")>Suspend</a>";
        }else{
            status = "<a href=# onclick=on("+full[0]+")>Activate</a>";
        }

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="#" onclick=view("'+full[0]+'"); data-toggle="modal" data-target="#myModal2">Detail</a></li><li>'+status+'</li><li><a href="#" onclick=reset("'+full[0]+'"); data-toggle="modal" data-target="#myModal">Reset Password</a></li><li><a href="#" class="setManajer" data-toggle="modal" data-target="#myModal3" data-id="'+full[0]+'" data-nama="'+full[2]+'">Pilih Manajer</a></li></ul></div>';
    }

    function phoneCol(data, type, full) {
        phone = "<a href=<?php echo $url; ?>"+full[4]+">"+full[4]+"</a>";
        return ''+phone+'';
    }
//});

    function view(id){

        $.ajax({
            url: "modul/ajax/dataSales.php?view&id="+id,
            success: function(respon)
            {
                $('.result').html(respon);
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function on(id){

        $.ajax({
            url: "modul/ajax/dataSales.php?on&id="+id,
            success: function(respon)
            {
                reload();
                success("Account activated");      
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function off(id){

        $.ajax({
            url: "modul/ajax/dataSales.php?off&id="+id,
            success: function(respon)
            {
                reload();
                success("Account suspended");
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function reset(id){

        $.ajax({
            url: "modul/ajax/dataSales.php?reset&id="+id,
            success: function(respon)
            {
                $('.result').html(respon);
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function updPst(val,id) {

        $.ajax({
            url: "modul/ajax/dataSales.php?updPst&pst="+val+"&id="+id,
            success: function(respon)
            {
                reload();
                success("Position updated");    
            },
            error: function(err) {
                console.log(err);
            }          
        });

    }

    function success(msg){
        swal("Done", msg , "success");
    }

    function error(msg){
        swal("Gagal", msg , "error");
    }

    function reload(){
        otable.ajax.reload( null, false );
    }

    $(document).on("click", ".setManajer", function () {
        var idSales = $(this).data('id');
        var nama = $(this).data('nama');
        $(".modal-body #idSales").val( idSales );
        $(".modal-body #nama").val( nama );
    });

    $(function(){
        $('#setManajer').on('click', function(e){           
            var coord = document.getElementById('coordinator').value;
            e.preventDefault();
            if(coord==""){
                error("Lengkapi data manajer");
                document.getElementById("coordinator").focus();
                return false;
            }else{
                $.ajax({
                    url: "modul/ajax/dataSales.php",
                    type: "post",
                    data: $('form').serialize(),
                    success: function(data)
                    {   
                        success("Manajer dipilih");                        
                    }           
                });
            }
        });
    });

/*
    var ATTRIBUTES = ['title', 'content', 'id', 'pict'];
    $('[data-toggle="modal"]').on('click', function (e) {
        // convert target (e.g. the button) to jquery object
        var $target = $(e.target);
        // modal targeted by the button
        var modalSelector = $target.data('target');
        
        // iterate over each possible data-* attribute
        ATTRIBUTES.forEach(function (attributeName) {
            // retrieve the dom element corresponding to current attribute
            var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
            var dataValue = $target.data(attributeName);

            $('#title').val($target.data('title'));
            $('#content').val($target.data('content'));            
            $('#id').val($target.data('id'));
            $("#pict").attr("src", $target.data('pict'));
            
            $modalAttribute.text(dataValue || '');
        });
    });*/



</script>


<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b><i class='fa fa-key'></i> Reset Password </b></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal  -->
<div class="modal fade sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
                <h4 class="modal-title" id="modal2Label"><b><i class="fa fa-info-circle" aria-hidden="true"></i> Detail</b></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal  -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b><i class="fa fa-user" aria-hidden="true"></i> Pilih Manajer</b></h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
            <input type="hidden" class="form-control" name="setManajer" value="setManajer"/>
            <input type="hidden" class="form-control" name="idSales" id="idSales" value=""/>
            <input type="text" class="form-control" name="nama" id="nama" value="" disabled/>
            <br>
            <div class="form-group">
                <select id="coordinator" name="coordinator" class="form-control" class="defaultSelect">
                    <option value="" disabled selected hidden>Pilih Manajer</option>
                    <?php  
                    $combo = getCombo('sales');
                    foreach($combo as $data){
                    ?>
                    <option value="<?php echo"$data[idSales]"; ?>"><?php echo $data['fullName']." (".$data['phone']." - ".$data['email'].")"; ?></option>
                    <?php } ?>
                </select>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="setManajer" class="btn btn-primary"> Simpan</button>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>