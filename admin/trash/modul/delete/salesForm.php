<?php if(isset($_GET['id'])){
    $data = edit('starsales',$_GET['id']);
?>

    <script>

        $(document).ready(function() { 
            document.getElementsByName("mode")[0].value = 'upd';
            document.getElementById("model").innerHTML = 'Edit Star Sales';
            document.getElementsByName("id")[0].value = '<?php echo $data['id']; ?>';

            document.getElementsByName("fullName")[0].value = '<?php echo $data['fullname']; ?>';
            document.getElementsByName("email")[0].value = '<?php echo $data['email']; ?>';
            document.getElementsByName("phone")[0].value = '<?php echo $data['phone']; ?>';
            document.getElementsByName("workingday")[0].value = '<?php echo $data['workingday']; ?>';
            document.getElementsByName("position")[0].value = '<?php echo $data['position']; ?>';
            document.getElementsByName("interviewDay")[0].value = '<?php echo $data['interviewDay']; ?>';
            document.getElementsByName("interviewTime")[0].value = '<?php echo $data['interviewTime']; ?>';
            document.getElementsByName("description")[0].value = '<?php echo $data['description']; ?>';
            
        });

    </script>

<?php } ?>

<h2 class="text-center"><div id="model">Add Star Sales</div></h2>
<form action="query/qSales.php" method="post" enctype="multipart/form-data">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <label>Full Name</label>
        <input type="text" name="fullName" class="form-control" placeholder="Full Name">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control" placeholder="Email">
    </div>

    <div class="form-group">
        <label>Phone</label>
        <input type="text" name="phone" class="form-control" placeholder="Phone">
    </div>

    <div class="form-group">
        <label>Working Hours</label>
        <select name="workingday" class="form-control">
            <option value="" disabled selected>Choose one</option>
            <option value="1">Part Time</option>
            <option value="2">Full Time</option>
        </select>
    </div>


    <div class="form-group">
        <label>Position</label>
        <select name="position" class="form-control">
            <option value="" disabled selected>Choose one</option>
            <option value="1">Part Time</option>
            <option value="2">Full Time</option>
        </select>
    </div>

    <div class="form-group">
        <label>Interview Day</label>
        <select name="interviewDay" class="form-control">
            <option value="" disabled selected>Choose one</option>
            <option value="1">Saturday</option>
            <option value="2">Monday to Friday</option>
        </select>
    </div>

    <div class="form-group">
        <label>Interview Time</label>
        <select name="interviewTime" class="form-control">
            <option value="" disabled selected>Choose one</option>
            <option value="1">09:00</option>
            <option value="2">10:00</option>
            <option value="3">11:00</option>
            <option value="4">12:00</option>
            <option value="5">13:00</option>
        </select>
    </div>

    <div class="form-group">
        <label>Description</label>
        <textarea name="description" class="form-control"  rows="3" placeholder="Description"></textarea>
    </div>

    <div class="form-group">
        <button name=submit type="submit" class="btn btn-success">Save</button>
        <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
    </div>

</form>


<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    var dateNow = new Date();
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: dateNow
    });
});
</script>

<script type="text/javascript">
	var dengan_rupiah = document.getElementById('price');
	dengan_rupiah.addEventListener('keyup', function(e)
	{
		dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
	});

	dengan_rupiah.addEventListener('keydown', function(event)
	{
		limitCharacter(event);
	});

	function formatRupiah(bilangan, prefix)
	{
		var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function limitCharacter(event)
	{
		key = event.which || event.keyCode;
		if ( key != 188 // Comma
			 && key != 8 // Backspace
			 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
			 && (key < 48 || key > 57) // Non digit
			 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
			) 
		{
			event.preventDefault();
			return false;
		}
	}

    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 


    function init() {
        $("img[data-type=editable]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo2')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init2() {
        $("img[data-type=editable2]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo3')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init3() {
        $("img[data-type=editable3]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo4')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init();
            init1();
            init2();
            init3();
        });
    }));

</script>


<!--

<script language="javascript" type="text/javascript">
window.onload = function () {
    var fileUpload = document.getElementById("fileupload");
    fileUpload.onchange = function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = document.getElementById("dvPreview");
            dvPreview.innerHTML = "";
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            for (var i = 0; i < fileUpload.files.length; i++) {
                var file = fileUpload.files[i];
                if (regex.test(file.name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = document.createElement("IMG");
                        img.height = "100";
                        img.width = "100";
                        img.src = e.target.result;
                        dvPreview.appendChild(img);
                    }
                    reader.readAsDataURL(file);
                } else {
                    alert(file.name + " is not a valid image file.");
                    dvPreview.innerHTML = "";
                    return false;
                }
            }
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    }
};
</script>

<input id="fileupload" type="file" multiple="multiple" />
<hr />
<b>Live Preview</b>
<br />
<br />
<div id="dvPreview">
</div>-->