<?php if(empty($_POST['email'])){
    header("Location: {$_SERVER["HTTP_REFERER"]}");
}else{ ?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Kode OTP</h3></div>
                                    <div class="card-body">
                                        <div class="small mb-3 text-muted">Masukkan kode OTP yang anda terima.
                                        </div>
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="otp_cek">
                                            <input type="hidden" name="email" value="<?=$_POST['email']?>">                                                
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="one" name="one" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="two" name="two" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="three" name="three" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="four" name="four" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="five" name="five" type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim"></div>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

<?php } ?>