<?php if(isset($_GET['id'])){ 
    $data = editNews('article',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit News';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['dateAdd']; ?>';
        document.getElementsByName("keyword")[0].value = '<?php echo $data['keyword']; ?>';
    });

</script>

<?php } ?>

<form action="library/qNews.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><b>Add Blog</b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" class="form-control" placeholder="Title">
    </div>

    <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker'>
            <input type='text' class="form-control" name="dateAdd" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label>Keyword</label>
        <input type="text" name="keyword" class="form-control" placeholder="Keyword : separate with commas (,)">
    </div>

    <div class="form-group">
        <label>Category</label>
        <select name="category" class="form-control">
            <option value="" disabled selected>Pilih category</option>
            <option value="1">News</option>
            <option value="3">Inspiration</option>
            <option value="4">Announcement</option>
        </select>
    </div>

    <div class="form-group">
        <label>Content</label> 
        <div class="clear5"></div>
        <img id="companyLogo1" data-type="editable1" height="200px" width="350px"/>      
        <div class="clear5"></div>
        <textarea name=content placeholder=" Write something..." style="width:350px;height:400px;padding:20"><?php if(isset($_GET['id'])){ echo $data['content']; }?></textarea>
    </div>
    
</form>

<script>

function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }
    
    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
        });
    }));

</script>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>