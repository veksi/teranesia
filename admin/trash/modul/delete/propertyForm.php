<?php if(isset($_GET['id'])){ 
    $data = editProduct('product',$_GET['id']);
?>
    <script>
    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Project';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
        document.getElementsByName("category")[0].value = '<?php echo $data['category']; ?>';
        document.getElementsByName("price")[0].value = '<?php echo $data['price']; ?>';
        document.getElementsByName("price_num")[0].value = '<?php echo $data['price_num']; ?>';
        document.getElementsByName("type")[0].value = '<?php echo $data['type']; ?>';
        document.getElementsByName("pricem2")[0].value = '<?php echo $data['priceMeter']; ?>';
        document.getElementsByName("pricem2")[0].value = '<?php echo $data['priceMeter']; ?>';
        document.getElementsByName("telp")[0].value = '<?php echo $data['telp']; ?>';
        document.getElementsByName("fax")[0].value = '<?php echo $data['fax']; ?>';
        document.getElementsByName("pk")[0].value = '<?php echo $data['productKnowledge']; ?>';
        document.getElementsByName("pricelist")[0].value = '<?php echo $data['pricelist']; ?>';
        document.getElementsByName("coordinator")[0].value = '<?php echo $data['koordinator']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
        document.getElementsByName("imgDB")[0].value = '<?php echo $data['pict']; ?>';

        var leads = '<?=$data['leads'];?>';
        if(leads!=''){
            document.getElementById("cek").checked = true;
        }
    });
    </script>
<?php } ?>

<form action="library/qProduct.php" method="post" enctype="multipart/form-data">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>
    <input type=hidden name='imgDB'>
    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Project</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>
    <div class="clear10"></div>
    <div class="form-group">
        <img id="companyLogo1" data-type="editable1" <?php if(!empty(@$data['pict'])){?> src="<?php echo $imgProjectFolder.$data['pict']; ?> <?php }else{?> src="/api_dev/images/addimg.png" <?php } ?>"/>      
    </div>
    <div class="form-group">
        <label>Project name</label>
        <input type="text" name="title" class="form-control" placeholder="Project name : Lavanya Garden Residence">
    </div>
    <div class="form-group">
        <label>Category</label>
        <select name="category" class="form-control">
            <option value="" disabled selected>Select category</option>
            <option value="1">Apartment</option>
            <option value="2">House</option>
            <option value="3">Other</option>
        </select>
    </div>
    <div class="form-group">
        <label>Price</label>
        <input type="text" name="price" class="form-control" placeholder="Price : 900JT+" autocomplete="off">
    </div>
    <div class="form-group">
        <label>Nominal price</label>
        <input type="text" name="price_num" class="form-control" placeholder="Nominal price : 900000000" autocomplete="off">
    </div>
    <div class="form-group">
        <label>Location</label>
        <div class="clearfix"></div>
        <textarea name=location placeholder="Location : Bukit Cinere, Jakarta Selatan" class="form-control"><?php if(isset($_GET['id'])){ echo $data['location']; }?></textarea>
    </div>
    <div class="form-group">
        <label>Unit type</label>
        <div class="clearfix"></div>
        <textarea name=typeUnit placeholder="Unit type : Studio, 1-2 Bed Room" class="form-control"><?php if(isset($_GET['id'])){ echo $data['typeUnit']; }?></textarea>
    </div>
    <div class="form-group">
        <label>Type</label>
        <input type="text" name="type" class="form-control" placeholder="Type : Residensial">
    </div>
    <div class="form-group">
        <label>Price/m2</label>
        <input type="text" name="pricem2" class="form-control" placeholder="Price/m2">
    </div>
    <div class="form-group">
        <label>Description</label>
        <div class="clearfix"></div>
        <textarea name=description placeholder="Description" class="form-control" style="height:200px"><?php if(isset($_GET['id'])){ echo $data['description']; }?></textarea>
    </div>
    <div class="form-group">
        <label>Address</label>
        <div class="clearfix"></div>
        <textarea name=address placeholder="Address" class="form-control"><?php if(isset($_GET['id'])){ echo $data['address']; }?></textarea>
    </div>

    <!--
    <div class="form-group row">
        <div class="col-lg-6 col-md-3">
            <label>Commision</label>
            <textarea name=commision placeholder="Commision" class="form-control"><?php if(isset($_GET['id'])){ echo $data['commision']; }?></textarea>
        </div>
        <div class="col-lg-6 col-md-3">
            <label>Commision Leads</label>
            <textarea name=commision placeholder="Commision" class="form-control"><?php if(isset($_GET['id'])){ echo $data['commision']; }?></textarea>
        </div>
    </div>-->

    <div class="form-group row">
        <div class="col-lg-6 col-md-3">
            <label>Commision</label>
            <textarea name=commision placeholder="Commision" class="form-control"><?php if(isset($_GET['id'])){ echo $data['commision']; }?></textarea>
        </div>
        <div class="col-lg-6">
            <label>Commision Leads</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <input type="checkbox" name="cek" id="cek" aria-label="...">
                </span>
                <textarea name=commisionLeads placeholder="Commision Leads" class="form-control" <?php if(@$data['leads']==''){ echo "disabled"; }?> ><?php if(isset($_GET['id'])){ echo $data['commisionLeads']; }?></textarea>
            </div>
        </div>
    </div>   

    <div class="form-group">
        <label>Telp</label>
        <input type="text" name="telp" class="form-control" placeholder="Telp" value="(021) 2789 9689">
    </div>
    <div class="form-group">
        <label>Fax</label>
        <input type="text" name="fax" class="form-control" placeholder="Fax" value="(021) 2789 9689">
    </div>
    <div class="form-group">
        <label>Product knowledge</label>
        <input type="text" name="pk" class="form-control" placeholder="http://">
    </div>
    <div class="form-group">
        <label>Pricelist</label>
        <input type="text" name="pricelist" class="form-control" placeholder="http://">
    </div>
    <div class="form-group">
        <label>Coordinator</label>
        <select id="coordinator" name="coordinator" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select coordinator</option>
            <?php  
            $combo = getCombo('sales');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idSales]"; ?>"><?php echo $data['fullName']." (".$data['phone']." - ".$data['email'].")"; ?></option>
            <?php } ?>
        </select>
    </div>
        <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker'>
            <input type='text' class="form-control" name="dateAdd" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
</form>

<script>
    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           

            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);
            $(e.parentElement).append(_inputFile);
            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };

    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
        });
    }));
</script>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });
    });

    var chk = document.querySelector("input[name=cek]");
    var textarea = document.querySelector("textarea[name=commisionLeads");
    chk.addEventListener("click", enable);
    function enable() {
        textarea.disabled = !this.checked;
    }
</script>