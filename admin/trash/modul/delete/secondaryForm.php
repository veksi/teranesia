<?php if(isset($_GET['id'])){ 
    $data = editSec('secondary',$_GET['id']);

    $wilayah = explode(".",$data['wilayah_indonesia']);
    $prop1 = $wilayah[0];
    $kota1 = $wilayah[1];
    $kec1 = $wilayah[2];
    $kel1 = $wilayah[3];

?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Properti';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("title")[0].value = '<?php echo $data['title']; ?>';
        document.getElementsByName("category")[0].value = '<?php echo $data['category']; ?>';
        document.getElementsByName("price")[0].value = '<?php echo $data['price']; ?>';
        document.getElementById("<?php echo "types".$data['types']; ?>").checked = true;
        document.getElementsByName("addres")[0].value = '<?php echo $data['addres']; ?>';
        document.getElementsByName("certificate")[0].value = '<?php echo $data['certified']; ?>';
        document.getElementsByName("bedroom")[0].value = '<?php echo $data['bedroom']; ?>';
        document.getElementsByName("bathroom")[0].value = '<?php echo $data['bathroom']; ?>';
        document.getElementsByName("maidsroom")[0].value = '<?php echo $data['maidsroom']; ?>';
        document.getElementsByName("garage")[0].value = '<?php echo $data['garage']; ?>';
        document.getElementsByName("carPort")[0].value = '<?php echo $data['carPort']; ?>';
        document.getElementsByName("surface")[0].value = '<?php echo $data['surface']; ?>';
        document.getElementsByName("building")[0].value = '<?php echo $data['building']; ?>';
        document.getElementsByName("floor")[0].value = '<?php echo $data['flor']; ?>';
        document.getElementsByName("facing")[0].value = '<?php echo $data['facing']; ?>';
        document.getElementsByName("description")[0].value = '<?php echo $data['descript']; ?>';
        document.getElementsByName("prop")[0].value = '<?php echo $prop1; ?>';

        var kota = ajaxkota('<?php echo $prop1; ?>');

        document.getElementsByName("imgDB1")[0].value = '<?php echo $data['photo1']; ?>';
        document.getElementsByName("imgDB2")[0].value = '<?php echo $data['photo2']; ?>';
        document.getElementsByName("imgDB3")[0].value = '<?php echo $data['photo3']; ?>';
        document.getElementsByName("imgDB4")[0].value = '<?php echo $data['photo4']; ?>';
        document.getElementsByName("imgDB5")[0].value = '<?php echo $data['photo5']; ?>';
        document.getElementsByName("imgDB6")[0].value = '<?php echo $data['photo6']; ?>';
        document.getElementsByName("imgDB7")[0].value = '<?php echo $data['photo7']; ?>';
        document.getElementsByName("imgDB8")[0].value = '<?php echo $data['photo8']; ?>';
    
    });

</script>

<?php  } ?>

<script type="text/javascript" src="js/ajax_kota.js"></script>

<form action="library/qSecondary.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>
    <input type=hidden name='imgDB1'>
    <input type=hidden name='imgDB2'>
    <input type=hidden name='imgDB3'>
    <input type=hidden name='imgDB4'>
    <input type=hidden name='imgDB5'>
    <input type=hidden name='imgDB6'>
    <input type=hidden name='imgDB7'>
    <input type=hidden name='imgDB8'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Tambah Properti</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Judul</label>
        <input type="text" name="title" class="form-control" placeholder="Contoh : Apartemen 2 kamar tidur dengan pemandangan laut" required>
    </div>

    <div class="form-group">
        <label>Kategori Properti</label>
        <select name="category" class="form-control" required>
            <option value="" disabled selected>Pilih Kategori Properti</option>
            <option value="1">Rumah</option>
            <option value="2">Tanah</option>
            <option value="3">Apartemen</option>
            <option value="4">Kantor</option>
            <option value="5">Vila</option>
            <option value="6">Pabrik</option>
            <option value="7">Bangunan</option>
            <option value="8">Gudang</option>
            <option value="9">Ruko</option>
        </select>
    </div>

    <div class="form-group">
        <label>Kebutuhan</label>
        <div class="clearfix"></div>
        <label class="radio-inline"><input type="radio" name="types" id="types1" value="1" required>Jual</label>
        <label class="radio-inline"><input type="radio" name="types" id="types2" value="2">Sewa</label>
    </div>

    <div class="form-group">
        <label>Gambar</label>
        <div class="row text-center">
            <div class="col-sm-5">
                <div class="card text-center">
                    <div class="card-block">
                        Foto utama
                        <div class="clearfix"></div>
                        <img id="companyLogo1" data-type="editable1" <?php if(!empty(@$data['photo1'])){?> src="<?php echo $imgSecondaryFolder.$data['photo1']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-center">
                    <div class="card-block">
                        Foto lainnya
                        <div class="clearfix"></div>
                        <img id="companyLogo2" data-type="editable2" <?php if(!empty(@$data['photo2'])){?> src="<?php echo $imgSecondaryFolder.$data['photo2']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo3" data-type="editable3" <?php if(!empty(@$data['photo3'])){?> src="<?php echo $imgSecondaryFolder.$data['photo3']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo4" data-type="editable4" <?php if(!empty(@$data['photo4'])){?> src="<?php echo $imgSecondaryFolder.$data['photo4']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo5" data-type="editable5" <?php if(!empty(@$data['photo5'])){?> src="<?php echo $imgSecondaryFolder.$data['photo5']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo6" data-type="editable6" <?php if(!empty(@$data['photo6'])){?> src="<?php echo $imgSecondaryFolder.$data['photo6']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo7" data-type="editable7" <?php if(!empty(@$data['photo7'])){?> src="<?php echo $imgSecondaryFolder.$data['photo7']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                        <img id="companyLogo8" data-type="editable8" <?php if(!empty(@$data['photo8'])){?> src="<?php echo $imgSecondaryFolder.$data['photo8']; ?>" <?php }else{ ?> src="systemImg/addimg.png" <?php } ?> />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear10"></div>

    <div class="form-group">
        <label>Harga (Rp)</label>
        <input type="number" name="price" id="price" class="form-control" placeholder="2000000000" required><!--onChange="convert(this.value)"-->
    </div>

    <div class="form-group">
        <label>Propinsi</label>
        <select name="prop" id="prop" onchange="ajaxkota(this.value)" class="form-control" required>
            <option value="" disabled selected hidden>Pilih Provinsi</option>
            <?php  
            $combo = getComboProp('wilayah_indonesia');
            foreach($combo as $data){
            ?>
            <option value="<?php echo $data['lokasi_propinsi']; ?>"><?php echo $data['lokasi_nama']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Kota / Kabupaten</label>
        <select name="kota" id="kota" onchange="ajaxkec(this.value)" class="form-control" required>
            <option value="" disabled selected>Pilih Kota / Kabupaten</option>
        <select>
    </div>

    <div class="form-group">
        <label>Kecamatan</label>
        <select name="kec" id="kec" onchange="ajaxkel(this.value)" class="form-control" required >
            <option value="" disabled selected>Pilih Kecamatan</option>
        <select>
    </div>

    <div class="form-group">
        <label>Kelurahan</label>
        <select name="kel" id="kel" class="form-control" required>
            <option value="" disabled selected>Pilih kelurahan</option>
        <select>
    </div>


    <div class="form-group">
        <label>Alamat</label>
        <textarea name="addres" class="form-control"  rows="3" placeholder="Masukkan nama jalan, nomer bangunan, dsb"></textarea>
    </div>


    <div class="form-group">
        <label>Sertifikat</label>
        <select name="certificate" class="form-control">
            <option value="" disabled selected>Pilih Sertifikat</option>
            <option value="1">Hak Milik</option>
            <option value="2">Hak Guna Bangunan</option>
            <option value="3">Hak Guna Usaha</option>
            <option value="4">Akta Jual Beli (AJB)</option>
            <option value="5">Perjanjian Pengikatan Jual Beli</option>
        </select>
    </div>

    <div class="form-group">
        <label>Kamar Tidur</label>
        <select name="bedroom" class="form-control" >
            <option value="" disabled selected>Pilih Kamar Tidur</option>
            <option value="0">Tidak ada</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
        </select>
    </div>

    <div class="form-group">
        <label>Kamar Mandi</label>
        <select name="bathroom" class="form-control" >
            <option value="" disabled selected>Pilih Kamar Mandi</option>
            <option value="0">Tidak ada</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
        </select>
    </div>

    <div class="form-group">
        <label>Kamar Pembantu</label>
        <select name="maidsroom" class="form-control" >
            <option value="" disabled selected>Pilih Kamar Pembantu</option>
            <option value="0">Tidak ada</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
        </select>
    </div>

    <div class="form-group">
        <label>Garasi</label>
        <select name="garage" class="form-control" >
            <option value="" disabled selected>Pilih Garasi</option>
            <option value="0">Tidak ada</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
        </select>
    </div>

    <div class="form-group">
        <label>Carport</label>
        <select name="carPort" class="form-control" >
            <option value="" disabled selected>Pilih Carport</option>
            <option value="0">Tidak ada</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5+</option>
        </select>
    </div>

    <div class="form-group">
        <label>Luas Tanah (m2)</label>
        <input type="number" name="surface" class="form-control" placeholder="Luas Tanah" >
    </div>

    <div class="form-group">
        <label>Luas Bangunan (m2)</label>
        <input type="number" name="building" class="form-control" placeholder="Luas Bangunan">
    </div>

    <div class="form-group">
        <label>Jumlah Lantai</label>
        <input type="number" name="floor" class="form-control" placeholder="Jumlah Lantai">
    </div>

    <div class="form-group">
        <label>Bangunan Menghadap</label>
        <select name="facing" class="form-control" >
            <option value="" disabled selected>Pilih Bangunan Menghadap</option>
            <option value="1">Utara</option>
            <option value="2">Timur Laut</option>
            <option value="3">Timur</option>
            <option value="4">Tenggara</option>
            <option value="5">Selatan</option>
            <option value="6">Barat Daya</option>
            <option value="7">Barat</option>
            <option value="8">Barat Laut</option>
        </select>
    </div>
    
    <div class="form-group">
        <label>Informasi Tambahan</label>
        <textarea name="description" class="form-control"  rows="3" placeholder="Informasi Tambahan"></textarea>
    </div>

    <div class="pull-right">
        <button name=submit type="submit" class="btn btn-success">Save</button>
        <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
    </div>

    <div class="clearfix"></div>
    
    <div class="clear10"></div>
    <div class="clear10"></div>
    
</form>

<script type="text/javascript">
/*
    var dengan_rupiah = document.getElementById('price');
	dengan_rupiah.addEventListener('keyup', function(e)
	{
		dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
	});

	dengan_rupiah.addEventListener('keydown', function(event)
	{
		limitCharacter(event);
	});

	function formatRupiah(bilangan, prefix)
	{
		var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function limitCharacter(event)
	{
		key = event.which || event.keyCode;
		if ( key != 188 // Comma
			 && key != 8 // Backspace
			 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
			 && (key < 48 || key > 57) // Non digit
			 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
			) 
		{
			event.preventDefault();
			return false;
		}
	}*/



    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
        return true;
    }

        
    function showMyImage(fileInput) {
        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {           
            var file = files[i];
            var imageType = /image.*/;     
            if (!file.type.match(imageType)) {
                continue;
            }           
            var img=document.getElementById("thumbnil");            
            img.file = file;    
            var reader = new FileReader();
            reader.onload = (function(aImg) { 
                return function(e) { 
                    aImg.src = e.target.result; 
                }; 
            })(img);
            reader.readAsDataURL(file);
        }    
    }

    function removeMyImage() {
        document.getElementById("thumbnil").src = "";
    } 

    function init1() {
        $("img[data-type=editable1]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo1')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init2() {
        $("img[data-type=editable2]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo2')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init3() {
        $("img[data-type=editable3]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo3')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init4() {
        $("img[data-type=editable4]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo4')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init5() {
        $("img[data-type=editable5]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo5')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init6() {
        $("img[data-type=editable6]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo6')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init7() {
        $("img[data-type=editable7]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo7')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function init8() {
        $("img[data-type=editable8]").each(function (i, e) {
            var _inputFile = $('<input/>')
                .attr('name', 'photo8')
                .attr('type', 'file')
                .attr('hidden', 'hidden')
                .attr('onchange', 'readImage()')
                .attr('data-image-placeholder', e.id);

            $(e.parentElement).append(_inputFile);

            $(e).on("click", _inputFile, triggerClick);
        });
    }

    function triggerClick(e) {
        e.data.click();
    }

    Element.prototype.readImage = function () {
        var _inputFile = this;
        if (_inputFile && _inputFile.files && _inputFile.files[0]) {
            var _fileReader = new FileReader();
            _fileReader.onload = function (e) {
                var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
                var _img = $("#" + _imagePlaceholder);
                _img.attr("src", e.target.result);
            };
            _fileReader.readAsDataURL(_inputFile.files[0]);
        }
    };


    (function (yourcode) {
        "use strict";
        yourcode(window.jQuery, window, document);
    }(function ($, window, document) {
        "use strict";
        $(function () {
            init1();
            init2();
            init3();
            init4();
            init5();
            init6();
            init7();
            init8();
        });
    }));

</script>


<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>
