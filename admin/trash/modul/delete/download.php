<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Download Data</b></h4>
</div>
<div class="clear10"></div>

<div class="form-group col-md-2">
    <a href="modul/xls_sales.php" class="btn btn-primary">
        <div class="card-body" style="width: 10rem;">            
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Sales<br>&nbsp;
        </div>
    </a>
</div>

<div class="form-group col-md-2">
    <a href="modul/xls_prospect.php" class="btn btn-success">
        <div class="card-body" style="width: 10rem;">        
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Prospect<br>&nbsp;
        </div>
    </a>
</div>

<div class="form-group col-md-2">
    <a href="modul/xls_project.php" class="btn btn-danger">
        <div class="card-body" style="width: 10rem;">        
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Project<br>&nbsp;
        </div>
    </a>
</div>

<div class="form-group col-md-2">
    <a href="modul/xls_secondary.php" class="btn btn-info">
        <div class="card-body" style="width: 10rem;">        
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Secondary<br>&nbsp;
        </div>
    </a>
</div>

<div class="form-group col-md-2">
    <a href="modul/xls_prospect_act.php" class="btn btn-success">
        <div class="card-body" style="width: 10rem;">        
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Prospect<br> Activation
        </div>
    </a>
</div>

<div class="form-group col-md-2">
    <a href="modul/xls_login_log.php" class="btn btn-success">
        <div class="card-body" style="width: 10rem;">        
            <i class="fa fa-cloud-download fa-3x" aria-hidden="true"></i>
            <div class="clearfix"></div>
            Login<br> Log
        </div>
    </a>
</div>