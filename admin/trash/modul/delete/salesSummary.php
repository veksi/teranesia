<script>
$(document).ready(function() { 
    $('#all').addClass('active');
    document.getElementsByName("area")[0].value = '<?=@$_POST['area'];?>';
});
</script>

<?php 
$monthName = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
$now       = date('Y');
$m         = date('m');


if(empty($_POST['year'])){
    $year = $now; 
}else{ 
    $year = $_POST['year']; 
};

if(empty($_POST['month'])){
    $month = $m-0;
}else{ 
    $month = $_POST['month']; 
};

if(empty($_POST['area'])){
    $area = "all";
}else{ 
    $area = $_POST['area']; 
};

$tot_web_last = 0;
$tot_web = 0;
$tot_app_last = 0;
$tot_app = 0;

if($month=="All"){
    
    for($i=1;$i<=count($monthName);$i++){
        
        $arrMonth[] = '"'.$monthName[$i].'"';

        $regWeb = getRegWeb($i,$year,$area);
        $arrRegWeb[] = $regWeb;        

        $regWebLast = getRegWeb($i,$year-1,$area);
        $arrRegWebLast[] = $regWebLast;
        
        $regApp = getRegApp($i,$year,$area);
        $arrRegApp[] = $regApp;

        $regAppLast = getRegApp($i,$year-1,$area);
        $arrRegAppLast[] = $regAppLast;

        $tot_web = $tot_web + $regWeb;
        $tot_web_last = $tot_web_last + $regWebLast;
        $tot_app = $tot_app + $regApp;
        $tot_app_last = $tot_app_last + $regAppLast;

        $user = getActivity($i,$year,'',$area);
        $arrUser[] = $user;

        $activity = getActivity($i,$year,'activity',$area);
        $arrActivity[] = $activity;
        
    }

    $pria = getPria($year,$area);
    $wanita = getWanita($year,$area);

}else{

    for($i=1;$i<=31;$i++){        

        $arrMonth[] = '"'.$i.'"';

        $regWeb = getRegWebDay($i,$month,$year,$area);
        $arrRegWeb[] = $regWeb;

        $regWebLast = getRegWebDay($i,$month-1,$year,$area);
        $arrRegWebLast[] = $regWebLast;

        $regApp = getRegAppDay($i,$month,$year,$area);
        $arrRegApp[] = $regApp;

        $regAppLast = getRegAppDay($i,$month-1,$year,$area);
        $arrRegAppLast[] = $regAppLast;

        $tot_web = $tot_web + $regWeb;
        $tot_web_last = $tot_web_last + $regWebLast;
        $tot_app = $tot_app + $regApp;
        $tot_app_last = $tot_app_last + $regAppLast;

        $user = getActivityDay($i,$month,$year,'',$area);
        $arrUser[] = $user;

        $activity = getActivityDay($i,$month,$year,'activity',$area);
        if(empty($activity)){
            $arrActivity[] = 0;
        }else{
            $arrActivity[] = $activity;
        }        
    }

    $pria = getPriaDay($month,$year,$area);
    $wanita = getWanitaDay($month,$year,$area);

}

?>

<form action="?sum_sales" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <h4 class="pull-left"><b><?php echo $monthName[$month-1]; ?> - <?php echo $monthName[$month]; ?> <?php echo $year; ?></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success pull-right">Submit</button>
        </div>
        
        <div class="pull-right">
            <select id="area" name="area" class="form-control">
                <option value="" selected>All Area</option>
                <option value="jabodetabek">Jabodetabek</option>    
                <option value="jogjakarta">Yogyakarta</option>
                <option value="surabaya">Surabaya</option>
                <option value="makassar" disabled>Makassar</option>
                <option value="bandung">Bandung</option>
            </select>
        </div>
        <div class="pull-right">
            <select name="month" class="form-control pull-right">
                <option value="All">All</option>
                <?php
                for($c=1; $c<=12; $c++){ 
                    if($c==$month){
                        echo"<option value=$c selected> $monthName[$c] </option>";
                    }else{
                        echo"<option value=$c> $monthName[$c] </option>";
                    }
                }?>
            </select>
        </div>
        <div class="pull-right">
            <select name="year" class="form-control">
                <?php
                for ($a=2018;$a<=$now;$a++){ 
                    if($a==$year){
                        echo"<option value='$a' selected> $a </option>";
                    }else{
                        echo "<option value='$a'>$a</option>";
                    }
                }?>
            </select>
        </div>
    </div>
</form>

<div class="clear10"></div>

<script src="js/highcharts.js"></script>

<div id="containerSS" style="width:100%; height: auto; margin: 0 auto;"></div>
<script type="text/javascript">
    Highcharts.chart('containerSS', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Register'
        },
        xAxis: {
            title: {
                text: '<?=$monthName[$month-1];?> - <?=$monthName[$month];?>',
            },
            categories: [<?= join($arrMonth, ',') ?>],
        },
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        legend: {
            align: 'right',
            verticalAlign: 'top',
        },
        series: [
        {
            name: 'Website (Last)',
            data: [<?= join($arrRegWebLast, ',') ?>],
            color: '#058DC7'
        },
        {
            name: 'Website',
            data: [<?= join($arrRegWeb, ',') ?>],
            color: '#058DC7'
        },
        {
            name: 'Application (Last)',
            data: [<?= join($arrRegAppLast, ',') ?>],
            color: '#DDDF00'
        },
        {
            name: 'Application',
            data: [<?= join($arrRegApp, ',') ?>],
            color: '#DDDF00'
        }
        ]
    });
</script>
<br>
<div class="row">
    <b><div class="col-xs-6"><?=$monthName[$month-1];?></div><div class="col-xs-6"><?=$monthName[$month];?></div></b>
    <div class="col-xs-3">Register By Web </div><div class="col-xs-3"><?=$tot_web_last?></div><div class="col-xs-3">Register By Web</div><div class="col-xs-3"><?=$tot_web?></div>
    <div class="col-xs-3">Register By App</div><div class="col-xs-3"><?=$tot_app_last?></div><div class="col-xs-3">Register By App</div><div class="col-xs-3"><?=$tot_app?></div>    
    <b><div class="col-xs-3">Total</div><div class="col-xs-3"><?=$tot_app_last+$tot_web_last?></div><div class="col-xs-3">Total</div><div class="col-xs-3"><?=$tot_app+$tot_web?></div></b>
</div>
 
<br>
<br>
<ul class="nav nav-tabs">
    <li class="active"><a href="#all" data-toggle="tab"><i class="fa fa-user"></i> <span class="textTab">Login App</span></a></li>
    <li><a href="#uniq" data-toggle="tab"><i class="fa fa-bar-chart"></i> <span class="textTab">Views</span></a></li>
</ul>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane" id="all">
        
        <br>
        <div id="containerAct"></div>
        <script type="text/javascript">
            Highcharts.chart('containerAct', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Activity '
                },
                xAxis: {
                    title: {
                        text: '<?=$monthName[$month];?>',
                    },
                    categories: [<?= join($arrMonth, ',') ?>],
                },
                yAxis: {
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                legend: {
                    align: 'right',
                    verticalAlign: 'top',
                },
                series: [{
                    //showInLegend: false,
                    name: 'User',
                    data: [<?= join($arrUser, ',') ?>],
                    //color: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                    color: '#DDDF00'
                }]
            });
        </script>
    </div>
    <div class="tab-pane fade" id="uniq">
        <br>
        <div id="containerAct2"></div>
        <script type="text/javascript">
            Highcharts.chart('containerAct2', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Activity'
                },
                xAxis: {
                    title: {
                        text: '<?=$monthName[$month];?>',
                    },
                    categories: [<?= join($arrMonth, ',') ?>],
                },
                yAxis: {
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                legend: {
                    align: 'right',
                    verticalAlign: 'top',
                },
                series: [{
                    name: 'Views',
                    data: [<?= join($arrActivity, ',') ?>],
                    //color: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                    color: '#DDDF00'
                }]
            });
        </script>
    </div>
</div>

<br>
<div id="containerJK" style="width:100%; height: auto; margin: 0 auto;"></div>
<script type="text/javascript">
    Highcharts.chart('containerJK', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Gender'
        },
        xAxis: {
            categories: ['Gender']
        },
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
        'name':'Total',
        'data':[
            ['Pria',<?=$pria?>],
            ['Wanita',<?=$wanita?>],
        ]
        }]
    });
</script>

<div class="clear10"></div>
<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {
            window.location.href='library/qProspect.php?prospect&del&id='+id;
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>