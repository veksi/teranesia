<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Project</b></h4>
    <a href="?project&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Project Name</th>
                <th>Category</th>
                <th>Date</th>
                <th>Editor</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('product');
            $no=1;
            foreach($list as $data){

                if($data['publish']==1){
                    $status = "<a href=library/qProduct.php?project&on&id=".$data['idProduct'].">Draft</a>";
                }else{
                    $status = "<a href=library/qProduct.php?project&off&id=".$data['idProduct'].">Publish</a>";
                }
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td style="text-align: center;">
                    <img height="50px" width="100px" src="<?php echo $imgProjectFolder.$data['pict']; ?>"/>      
                </td>
                <td>
                    <?php echo $data['title']; ?>
                    <div class="clearfix"></div>
                    <?php if($data['publish']=='1'){ ?>
                        <span class="label label-success">Publish</span>
                    <?php }else{ ?>
                        <span class="label label-info">Draft</span>
                    <?php } ?>
                </td>
                <td>
                    <?php if($data['category']=='1'){ ?>
                        Apartment
                    <?php }elseif($data['category']=='2'){ ?>
                        House
                    <?php }else{ ?>
                        Other
                    <?php } ?>
                </td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getUser($data['editor']); ?></td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><?php echo $status; ?></li>
                            <li><a href="#">View</a></li>
                            <li><a href="?project&mode=ins&id=<?php echo $data['idProduct']; ?>">Edit</a></li>
                            <li><a href="#" onclick=deleteRecord('product','<?php echo $data['idProduct']; ?>','<?php echo urlencode($data['pict']); ?>'); >Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qProduct.php?project&del&id='+id+'&photo='+photo;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

