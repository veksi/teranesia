<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Blog</b></h4>
    <a href="?news&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Title</th>
                <th>Date</th>
                <th>Editor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getList('article');
            $no=1;
            foreach($list as $data){

            if($data['publish']==1){
                $status = "<a href=query/qNews.php?news&draft&id=".$data['idArticle'].">Publish</a>";
            }else{
                $status = "<a href=query/qNews.php?news&publish&id=".$data['idArticle'].">Draft</a>";
            }
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td style="text-align: center;">
                    <img height="50px" width="100px" src="<?php echo $imgNewsFolder.$data['pict']; ?>"/>      
                </td>
                <td>
                    <?php echo $data['title']; ?>
                    <div class="clearfix"></div>
                    <?php if($data['publish']=='1'){ ?>
                        <span class="label label-success">Publish</span>
                    <?php }else{ ?>
                        <span class="label label-info">Draft</span>
                    <?php } ?>
                </td>
                <td><?php echo $data['insertDate']; ?></td>
                <td><?php echo getUser($data['editor']); ?></td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><?php echo $status; ?></li>
                            <li><a href="#">View</a></li>
                            <li><a href="?project&mode=ins&id=<?php echo $data['idProduct']; ?>">Edit</a></li>
                            <li><a href="#" onclick=deleteRecord('product','<?php echo $data['idProduct']; ?>','<?php echo urlencode($data['pict']); ?>'); >Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->
	

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
$(function() {
    $('#example1').dataTable();
});
</script>