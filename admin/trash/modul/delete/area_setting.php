<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left">Lokasi Acara</h4>
    <a href="#" data-toggle='modal' data-target='#myModal' class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Kota</th>
                <th>Alamat</th>
                <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getListTbl('event_area','','');
            $no=1;
            foreach($list as $data){
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $data['area']; ?></td>
                <td><?php echo $data['address']; ?></td>
                <td style="text-align: center;">
                   
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#" data-toggle='modal' data-target='#myModal' data-id='<?php echo $data['id'];?>' data-kota='<?php echo $data['area'];?>' data-alamat='<?php echo $data['address'];?>' >Edit</a></li>
                            <li><a href="#" onclick="deleteRecord('<?php echo $data['id']; ?>');" >Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->


<!-- Modal  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Add Lokasi</b></h4>
      </div>
      <div class="modal-body">

        <form>
            <div class="modal-body">
                <input type="hidden" name="id" class="form-control" id="id">
                <div class="form-group">
                    <label>Kota</label>
                    <input type="text" name="kota" class="form-control" id="kota" placeholder="Kota">
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea id=alamat name=alamat placeholder="Address" class="form-control" style="height:100px"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button name=submit type="submit" class="btn btn-success" data-dismiss="modal" id="save">Save</button>
                <button type="button" data-dismiss="modal" class="btn btn-info">Cancel</button>
            </div>
        </form>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

    function deleteRecord(id){

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                
                $.ajax({
                    url: 'modul/ajax/dataArea.php?del&id='+id,
                    success: function(respon)
                    {
                        
                        //swal("Your data has been deleted!", {
                         //   icon: "success",
                        //});
                        swal({
                            //title: "Terima Kasih",
                            text: "Your data has been deleted!",
                            icon: "success"
                        }).then(okay => {
                            if (okay) {
                                location.reload();
                            }
                        });
                                               
                    }       
                });
                
            } else {
                //swal("Your data is safe!");
            }
        });
    }

    $(function(){
        $('#save').on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: "modul/ajax/dataArea.php?save",
                type: "post",
                data: $('form').serialize(),
                success: function(data)
                {   
                    swal({
                        title: "Berhasil",
                        text: "Lokasi berhasil ditambahkan",
                        icon: "success"
                    }).then(okay => {
                        if (okay) {
                            location.reload();
                        }
                    });
                    //swal("Berhasil", "Lokasi berhasil ditambahkan", "success"); 
                }           
            });
        });
    });

    var ATTRIBUTES = ['id','kota','alamat' ];
    $('[data-toggle="modal"]').on('click', function (e) {
        var $target = $(e.target);
        var modalSelector = $target.data('target');
        ATTRIBUTES.forEach(function (attributeName) {
            var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
            var dataValue = $target.data(attributeName);
            $('#id').val($target.data('id'));
            $('#kota').val($target.data('kota'));
            $('#alamat').val($target.data('alamat'));       

            $modalAttribute.text(dataValue || '');
        });
    });

</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>

