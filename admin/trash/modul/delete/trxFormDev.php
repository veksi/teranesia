<?php if(isset($_GET['id'])){ 
    $data = editTrx('trx',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Prospect Activation';        
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("namaSales")[0].value = '<?=getSales($data['idSales'])['fullName'];?>';
        document.getElementsByName("idSales")[0].value = '<?php echo $data['idSales']; ?>';
        document.getElementsByName("namaProspect")[0].value = '<?=getProspect($data['idProspect']);?>';
        document.getElementsByName("idProspect")[0].value = '<?php echo $data['idProspect']; ?>';
        document.getElementsByName("idProduct")[0].value = '<?php echo $data['idProduct']; ?>';
        document.getElementsByName("stsSales")[0].value = '<?php echo $data['stsSales']; ?>';
        //document.getElementsByName("nominal")[0].value = '<?php echo $data['nominal']; ?>';
        document.getElementsByName("nominal")[0].value = '<?php echo "Rp. ".number_format((float)$data['nominal'],0,",","."); ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
    });
    
</script>

<?php } ?>

<?php if(isset($_GET['idSales'])){ ?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("namaSales")[0].value = '<?=getSales($_GET['idSales'])['fullName'];?>';
        document.getElementsByName("idSales")[0].value = '<?php echo $_GET['idSales']; ?>';
        document.getElementsByName("namaProspect")[0].value = '<?=getProspect($_GET['idProspect']);?>';
        document.getElementsByName("idProspect")[0].value = '<?php echo $_GET['idProspect']; ?>';
    });

</script>

<?php } ?>

<form action="library/qTrx.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Prospect Activation</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Sales name</label>
    <!--    <input type="text" id="namaSales" class="form-control" name="namaSales" placeholder="Sales name" value="">
        <input type="hidden" id="idSales" name="idSales">-->
        <div class="frmSearch">
            <input type="text" id="namaSales"  name="namaSales" placeholder="Sales name" autocomplete="off" class="form-control"/>
            <input type="hidden" id="idSales" />
            <div class="frmSearch2">
            <div class="boxSearch">
                <div id="suggesstion-box"></div>
            </div>
            </div></div>
    </div>

    <div class="form-group">
        <label>Prospect name</label>
        <input type="text" id="namaProspect" class="form-control" name="namaProspect" placeholder="Prospect name" value="">
        <input type="hidden" id="idProspect" name="idProspect">
    </div>
    
    <div class="form-group">
        <label>Project name</label>
        <select id="idProduct" name="idProduct" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select project name</option>
            <?php  
            $combo = getComboProject('product');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idProduct]"; ?>"><?php echo $data['title']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Status sales</label>
        <select name="stsSales" class="form-control">
            <option value="" disabled selected>Select status sales</option>
            <option value="1">Finder</option>
            <option value="2">Closer</option>
            <option value="3">Finder/Closer</option>
        </select>
    </div>

    <div class="form-group">
        <label>Nominal (Rp)</label>
        <!--<input type="number" name="nominal" id="nominal" class="form-control" placeholder="2000000000" autocomplete="off" onkeypress="return hanyaAngka(event)" required>-->
        <input type="text" name="nominal" id="nominal" class="form-control" placeholder="Rp. 200.000.000" onkeypress="return hanyaAngka(event)" autocomplete="off" >
    </div>

    <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control" name="dateAdd" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    
</form>

<script src="datetime/js/jQuery.js"></script>
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

        var dateNow = new Date();
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>

<style>
    .autocomplete-suggestions {
        border: 1px solid #999;
        background: #FFF;
        overflow: auto;
    }
    .autocomplete-suggestion {
        padding: 2px 5px;
        white-space: nowrap;
        overflow: hidden;
    }
    .autocomplete-selected {
        background: #F0F0F0;
    }
    .autocomplete-suggestions strong {
        font-weight: normal;
        color: #3399FF;
    }
    .autocomplete-group {
        padding: 2px 5px;
    }
    .autocomplete-group strong {
        display: block;
        border-bottom: 1px solid #000;
    }

</style>

<script src="js/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
       
        $( "#namaProspect" ).autocomplete({
            serviceUrl: "modul/ajax/autocompleteProspect.php",   // Kode php untuk prosesing data.
            dataType: "JSON",           // Tipe data JSON.
            onSelect: function (suggestion) {
                $( "#namaProspect" ).val("" + suggestion.value);
                $( "#idProspect" ).val("" + suggestion.id);
            }
        });
    })

    var dengan_rupiah = document.getElementById('nominal');
	dengan_rupiah.addEventListener('keyup', function(e)
	{
		dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
	});

	dengan_rupiah.addEventListener('keydown', function(event)
	{
		limitCharacter(event);
	});

	function formatRupiah(bilangan, prefix)
	{
		var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function limitCharacter(event)
	{
		key = event.which || event.keyCode;
		if ( key != 188 // Comma
			 && key != 8 // Backspace
			 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
			 && (key < 48 || key > 57) // Non digit
			 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
			) 
		{
			event.preventDefault();
			return false;
		}
	}


    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
        return true;
    }

</script>

<style>
    .frmSearch {
        overflow: hidden;
        width:100%;
    }    
    .frmSearch2 {
        overflow: hidden;
        background-color:white;
        position:absolute;
        width:100%;
        max-width:350px
    }    
    
    .boxSearch {
        height:100%;
        max-height:300px;
        width:100%;
    }
    .show {
        overflow-y: scroll;
        overflow-x: hidden;
    }
    
  
   
</style>
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$("#namaSales").keyup(function(){

        if ($('#namaSales').val() == '') {
            $('.boxSearch').removeClass('show');
        } else {
            $('.boxSearch').addClass('show');
        }
        
		$.ajax({
		type: "POST",
		url: "modul/ajax/readCountry.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#namaSales").css("background","#FFF url(https://www.starhome.co.id/admin/modul/ajax/LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#namaSales").css("background","#FFF");            
		}
		});
	});
});

function selectCountry(id,name) {
    $("#namaSales").val(name);
    $("#idSales").val(id);
    $("#suggesstion-box").hide();
    $('.boxSearch').removeClass('show');
}
</script>

<?php 
$sentence = "Hello everybody in the world.";
$Words = explode(" ", $Sentence);
$WordCount = count($Words);
$NewSentence = '';
for ($i = 0; $i < $WordCount; ++$i) {
    if ($i < 1) {
        $NewSentence .= '<strong>' . $Words[$i] . '</strong> ';
    } else {
        $NewSentence .= $Words[$i] . ' ';
    }
}
echo preg_replace('/^(\S+(\s+\S+)?)/', '<b>$1</b>', $sentence) ?>