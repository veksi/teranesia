<script>
$(document).ready(function() { 
    $('#all').addClass('active');
});
</script>

<?php
$monthName = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
$now       = date('Y');
$m         = date('m');

if(empty($_POST['year'])){
    $year = $now;
}else{
    $year = $_POST['year']; 
};

if(empty($_POST['month'])){
    $month = $m-0; 
}else{ 
    $month = $_POST['month']; 
};

if($month=="All"){

    for($i=1;$i<=count($monthName);$i++){

        $arrMonth[] = '"'.$monthName[$i].'"';

        $inputAll = getInputProspect("",$i,$year,"");
        $arrInputAll[] = $inputAll;

        $actAll = getActivateProspect("",$i,$year," AND activate ='1' ");
        $arrActivateAll[] = $actAll;

        $inputSS = getInputProspect("",$i,$year," AND ( (idSales!='3342') AND (idSales!='3555') ) ");
        $arrInputSS[] = $inputSS;

        $actSS = getActivateProspect("",$i,$year," AND activate ='1' AND ( (idSales!='3342') AND (idSales!='3555') ) ");
        $arrActivateSS[] = $actSS;

        $inputSH = getInputProspect("",$i,$year," AND ( (idSales='3342') OR (idSales='3555') ) ");
        $arrInputSH[] = $inputSH;

        $actSH = getActivateProspect("",$i,$year," AND activate ='1' AND ( (idSales='3342') OR (idSales='3555') ) ");
        $arrActivateSH[] = $actSH;
    }

}else{

    for($i=1;$i<=31;$i++){

        $arrMonth[] = '"'.$i.'"';
        
        $inputAll = getInputProspect(" AND DAY(insertDate) = '$i' ",$month,$year,"");
        $arrInputAll[] = $inputAll;

        $actAll = getActivateProspect(" AND DAY(insertDate) = '$i' ",$month,$year," AND activate ='1' ");
        $arrActivateAll[] = $actAll;

        $inputSS = getInputProspect(" AND DAY(insertDate) = '$i' ",$month,$year," AND ( (idSales!='3342') AND (idSales!='3555') ) ");
        $arrInputSS[] = $inputSS;

        $actSS = getActivateProspect(" AND DAY(insertDate) = '$i' ",$month,$year," AND activate ='1' AND ( (idSales!='3342') AND (idSales!='3555') ) ");
        $arrActivateSS[] = $actSS;

        $inputSH = getInputProspect(" AND DAY(insertDate) = '$i' ",$month,$year," AND ( (idSales='3342') OR (idSales='3555') ) ");
        $arrInputSH[] = $inputSH;

        $actSH = getActivateProspect(" AND DAY(insertDate) = '$i' ",$month,$year," AND activate ='1' AND ( (idSales='3342') OR (idSales='3555') ) ");
        $arrActivateSH[] = $actSH;
    }
}

?>

<form action="?sum_prospect" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <h4 class="pull-left"><b>Summary <?php echo $monthName[$month-1]; ?> <?php echo $year; ?></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success pull-right">Submit</button>
        </div>
        <div class="pull-right">
            <select name="month" class="form-control pull-right">
                <!--<option value="" disabled selected hidden>Month</option>-->
                <option value="All" selected>All</option>
                <?php
                $monthName=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                for($c=1; $c<=12; $c++){ 
                    $monthNumber = $c-1; 
                    if($c==$month){
                        echo"<option value=$c selected> $monthName[$monthNumber] </option>";
                    }else{
                        echo"<option value=$c> $monthName[$monthNumber] </option>";
                    }
                }?>
            </select>
        
        </div>

        <div class="pull-right">
        
            <select name="year" class="form-control">
                <?php
                
                for ($a=2018;$a<=$now;$a++){ 
                    if($a==$year){
                        echo"<option value='$a' selected> $a </option>";
                    }else{
                        echo "<option value='$a'>$a</option>";
                    }
                }?>
            </select>

        </div>
    
    </div>

</form>

<div class="clear10"></div>
<script src="js/highcharts.js"></script>

<br>
<ul class="nav nav-tabs">
    <li class="active"><a href="#all" data-toggle="tab"><i class="fa fa-bar-chart"></i> <span class="textTab">All</span></a></li>
    <li><a href="#starsales" data-toggle="tab"><i class="fa fa-bar-chart"></i> <span class="textTab">Star Sales</span></a></li>
    <li><a href="#starhome" data-toggle="tab"><i class="fa fa-bar-chart"></i> <span class="textTab">Star Home</span></a></li>
</ul>

<div id="myTabContent" class="tab-content">
    <div class="tab-pane" id="all">
        <div id="containerAll" style="width:100%; height: auto; margin: 0 auto;"></div>
        <script type="text/javascript">
            Highcharts.chart('containerAll', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'All'
                },
                xAxis: {
                    categories: [<?= join($arrMonth, ',') ?>]
                },
                yAxis: {
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Input',
                    data: [<?= join($arrInputAll, ',') ?>]
                }, {
                    name: 'Activation',
                    data: [<?= join($arrActivateAll, ',') ?>]
                }]
            });
        </script>
    </div>
    <div class="tab-pane" id="starsales">
        <div id="containerSS" style="width:100%; height: auto; margin: 0 auto;"></div>
        <script type="text/javascript">
            Highcharts.chart('containerSS', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'STARSALES'
                },
                xAxis: {
                    categories: [<?= join($arrMonth, ',') ?>]
                },
                yAxis: {
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Input',
                    data: [<?= join($arrInputSS, ',') ?>]
                }, {
                    name: 'Activation',
                    data: [<?= join($arrActivateSS, ',') ?>]
                }]
            });
        </script>
    </div>
    <div class="tab-pane fade" id="starhome">
        <div id="containerSH" style="width:100%; height: auto; margin: 0 auto;"></div>
        <script type="text/javascript">
            Highcharts.chart('containerSH', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'STARHOME'
                },
                xAxis: {
                    categories: [<?= join($arrMonth, ',') ?>]
                },
                yAxis: {
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Input',
                    data: [<?= join($arrInputSH, ',') ?>]
                }, {
                    name: 'Activation',
                    data: [<?= join($arrActivateSH, ',') ?>]
                }]
            });
        </script>
    </div>
<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Finder</th>
                <th>Total Input</th>
                <th>Total Activation</th>                
            </tr>
        </thead>
        <tbody>
            <?php
            $list = getListGroup('prospect',$year,$month);
            $no=1;
            foreach($list as $data){
                $dataSales = getSales($data['idSales']);
            ?>
            <tr>
                <td><?php echo $no; ?></td>                
                <td><?php echo $dataSales['fullName'].' <small>('.$dataSales['email'].')</small>' ?></td>
                <td><?php echo getCount('prospect',$data['idSales'],$year,$month); ?></td>
                <td><?php echo getCountActivate('prospect',$data['idSales'],$year,$month); ?></td>
            </tr>
            <?php $no++;} ?>                                    
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {
            window.location.href='library/qProspect.php?prospect&del&id='+id;
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">
    $(function() {
        $('#example1').dataTable();
    });
</script>