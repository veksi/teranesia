<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=prospect_activation_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>ID</th>
        <th>Finder</th>
        <th>Prospek</th>
        <th>Produk</th>
        <th>Progress</th>
        <th>Closer</th>
        <th>Tgl Visit</th>
        <th>Tgl Booking</th>            
        <th>Tgl Input</th>    
        <th>Informasi</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getList('trx');
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>            
            <td>PA<?php echo $data["idTrx"]; ?></td>
            <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName']; ?></td>
            <td><?php echo getProspect($data['idProspect']); ?></td>
            <td><?php echo getProduct($data['idProduct']); ?></td>
            <td><?php echo getProgressTrx($data['progress']); ?></td>            
            <td><?php $dataSales = getSales($data['idSales']); echo $dataSales['fullName']; ?></td>
            <td><?php echo $data['visitDate'].' '.$data['visitTime']; ?></td>
            <td><?php echo $data["bookingDate"]; ?></td>
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php echo $data['information']; ?></td>            
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>
