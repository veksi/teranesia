<?php
$create = date("dmYhis");
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=sales_".$create.".xls");

require "../setting/connection.php"; 
require "../library/function.php";
require "../library/functionAJAX.php";
require "../library/queryAdmin.php";

?>
<table border=1>
    <thead>
        <th>No</th>
        <th>#</th>
        <th>ID</th>
        <th>Nama</th>
        <th>Domisili</th>
        <th>Posisi</th>
        <th>Level</th>
        <th>Tgl Lahir</th>
        <th>Email</th>
        <th>Telepon</th>
        <th>KTP</th>
        <th>NPWP</th>
        <th>Bank</th>
        <th>Rekening</th>
        <th>Refferal</th>
        <th>Status</th>
        <th>Tgl Bergabung</th>
        <th>Tgl Set Posisi</th>
        <th>Tgl Aktivasi</th>        
        <th>Editor</th>
    </thead>
    <tbody border=1>
        <?php
        $list = getReport('sales');

        $conn = db_connection();
        $query = mysqli_query($conn,"SELECT * FROM wa");
        $row = mysqli_fetch_assoc($query);        
        
        $no=1;
        foreach($list as $data){?>
        <tr>
            <td><?php echo $no; ?></td>            
            <td><?php echo $data['idSales']; ?></td>
            <td><a href="https://wa.me/<?=hp($data['phone']);?>?text=Hallo%20Bapak%20dan%20Ibu%20<?=$data['fullName'];?>%2C%0D%0A%0D%0AKami%20Star%20sales%20bermaksud%20mengundang%20Bapak%20dan%20ibu%20untuk%20mengikuti%20acara%20kami%20minggu%20ini%20%22JUAL%20PROPERTI%20JAMAN%20NOW%22%20dengan%20topik%20membahas%20Cara%20Mudah%20dan%20Gratis%20Memasarkan%20Property%20Lewat%20HP%0D%0A%0D%0ATEMPAT%20TERBATAS%0D%0A%0D%0ASegera%20bergabung%20%2C%20dan%20dapatkan%20banyak%20keuntungan%20hanya%20Modal%20handphone%2C%20%0D%0A%0D%0A1.%20Asiknya%20cari%20duit%20jual%20properti%0D%0A2.%20Komisi%20sangat%20besar%0D%0A3.%20Dibantu%20digital%20marketing%0D%0A4.%20Dapat%20reward%20dan%20Bonus%20tak%20terbatas%0D%0A5.%20Dapat%20training%20khusus%0D%0A%0D%0AAcara%20GRATIS%0D%0A%0D%0AAdapun%20tahapan%20sesi%20%3A%0D%0A1.%20Pengenalan%20Starsales%20dan%20Starhome%0D%0A2.%20Training%20teknik%20Cara%20Mudah%20dan%20Gratis%20Memasarkan%20Property%20Lewat%20HP%0D%0A3.%20Sesi%20tanya%20jawab%0D%0A%0D%0AHari%20Sabtu%2C%20Tgl%2021%20September%202019%0D%0A%0D%0A1.%20Synergy%20Building%20Alam%20Sutera%20lt.15%2A%0D%0Ajam%20%3A%2010.30%20sd%20selesai%0D%0AJl.%20Jalur%20Sutera%20Barat%20Alam%20Sutera%20Tangerang%20GMaps%20%3A%20https%3A%2F%2Fgoo.gl%2Fmaps%2F6gwDT7FD1pK2%0D%0A%0D%0A2.%20Bekasi%20-%20Econnection%20Space%20Mega%20Bekasi%20Hypermall%20Lt.3%0D%0AJam%2010.30%20sd%20selesai%0D%0AJl.%20Jend.%20Ahmad%20Yani%20No.1%2C%20Bekasi%0D%0Alokasi%20%3A%20https%3A%2F%2Fgoo.gl%2Fmaps%2FQW4tM65VFt92%0D%0A%0D%0AHari%20Jumat%2C%20Tgl%2020%20September%202019%0D%0A%0D%0A3.%20Servo%20%40%20Philips%20New%20Building%20Lt.2%0D%0AJam%2014.00%20sd%20selesai%0D%0A%20Jl.%20Buncit%20Raya%20Kav%20100%20Jakarta%20Selatan%20seberang%20Mall%20Pejaten.%0D%0Alokasi%20%3A%20https%3A%2F%2Fgoo.gl%2Fmaps%2FVxzKBAtnPX4D7aAb8%0D%0A%0D%0AUntuk%20Pendaftaran%20isi%20Link%20Form%20Dibawah%20Ini%20%3A%0D%0A%0D%0Ahttps%3A%2F%2Fdocs.google.com%2Fforms%2Fd%2Fe%2F1FAIpQLSeIK_cGI5VkDSwRQWgu8Qpfag7-bmJ4VlU29gOMlalJM8hhXg%2Fviewform%3Fusp%3Dsf_link%0D%0A%0D%0ANote.%20Bila%20Link%20tidak%20bisa%20diklik%20bisa%20daftar%20dengan%20replay%20di%20whatsapp%20ini%0D%0A%0D%0ANama%20%3A%0D%0ALokasi%20%3A%0D%0A%0D%0AUntuk%20Info%20Lanjutan%20Hubungi%0D%0ACS%20Starsales%20%28%20LUSI%20%2F%20ASTRI%20%29%0D%0A081293785997">Undang</a></td>
            <td><?php echo $data['fullName']; ?></td>
            <td><?php echo getDomicile($data['domicile']); ?></td>
            <td><?php echo stsPosSales($data['position']); ?></td>
            <td><?php echo $data['level']; ?></td>
            <td><?php echo $data['bornDate']; ?></td>            
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['phone']; ?></td>
            <td><?php echo $data['ktp_number']; ?></td>
            <td><?php echo $data['npwp']; ?></td>
            <td><?php echo getBank($data['bankCode']); ?></td>
            <td><?php echo $data['account']; ?></td>
            <td><?php echo $data['refferal']; ?></td>
            <td><?php echo stsRegSales($data['active']); ?></td>            
            <td><?php echo $data['insertDate']; ?></td>
            <td><?php echo $data['joinPositionDate']; ?></td>
            <td><?php echo $data['activationDate']; ?></td>
            <td><?php echo getUser($data['activatedBy']); ?></td>
        </tr>
        <?php $no++;} ?>                                    
    </tbody>
</table>