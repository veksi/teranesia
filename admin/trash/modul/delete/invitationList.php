<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left">Undangan</h4>
    <a href="?invitation&mode=ins" class="btn btn-success pull-right">Add</a>
</div>

<div class="clear10"></div>

<div class="box-body table-responsive">
    <table id="tabelAuthor" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Target</th>
                <th>Area</th>                
                <th>Jangkauan</th>
                <th>Date Range</th>
                <th style="text-align:center">Action</th>                                                                                
                <th>Insert Date</th>
            </tr>
        </thead>                                            
    </table>
</div>

<!--<script src="dataTable/js/jquery-1.11.1.min.js"></script>-->
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

   // $(document).ready(function() {

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var otable = $('#tabelAuthor').DataTable({

            "processing": true,
            responsive: true,
            "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
            "serverSide": true,
            "ajax": "modul/ajax/dataInvitation.php?list",
            "order": [[ 7, "desc" ]],
            "stateSave": false, //tetap pada current page apapun yg terjadi
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // table.order = [[ 5, "desc" ]];
            },
            "fnCreatedRow": function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            },

            "columnDefs": [
                /*
                { 
                    "render": imgCol,
                    "targets": 1 
                },
                { 
                    "render": titleCol,
                    "targets": 2 
                },
                { 
                    "data": 4,
                    "targets": 3 
                },
                
*/              { 
                    "render": jangkauanCol,
                    "targets": 4 
                },
                { 
                    "render": rangeCol,
                    "targets": 5 
                },
                { 
                    "render": actionCol,
                    "targets": 6 
                },
                {
                    "data": 7,
                    "targets": 7,
                    "visible":false
                },
            ],

        });

        $('#tabelAuthor_filter input').unbind();
        $('#tabelAuthor_filter input').bind('keyup', function(e) {
            if(e.keyCode === 13) {
                otable.search( this.value ).draw();
            }
        });  

        function jangkauanCol(data, type, full) {

            return ''+full[4]+' Orang <br>'+full[8]+' Terkirim';

        }

        function rangeCol(data, type, full) {
            return ''+full[5]+' s/d '+full[6];
        }

        function actionCol(data, type, full) {
            if(full[4]==0){
                viewBtn = "<a href=# onclick=generate("+full[0]+")>Generate</a>";
            }else{
                viewBtn = "<a href=?invitation&mode=view&idEvent="+full[0]+">View</a>";
            }

            return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu"><li>'+viewBtn+'</li><li><a href="#" onclick=detail("'+full[0]+'"); data-toggle="modal" data-target="#myModal">Detail</a></li><li><a href="?invitation&mode=ins&id='+full[0]+'">Edit</a></li><li><a href=# onclick=deleteRecord("'+full[0]+'")>Delete</a></li></ul></div>';

        }

        function success(msg){
            swal("Done", msg, "success");
        }

        function reload(){
            otable.ajax.reload( null, false );
        }

        function generate(id){

            $.ajax({
                url: "modul/ajax/dataInvitation.php?generate&id="+id,
                success: function(respon)
                {
                    reload();
                    success('Proses generate berhasil');      
                    //alert(respon);
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

        function deleteRecord(id){

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    
                    $.ajax({
                        url: "modul/ajax/dataInvitation.php?del&id="+id,
                        success: function(respon)
                        {
                            reload();
                            swal("Your data has been deleted!", {
                                icon: "success",
                            });
                            
                        }       
                    });
                    
                } else {
                    //swal("Your data is safe!");
                }
            });
        }

        function detail(id){

            $.ajax({
                url: "modul/ajax/dataInvitation.php?detail&id="+id,
                success: function(respon)
                {
                    $('.result').html(respon);
                },
                error: function(err) {
                    console.log(err);
                }          
            });

        }

</script>

<!-- Modal  -->
<div class="modal fade sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                
                <h4 class="modal-title" id="modal2Label"><b><i class='fa fa-eye'></i> Detail</b></h4>
            </div>
            <div class="modal-body">
                <div class="result"></div>
            </div>
        </div>
    </div>
</div>