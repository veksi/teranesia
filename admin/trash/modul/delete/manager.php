<div class="clear10"></div>
<div class="form-group">
    <h4 class="pull-left"><b>Manager</b></h4>
</div>
<div class="clear10"></div>
<div class="row text-center" >
    <?php 
    $data = getListTbl('sales','position = 3 ',''); 
    foreach($data as $row){
        if(!empty(@$row['photo']))
        { 
            $pp = "src=https://www.starhome.co.id/api/images/sales/".$row['photo']; 
        }else{ 
            $pp="src='https://www.starhome.co.id/api/images/sales/89.png'";
        }
    ?>
    <div class="col-md-3">            
       <img class="avatar img-circle img-thumbnail" alt="avatar" style="width:100px;height:100px;" <?=$pp;?> >    
        <h4><?=ucwords($row['fullName']);?> <span class="badge"><?=number_format(dbGet('closer','manager = '.$row['idSales'].''));?></span></h4>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Paid</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','status = 1 AND manager ='.$row['idSales'].''));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Unpaid</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','status != 1 AND manager ='.$row['idSales'].''));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-primary">Certified</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','certified = 1 AND manager ='.$row['idSales'].''));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Uncertified</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer',"(certified = '' OR certified IS NULL OR certified = 0) AND manager =".$row['idSales'].""));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> Silver</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"position = 2 AND level = 'silver' AND refferal ='".$row['nik']."'"));?></span></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> <i class="fa fa-trophy" aria-hidden="true"></i> Gold</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"position = 2 AND level = 'gold' AND refferal ='".$row['nik']."'"));?></span></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> <i class="fa fa-trophy" aria-hidden="true"></i> <i class="fa fa-trophy" aria-hidden="true"></i> Platinum</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"position = 2 AND level = 'platinum' AND refferal ='".$row['nik']."'"));?></span></div>
            <hr>
        </div>
    </div>
    <?php } ?>
</div>
<hr>

<div class="clear10"></div>
<div id="divActivity"></div>