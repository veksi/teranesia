<?php
$absenExist = dbGetRow('absensi',"kegiatan_materi = '".$sesi_ID."' AND user = '".$_SESSION['member']."'",'','',''); 

if(!empty(@$absenExist['foto'])){ 
  $abs = $domain."images/absensi/".$absenExist['foto']; 
}else{ 
  $abs = $domain."images/nofoto.png";
}

if(!empty(@$absenExist['ttd_img'])){ 
  $ttd = $domain."images/signature/".$absenExist['ttd_img']; 
}else{ 
  $ttd = "";
}
?>

<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
  <div class="jumbotron p-2 mb-4 bg-white">
    <div class="row justify-content-md-center">
      <div class="col-sm-8">
      
        <label><h5><?=$sesi['title']?></h5></label>
        
        <hr>

        <form action="<?=$domain?>modul/ajax/data_pelatihan_diikuti_materi_absensi.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">
          <input type="hidden" name='mode' value="ins">
          <input type="hidden" name='sesi' value="<?=$sesi_ID?>">

          <div class="row ">
            <div class="col">Nama</div>
            <div class="col"><?=$member['first_name'].' '.$member['last_name']?></div>
          </div>
          <div class="row title">
            <div class="col">Tempat / Tanggal Lahir</div>
            <div class="col"><?=$member['bornPlace']?> / <?=tgl_indo($member['bornDate'],false)?></div>
          </div>
          <div class="row title">
            <div class="col">Email</div>
            <div class="col"><?=$member['email']?></div>
          </div>
          <div class="row">
            <div class="col">Handphone</div>
            <div class="col"><?=$member['hp']?></div>
          </div>
          <div class="row title">
            <div class="col">Alamat</div>
            <div class="col"><?=$member['alamat']?></div>
          </div>
          <div class="row title">
            <div class="col">Perusahaan</div>
            <div class="col"><?=$member['company']?></div>
          </div>
          <div class="row title">
            <div class="col">Jabatan</div>
            <div class="col"><?=$member['jabatan']?></div>
          </div>
          <div class="row title">
            <div class="col">Alamat Perusahaan</div>
            <div class="col"><?=$member['company_address']?></div>
          </div>
          <div class="row title">
            <div class="col">Telp</div>
            <div class="col"><?=$member['telp']?></div>
          </div>
          <div class="clearfix"></div>
          <br><br>

          <form method="POST" action="storeImage.php">
            <div class="row">
              <div class="col">
                
                <br/>
                
              </div>
              <div class="col">
                <div id="results">Your captured image will appear here...</div></div>
              </div>

                <br/>
                <button class="btn btn-success">Submit</button>
            </div>
          </form>
          
          <div class="row">

            <div class="col-sm-6 text-left">
              <div class="text-center">
                <?php if(empty($absenExist['ttd_img'])){ ?>
                  <img id="pp" data-type="editable1" class="avatar img-thumbnail" alt="avatar" style="width:250px;height:250px;" src="<?=$abs?>" >
                  <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                <?php }else{ ?>
                  <img class="avatar img-thumbnail" alt="avatar" style="width:250px;height:250px;" src="<?=$abs?>" >
                <?php } ?>
                <div class="clearfix"></div>
                <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
              </div>
              <br>
            </div>

            <div class="col-sm-6 text-right">
              <div style="width:250px;height:250px;margin:auto;border:1px solid #DEE2E6" class="rounded">
                <?php if(empty($absenExist['ttd_img'])){ ?>
                  <a class="btn hapus btn-light" style="position:absolute;margin-left:-40"><i class="fa fa-times" aria-hidden="true"></i></a>
                  <div class="tandatangan2"></div>
                <?php }else{ ?>
                  <img src="<?=$domain?>images/signature/<?=$absenExist['ttd_img']?>">
                <?php } ?>
                <div class="clearfix"></div>
                <small id="emailHelp" class="form-text text-muted text-center">* tanda tangan pad</small>
              </div>
              <br>
            </div>

          </div>
        
          <div class="lead text-center py-4">                 
            <input type="hidden" name='ttd' id='ttd'>
            <?php if(empty($absenExist['id'])){ ?>
              <button name=submit type="submit" class="btn btn-primary loadingBtn">Kirim</button>     
              <!--<input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-lg"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim">-->
            <?php } ?>              
          </div>

        </form>

      </div>
    </div>          
  </div>        
</div>

<script>

$(document).ready(function() {

  $('.tandatangan2').on('mouseup touchend', function(e) {
      var dataToBeSaved =  $(".tandatangan2").jSignature("getData");
      $('#ttd').val(dataToBeSaved);//ini utk kedatabase                            
  });   

  $(".tandatangan2").jSignature({'background-color':'#f0f3fa','height':'100%','width':'100%','lineWidth':'2'});         
  
  $('.hapus').on('click', function(e) {
      e.preventDefault();
      $('.tandatangan2').jSignature("reset");
      $('#ttd').val('');
  });

});   

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "../../../images/nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};

(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){
  var ttd      = $("#ttd").val();
  var foto     = document.getElementsByName("photo1")[0].value;

  if (ttd=="" || foto==""){
      swal("","Lengkapi data dengan benar","warning",).then(function(){ $(".loadingBtn").html('Kirim'); });     
      return false;
  }else{
      return true;
  }
}

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
  
    Webcam.set({
        width: 320,
        height:  240,
        dest_width: 320,
        dest_height: 240,
        image_format: 'jpeg',
        jpeg_quality: 100
    });
                        
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }

    function turnOff() {
      Webcam.reset()
    };

    function turnOn() {
      Webcam.attach( '#my_camera' );
    }
</script>
 
 <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-success addBtn"><i class="fa fa-plus"></i> Add</a>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form id="insert">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" onclick="turnOff()" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <script type="text/javascript">                 
                    $('#myModal').on('shown.bs.modal', function () {                                        
                      turnOn();
                    });
                    $("#myModal").on('hidden.bs.modal', function(){
                      turnOff()
                    });
                  </script> 
                  <div id="my_camera"></div>
                  <input type="hidden" name="image" class="image-tag">
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                    <input type=button value="Take Snapshot" onClick="take_snapshot()" class="btn btn-success">
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->