<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/function.php";
    include "../../library/queryAdmin.php";
    include "../../library/PassHash.php";

    $id = @$_GET['id'];
    $pst = @$_GET['pst'];
    $lvl = @$_GET['lvl'];

    if(isset($_GET['agent'])){    
        if(isset($_GET['pending'])){
            $extraWhere = " active = 0 AND (office != '') ";
        }elseif(isset($_GET['suspend'])){
            $extraWhere = " active = 2 AND (office != '') ";    
        }else{
            $extraWhere = " active = 1 AND (office != '') ";
        }
    }else{
        if(isset($_GET['pending'])){
            $extraWhere = " active = 0 AND (office = '' OR office IS NULL) ";
        }elseif(isset($_GET['suspend'])){
            $extraWhere = " active = 2 AND (office = '' OR office IS NULL) ";    
        }else{
            $extraWhere = " active = 1 AND (office = '' OR office IS NULL)  ";
        }
    }

    if(isset($_GET['list'])){
        
        $primaryKey = 'idSales';
        $table = 'sales';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `domicile` AS `b` ON (`a`.`domicile` = `b`.`idDomicile`) 
            LEFT JOIN `city` AS `c` ON (`a`.`idCity` = `c`.`idCity`)
            LEFT JOIN `wilayah_indonesia` AS `d` ON (`a`.`homeTown` = `d`.`lokasi_kode`)
            ";

        $columns = array(

            array( 'db' => '`a`.`idSales`', 'dt' => 0, 'field' => 'idSales' ),
            array( 'db' => '`a`.`idSales`', 'dt' => 1, 'field' => 'idSales' ),
            array( 'db' => '`a`.`fullName`', 'dt' => 2, 'field' => 'fullName' ),
            array( 'db' => '`b`.`domicile`', 'dt' => 3, 'field' => 'domicile' ),
            array( 
                'db' => '`a`.`phone`',
                'dt' => 4,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 5, 'field' => 'insertDate' ),
            array( 
                'db' => '`a`.`idSales`',
                'dt' => 6,
                'field' => 'idSales',
                'formatter' => function($d) {
                        return getLastLogin($d);
                    }
            ),
            array( 'db' => '`a`.`position`',  'dt' => 7, 'field' => 'position' ),
            array( 'db' => '`a`.`active`',  'dt' => 8, 'field' => 'active' ),
            array( 'db' => '`a`.`email`',  'dt' => 9, 'field' => 'email' ),
            array( 'db' => '`c`.`city`', 'dt' => 10, 'field' => 'city' ),
            array( 'db' => '`d`.`wilayah`', 'dt' => 11, 'field' => 'wilayah' ),
            array( 'db' => '`a`.`level`', 'dt' => 12, 'field' => 'level' ),
            /*array( 
                'db' => '`d`.`wilayah`',
                'dt' => 11,
                'field' => 'wilayah',
                'formatter' => function($e) {
                        return str_replace(" ","",$e);
                    }
            ),*/
            /*array( 'db' => '`a`.`ktp_number`', 'dt' => 12, 'field' => 'ktp_number' ),*/

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );

    }elseif(isset($_GET['updPst'])){

        $date = date("Y-m-d H:i:s");

        $conn = db_connection();
        $sql = mysqli_query($conn,"UPDATE sales SET position = '$pst', joinPositionDate = '$date' WHERE idSales = '$id' ");

        if($pst=='2'){
            $query = mysqli_query($conn," SELECT idSales FROM closer WHERE idSales = '$id' ");
            $result = mysqli_num_rows($query);
            if($result==0){
                $sql = mysqli_query($conn,"INSERT INTO closer SET idSales = '$id', insertDate = '$date' ");
            }
        }else{
            $sql = mysqli_query($conn,"DELETE FROM closer WHERE idSales = '$id' ");
        }
       
        mysqli_close($conn);

    }elseif(isset($_GET['updLvl'])){

        $date = date("Y-m-d H:i:s");

        $conn = db_connection();
        $sql = mysqli_query($conn,"UPDATE sales SET level = '$lvl', levelPositionDate = '$date' WHERE idSales = '$id' ");

        mysqli_close($conn);

    }elseif(isset($_GET['view'])){
        $data = getSales($id); 
    ?>
        <table class="table table-striped table-borderless">
            <tbody>
                <tr><td width="200">NIK</td><td><?=$data['nik'];?></td></tr>                            
                <tr><td>Nama</td><td><?=$data['fullName'];?></td></tr>                            
                <tr><td>Telp</td><td><?=$data['phone'];?></td></tr>
                <tr><td>Email</td><td><?=$data['email'];?></td></tr>
                <tr><td>Tgl Bergabung</td><td><?=$data['insertDate'];?></td></tr>
                <tr><td>Position</td><td><?=$data['position'];?></td></tr>
                <tr><td>Level</td><td><?=$data['level'];?></td></tr>
            </tbody>
        </table>

    <?php

    }elseif(isset($_GET['on'])){

        $date = date("Y-m-d H:i:s");

        $conn = db_connection();
        $sql = mysqli_query($conn,"UPDATE sales SET active = '1', activatedBy = '$_SESSION[idUser]', activationDate = '$date' WHERE idSales = '$id' ");

        mysqli_close($conn);

    }elseif(isset($_GET['off'])){
        
        $date = date("Y-m-d H:i:s");

        $conn = db_connection();
        $sql = mysqli_query($conn,"UPDATE sales SET active = '2', activatedBy = '$_SESSION[idUser]', activationDate = '$date' WHERE idSales = '$id' ");

        mysqli_close($conn);

    }elseif(isset($_GET['reset'])){

        $ranPwd = random_code2(5);
        $newPwd = PassHash::hash($ranPwd);
        $newPassword = md5($ranPwd);

        $conn = db_connection();
        $sql = mysqli_query($conn,"UPDATE sales SET pwd = '$newPwd', password = '$newPassword' WHERE idSales = '$id' ");
        
        mysqli_close($conn);

    ?>

        <div class="modal-body">
            <div class="clear10"></div>
            <span id="modal-title"><?=$ranPwd?></span>
            <div class="clear10"></div>
        </div>

    <?php

    }
    
}

?>
