<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";

    $imgNewsFolder = "../../../api/images/article/";

    $id = @$_GET['id'];
    $extraWhere = " category = '4' AND publish = 1 ";

    if(isset($_GET['list'])){

        $table = 'article';
        $primaryKey = 'idArticle';

        $columns = array(

            array( 'db' => 'idArticle', 'dt' => 0 ),
            array( 'db' => 'pict', 'dt' => 1 ),
            array( 'db' => 'title', 'dt' => 2 ),
            array( 'db' => 'category',  'dt' => 3 ),
            array( 'db' => 'insertDate',  'dt' => 4 ),   
            array( 
                'db' => 'editor',
                'dt' => 5,
                'formatter' => function($d) {
                        return getUser($d);
                    }
                ),
            array( 'db' => 'publish',  'dt' => 6 ),
            array( 'db' => 'status',  'dt' => 7 ),
        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, '', $extraWhere)
        );
        
    }elseif(isset($_GET['lived'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE article SET status = '1' WHERE idArticle = '$id' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['closed'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE article SET status = '0' WHERE idArticle = '$id' ");

        mysqli_close($conn);
        
    }
}

?>