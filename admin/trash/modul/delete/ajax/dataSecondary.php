<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";
    include "../../library/function.php";

    $id = @$_GET['id'];

    if(isset($_GET['pending'])){
        $extraWhere = " approved = 0 AND sts_on_user = 1 AND deleted = 0 ";
    }elseif(isset($_GET['suspend'])){
        $extraWhere = " approved = 2 AND sts_on_user = 1 AND deleted = 0 ";
    }elseif(isset($_GET['sold'])){
        $extraWhere = " approved = 1 AND sts_trx = 2 AND sts_on_user = 1 AND deleted = 0 ";
    }elseif(isset($_GET['rented'])){
        $extraWhere = " approved = 1 AND sts_trx = 1 AND sts_on_user = 1 AND deleted = 0 ";
    }elseif(isset($_GET['deleted'])){
        $extraWhere = " deleted = 1 ";
    }else{
        $extraWhere = " approved = 1 AND sts_on_user = 1 AND deleted = 0 ";
    }

    if(isset($_GET['list'])){

        $primaryKey = 'id';
        $table = 'secondary';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`) 
            LEFT JOIN `category_property` AS `c` ON (`a`.`category` = `c`.`idCategory`)
            LEFT JOIN `wilayah_indonesia` AS `d` ON (`a`.`wilayah_indonesia` = `d`.`lokasi_kode`)
            LEFT JOIN `master_satuan` AS `e` ON (`a`.`satuan` = `e`.`id`)
            LEFT JOIN `master_rented_sold` AS `f` ON (`a`.`sts_trx` = `f`.`id`) ";

        $columns = array(

            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`a`.`photo1`', 'dt' => 1, 'field' => 'photo1' ),
            array( 'db' => '`a`.`title`',  'dt' => 2, 'field' => 'title' ),
            array( 'db' => '`c`.`category`','dt' => 3, 'field' => 'category'), 
            array( 
                'db' => '`a`.`price`',
                'dt' => 4,
                'field' => 'price',
                'formatter' => function($b) {
                        return "Rp. ".number_format((float)$b);
                    }
                ),   
            array( 
                'db' => '`a`.`wilayah_indonesia`',
                'dt' => 5,
                'field' => 'wilayah_indonesia',
                'formatter' => function($c) {

                        $lokasi = explode('.',$c);
                        $prop = @$lokasi[0];
                        $kab = @$lokasi[1];
                        $kec = @$lokasi[2];
                        $kel = @$lokasi[3];

                        $area = 
                        ucwords(strtolower(getProp('wilayah_indonesia',$prop))).','.
                        ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab))).','.
                        ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec))).','.
                        ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));

                        return $area;
                    }
                ),  
            array( 'db' => '`a`.`insertDate`',  'dt' => 6,'field' => 'insertDate'),
            array( 'db' => '`a`.`approved`',  'dt' => 7,'field' => 'approved'),
            array( 'db' => '`a`.`imgValid`',  'dt' => 8,'field' => 'imgValid'),
            array( 'db' => '`b`.`fullName`', 'dt' => 9,'field' => 'fullName'),   
            array( 
                'db' => '`b`.`phone`',
                'dt' => 10,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            array( 'db' => '`d`.`lokasi_nama`', 'dt' => 11,'field' => 'lokasi_nama'),   
            array( 'db' => '`a`.`code`', 'dt' => 12,'field' => 'code'),
            array( 'db' => '`e`.`information`', 'dt' => 13,'field' => 'information'),
            array( 
                'db' => '`a`.`id`',
                'dt' => 14,
                'field' => 'id',
                'formatter' => function($a) {
                        return countNote($a);
                    }
            ),
            array( 'db' => '`a`.`sts_trx`', 'dt' => 15,'field' => 'sts_trx'),        
            array( 'db' => '`a`.`types`', 'dt' => 16,'field' => 'types'),        
            array( 'db' => '`a`.`slug`', 'dt' => 17,'field' => 'slug'),

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );

    }elseif(isset($_GET['viewNote'])){

        $data = getNote('note_secondary',$id);

    ?>

        <div class="form-group">
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Editor</th>
                        <th scope="col">Note</th>                
                        <th scope="col">Date</th>     
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no = 1;
                    foreach($data as $value){ 
                    ?>
                        <tr>
                            <td><?=$no++?></td>
                            <td><?=getUser($value['editor']);?></td>
                            <td><?=$value['note'];?></td>
                            <td><?=$value['insertDate'];?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>

    <?php

        //mysqli_close($conn);

    }elseif(isset($_GET['view'])){

        $data = getDetail('secondary',$id); 
        $dataSales = getSales($data['idSales']);

        $arrPict = array(
            $data['photo1'],
            $data['photo2'],
            $data['photo3'],
            $data['photo4'],
            $data['photo5'],
            $data['photo6'],
            $data['photo7'],
            $data['photo8']
        );
        
        $arrPict = (array_filter($arrPict));

    ?>
            
        <?php if($data['sts_trx']==1){ ?>
            <img height=300px width=300px src=../images/icon/rented.png class=sts_trx_view>
        <?php }elseif($data['sts_trx']==2){ ?>
            <img height=300px width=300px src=../images/icon/sold.png class=sts_trx_view>
        <?php } ?>

        <div class="form-group">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php 
                        $no = 0;
                        foreach($arrPict as $value){ 
                        $no++;  
                        ?>                    
                        <li data-target="#myCarousel" data-slide-to="<?=$no?>" <?php if($no==1){echo 'class="active"';} ?> ></li>
                    <?php } ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php 
                    $no = 0;
                    foreach($arrPict as $value){ 
                    $no++;  
                    ?>                    
                        <div class="item <?php if($no==1){echo 'active';} ?>">
                            <img src="<?=$imgSecondaryFolder.$value; ?>" alt="<?=$value?>" style="width:100%;height:70%;" >
                        </div>
                    <?php } ?>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        
        <div class="pull-right">
            <a href=# onclick="copyClipboard()">copy <i class="fa fa-clone fa-1x"></i></a>
        </div>
        
        <table class="table">
            <tbody>
                <tr>
                    <td width="200">Sales</td><td><?php echo $dataSales['fullName']; ?> - <?php echo $dataSales['phone']; ?></td>
                </tr>
            </tbody>
        </table>

        <div class="code-bg" id="divClipboard">                
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width="200">Judul</td><td><?=$data['title'].' - SH'.$data['id'];?></td>
                    </tr>                            
                    <tr>
                        <td>Kategori properti</td><td><?php echo getCategory($data['category']); ?></td>
                    </tr>
                    <tr>
                        <td>Kebutuhan</td><td><?php echo getNeed($data['types']); ?></td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td><?php 
                        $lokasi = explode('.',$data['wilayah_indonesia']);
                        $prop = $lokasi[0];
                        $kab = $lokasi[1];
                        $kec = $lokasi[2];
                        $kel = $lokasi[3];

                        echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));
                        echo ",";
                        echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab)));
                        echo ",";
                        echo ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec)));
                        echo ",";
                        echo ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Harga</td><td><?php echo "Rp. ".number_format((float)$data['price']); ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td><td><?php echo $data['addres']; ?></td>
                    </tr>
                    <tr>
                        <td>Sertifikat</td><td><?php echo getSertifikat($data['certified']); ?></td>
                    </tr>
                    <tr>
                        <td>Kamar Tidur</td><td><?php echo getRoom($data['bedroom']); ?></td>
                    </tr>
                    <tr>
                        <td>Kamar Mandi</td><td><?php echo getRoom($data['bathroom']); ?></td>
                    </tr>
                    <tr>
                        <td>Kamar Pembantu</td><td><?php echo getRoom($data['maidsroom']); ?></td>
                    </tr>
                    <tr>
                        <td>Garasi</td><td><?php echo getRoom($data['garage']); ?></td>
                    </tr>
                    <tr>
                        <td>Car Port</td><td><?php echo getRoom($data['carPort']); ?></td>
                    </tr>
                    <tr>
                        <td>Luas Tanah</td><td><?php echo $data['surface']." m2"; ?></td>
                    </tr>
                    <tr>
                        <td>Luas Bangunan</td><td><?php echo $data['building']." m2"; ?></td>
                    </tr>
                    <tr>
                        <td>Lantai</td><td><?php echo $data['flor']; ?></td>
                    </tr>
                    <tr>
                        <td>Bangunan Menghadap</td><td><?php echo getFacing($data['facing']); ?></td>
                    </tr>
                    <tr>
                        <td>Informasi Tambahan</td><td><?php echo $data['descript']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <table class="table">
            <tbody>
                <tr>
                    <td width="200">Dilihat</td><td><?php echo $data['viewer']; ?></td>
                </tr>
            </tbody>
        </table>

    <?php

        //mysqli_close($conn);
        
    }elseif(isset($_GET['approve'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        mysqli_query($conn,"UPDATE secondary SET approved = '1', approvedDate = '$date', editor = '$_SESSION[idUser]' WHERE id = '$id'");
        mysqli_close($conn);
    }elseif(isset($_GET['downloadImg'])){
        
        $conn = db_connection();
        $query = mysqli_query($conn,"SELECT photo1,photo2,photo3,photo4,photo5,photo6,photo7,photo8 FROM secondary WHERE id = '$id' ");
        while($row = mysqli_fetch_assoc($query)){
            $result[] = $row['photo1'];
            $result[] = $row['photo2'];
            $result[] = $row['photo3'];
            $result[] = $row['photo4'];
            $result[] = $row['photo5'];
            $result[] = $row['photo6'];
            $result[] = $row['photo7'];
            $result[] = $row['photo8'];
        }
        //return $result;

        echo json_encode(array_filter($result));

    }elseif(isset($_GET['suspend'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        mysqli_query($conn,"UPDATE secondary SET approved = '2', suspendDate = '$date', editor = '$_SESSION[idUser]' WHERE id = '$id'");
        mysqli_close($conn);
    }elseif(isset($_GET['note'])){
        $conn = db_connection();
        mysqli_query($conn,"INSERT note_secondary SET note = '$_POST[note]', editor = '$_SESSION[idUser]', idSecondary = '$_POST[id]'");
        mysqli_close($conn);
    }elseif(isset($_GET['rented'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE secondary SET sts_trx = '1', editor = '$_SESSION[idUser]' WHERE id = '$id'");
        mysqli_close($conn);
    }elseif(isset($_GET['sold'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE secondary SET sts_trx = '2', editor = '$_SESSION[idUser]' WHERE id = '$id'");
        mysqli_close($conn);
    }elseif(isset($_GET['available'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE secondary SET sts_trx = '0', editor = '$_SESSION[idUser]' WHERE id = '$id'");
        mysqli_close($conn);
    }elseif(isset($_GET['availabless'])){
       
        function createZip($files = array(), $destination = '', $overwrite = false) {
            if(file_exists($destination) && !$overwrite) { return false; }
                $validFiles = [];
        
            if(is_array($files)) {
        
            foreach($files as $file) {
        
                if(file_exists($file)) {
        
                    $validFiles[] = $file;
        
                }
        
            }
        
            }
        
        
            if(count($validFiles)) {
        
            $zip = new ZipArchive();
        
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
        
                return false;
        
            }
        
        
            foreach($validFiles as $file) {
        
                $zip->addFile($file,$file);
        
            }
        
        
            $zip->close();
        
            return file_exists($destination);
        
            }else{
        
            return false;
        
            }
        
        }
        
        
        $fileName = 'my-archive.zip';
        
        $files_to_zip = ['demo1.jpg', 'demo2.jpg'];
        
        $result = createZip($files_to_zip, $fileName);
        
        
        header("Content-Disposition: attachment; filename=\"".$fileName."\"");
        
        header("Content-Length: ".filesize($fileName));
        
        readfile($fileName);
    }

    
}
?>