<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";

    $imgNewsFolder = "../../../api/images/article/";

    $id = @$_GET['id'];
    $groupBy    = " idEvent ";

    if(isset($_GET['list'])){

        $primaryKey = 'id';
        $table = 'invitation';
        $joinQuery = " FROM `{$table}` AS `a` ";

        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`a`.`title`', 'dt' => 1, 'field' => 'title' ),
            //array( 'db' => '`a`.`target`', 'dt' => 2, 'field' => 'target' ),
            //array( 'db' => '`a`.`area`', 'dt' => 3, 'field' => 'area' ),
            array( 
                'db' => '`a`.`target`',
                'dt' => 2,
                'field' => 'target',
                'formatter' => function($b) {
                        return ucwords($b);
                    }
            ),
            array( 
                'db' => '`a`.`area`',
                'dt' => 3,
                'field' => 'area',
                'formatter' => function($a) {
                        return ucwords($a);
                    }
            ),
            array( 
                'db' => '`a`.`id`',
                'dt' => 4,
                'field' => 'id',
                'formatter' => function($c) {
                        $res = number_format(dbGet('invitation_result','idEvent = '.$c.'' ));
                        return $res;
                    }
            ),
            //array( 'db' => '`a`.`msg`', 'dt' => 4, 'field' => 'msg' ),
            array( 
                'db' => '`a`.`start_date`',
                'dt' => 5,
                'field' => 'start_date',
                'formatter' => function($d) {
                        $res = date("d-m-Y",strtotime($d));
                        return $res;
                    }
            ),
            array( 
                'db' => '`a`.`end_date`',
                'dt' => 6,
                'field' => 'end_date',
                'formatter' => function($e) {
                        $res = date("d-m-Y",strtotime($e));
                        return $res;
                    }
            ),
            //array( 'db' => '`a`.`start_date`', 'dt' => 5, 'field' => 'start_date' ),
            //array( 'db' => '`a`.`end_date`', 'dt' => 6, 'field' => 'end_date' ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 7, 'field' => 'insertDate' ),
            array( 
                'db' => '`a`.`id`',
                'dt' => 8,
                'field' => 'id',
                'formatter' => function($f) {
                        $res = number_format(dbGet('invitation_result',' send = 1 AND idEvent = '.$f.'' ));
                        return $res;
                    }
            ),
        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, '', '')
        );
        
    }elseif(isset($_GET['generate'])){

        $conn = db_connection();

        $query = mysqli_query($conn," SELECT * FROM invitation WHERE id = '$id' ");
        $row = mysqli_fetch_assoc($query);

        $target = explode(', ',$row['target']);
        $targetCount = count($target);

        $area = explode(', ',$row['area']);
        $areaCount = count($area);

        $startDate = $row['start_date'];
        $endDate = $row['end_date'];
        
        $rangeDate = "AND (insertDate BETWEEN '".$startDate."' AND '".$endDate."')";

        if(in_array("all",$area)){
            $rangeArea = "";
        }elseif(in_array("nonjabodetabek",$area)){
            $rangeArea = " AND (domicile == '86' OR idCity != '11' OR idCity != '12' OR idCity != '15' OR idCity != '16' OR idCity != '17' OR idCity != '18' OR idCity != '19' OR idCity != '23' OR idCity != '24' OR idCity != '27' OR homeTown NOT LIKE '%31' OR homeTown NOT LIKE '%32.16' OR homeTown NOT LIKE '%32.01' OR homeTown NOT LIKE '%32.75' OR homeTown NOT LIKE '%32.71' OR homeTown NOT LIKE '%32.76' ) ";
        }else{
            $rangeArea = " AND ( ";
            for($a=0;$a<$areaCount;$a++){

                if($area[$a]=="jabodetabek"){
                    $rangeArea .= " (domicile != '86' OR idCity = '11' OR idCity = '12' OR idCity = '15' OR idCity = '16' OR idCity = '17' OR idCity = '18' OR idCity = '19' OR idCity = '23' OR idCity = '24' OR idCity = '27' OR homeTown LIKE '%31' OR homeTown LIKE '%32.16' OR homeTown LIKE '%32.01' OR homeTown LIKE '%32.75' OR homeTown LIKE '%32.71' OR homeTown LIKE '%32.76' ) ";
                }
                if($area[$a]=="yogjakarta"){
                    $rangeArea .= " (homeTown LIKE '%34%') ";
                }
                if($area[$a]=="surabaya"){
                    $rangeArea .= " (homeTown LIKE '%35.78%') ";
                }
                if($area[$a]=="bandung"){
                    $rangeArea .= " (homeTown LIKE '%32.73%' OR homeTown LIKE '%32.04%' OR homeTown LIKE '%32.80%') ";
                }

                if($areaCount-1>$a){ $rangeArea .= " OR "; }
            }

            $rangeArea .= " ) ";
        }

        if(in_array("all",$target)){
            $rangeTarget = "";
        }else{
            $rangeTarget = " AND ( ";
            for($b=0;$b<$targetCount;$b++){
                if($target[$b]=="finder"){
                    $rangeTarget .= " (position = 1 OR position = '') ";
                }
                if($target[$b]=="closer"){
                    $rangeTarget .= " position = 2 ";
                }
                if($target[$b]=="manager"){
                    $rangeTarget .= " position = 3 ";
                }

                if($targetCount-1>$b){ $rangeTarget .= " OR "; }
            }
            $rangeTarget .= " ) ";
        }

        //print_r($rangeTarget);
      
        $query = mysqli_query($conn," SELECT * FROM sales WHERE (active = 0 OR active = 1) $rangeArea $rangeTarget $rangeDate ");
        while($row = mysqli_fetch_assoc($query)){
            
            mysqli_query($conn,"INSERT INTO invitation_result SET
                idEvent = '$id',
                idSales = '$row[idSales]'
            ");

        }

        mysqli_close($conn);
        
    }elseif(isset($_GET['del'])){

        $conn = db_connection();
        mysqli_query($conn,"DELETE FROM invitation_result WHERE idEvent = '$id' ");
        mysqli_query($conn,"DELETE FROM invitation WHERE id = '$id' ");

        mysqli_close($conn);

    }elseif(isset($_GET['detail'])){
        $data = getDetail('invitation',$id); 
        
    ?>
        <table class="table table-striped">
            <tbody>
                <tr><td width="200">Judul</td><td><?=$data['title'];?></td></tr>                            
                <tr><td>Area</td><td><?=ucwords($data['area']);?></td></tr>
                <tr><td>Target</td><td><?=ucwords($data['target']);?></td></tr>
                <tr><td>Date Range</td><td><?=date("d-m-Y",strtotime($data['start_date']));?> s/d <?=date("d-m-Y",strtotime($data['end_date']));?></td></tr>
                <tr><td>Message</td><td><pre><?=$data['msg'];?></pre></td></tr>
                <tr><td>Jangkauan</td><td><?=number_format(dbGet('invitation_result','idEvent = '.$id.'' ));?> Orang - <?=number_format(dbGet('invitation_result','send = 1 AND idEvent = '.$id.'' ));?> Terkirim</td></tr>
            </tbody>
        </table>
    <?php
    }
}
?>