<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/function.php";
    include "../../library/queryAdmin.php";
    include "../../library/PassHash.php";

    $idSales = @$_GET['id'];
    $idEvent = @$_GET['idEvent'];
    $area = @$_GET['area'];

    if(@$_GET['sts2']=='finder2'){
        $extraWhere = " `b`.`office` = '' AND `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' AND (`b`.`position` = '' || `b`.`position` = '1' ) ";
    }else if(@$_GET['sts2']=='closer2'){
        //$extraWhere = " `b`.`office` = '' AND `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' AND `b`.`position` = '2' ";
        $extraWhere = " `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' AND `b`.`position` = '2' ";
    }else if(@$_GET['sts2']=='agent2'){
        $extraWhere = " `b`.`office` != '' AND `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' ";
    }else if(@$_GET['sts2']=='coordinator2'){
        $extraWhere = " `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' AND `b`.`position` = '3' ";
    }else if(@$_GET['sts2']=='dc'){
        $extraWhere = " `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' AND `a`.`join_closer` = '1' ";
    }else if(@$_GET['sts2']=='ph'){ 

        $conn = db_connection();
        $query = mysqli_query($conn,"SELECT idSales FROM event_participant WHERE `idEvent` = '$idEvent' AND `area` = '$area' ");
        while($row = mysqli_fetch_assoc($query)){
            $result[] = $row['idSales'];
        }
        
        if(empty($result)){$res = 0;}else{$res = implode(", ",$result);}
        
        $extraWhere = " `a`.idSales IN ($res) ";
        $groupBy = " `a`.idSales ";  
        $having = " COUNT(`a`.idSales) = 1 ";
    }else{
        $extraWhere = " `b`.`office` = '' AND `a`.`idEvent` = '$idEvent' AND `a`.`area` = '$area' ";
    }

    if(isset($_GET['list'])){
        $primaryKey = 'id';
        $table = 'event_participant';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)
            ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`b`.`idSales`', 'dt' => 1, 'field' => 'idSales' ),
            array( 'db' => '`b`.`fullName`', 'dt' => 2, 'field' => 'fullName' ),
            array( 
                'db' => '`b`.`phone`',
                'dt' => 4,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 5, 'field' => 'insertDate' ),
            array( 
                'db' => '`b`.`idSales`',
                'dt' => 6,
                'field' => 'idSales',
                'formatter' => function($d) {
                        return getLastLogin($d);
                    }
            ),
            array( 'db' => '`b`.`position`',  'dt' => 7, 'field' => 'position' ),
            array( 'db' => '`b`.`active`',  'dt' => 8, 'field' => 'active' ),
            array( 'db' => '`b`.`email`',  'dt' => 9, 'field' => 'email' ),
            array( 'db' => '`a`.`note`', 'dt' => 10, 'field' => 'note' ),   
            array( 'db' => '`a`.`join_closer`', 'dt' => 11, 'field' => 'join_closer' ),   

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
        );

    }elseif(isset($_GET['del'])){
        $conn = db_connection();
        mysqli_query($conn," DELETE FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        mysqli_close($conn);

    }elseif(isset($_GET['note'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE event_participant SET note = '$_POST[note]' WHERE id = '$_POST[id]' ");
        mysqli_close($conn);
    }elseif(isset($_GET['joinCloser'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE event_participant SET join_closer = 1 WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        mysqli_close($conn);
    }elseif(isset($_GET['cancelCloser'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE event_participant SET join_closer = '' WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        mysqli_close($conn);
    }
}
?>