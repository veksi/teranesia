<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/function.php";
    include "../../library/queryAdmin.php";
    include "../../library/PassHash.php";

    $idSales = @$_GET['id'];
    $idEvent = @$_GET['idEvent'];
    $source = @$_GET['source'];
    $groupBy    = " a.idSales ";
    $extraWhere = "";
    //if($_GET['sts']=='closer'){ 
    //    $extraWhere = " active = 1 AND office = '' AND position = '2' ";
    //}elseif($_GET['sts']=='agent'){ 
     //   $extraWhere = " active = 1 AND office != '' ";
    //}elseif($_GET['sts']=='coordinator'){ 
    //    $extraWhere = " active = 1 AND office = '' AND position = '3' ";
    //}else{
        //$extraWhere = " active = 1 ";
    //}

    if(isset($_GET['list'])){
        $primaryKey = 'id';
        $table = 'event_participant';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)
            ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`a`.`idSales`', 'dt' => 1, 'field' => 'idSales' ),
            array( 'db' => '`b`.`fullName`', 'dt' => 2, 'field' => 'fullName' ),
            array( 'db' => '`b`.`domicile`', 'dt' => 3, 'field' => 'domicile' ),
            array( 
                'db' => '`a`.`idSales`',
                'dt' => 4,
                'field' => 'idSales',
                'formatter' => function($c) {
                        $res = number_format(dbGet('event_participant','idSales = '.$c.'' ));
                        return $res;
                    }
            ),    
            array( 'db' => '`a`.`insertDate`', 'dt' => 5, 'field' => 'insertDate' ),
            array( 
                'db' => '`b`.`phone`',
                'dt' => 6,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            //array( 'db' => '`a`.`position`',  'dt' => 7, 'field' => 'position' ),
            array( 
                'db' => '`b`.`position`',
                'dt' => 7,
                'field' => 'position',
                'formatter' => function($a) {
                        return stsPosSales($a);
                    }
            ),    
            array( 'db' => '`b`.`active`',  'dt' => 8, 'field' => 'active' ),
            array( 'db' => '`b`.`phone`',  'dt' => 9, 'field' => 'phone' ),
        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );

    }elseif(isset($_GET['present'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '1', actual_attend = '1', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]', source = '$source' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '1', actual_attend = '1', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]', source = '$source' ");
        }
        mysqli_close($conn);
    }elseif(isset($_GET['presentPlan'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '1', actual_attend = '0', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '1', actual_attend = '0', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }
        mysqli_close($conn);
    }elseif(isset($_GET['notPresent'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '0', actual_attend = '0', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '0', actual_attend = '0', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }
        mysqli_close($conn);
    }
}
?>