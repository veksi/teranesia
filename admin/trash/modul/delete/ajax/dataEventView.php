<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/function.php";
    include "../../library/queryAdmin.php";
    include "../../library/PassHash.php";

    $idSales = @$_GET['id'];
    $idEvent = @$_GET['idEvent'];
    $source = @$_GET['source'];
    $area = @$_GET['area'];

    //if($_GET['sts']=='closer'){ 
    //    $extraWhere = " active = 1 AND office = '' AND position = '2' ";
    //}elseif($_GET['sts']=='agent'){ 
     //   $extraWhere = " active = 1 AND office != '' ";
    //}elseif($_GET['sts']=='coordinator'){ 
    //    $extraWhere = " active = 1 AND office = '' AND position = '3' ";
    //}else{
        $extraWhere = " active != 2 ";
    //}

    if(isset($_GET['list'])){
        $primaryKey = 'idSales';
        $table = 'sales';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `domicile` AS `b` ON (`a`.`domicile` = `b`.`idDomicile`) 
            LEFT JOIN `city` AS `c` ON (`a`.`idCity` = `c`.`idCity`)
            LEFT JOIN `wilayah_indonesia` AS `d` ON (`a`.`homeTown` = `d`.`lokasi_kode`)            
            ";
        $columns = array(
            array( 'db' => '`a`.`idSales`', 'dt' => 0, 'field' => 'idSales' ),
            array( 'db' => '`a`.`idSales`', 'dt' => 1, 'field' => 'idSales' ),
            array( 'db' => '`a`.`fullName`', 'dt' => 2, 'field' => 'fullName' ),
            array( 'db' => '`b`.`domicile`', 'dt' => 3, 'field' => 'domicile' ),
            array( 
                'db' => '`a`.`phone`',
                'dt' => 4,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 5, 'field' => 'insertDate' ),
            array( 
                'db' => '`a`.`idSales`',
                'dt' => 6,
                'field' => 'idSales',
                'formatter' => function($d) {
                        return getLastLogin($d);
                    }
            ),
            array( 'db' => '`a`.`position`',  'dt' => 7, 'field' => 'position' ),
            /*array( 
                'db' => '`a`.`position`',
                'dt' => 7,
                'field' => 'position',
                'formatter' => function($a) {
                        return stsPosSales($a);
                    }
            ),    */
            array( 'db' => '`a`.`active`',  'dt' => 8, 'field' => 'active' ),
            array( 'db' => '`a`.`email`',  'dt' => 9, 'field' => 'email' ),
            array( 'db' => '`c`.`city`', 'dt' => 10, 'field' => 'city' ),
            array( 'db' => '`d`.`wilayah`', 'dt' => 11, 'field' => 'wilayah' ),
            array( 'db' => '`a`.`phone`', 'dt' => 12, 'field' => 'phone' ),
                   

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );

    }elseif(isset($_GET['present'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '1', actual_attend = '1', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]', source = '$source', area = '$area' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '1', actual_attend = '1', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]', source = '$source', area = '$area'  ");
        }
        mysqli_close($conn);
    }elseif(isset($_GET['presentPlan'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '1', actual_attend = '0', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '1', actual_attend = '0', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }
        mysqli_close($conn);
    }elseif(isset($_GET['notPresent'])){
        $date = date("Y-m-d H:i:s");
        $conn = db_connection();
        $query = mysqli_query($conn," SELECT * FROM event_participant WHERE idSales = '$idSales' AND idEvent = '$idEvent' ");
        $result = mysqli_num_rows($query);
        if($result==1){
            $sql = mysqli_query($conn,"UPDATE event_participant SET attend = '0', actual_attend = '0', insertDate = '$date' WHERE idSales = '$idSales' AND idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }else{
            $sql = mysqli_query($conn,"INSERT INTO event_participant SET attend = '0', actual_attend = '0', insertDate = '$date', idSales = '$idSales', idEvent = '$idEvent', editor = '$_SESSION[idUser]' ");
        }
        mysqli_close($conn);
    }
}
?>