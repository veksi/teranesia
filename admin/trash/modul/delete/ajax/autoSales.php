<?php
include "../../setting/connection.php";
include "../../library/queryAdmin.php";
include "../../library/function.php";

// Set header type konten.
header("Content-Type: application/json; charset=UTF-8");

$host = "localhost";
$user="stat2576_starhome";
$password="starhome2017";
$db="stat2576_starsales";
//$conn = mysqli_connect($host,$user,$password,$db);

// Koneksi ke database.
$conn = new mysqli($host,$user,$password,$db);
$search = $_GET["query"];

$query  = $conn->query("SELECT * FROM sales WHERE fullName LIKE '%$search%' OR phone LIKE '%$search%' ORDER BY fullName ASC");
$result = $query->fetch_All(MYSQLI_ASSOC);

// Cek apakah ada yang cocok atau tidak.
if (count($result) > 0) {
    foreach($result as $data) {
        $output['suggestions'][] = [
            'id' => $data['idSales'],
            'value'  => $data['fullName'].' ('.hp($data['phone']).')'
        ];
    }

    // Encode ke JSON.
    echo json_encode($output);

// Jika tidak ada yang cocok.
} else {
    $output['suggestions'][] = [
        'id' => '',
        'value'  => ''
    ];

    // Encode ke JSON.
    echo json_encode($output);
}
