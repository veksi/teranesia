<?php
session_name("admin");
session_start();

include "../../setting/connection.php";
include "../../library/queryAdmin.php";

//$imgNewsFolder = "../../../api/images/article/";

$id = @$_GET['id'];

if(isset($_GET['list'])){

    $table = 'principal_office';
    $primaryKey = 'id';

    $columns = array(

        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'office_name', 'dt' => 1 ),
        array( 'db' => 'logo', 'dt' => 2 ),
        array( 'db' => 'phone', 'dt' => 3 ),
        array( 'db' => 'address',  'dt' => 4 ),
        array( 'db' => 'insertDate',  'dt' => 5 ),   
        array( 'db' => 'category',  'dt' => 6 ),
        array( 'db' => 'area',  'dt' => 7 ),
        array( 
            'db' => 'editor',
            'dt' => 8,
            'formatter' => function($d) {
                    return getUser($d);
                }
            ),
        array( 'db' => 'publish',  'dt' => 9 ),
        array( 'db' => 'situs',  'dt' => 10 ),
        
    );

    require( 'ssp.class.php' );

    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    );

}elseif(isset($_GET['view'])){

	$conn = db_connection();
    $query = mysqli_query($conn,"SELECT * FROM article WHERE idArticle = '$id' ");
    $row = mysqli_fetch_assoc($query);
    echo json_encode($row);
    
    mysqli_close($conn);
    
}elseif(isset($_GET['on'])){

    $conn = db_connection();
    mysqli_query($conn,"UPDATE article SET publish = '1' WHERE idArticle = '$id' ");

    mysqli_close($conn);
    
}elseif(isset($_GET['off'])){

    $conn = db_connection();
    mysqli_query($conn,"UPDATE article SET publish = '0' WHERE idArticle = '$id' ");

    mysqli_close($conn);
    
}elseif(isset($_GET['del'])){

    //global $imgNewsFolder;
    $conn = db_connection();

    $query = mysqli_query($conn,"SELECT pict FROM article WHERE idArticle = '$id' ");
    $row = mysqli_fetch_assoc($query);
    
    if( (!empty($row['pict'])) ){
        unlink($imgNewsFolder.$row['pict']);
    }

    mysqli_query($conn,"DELETE FROM article WHERE idArticle = '$id' ");

    mysqli_close($conn);
    
}

?>