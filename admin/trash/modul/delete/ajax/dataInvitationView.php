<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/function.php";
    include "../../library/queryAdmin.php";

    $imgNewsFolder = "../../../api/images/article/";

    $idSales = @$_GET['idSales'];
    $idEvent = @$_GET['idEvent'];
    //$area = @$_GET['area'];
    //$extraWhere = " idEvent = '$idEvent' AND area = '$area' ";
    $extraWhere = " idEvent = '$idEvent' ";

    if(isset($_GET['list'])){

        $primaryKey = 'id';
        $table = 'invitation_result';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)
            LEFT JOIN `invitation` AS `c` ON (`c`.`id` = `a`.`idEvent`)
            ";

        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`b`.`fullName`', 'dt' => 1, 'field' => 'fullName' ),
            array( 
                'db' => '`b`.`phone`',
                'dt' => 2,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ),
            array( 'db' => '`a`.`send`', 'dt' => 3, 'field' => 'send' ),
            array( 'db' => '`c`.`msg`', 'dt' => 4, 'field' => 'msg' ),
            /*array( 
                'db' => '`c`.`msg`',
                'dt' => 4,
                'field' => 'msg',
                'formatter' => function($b) {

                    $newMsg = str_replace("[nama]", , $b);
                    return $newMsg;

                }
            ),*/
        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
        
    }elseif(isset($_GET['send'])){
        $conn = db_connection();
        mysqli_query($conn,"UPDATE invitation_result SET send = '1' WHERE id = '$idSales' ");
        mysqli_close($conn);
        
    }
}

?>