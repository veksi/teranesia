
<?php
session_name("admin");
session_start();

include "../../setting/connection.php";
include "../../library/queryAdmin.php";

$primaryKey = 'idProspect';
$table = 'prospect';
$joinQuery = " FROM `{$table}` AS `a` 
    LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)     
    ";

$columns = array(

    array( 'db' => '`a`.`idProspect`', 'dt' => 0, 'field' => 'idProspect' ),
    array( 'db' => '`b`.`fullName`', 'dt' => 1, 'field' => 'fullName' ),
    array( 'db' => '`a`.`name`', 'dt' => 2, 'field' => 'name' ),
    array( 'db' => '`a`.`phone`', 'dt' => 3, 'field' => 'phone' ),
    array( 'db' => '`a`.`insertDate`', 'dt' => 4, 'field' => 'insertDate' ),
    array( 
        'db' => '`a`.`editor`',
        'dt' => 5,
        'field' => 'editor',
        'formatter' => function($d) {
                return getUser($d);
            }
        ),
    array( 'db' => '`a`.`idSales`', 'dt' => 6, 'field' => 'idSales' ),
);

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, null)
);

?>
