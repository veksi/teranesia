<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";
    include "../../library/function.php";


    $id = @$_GET['id'];

    if(isset($_GET['list'])){

        $primaryKey = 'id';
        $table = 'buyer';
        $joinQuery = " FROM `{$table}` AS `a` 
            LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`) 
            LEFT JOIN `category_property` AS `c` ON (`a`.`category` = `c`.`idCategory`)
            LEFT JOIN `wilayah_indonesia` AS `d` ON (`a`.`wilayah_indonesia` = `d`.`lokasi_kode`)
            ";
        //$extraCondition = "`id_client`=".$ID_CLIENT_VALUE;

        $columns = array(

            array( 'db' => '`a`.`id`', 'dt' => 0, 'field' => 'id' ),
            array( 'db' => '`a`.`title`',  'dt' => 1, 'field' => 'title' ),
            array( 'db' => '`c`.`category`','dt' => 2, 'field' => 'category'), 
            /*array( 'db' => '`a`.`types`','dt' => 3, 'field' => 'types'), */
            array( 
                'db' => '`a`.`types`',
                'dt' => 3,
                'field' => 'types',
                'formatter' => function($a) {
                        if($a==1){
                            $need = "Beli";
                        }else if($a==2){
                            $need = "Sewa";
                        }else{
                            $need = "Unknown";
                        }
                        return $need;
                    }
                ),   
            array( 
                'db' => '`a`.`budget`',
                'dt' => 4,
                'field' => 'budget',
                'formatter' => function($b) {
                        return "Rp. ".number_format((float)$b);
                    }
                ),   
            array( 
                'db' => '`a`.`wilayah_indonesia`',
                'dt' => 5,
                'field' => 'wilayah_indonesia',
                'formatter' => function($c) {

                        $lokasi = explode('.',$c);
                        $prop = @$lokasi[0];
                        $kab = @$lokasi[1];
                        $kec = @$lokasi[2];
                        $kel = @$lokasi[3];

                        $area = 
                        ucwords(strtolower(getProp('wilayah_indonesia',$prop))).','.
                        ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab))).','.
                        ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec))).','.
                        ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));

                        return $area;
                    }
                ),  
            array( 'db' => '`a`.`insertDate`',  'dt' => 6,'field' => 'insertDate'),
            array( 'db' => '`a`.`approved`',  'dt' => 7,'field' => 'approved'),
            array( 'db' => '`b`.`fullName`', 'dt' => 8,'field' => 'fullName'),   
            array( 
                'db' => '`b`.`phone`',
                'dt' => 9,
                'field' => 'phone',
                'formatter' => function($a) {
                        return hp($a);
                    }
            ), 
            array( 'db' => '`d`.`lokasi_nama`', 'dt' => 10,'field' => 'lokasi_nama'),
            array( 'db' => '`a`.`code`', 'dt' => 11,'field' => 'code'),
            array( 'db' => '`a`.`descript`', 'dt' => 12,'field' => 'descript')

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, null)
        );

    }elseif(isset($_GET['view'])){

        //$conn = db_connection();
        //$query = mysqli_query($conn,"SELECT * FROM article WHERE idArticle = '$id' ");
        //$row = mysqli_fetch_assoc($query);

        $data = getDetail('buyer',$id); 
        $dataSales = getSales($data['idSales']);

    ?>
                
            <h3 class="card-title"><?php echo $data['title']; ?></h3>
            
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width="200">Sales</td><td><?php echo $dataSales['fullName']; ?> - <?php echo $dataSales['phone']; ?></td>
                    </tr>
                    <tr>
                        <td>Kategori properti</td><td><?php echo getCategory($data['category']); ?></td>
                    </tr>
                    <tr>
                        <td>Kebutuhan</td><td><?php echo getNeed($data['types']); ?></td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td><?php 
                        $lokasi = explode('.',$data['wilayah_indonesia']);
                        $prop = $lokasi[0];
                        $kab = $lokasi[1];
                        $kec = $lokasi[2];
                        $kel = $lokasi[3];

                        echo ucwords(strtolower(getProp('wilayah_indonesia',$prop)));
                        echo ",";
                        echo ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab)));
                        echo ",";
                        echo ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec)));
                        echo ",";
                        echo ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Budget</td><td><?php echo "Rp. ".number_format((float)$data['budget']); ?></td>
                    </tr>
                    <tr>
                        <td>Informasi Tambahan</td><td><?php echo $data['descript']; ?></td>
                    </tr>
                </tbody>
            </table>
    

    <?php
        
    }elseif(isset($_GET['approve'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE buyer SET approved = '1', editor = '$_SESSION[idUser]' WHERE id = '$id'");

        mysqli_close($conn);

    }elseif(isset($_GET['suspend'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE buyer SET approved = '2', editor = '$_SESSION[idUser]' WHERE id = '$id'");

        mysqli_close($conn);

    }

}

?>