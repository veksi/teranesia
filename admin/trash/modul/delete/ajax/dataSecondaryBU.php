
<?php

include "../../setting/connection.php";
include "../../library/queryAdmin.php";
include "../../library/function.php";

$table = 'secondary';
$primaryKey = 'id';

$columns = array(

    array( 'db' => 'id', 'dt' => 0 ),
    array( 'db' => 'photo1', 'dt' => 1 ),
    array( 'db' => 'title',  'dt' => 2 ),
    array( 
        'db' => 'category',
        'dt' => 3,
        'formatter' => function($a) {
                return getCategory($a);
            }
        ), 
    array( 'db' => 'price',  'dt' => 4 ), 
    array( 
        'db' => 'price',
        'dt' => 4,
        'formatter' => function($b) {
                return "Rp. ".number_format($b);
            }
        ),   
    array( 
        'db' => 'wilayah_indonesia',
        'dt' => 5,
        'formatter' => function($c) {

                $lokasi = explode('.',$c);
                $prop = $lokasi[0];
                $kab = $lokasi[1];
                $kec = $lokasi[2];
                $kel = $lokasi[3];

                $area = 
                ucwords(strtolower(getProp('wilayah_indonesia',$prop))).','.
                ucwords(strtolower(getKab('wilayah_indonesia',$prop,$kab))).','.
                ucwords(strtolower(getKec('wilayah_indonesia',$prop,$kab,$kec))).','.
                ucwords(strtolower(getKel('wilayah_indonesia',$prop,$kab,$kec,$kel)));

                return $area;
            }
        ),  
    array( 'db' => 'insertDate',  'dt' => 6),
    array( 'db' => 'approved',  'dt' => 7),
    array( 'db' => 'imgValid',  'dt' => 8),
    array( 
        'db' => 'idSales',
        'dt' => 9,
        'formatter' => function($d) {

                $dataSales = getSales($d);
                return $dataSales['fullName'].' ('.$dataSales['phone'].')';
            }
        ),   

);

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

?>

