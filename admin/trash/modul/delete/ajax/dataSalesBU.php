
<?php

include "../../setting/connection.php";
include "../../library/queryAdmin.php";

$table = 'sales';
$primaryKey = 'idSales';

$columns = array(

    array( 'db' => 'idSales', 'dt' => 0 ),
    array( 'db' => 'idSales', 'dt' => 1 ),
    array( 'db' => 'fullName', 'dt' => 2 ),
    array( 
        'db' => 'domicile',
        'dt' => 3,
        'formatter' => function($a) {
                return getDomicile($a);
            }
        ),
    array( 'db' => 'phone',  'dt' => 4 ),
    array( 'db' => 'insertDate',  'dt' => 5 ),   
    array( 
        'db' => 'activatedBy',
        'dt' => 6,
        'formatter' => function($d) {
                return getUser($d);
            }
        ),
    array( 'db' => 'position',  'dt' => 7 ),
    array( 'db' => 'active',  'dt' => 8 ),
    array( 'db' => 'email',  'dt' => 9 ),
    array( 'db' => 'active',  'dt' => 10 ),
    array( 
        'db' => 'idCity',
        'dt' => 11,
        'formatter' => function($e) {
                return getCity($e);
            }
        ),
);

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

?>
