<?php
session_name("admin");
session_start();

include "../../setting/connection.php";
include "../../library/queryAdmin.php";
include "../../library/function.php";

$primaryKey = 'idTrx';
$table = 'trx';
$joinQuery = " FROM `{$table}` AS `a` 
    LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)     
    LEFT JOIN `product` AS `c` ON (`a`.`idProduct` = `c`.`idProduct`)     
    LEFT JOIN `prospect` AS `d` ON (`a`.`idProspect` = `d`.`idProspect`)
    ";

$columns = array(

    array( 'db' => '`a`.`idTrx`', 'dt' => 0, 'field' => 'idTrx' ),
    array( 'db' => '`b`.`fullName`', 'dt' => 1, 'field' => 'fullName' ),
    array( 'db' => '`d`.`name`', 'dt' => 2, 'field' => 'name' ),
    array( 'db' => '`c`.`title`', 'dt' => 3, 'field' => 'title' ),
    array( 'db' => '`a`.`stsSales`', 'dt' => 4, 'field' => 'stsSales' ),
    array( 
        'db' => '`a`.`nominal`',
        'dt' => 5,
        'field' => 'nominal',
        'formatter' => function($b) {
                return "Rp. ".number_format((float)$b);
            }
        ),
    array( 
        'db' => '`a`.`progress`',
        'dt' => 6,
        'field' => 'progress',
        'formatter' => function($c) {
                return getProgressTrx($c);
            }
        ),
    array( 'db' => '`a`.`insertDate`', 'dt' => 7, 'field' => 'insertDate' ),
    array( 'db' => '`a`.`publish`', 'dt' => 8, 'field' => 'publish' ),
    array( 'db' => '`b`.`idSales`', 'dt' => 9, 'field' => 'idSales' ),
    array( 'db' => '`a`.`publish`', 'dt' => 10, 'field' => 'publish' ),
    array( 
        'db' => '`d`.`phone`',
        'dt' => 11,
        'field' => 'phone',
        'formatter' => function($d) {
                return hp($d);
            }
    ),
);

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, null)
);

?>
