<?php
// Set header type konten.
header("Content-Type: application/json; charset=UTF-8");

// Deklarasi variable untuk koneksi ke database.
$host     = "localhost";// Server database
$username = "stat2576_starhome";     // Username database
$password = "starhome2017";     // Password database
$database = "stat2576_starsales";     // Nama database

// Koneksi ke database.
$conn = new mysqli($host, $username, $password, $database);

// Deklarasi variable keyword buah.
$namaSales = $_GET["query"];

// Query ke database.
$query  = $conn->query("SELECT * FROM prospect WHERE name LIKE '%$namaSales%' OR phone LIKE '%$namaSales%' ORDER BY name DESC");

// Fetch hasil query.
$result = $query->fetch_All(MYSQLI_ASSOC);

// Cek apakah ada yang cocok atau tidak.
if (count($result) > 0) {
    foreach($result as $data) {
        $output['suggestions'][] = [
            //'value' => $data['fullName'],
            //'buah'  => $data['fullName']
            'id'    => $data['idProspect'],
            'value' => $data['name'].' ('.$data['phone'].')'
        ];
    }

    // Encode ke JSON.
    echo json_encode($output);

} else {
    $output['suggestions'][] = [
        'id' => '',
        'value'  => ''
    ];

    // Encode ke JSON.
    echo json_encode($output);
}
