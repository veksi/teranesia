<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";

    $id = @$_GET['id'];
    $idSales = @$_GET['idSales'];

    if(isset($_GET['list'])){

        $table = 'closer';
        $primaryKey = 'id';
        $joinQuery = " FROM `{$table}` AS `a` 
        LEFT JOIN `sales` AS `b` ON (`a`.`idSales` = `b`.`idSales`)
        LEFT JOIN `master_paid` AS `c` ON (`a`.`status` = `c`.`id`)        
        ";

        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 'db' => '`b`.`fullName`', 'dt' => 1,'field' => 'fullName'),
            array( 'db' => '`a`.`manager`', 'dt' => 2,'field' => 'manager'),
            /*array( 
                'db' => '`a`.`manager`',
                'dt' => 2,
                'field' => 'manager',
                'formatter' => function($a) {
                        return getSales($a)['fullName'];
                    }
            ),*/
            array( 'db' => '`a`.`insertDate`', 'dt' => 3,'field' => 'insertDate'),
            array( 'db' => '`a`.`status`', 'dt' => 4,'field' => 'status'),
            array( 'db' => '`b`.`email`', 'dt' => 5,'field' => 'email'),
            array( 'db' => '`a`.`idSales`', 'dt' => 6,'field' => 'idSales'),
            array( 'db' => '`a`.`certified`', 'dt' => 7,'field' => 'certified'),
            array( 'db' => '`b`.`nik`', 'dt' => 8,'field' => 'nik'),
            array( 'db' => '`c`.`paid`', 'dt' => 9,'field' => 'paid'),
            array( 
                'db' => '`a`.`manager`',
                'dt' => 10,
                'field' => 'manager',
                'formatter' => function($a) {
                        return getSales($a)['fullName'];
                    }
            )
        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, '')
        );

    }elseif(isset($_POST['paid'])){
        //$dated = date("Y-m-d H:i:s");
        $conn = db_connection();
        mysqli_query($conn,"UPDATE closer SET status = '1',payDate = '$_POST[payDate]' WHERE idSales = '$_POST[idSales]' ");
        mysqli_query($conn,"UPDATE sales SET position = '2',joinPositionDate = '$_POST[payDate]' WHERE idSales = '$_POST[idSales]' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['unpaid'])){

        $conn = db_connection();
        //mysqli_query($conn,"UPDATE closer SET status = '0',manager = NULL WHERE id = '$id' ");
        mysqli_query($conn,"UPDATE closer SET status = '0' WHERE id = '$id' ");
        mysqli_query($conn,"UPDATE sales SET position = '1' WHERE idSales = '$idSales' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['certified'])){
        $dated = date("Y-m-d H:i:s");
        $conn = db_connection();
        mysqli_query($conn,"UPDATE closer SET certified = '1',certifiedDate = '$dated' WHERE id = '$id' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['notcertified'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE closer SET certified = '0' WHERE id = '$id' ");

        mysqli_close($conn);
        
    }elseif(isset($_POST['setManajer'])){
        $dated = date("Y-m-d H:i:s");
        $conn = db_connection();

        $query = mysqli_query($conn," SELECT idSales FROM closer WHERE idSales = '$_POST[idSales]' ");
        $result = mysqli_num_rows($query);
        if($result==0){
            $sql = mysqli_query($conn,"INSERT INTO closer SET idSales = '$_POST[idSales]', manager = '$_POST[coordinator]', insertDate = '$dated' ");
        }else{
            $sql = mysqli_query($conn,"UPDATE closer SET manager = '$_POST[coordinator]' WHERE idSales = '$_POST[idSales]' ");
        }
       
        mysqli_close($conn);
    }
}

?>