

<?php if(isset($_GET['id'])){ 
    $data = editTrx('trx',$_GET['id']);
?>

<script>

    $(document).ready(function() { 
        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("model").innerHTML = 'Edit Prospect Activation';
        document.getElementsByName("id")[0].value = '<?php echo $_GET['id']; ?>';
        document.getElementsByName("idSales")[0].value = '<?php echo $data['idSales']; ?>';
        document.getElementsByName("idProspect")[0].value = '<?php echo $data['idProspect']; ?>';
        document.getElementsByName("idProduct")[0].value = '<?php echo $data['idProduct']; ?>';
        document.getElementsByName("stsSales")[0].value = '<?php echo $data['stsSales']; ?>';
        document.getElementsByName("nominal")[0].value = '<?php echo $data['nominal']; ?>';
        document.getElementsByName("dateAdd")[0].value = '<?php echo $data['insertDate']; ?>';
    
    });

</script>

<?php } ?>        

<form action="library/qTrx.php" method="post" enctype="multipart/form-data">
    
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id'>

    <div class="form-group">
        <h4 class="pull-left"><b><div id="model">Add Prospect Activation</div></b></h4>
        <div class="pull-right">
            <button name=submit type="submit" class="btn btn-success">Save</button>
            <button type="button" onclick="goBack()" class="btn btn-info">Cancel</button>
        </div>
    </div>

    <div class="clear10"></div>

    <div class="form-group">
        <label>Sales name</label>
        <input type="text" id="namaSales" name="namaSales" class="form-control" placeholder="Ketik nama sales">
        <input type="text" id="idSales" name="idSales">
        
       
    </div>

    <div class="form-group">
        <label>Prospect name</label>
        <input type="text" id="namaProspek" name="namaProspek" class="form-control" placeholder="Ketik nama prospek" value="">        
        <input type="text" id="idProspect">
       

    </div>
    
    <div class="form-group">
        <label>Project name</label>
        <select id="idProduct" name="idProduct" class="form-control" class="defaultSelect">
            <option disabled selected hidden>Select project name</option>
            <?php  
            $combo = getComboProject('product');
            foreach($combo as $data){
            ?>
            <option value="<?php echo"$data[idProduct]"; ?>"><?php echo $data['title']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Status sales</label>
        <select name="stsSales" class="form-control">
            <option value="" disabled selected>Select status sales</option>
            <option value="1">Finder</option>
            <option value="2">Closer</option>
            <option value="3">Finder/Closer</option>
        </select>
    </div>

    <div class="form-group">
        <label>Nominal (Rp)</label>
        <input type="number" name="nominal" id="nominal" class="form-control" placeholder="2000000000" autocomplete="off" required><!--onChange="convert(this.value)"-->
    </div>

    <div class="form-group">
        <label>Date created</label>
        <div class='input-group date' id='datetimepicker2'>
            <input type='text' class="form-control" name="dateAdd" placeholder="Date created"/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
    </div>
    
</form>


<!--<script src="datetime/js/jQuery.js"></script>-->
<script src="datetime/js/bootstrap.min.js"></script>
<script src="datetime/js/moment.js"></script>
<script src="datetime/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function() {
        var dateNow = new Date();
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

        var dateNow = new Date();
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            defaultDate: dateNow
        });

    });
</script>
<!--
<script src="modul/ajax/jquery-3.2.1.min.js"></script>
<script src="modul/ajax/jquery.autocomplete.min.js"></script>-->

        <style>
           /*
            input[type=text] {
                border: 2px solid #bdbdbd;
                font-family: 'Roboto', Arial, Sans-serif;
            	font-size: 15px;
            	font-weight: 400;
                padding: .5em .75em;
                width: 300px;
            }
            input[type=text]:focus {
                border: 2px solid #757575;
            	outline: none;
            }*/
            .autocomplete-suggestions {
                border: 1px solid #999;
                background: #FFF;
                overflow: auto;
            }
            .autocomplete-suggestion {
                padding: 2px 5px;
                white-space: nowrap;
                overflow: hidden;
            }
            .autocomplete-selected {
                background: #F0F0F0;
            }
            .autocomplete-suggestions strong {
                font-weight: normal;
                color: #3399FF;
            }
            .autocomplete-group {
                padding: 2px 5px;
            }
            .autocomplete-group strong {
                display: block;
                border-bottom: 1px solid #000;
            }
        </style>
    

<script type="text/javascript">

    $( "#namaSales" ).autocomplete({
        serviceUrl: "modul/ajax/autoSales.php",   // Kode php untuk prosesing data.
        dataType: "JSON",           // Tipe data JSON.
        onSelect: function (suggestion) {
            $( "#namaSales" ).val(""+suggestion.value);
            $( "#idSales" ).val(""+suggestion.id);
            
        }
    });

    
    var nama = $("#namaProspek").val();
    var idSales = $('#idSales').val();

    $( "#namaProspek" ).autocomplete({ 
        serviceUrl: "modul/ajax/autoProspek.php",   // Kode php untuk prosesing data.
        dataType: "JSON",           // Tipe data JSON.
        params: {entity_type:$('#idSales').val()},
        onSelect: function (suggestion) {alert(idSales);
            $( "#namaProspek" ).val("" + suggestion.value);
            $( "#idProspect" ).val("" + suggestion.id);
        }
    });

</script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="//code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script>
$(function() {
    var idSales = $('#idSales').val();
  $( "#daftar_kota" ).autocomplete({
    source: 'modul/ajax/source.php?id='+idSales,
  });
});
</script>
<div class="ui-widget">
  <label for="daftar_kota">Kota: </label>
  <input id="daftar_kota">
</div>