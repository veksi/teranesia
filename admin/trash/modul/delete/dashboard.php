<script type="text/javascript">
    $(document).ready(function() {
       /* $("#divSales").load("https://www.starhome.co.id/admin/library/record_count.php?sales").fadeIn("slow");
        $("#divProject").load("https://www.starhome.co.id/admin/library/record_count.php?project").fadeIn("slow");
        $("#divProspect").load("https://www.starhome.co.id/admin/library/record_count.php?prospect").fadeIn("slow");
        $("#divTrx").load("https://www.starhome.co.id/admin/library/record_count.php?trx").fadeIn("slow");
        $("#divActivity").load("https://www.starhome.co.id/admin/library/record_count.php?activity").fadeIn("slow");
        /* var refreshId = setInterval(function() {
            $("#divSales").load("https://www.starhome.co.id/admin/library/record_count.php?sales").fadeIn("slow");
            $("#divProject").load("https://www.starhome.co.id/admin/library/record_count.php?project").fadeIn("slow");
            $("#divProspect").load("https://www.starhome.co.id/admin/library/record_count.php?prospect").fadeIn("slow");
            $("#divTrx").load("https://www.starhome.co.id/admin/library/record_count.php?trx").fadeIn("slow");
            $("#divActivity").load("https://www.starhome.co.id/admin/library/record_count.php?activity").fadeIn("slow");
        }, 10000);*/
    });
</script>

<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Dashboard</b></h4>
</div>

<div class="clear10"></div>
<?php //$time = date("H:i:s");
//echo $time; ?>
<!--Google page rank : <a href="https://smallseotools.com/google-pagerank-checker/" rel="nofollow noopener" title="https://smallseotools.com/google-pagerank-checker/"><img src="https://smallseotools.com/pr" align="absmiddle" alt="https://smallseotools.com/google-pagerank-checker/" border="0" /></a>-->
<div class="clear10"></div>
<div class="clear10"></div>
<div class="row text-center" >
    <div class="col-md-3">            
        <button type="button" class="btn btn-danger btn-circle btn-xl">
            <i class="fa fa-user text-white"></i>                
        </button>
        <h4>Finder <?=number_format(dbGet('sales','position = 1 '));?></h4>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Active</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('sales','active = 1 AND position = 1 '));?></div>
            <hr>
        </div>            
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Pending</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('sales','active = 0 AND position = 1 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Suspend</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('sales','active = 2 AND position = 1 '));?></div>
            <hr>
        </div>
    </div>
    <div class="col-md-3">            
        <button type="button" class="btn btn-warning btn-circle btn-xl">
            <i class="fa fa-user-plus text-white"></i>                
        </button>
        <h4>Closer <?=number_format(dbGet('closer',''));?></h4>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Paid</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','status = 1'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Unpaid</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','status != 1 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-primary">Certified</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer','certified = 1 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Uncertified</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer',"certified = '' OR certified IS NULL OR certified = 0 "));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-warning">Managerized</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer',"manager IS NULL "));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Unmanagerized</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('closer',"manager IS NOT NULL "));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> Bronze</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"level = 'bronzex' "));?></span></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> Silver</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"level = 'silver' "));?></span></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> Gold</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"level = 'gold' "));?></span></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><i class="fa fa-trophy" aria-hidden="true"></i> Platinum</div>
            <div class="col-md-6 text-right"><span class="badge"><?=number_format(dbGet('sales',"level = 'platinum' "));?></span></div>
            <hr>
        </div>
    </div>
    
    <div class="col-md-3">            
        <button type="button" class="btn btn-primary btn-circle btn-xl">
            <i class="fa fa-handshake-o text-white"></i>
        </button>
        <h4>Prospect <?=number_format(dbGet('prospect',''));?></h4>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Activation</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx',''));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Attempted</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx','progress = 0'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-warning">Contacted</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx','progress = 1'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-primary">Visited</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx','progress = 2'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Booked</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx','progress = 3'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Finished</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx','progress = 4'));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Undefine</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('trx'," progress IS NULL OR progress = '' "));?></div>
            <hr>
        </div>
    </div>
    <div class="col-md-3">            
        <button type="button" class="btn btn-warning btn-circle btn-xl">
            <i class="fa fa-hand-peace-o text-white"></i>            
        </button>
        <h4>Secondary <?=number_format(dbGet('secondary',''));?></h4>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-success">Active</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','approved = 1 '));?></div>
            <hr>
        </div>            
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-info">Pending</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','approved = 0 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Suspend</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','approved = 2 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-primary">Sold</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','approved = 1 AND sts_trx = 2 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-warning">Rented</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','approved = 1 AND sts_trx = 1 '));?></div>
            <hr>
        </div>
        <div class="row" style="padding:0 5">
            <div class="col-md-6 text-left"><span class="label label-danger">Deleted</span></div>
            <div class="col-md-6 text-right"><?=number_format(dbGet('secondary','deleted = 1 '));?></div>
            <hr>
        </div>
    </div>
</div>
<hr>

<div class="clear10"></div>
<div id="divActivity"></div>