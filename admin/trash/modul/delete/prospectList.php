<div class="clear10"></div>

<div class="form-group">
    <h4 class="pull-left"><b>Prospect List</b></h4>
    <div class="pull-right">
        <a href="?prospect&mode=ins" class="btn btn-success">Add</a>
    </div>    
</div>

<div class="clear10"></div>


<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Finder</th>
                <th>Prospect Name</th>
                <th>Phone</th>
                <th>Date</th>
                <th>Editor</th>
                <th style="text-align:center">Action</th>                                                                                
        </tr>
        </thead>                                            
    </table>
</div>

<script type="text/javascript">
    function deleteRecord(tabel,id,photo){
        if(confirm('Are you sure to remove this ?'))
        {

            window.location.href='library/qProspect.php?prospect&del&id='+id;
          
        }
    }
</script>

<script src="dataTable/js/jquery-1.11.1.min.js"></script>
<script src="dataTable/js/bootstrap.min.js"></script>
<script src="dataTable/js/jquery.dataTables.min.js"></script>
<script src="dataTable/js/dataTables.bootstrap.js"></script>	
<script type="text/javascript">

$(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#example1').DataTable({

        "processing": true,
        "language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax": "modul/ajax/dataProspect.php?list",
        "order": [[ 4, "desc" ]],
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },

        "columnDefs": [
            { 
                    "render": finderCol,
                    "targets": 1 
            },
            { 
                    "render": prospekCol,
                    "targets": 2 
            },
            { 
                "render": actionCol,
                "targets": 6 
            },

        ],

    });

    $('#example1_filter input').unbind();
    $('#example1_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });  

    function finderCol(data, type, full) {     
        return ''+full[1]+' <small>('+full[6]+')</small>';
    }

    function prospekCol(data, type, full) {     
        return ''+full[2]+' <small>('+full[0]+')</small>';
    }

    function actionCol(data, type, full) {

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button><ul class="dropdown-menu">'+
        '<li><a href="?prospect&mode=ins&id='+full[0]+'">Edit</a></li>'+
        '<li><a href="#" onclick=deleteRecord("prospect","'+full[0]+'")">Delete</a></li>'+
        '<li><a href="?trx&mode=ins&idSales='+full[6]+'&idProspect='+full[0]+'" >Add Prospect Activation</a></li></ul></div>';
        
    }

});
</script>

