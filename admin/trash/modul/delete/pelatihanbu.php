<div class="container">
  <div class="row">
    <?php 
    $pelatihan = dbGetAll('kegiatan','publish=1','insertDate','DESC','');
    foreach($pelatihan as $pelatihanVal){

      if(empty($pelatihanVal['banner'])){
        $banner = $imgserver.'images/logo.png';
      }else{
        $banner = $imgserver.'images/banner/'.$pelatihanVal['banner'];
      }
    ?>

    <div class="col-sm-3 mb-4">
      <a href="<?=$instrukturPath?>pelatihan/<?=seo($pelatihanVal['title'])?>-<?=encrypt($pelatihanVal['id'])?>" class="shortLink">
        <div class="card text-dark">
            <img src="<?=$banner?>" alt="..." class="img-fluid" style="height:200px">
            <div class="card-body">
              <h5 class="card-title" style="height:50px;overflow:hidden;"><?=$pelatihanVal['title']?></h5>
              <!--<p class="card-text" style="height:50px;overflow:hidden;"><?=trimString($pelatihanVal['konten'],'5')?></p>-->
              <p class="card-text" style="height:25px;overflow:hidden;">
                <span class='badge badge-success font-weight-normal'><?=count(explode('|',$pelatihanVal['jadwal']))?> Hari</span>
                <?=status($pelatihanVal['status'])?>
              </p>
            </div>
        </div>
      </a>
    </div>

    <?php } ?>
  </div>
</div>