<div class="container-fluid">
    <div class="box-body table-responsive px-2">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Judul Pelatihan</th>
                    <th style="border-bottom:none;font-weight:600">Total Sesi</th>
                    <th style="border-bottom:none;font-weight:600">Jadwal</th>
                    <th style="border-bottom:none;font-weight:600"></th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="<?=$instrukturPath;?>plugin/dataTable/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
   
   $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({
        "processing": true,
        //"language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax":{
            url: '<?=$instrukturPath;?>modul/ajax/data_materi.php',
            headers: { Authorization: '<?=$_SESSION['token_instruktur']?>' },
        },
        //"order": [[ 6, "desc" ], [ 7, "desc" ]],
        "order": [[ 4, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": titleCol,
                "targets": 1
            },
            {
                "data": 5,
                "targets": 4,
                "visible":false
            }
        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function titleCol(data, type, full) {
        if(full[6]==1){
            link = "<a href=<?=$instrukturPath?>materi-saya/"+slugify(full[1])+"-"+full[4]+" >"+full[1]+"</a>";
        }else{
            link = "<a href=# onclick=look() >"+full[1]+"</a>";
        }     
        return ''+link+'';
    }

    function look(){
        swal("Maaf","Lengkapi data profil terlebih dulu","error");     
    }

</script>