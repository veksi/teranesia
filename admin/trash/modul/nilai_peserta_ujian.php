<div class="container-fluid">
    <div class="box-body table-responsive px-2">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Sesi Ujian</th>
                    <th style="border-bottom:none;font-weight:600">Nilai</th>
                    <th style="border-bottom:none;font-weight:600">Waktu Ujian</th>
                    <th style="border-bottom:none;font-weight:600">Status</th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="<?=$instrukturPath;?>plugin/dataTable/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
   
    var kegiatan    = '<?=encrypt($id)?>';
    var peserta     = '<?=encrypt($peserta)?>';
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({
        "processing": true,
        //"language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax":{
            url: '<?=$instrukturPath;?>modul/ajax/data_nilai_peserta_ujian.php?kegiatan='+kegiatan+'&peserta='+peserta,
            headers: { Authorization: '<?=$_SESSION['token_instruktur']?>' },
        },
        "order": [[ 3, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": statusCol,
                "targets":4
            },
        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function titleCol(data, type, full) {
        return ''+full[1]+''+full[2];
    }

    function statusCol(data, type, full) {
        var standar = <?=$standar_ujian?>;
        if(full[2]>=standar){
            status = "<span class='badge badge-pill badge-success font-weight-normal'>Lulus</span>";
        }else{
            if(full[4]==0){
                status = '<span class="badge badge-pill badge-danger font-weight-normal">Tidak Lulus</span>';
            }else{
                status = "<span class='badge badge-pill badge-info font-weight-normal'>Sudah Remedial</span>";
            }
        }
        return ''+status+'';

    }

</script>