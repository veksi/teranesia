<?php 
    $page_url = explode('?',$currentUrl)[0];
    $item_per_page = 12;

    if(isset($_GET["page"])){ //Get page number from $_GET["page"]
        $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
    }else{
        $page_number = 1; //if there's no page number, set it to 1
    }
    
    $results = dbGetNum('kegiatan','publish = 1','',''); //get total number of records from database
    $get_total_rows = $results; //hold total records in variable
    $total_pages = ceil($get_total_rows/$item_per_page); //break records into pages

    $page_position = (($page_number-1) * $item_per_page); //get starting position to fetch the records
    $pelatihan = dbGetAll('kegiatan','publish = 1','insertDate','DESC',''.$page_position.','.$item_per_page);
?>

<?php if($results>0){ ?>

  <div class="container">
    <div class="row">
      <?php 
      
      foreach($pelatihan as $pelatihanVal){

        if(empty($pelatihanVal['banner'])){
          $banner = '../../../images/nofoto.png';
        }else{
          $banner = $imgserver.'images/banner/'.$pelatihanVal['banner'];
        }
      ?>
      <!--
      <div class="col-sm-3 mb-4">
        <a href="<?=$instrukturPath?>pelatihan/<?=seo($pelatihanVal['title'])?>-<?=encrypt($pelatihanVal['id'])?>" class="shortLink">
          <div class="card text-dark">
              <img src="<?=$banner?>" alt="..." class="img-fluid" style="height:300px">
              <div class="card-body">
                <h5 class="card-title" style="height:50px;overflow:hidden;"><?=$pelatihanVal['title']?></h5>
                <p class="card-text" style="height:25px;overflow:hidden;">
                  <span class='badge badge-success font-weight-normal'><?=count(explode('|',$pelatihanVal['jadwal']))?> Hari</span>
                  <?=status($pelatihanVal['status'])?>
                </p>
              </div>
          </div>
        </a> 
      </div>-->

      <div class="col-sm-3 mb-4">
        <a href="<?=$instrukturPath?>pelatihan/<?=seo($pelatihanVal['title'])?>-<?=encrypt($pelatihanVal['id'])?>" class="shortLink">
          <div class="card text-white">
            <img src="<?=$banner?>" alt="..." class="card-img img-fluid" style="height:320px">
            <div class="card-img-overlay d-flex flex-column p-0">
              <div class="mt-auto">
                <div class="p-3" style="background-color:rgba(0,0,0,0.6)">
                  <h5 class="card-title" style="height:50px;overflow:hidden;"><?=$pelatihanVal['title']?></h5>
                  <p class="card-text" style="height:25px;overflow:hidden;">
                    <span class='badge badge-success font-weight-normal'><?=count(explode('|',$pelatihanVal['jadwal']))?> Hari</span>
                    <?=status($pelatihanVal['status'])?>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>

      <?php } ?>
    </div>

    <div class="clearfix"></div>
    <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
  </div>

<?php } ?>