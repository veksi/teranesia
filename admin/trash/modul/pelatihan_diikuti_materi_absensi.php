<?php
$absenExist = dbGetRow('absensi_instruktur',"kegiatan_materi = '".$sesi_ID."' AND instruktur = '".$_SESSION['instruktur']."'",'','',''); 

if(!empty(@$absenExist['foto'])){ 
  $abs = $imgserver."images/absensi/instruktur/".$absenExist['foto']; 
}else{ 
  $abs = $imgserver."images/nofoto.png";
}

if(!empty(@$absenExist['ttd_img'])){ 
  $ttd = $imgserver."images/signature/instruktur/".$absenExist['ttd_img']; 
}else{ 
  $ttd = "";
}
?>

<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
  <div class="jumbotron p-2 mb-4 bg-white">
    <div class="row justify-content-md-center">
      <div class="col-sm-8">
      
        <label><h5><?=$sesi['title']?></h5></label>
        
        <hr>

        <form action="<?=$instrukturPath?>modul/ajax/data_pelatihan_diikuti_materi_absensi.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">
          <input type="hidden" name='mode' value="ins">
          <input type="hidden" name='pelatihan' value="<?=$id?>">
          <input type="hidden" name='sesi' value="<?=$sesi_ID?>">

          <div class="row ">
            <div class="col">Nama</div>
            <div class="col"><?=$instruktur['first_name'].' '.$instruktur['last_name']?></div>
          </div>
          <div class="row title">
            <div class="col">Tempat / Tanggal Lahir</div>
            <div class="col"><?=$instruktur['bornPlace']?> / <?=tgl_indo($instruktur['bornDate'],false)?></div>
          </div>
          <div class="row title">
            <div class="col">Email</div>
            <div class="col"><?=$instruktur['email']?></div>
          </div>
          <div class="row">
            <div class="col">Telp</div>
            <div class="col"><?=$instruktur['telp']?></div>
          </div>
          <div class="row title">
            <div class="col">Perusahaan</div>
            <div class="col"><?=$instruktur['instansi']?></div>
          </div>
          <div class="row title">
            <div class="col">No NPWP</div>
            <div class="col"><?=$instruktur['no_npwp']?></div>
          </div>          
          <div class="row title">
            <div class="col">Bank</div>
            <div class="col"><?=$instruktur['bank']?></div>
          </div>
          <div class="row title">
            <div class="col">Rekening Bank</div>
            <div class="col"><?=$instruktur['rek_bank']?></div>
          </div>        
          <div class="clearfix"></div>
          <br><br>

        
          <div class="row">
            <div class="col-sm-6 mx-auto">
              <div style="width:250px;height:250px;margin:auto;">
                <?php if(empty($absenExist['foto'])){ ?>
                  <a class="btn btn-light" style="position:absolute;" href="#" data-toggle="modal" data-target="#myModal" class="btn btn-success addBtn"><i class="fa fa-camera fa-2x" aria-hidden="true"></i></a>
                  <div id="jepret" class="avatar img-thumbnail" style="width:250px !important;height:250px !important;"></div>
                <?php }else{ ?>
                  <img class="avatar img-thumbnail" src="<?=$imgserver?>images/absensi/instruktur/<?=$absenExist['foto']?>" style="width:250px !important;height:250px !important;">
                <?php } ?>
                <div class="clearfix"></div>
                <small id="emailHelp" class="form-text text-muted text-left">* foto</small>
              </div>
              <div class="clearfix"></div>
              <br>
              <br>
            </div>

            <div class="col-sm-6 mx-auto">
              <div style="width:250px;height:250px;margin:auto;border:1px solid #DEE2E6" class="rounded">
                <?php if(empty($absenExist['ttd_img'])){ ?>
                  <a class="btn hapus btn-light" style="position:absolute;"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a>
                  <div class="tandatangan2"></div>
                <?php }else{ ?>
                  <img src="<?=$imgserver?>images/signature/instruktur/<?=$absenExist['ttd_img']?>">
                <?php } ?>
                <div class="clearfix"></div>
                <small id="emailHelp" class="form-text text-muted">* tanda tangan</small>
              </div>
              <br>
            </div>

          </div>
        
          <div class="lead text-center py-4">                 
            <input type="hidden" name='ttd' id='ttd'>
            <input type="hidden" name="image" class="image-tag">
            <?php if(empty($absenExist['id'])){ ?>
              <button name=submit type="submit" class="btn btn-primary loadingBtn">Kirim</button>     
              <!--<input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-lg"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim">-->
            <?php } ?>              
          </div>

        </form>

      </div>
    </div>          
  </div>        
</div>

<script>

$(document).ready(function() {

  $('.tandatangan2').on('mouseup touchend', function(e) {
      var dataToBeSaved =  $(".tandatangan2").jSignature("getData");
      $('#ttd').val(dataToBeSaved);//ini utk kedatabase                            
  });   

  $(".tandatangan2").jSignature({'background-color':'white','height':'100%','width':'100%','lineWidth':'2'});         
  
  $('.hapus').on('click', function(e) {
      e.preventDefault();
      $('.tandatangan2').jSignature("reset");
      $('#ttd').val('');
  });

});   

function validasi(){
  var ttd      = $("#ttd").val();
  var foto     = document.getElementsByName("image")[0].value;

  if (ttd=="" || foto==""){
      swal("","Lengkapi data dengan benar","warning",).then(function(){ $(".loadingBtn").html('Kirim'); });     
      return false;
  }else{
      return true;
  }
}

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    /*
      var width2 = $(".modal-body").width();
      //var height2 = $(".modal-body").height();
      var height2 = 450;
      
      Webcam.set({
          width: width2,
          height: height2,
          dest_width: 240,
          dest_height: 240,
          //crop_width:320,
          //crop_height:240,
          image_format: 'jpeg',
          jpeg_quality: 100
      });*/

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('jepret').innerHTML = '<img style="width:240px !important;height:240px !important;" src="'+data_uri+'"/>';
        } );
    }

    function turnOff() {
      Webcam.reset();
      Webcam.set({
          width: 0,
          height: 0
      });
    };

    function turnOn() {
      var width2 = $(".modal-body").width();
      //var height2 = $(".modal-body").height();
      var height2 = 350;
 
      Webcam.set({
          width: width2,
          height: height2,
          //dest_width: 240,
          //dest_height: 240,  
          //crop_width:width2,
          //crop_height:height2,     
      });

      Webcam.attach( '#my_camera' );
    }
</script>
 

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form id="insert">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" onclick="turnOff()" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" style="height:350px">
                  
                  <script type="text/javascript">                 
                    $('#myModal').on('shown.bs.modal', function () {                                        
                      turnOn();
                    });
                    $("#myModal").on('hidden.bs.modal', function(){
                      turnOff();
                    });
                  </script> 
                  
                  <div id="my_camera" style="margin:auto"></div>                 

                </div>

                <div class="modal-footer mx-auto">
                    <a href="#" onClick="take_snapshot()" data-dismiss="modal" class="btn btn-success"><i class="fa fa-camera fa-2x" aria-hidden="true"></i></a>
                </div>
               
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

