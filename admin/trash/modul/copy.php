<footer class="py-4 bg-transparent mt-auto">
    <div class="container-fluid ">
        <div class="small text-center">
            <a class="text-white" href="http://pixelinia.com/" target="_blank">Powered by pixelinia.com</a>
        </div>
    </div>
</footer>