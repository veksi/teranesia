<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
  <div class="jumbotron p-4 mb-4 bg-white">
    <div class="row justify-content-md-center">
      <div class="col-sm-9">
        
        <p class="lead">Lengkapi berkas persyaratan untuk mengikuti kegiatan ini</p>
        
        <form action="<?=$domain?>modul/ajax/data_pelatihan_diikuti_upload_berkas.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">  
          <input type="hidden" name='mode' value="ins">
          <input type="hidden" name='pelatihan' value="<?=$id?>">
          <div class="row">
            <div class="col-sm">
              <div class="form-group">
                <div class="custom-file">
                  <input type="file" name="syarat[]" class="custom-file-input" id="customFile" multiple>
                  <label class="custom-file-label" for="customFile" style="height:38px;overflow:hidden;">Choose file</label>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <button type="submit" value="Upload" class="btn btn-primary btn-block loadingBtn">Upload</button>
            </div>
          </div>
        </form>

        <hr class="my-4">

        <div class="row">
          <?php $syarat       = dbGetAll('berkas_persyaratan',"pelatihan = '".$id."' AND user = '".$_SESSION['member']."'",'insertDate','DESC',''); ?>
          <?php foreach($syarat as $syaratVal){?>
            <div class="col-sm-3 mb-4">
              <a class="shortLink">
                <div class="card text-dark">
                  <div class="card-body">
                    <h5 class="card-title" style="height:50px;overflow:hidden;"><?=cekBerkasExt($syaratVal['berkas'])?></h5>
                    <p class="card-text" style="height:50px;overflow:hidden;"><?=$syaratVal['berkas']?></p>
                  </div>
                </div>
              </a>
            </div>
          <?php } ?>
        </div>

      </div>
    </div>          
  </div>        
</div>

<script>
$(document).ready(function() {
  $('input[type="file"]').on("change", function() {
    let filenames = [];
    let files = document.getElementById("customFile").files;
    if (files.length > 1) {
      filenames.push("Total Files (" + files.length + ")");
    } else {
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          filenames.push(files[i].name);
        }
      }
    }
    $(this)
      .next(".custom-file-label")
      .html(filenames.join(","));
  });
});

function validasi(){
  var file      = $("#customFile").val();
  if (file==""){
      swal("","Lengkapi data dengan benar","warning",).then(function(){ $("#customFile").focus();$(".loadingBtn").html('Upload'); });
      return false;
  }else{
      return true;
  }
}
</script>