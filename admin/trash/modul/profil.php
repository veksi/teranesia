<script>

    $(document).ready(function() { 
        var born    = '<?=$instruktur['bornDate'];?>';

        document.getElementsByName("mode")[0].value = 'upd';
        document.getElementById("name_d").value = '<?=$instruktur['first_name'];?>';
        document.getElementById("name_b").value = '<?=$instruktur['last_name'];?>';
        document.getElementById("telp").value = '<?=$instruktur['telp'];?>';

        if(born=='0000-00-00'){
            document.getElementById("Dates").value = '';
        }else{
            var bornVal = '<?=date("d-m-Y", strtotime($instruktur['bornDate']));?>';
            document.getElementById("Dates").value = bornVal;
        }
        
        document.getElementById("email").value = '<?=$instruktur['email'];?>';
        document.getElementsByName("imgDB1")[0].value = '<?=$instruktur['foto'];?>';
        document.getElementById("instansi").value = '<?=$instruktur['instansi'];?>';
        document.getElementById("born").value = '<?=$instruktur['bornPlace'];?>';
        document.getElementById("npwp").value = '<?=$instruktur['no_npwp'];?>';
        document.getElementById("bank").value = '<?=$instruktur['bank'];?>';
        document.getElementById("rek").value = '<?=$instruktur['rek_bank'];?>';

        document.getElementsByName("cv_DB")[0].value = '<?=$instruktur['cv_file'];?>';
        document.getElementsByName("ktp_DB")[0].value = '<?=$instruktur['ktp_file'];?>';

        $('.filename').html('<?=str_replace("_".$_SESSION['instruktur'],"",$instruktur['ktp_file'])?>');
        $('.filename2').html('<?=str_replace("_".$_SESSION['instruktur'],"",$instruktur['cv_file'])?>');

    });

</script>

<form action="<?=$instrukturPath?>modul/ajax/data_profil.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">
    <input type=hidden name='mode' value='add'>
    <input type=hidden name='id' value='<?=$id?>'>
    <input type=hidden id="ktp_DB" name='ktp_DB'>
    <input type=hidden id='cv_DB' name='cv_DB'>

    <div class="container-fluid">
        <div class="jumbotron p-4 mb-4 bg-white">
            <div class="row justify-content-md-center">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img id="pp" data-type="editable1" class="avatar rounded-circle img-thumbnail" alt="avatar" style="width:250px;height:250px;" src=<?=$dp;?> >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1'>
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
                <div class="col-sm-8">
                        
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Nama Depan</label>
                                <input class="form-control" id="name_d" name="name_d" type="text" placeholder="Nama depan" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Nama Belakang</label>
                                <input class="form-control" id="name_b" name="name_b" type="text" placeholder="Nama belakang" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Email</small>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" autocomplete="off" onkeyup="validEmail()">
                        <div class="invalid-feedback">Email tidak valid</div>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Password</small>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Handphone *</small>
                        <input type="text" id="telp" name="telp" class="form-control" placeholder="Handphone" autocomplete="off" >
                    </div>

                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Tempat Lahir *</label>
                                <input type="text" id="born" name="born" class="form-control" placeholder="Tempat Lahir" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Tanggal Lahir</label>
                                <div class="input-group date form-group" id="datepicker">
                                    <input type="text" class="form-control" id="Dates" name="Dates" placeholder="Pilih Tanggal" autocomplete="off"  />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <span class="count"></span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <small id="emailHelp" class="form-text text-muted">Alamat *</small>                   
                        <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Alamat"><?=@$instruktur['alamat'];?></textarea>
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Instansi *</small>
                        <input type="text" id="instansi" name="instansi" class="form-control" placeholder="Instansi" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">No NPWP *</small>
                        <input type="number" id="npwp" name="npwp" class="form-control" placeholder="No NPWP" autocomplete="off" >
                    </div>

                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Bank *</small>
                        <input type="text" id="bank" name="bank" class="form-control" placeholder="Bank" autocomplete="off" >
                    </div>
                    
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Rekening Bank *</small>
                        <input type="text" id="rek" name="rek" class="form-control" placeholder="Rekening Bank" autocomplete="off" >
                    </div>                    

                    <div class="row">
                        <div class="col">
                            <small id="emailHelp" class="pull-left form-text text-muted">KTP *</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">                                
                                <div class="custom-file">
                                    <input type="file" name="ktp" class="custom-file-input customFile" id="customFile">                                    
                                    <label class="custom-file-label filename" for="customFile" style="height:38px;overflow:hidden;">Choose file</label>
                                    <div class="valid-tooltip">Upload complete</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <div class="reload">
                                <?php if(empty($instruktur['ktp_file'])){ ?>
                                    <button class="btn btn-light btn-block" disabled>
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Tidak ada file
                                    </button>
                                <?php }else{ ?>
                                    
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="javascript:void(0)" onclick="deleteFile('ktp')" class="btn btn-primary btn-lg">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <a href="<?=$domain?>berkas/instruktur/<?=$instruktur['ktp_file']?>" target="_blank" class="btn btn-primary btn-lg">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <small id="emailHelp" class="pull-left form-text text-muted">CV *</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input type="file" name="cv" class="custom-file-input customFile2" id="customFile2">                                    
                                    <label class="custom-file-label filename2" for="customFile2" style="height:38px;overflow:hidden;">Choose file</label>
                                    <div class="valid-tooltip">Upload complete</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">  
                            <div class="reload2">                          
                                <?php if(empty($instruktur['cv_file'])){ ?>
                                    <button class="btn btn-light btn-block" disabled>
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> Tidak ada file
                                    </button>
                                <?php }else{ ?>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="javascript:void(0)" onclick="deleteFile('cv')" class="btn btn-primary btn-lg">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <a href="<?=$domain?>berkas/instruktur/<?=$instruktur['cv_file']?>" target="_blank" class="btn btn-primary btn-lg">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </div>                              
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br>

                    <div class="pull-right">
                        <button name=submit type="submit" class="btn btn-success loadingBtn">Save</button>                        
                        <button type="button" onclick="window.location.href='<?=$instrukturPath;?>dashboard'" class="btn btn-info">Cancel</button>
                    </div>

                    <div class="clearfix"></div>
                    
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

function HitungTitle(){
    var Teks = document.forminput.title.value.length;
    var total = document.getElementById('hasil');
    total.innerHTML = 200 - Teks;
}


function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "../../../images/nofoto.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function deleteFile(file){
    
    if(file=='ktp'){
        $('.filename').addClass("selected").html('');
        $("#ktp_DB").val('');
    }else{
        $('.filename2').addClass("selected").html('');
        $("#cv_DB").val('');
    }

    $.ajax({
        url: '<?=$instrukturPath;?>modul/ajax/data_profil.php?del&dokumen='+file,
        headers: { Authorization: '<?=$_SESSION['token']?>' },
        success: function(respon){
            $(".reload").load(window.location.href + " .reload" );
            $(".reload2").load(window.location.href + " .reload2" );
        }
    });
}

function validEmail(){
    var email   = $('#email').val();
    var at      = email.search("@");
    var dot     = email.match(/\./g).length;
    if( (at<=0) || (dot<1) ) { 
        $("#email").addClass('valid-tooltip');
    }else{
        $("#email").removeClass('is-invalid');
    }
}

function validasi(){
   
    var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var name_d      = $("#name_d").val();
    var name_b      = $("#name_b").val();
    var email       = $("#email").val();
    var password    = $("#password").val();
    var telp        = $("#telp").val();
    var born        = $("#born").val();
    var bornDate    = $("#Dates").val();
    var alamat      = $("#alamat").val();
    var instansi    = $("#instansi").val();
    var bank        = $("#bank").val();
    var rek         = $("#rek").val();
    var npwp        = $("#npwp").val();
    //var ktp         = $("#customFile").val();
    //var cv          = $("#customFile2").val();
    //var ktp_db      = $("#ktp_DB").val();
    //var cv_db       = $("#cv_DB").val();
    

    if (name_d==""){

        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#name_d").focus();$("#name_d").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (name_b==""){

        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#name_b").focus();$("#name_b").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (email==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#email").focus();$("#email").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (!filter.test(email)) {

        //swal("","Email anda tidak valid","warning",).then(function(){  });
        $("#email").focus();$("#email").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (password!="" && password.length<5){

        //swal("","Kata sandi minimal 5 karakter","warning",).then(function(){  });
        $("#password").focus();$("#password").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (telp==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#telp").focus();$("#telp").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (born==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#born").focus();$("#born").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (bornDate==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#Dates").focus();$("#Dates").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (alamat==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#alamat").focus();$("#alamat").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (instansi==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#instansi").focus();$("#instansi").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (npwp==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#npwp").focus();$("#npwp").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (bank==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#bank").focus();$("#bank").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;

    }else if (rek==""){
        //swal("","Lengkapi data dengan benar","warning",).then(function(){  });
        $("#rek").focus();$("#rek").addClass('is-invalid');$(".loadingBtn").html('Save');
        return false;
        
    /*}else if (ktp=="" && ktp_db==""){
      $("#customFile").focus();$("#customFile").addClass('is-invalid');$(".loadingBtn").html('Save');
      return false;

    }else if (cv=="" && cv_db==""){
      $("#customFile2").focus();$("#customFile2").addClass('is-invalid');$(".loadingBtn").html('Save');
      return false;*/

    }else{

        return true;

    }

}

$('.customFile').on('change',function(){

    var fileName = document.getElementById("customFile").files[0].name;
    $(this).next('.filename').addClass("selected").html(fileName);

    let files = new FormData(), // you can consider this as 'data bag'
    url = '<?=$instrukturPath;?>modul/ajax/data_profil.php?file&dokumen=ktp';
    files.append('ktp', $('#customFile')[0].files[0]);

    $.ajax({
        url: url,
        type: "POST",
        data: files,
        contentType: false,
        processData:false,
        success: function(respon){
            $(".customFile").addClass('is-valid');
            setTimeout(function(){ $(".customFile").removeClass('is-valid'); }, 1000);
            $(".reload").load(window.location.href + " .reload" );
        }
    });

});

$('.customFile2').on('change',function(){
    var fileName2 = document.getElementById("customFile2").files[0].name;
    $(this).next('.filename2').addClass("selected").html(fileName2);

    let files = new FormData(), // you can consider this as 'data bag'
    url = '<?=$instrukturPath;?>modul/ajax/data_profil.php?file&dokumen=cv';
    files.append('cv', $('#customFile2')[0].files[0]);

    $.ajax({
        url: url,
        type: "POST",
        data: files,
        contentType: false,
        processData:false,
        success: function(respon){
            $(".customFile2").addClass('is-valid');
            setTimeout(function(){ $(".customFile2").removeClass('is-valid'); }, 1000);
            $(".reload2").load(window.location.href + " .reload2" );
        }
    });

});

</script>

<!--multi datepicker -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

<script>
$(document).ready(function() {
    $('#datepicker').datepicker({
        numberOfMonths: 4,
        startDate: -Infinity,
        multidate: false,
        //multidateSeparator:'|',
        format: "dd-mm-yyyy",
        daysOfWeekHighlighted: "5,6",
        //datesDisabled: ['31/08/2017'],
        language: 'en',
        clearBtn:true,
        toggleActive:true,
        forceParse:true,
        todayHighlight:true,
    });
});
</script>