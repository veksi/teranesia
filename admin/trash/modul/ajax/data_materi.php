<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan';
    $id         = decrypt(@$_GET['id']);
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " instruktur = $instruktur ";
    $groupBy    = "kegiatan";

    if(isset($_GET['join'])){
        $result                 = dbGetNum($table,"user = '".$user."' AND kegiatan = '".$kegiatan."'",'','');
        
        if($result==0){
            $conn               = db_conn_sepro();
            $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
                kegiatan        = '$kegiatan',
                user            = '$user'
            ");  
            mysqli_close($conn);
            echo 1;
        }else{
            echo 0;
        }
        
    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` LEFT JOIN kegiatan_materi AS `b` ON `a`.id = `b`.kegiatan ";
        $columns = array(
            array( 
                'db' => '`a`.`id`',                
                'dt' => 0,
                'field' => 'id',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 
                'db' => '`a`.`id`',                
                'dt' => 1,
                'field' => 'id',
                'formatter' => function ($a) {                        
                        $pelatihan  = dbGetRow('kegiatan'," id = '".$a."'",'','')['title'];
                        return $pelatihan;
                    }
            ),        
            array( 
                'db' => '`a`.`id`',                
                'dt' => 2,
                'field' => 'id',
                'formatter' => function ($a, $row) use ($instruktur) {      
                        $materi = dbGetNum('kegiatan_materi',"instruktur = '".$instruktur."' AND kegiatan = '".$a."'",'','');
                        return $materi;           
                    }
            ),            
            array( 
                'db' => '`a`.`jadwal`',                
                'dt' => 3,
                'field' => 'jadwal',
                'formatter' => function ($b) {
                        $jadwal = explode("|",$b);
                        return tgl_indo(date('Y-m-d', strtotime($jadwal[0])),false).' - '.tgl_indo(date('Y-m-d', strtotime(end($jadwal))),false);
                    }
            ),
            array( 
                'db' => '`a`.`id`',                
                'dt' => 4,
                'field' => 'id',
                'formatter' => function ($kegiatan) {
                        return encrypt($kegiatan);
                    }
            ),
            array('db' => '`a`.`insertDate`','dt' => 5,'field' => 'insertDate'),
            array( 
                'db' => '`a`.`id`',                
                'dt' => 6,
                'field' => 'id',
                'formatter' => function ($a) {                        
                        $profil  = dbGetRow('instruktur'," id = '".$_SESSION['instruktur']."'",'','');
                        if( 
                            empty($profil['email'])||
                            empty($profil['first_name'])||
                            empty($profil['last_name'])||
                            empty($profil['bornPlace'])||
                            $profil['bornDate']=='0000-00-00'||
                            empty($profil['telp'])||
                            empty($profil['alamat'])||                            
                            empty($profil['instansi'])||
                            empty($profil['no_npwp'])||
                            empty($profil['bank'])||
                            empty($profil['rek_bank'])||
                            empty($profil['cv_file'])||
                            empty($profil['ktp_file'])                            
                        ){  return 0; }else{ return 1; }

                        
                    }
            ),
            
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }
}
?>