<?php
session_name("sepro_member");
session_start();

require "../../setting/global.php";

if(empty($_SESSION['member'])){
    
}else{

    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'evaluasi_ujian';    
    $user       = $_SESSION['member'];

    if(isset($_POST['upd'])){

        $score      = 0;
        $benar      = 0;
        $salah      = 0;
        $kosong     = 0;
        $arrJawaban = array();

        for ($i=0;$i<$_POST['jumlah'];$i++){
            $nomor = $_POST['id'][$i];
            if (empty($_POST['pilihan'][$nomor])){
                $kosong++;
                $arrJawaban[] = array("question" => $nomor, "answer" => $_POST['pilihan'][$nomor]);
            }else{
                $jawaban = $_POST['pilihan'][$nomor];
                $cek = dbGetNum('soal',"ans_key = '".$jawaban."' AND id = '".$nomor."'",'','');
                if($cek){
                    $benar++;
                }else{
                    $salah++;
                }
                $arrJawaban[] = array("question" => $nomor, "answer" => $_POST['pilihan'][$nomor]);
                    
            } 
            
            $score = $benar/$_POST['jumlah']*100;
            $hasil = $score;
        }

        $jawaban = JSON_ENCODE($arrJawaban, true);

        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            jawaban         = '$jawaban',
            expired         = 1,            
            nilai           = '$hasil' WHERE kegiatan_materi = '$_POST[sesi]' AND user = '$user'
        ");  
            
        mysqli_close($conn);
        
        //header('Location:'.$domain.'pelatihan-saya/materi/'.encrypt($_POST['sesi']));
        header("Location: {$_SERVER["HTTP_REFERER"]}");

    }
}
?>