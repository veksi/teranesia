<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'absensi';
    $sesi_ID    = @$_GET['sesi'];
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " kegiatan_materi = $sesi_ID ";

    if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 
                'db' => '`a`.`kegiatan_materi`',                
                'dt' => 1,
                'field' => 'kegiatan_materi',
                'formatter' => function ($a) {
                        $sesi       = dbGetRow('kegiatan_materi'," id = '".$a."'",'','');
                        $pelatihan  = dbGetRow('kegiatan'," id = '".$sesi['kegiatan']."'",'','')['title'];
                        return $pelatihan.' <i class="fa fa-chevron-right" aria-hidden="true"></i> '.$sesi['title'];
                    }
            ),
            array( 
                'db' => '`a`.`insertDate`',                
                'dt' => 2,
                'field' => 'insertDate',
                'formatter' => function ($b) {
                        return tgl_indo($b,true,true);
                    }
            ),
            
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>