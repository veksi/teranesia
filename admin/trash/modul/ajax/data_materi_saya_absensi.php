<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";
include "../../setting/connection.php";
include "../../library/function.php";
include "../../library/query.php";

$bypass = dbGetNum('instruktur',"token = '".$_SERVER['HTTP_AUTHORIZATION']."'",'','');

if(empty($_SESSION['instruktur']) && $bypass==0){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    $table      = 'absensi_instruktur';

    if($_POST['mode']=='ins'){

        $time           = date("his");

        $folderPath 	= "../../../images/signature/instruktur/";
        $image_parts 	= explode(";base64,", $_POST['ttd']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type 	= $image_type_aux[1];
        $image_base64 	= base64_decode($image_parts[1]);
        
        $fileName		= $_POST['jadwal'].$_SESSION['instruktur'].$time.'.'.$image_type;
        $file 			= $folderPath . $fileName;		

        file_put_contents($file, $image_base64);

        $img            = $_POST['image'];
        $folderPathFoto = "../../../images/absensi/instruktur/";
    
        $foto = explode(";base64,", $img);
        $ext = explode("image/", $foto[0]);
        $ext_img = $ext[1];
    
        $image_base64x = base64_decode($foto[1]);
        $fotoname = $_POST['jadwal'].$_SESSION['instruktur'].$time.'.'.$ext_img;
    
        $fileFoto = $folderPathFoto . $fotoname;
        file_put_contents($fileFoto, $image_base64x);

        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
            instruktur      = '$_SESSION[instruktur]',
            kegiatan        = '$_POST[pelatihan]',
            jadwal          = '$_POST[jadwal]',
            foto            = '$fotoname',
            ttd             = '$_POST[ttd]',
            ttd_img         = '$fileName'
        ");  
            
        mysqli_close($conn);
        header("Location: {$_SERVER["HTTP_REFERER"]}");
        //header('Location:'.$domain.'pelatihan-diikuti/materi/'.encrypt($_POST['jadwal']));

    }else{
        echo "dsa";
    }
}
?>