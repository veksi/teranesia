<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'evaluasi_ujian';
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $peserta    = urlencode(decrypt(@$_GET['peserta']));
    $editor     = $_SESSION['instruktur'];

    $extraWhere = " user = $peserta AND kegiatan = $kegiatan ";

    $primaryKey = 'id';
    $joinQuery  = " FROM `{$table}` AS `a` ";
    $columns = array(
        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
        array( 
            'db' => '`a`.`kegiatan_materi`',                
            'dt' => 1,
            'field' => 'kegiatan_materi',
            'formatter' => function ($a) {
                    $jadwalSesi     = dbGetRow('kegiatan_materi',"id = '".$a."'",'','','');
                    $jadwalKegiatan = dbGetRow('kegiatan',"id = '".$jadwalSesi['kegiatan']."'",'','','')['jadwal'];
                    $no=1;
                    foreach(explode("|",$jadwalKegiatan) as $val ){
                        if($val==$jadwalSesi['jadwal']){
                            $hari = $no;
                            break;
                        }else{
                            $hari = "";
                        }
                        $no++;
                    }
                    
                    return "Hari ".$hari." - ".$jadwalSesi['title'];
                }
        ),
        array( 'db' => '`a`.`nilai`', 'dt' => 2,'field' => 'nilai'),
        array( 
            'db' => '`a`.`insertDate`',                
            'dt' => 3,
            'field' => 'insertDate',
            'formatter' => function ($b) {
                    return tgl_indo($b,true,true);
                }
        ),
        array( 'db' => '`a`.`remedial`', 'dt' => 4,'field' => 'remedial'),
    );


    require( 'ssp.class.php' );

    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
    );
}
?>