<?php
session_name("admin");
session_start();

if(empty($_SESSION['idUser'])){

}else{

    include "../../setting/connection.php";
    include "../../library/queryAdmin.php";

    $imgNewsFolder = "../../../api/images/article/";

    $id = @$_GET['id'];

    if(isset($_GET['list'])){

        $table = 'article';
        $primaryKey = 'idArticle';
        $joinQuery = " FROM `{$table}` AS `a` 
        LEFT JOIN `category` AS `b` ON (`a`.`category` = `b`.`idCategory`)";

        $columns = array(

            array( 'db' => '`a`.`idArticle`', 'dt' => 0,'field' => 'idArticle'),
            array( 'db' => '`a`.`pict`', 'dt' => 1,'field' => 'pict'),
            array( 'db' => '`a`.`title`', 'dt' => 2,'field' => 'title'),
            array( 'db' => '`b`.`category`', 'dt' => 3,'field' => 'category'),
            array( 'db' => '`a`.`insertDate`', 'dt' => 4,'field' => 'insertDate'),
            array( 
                'db' => '`a`.`editor`',
                'dt' => 5,
                'field' => 'editor',
                'formatter' => function($a) {
                        return getUser($a);
                    }
            ),
            array( 'db' => '`a`.`publish`', 'dt' => 6,'field' => 'publish'),
            array( 'db' => '`a`.`push`', 'dt' => 7,'field' => 'push'),
            /*array( 'db' => 'idArticle', 'dt' => 0 ),
            array( 'db' => 'pict', 'dt' => 1 ),
            array( 'db' => 'title', 'dt' => 2 ),
            array( 'db' => 'category',  'dt' => 3 ),
            array( 'db' => 'insertDate',  'dt' => 4 ),   
            array( 
                'db' => 'editor',
                'dt' => 5,
                'formatter' => function($d) {
                        return getUser($d);
                    }
                ),
            array( 'db' => 'publish',  'dt' => 6 ),*/
            

        );

        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, '')
        );

    }elseif(isset($_GET['view'])){

        $conn = db_connection();
        $query = mysqli_query($conn,"SELECT * FROM article WHERE idArticle = '$id' ");
        $row = mysqli_fetch_assoc($query);
        echo json_encode($row);
        
        mysqli_close($conn);
        
    }elseif(isset($_GET['on'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE article SET publish = '1', status = '1' WHERE idArticle = '$id' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['off'])){

        $conn = db_connection();
        mysqli_query($conn,"UPDATE article SET publish = '0', status = '0' WHERE idArticle = '$id' ");

        mysqli_close($conn);
        
    }elseif(isset($_GET['del'])){

        //global $imgNewsFolder;
        $conn = db_connection();

        $query = mysqli_query($conn,"SELECT pict FROM article WHERE idArticle = '$id' ");
        $row = mysqli_fetch_assoc($query);
        
        if( (!empty($row['pict'])) ){
            unlink($imgNewsFolder.$row['pict']);
        }

        mysqli_query($conn,"DELETE FROM article WHERE idArticle = '$id' ");

        mysqli_close($conn);
        
    }
}

?>