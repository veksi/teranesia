<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";
include "../../setting/connection.php";   
include "../../library/function.php";
include "../../library/query.php";

$bypass = dbGetNum('instruktur',"token = '".$_SERVER['HTTP_AUTHORIZATION']."'",'','');

if(empty($_SESSION['instruktur']) && $bypass==0){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    $table      = 'absensi_instruktur';
    //$id         = decrypt(@$_GET['id']);
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " instruktur = $instruktur AND kegiatan = $kegiatan ";

    $primaryKey = 'id';
    $joinQuery  = " FROM `{$table}` AS `a` ";
    $columns = array(
        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
        array( 
            'db' => '`a`.`jadwal`',                
            'dt' => 1,
            'field' => 'jadwal',
            'formatter' => function ($a) {
                    $jadwalSesi     = dbGetRow('kegiatan_materi',"jadwal = '".$a."'",'','','');
                    $jadwalKegiatan = dbGetRow('kegiatan',"id = '".$jadwalSesi['kegiatan']."'",'','','')['jadwal'];
                    $no=1;
                    foreach(explode("|",$jadwalKegiatan) as $val ){
                        if($val==$jadwalSesi['jadwal']){
                            $hari = $no;
                            break;
                        }else{
                            $hari = "";
                        }
                        $no++;
                    }
                    
                    return "Hari ke ".$hari;
                }
        ),
        array( 
            'db' => '`a`.`insertDate`',                
            'dt' => 2,
            'field' => 'insertDate',
            'formatter' => function ($b) {
                    return tgl_indo($b,true,true);
                }
        ),
        
    );

    require( 'ssp.class.php' );

    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
    );

}
?>