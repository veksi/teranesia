<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan_materi';
    $id         = decrypt(@$_GET['id']);
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " instruktur = $instruktur ";
    $groupBy    = "kegiatan";

    if(isset($_GET['join'])){
        $result                 = dbGetNum($table,"user = '".$user."' AND kegiatan = '".$kegiatan."'",'','');
        
        if($result==0){
            $conn               = db_conn_sepro();
            $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
                kegiatan        = '$kegiatan',
                user            = '$user'
            ");  
            mysqli_close($conn);
            echo 1;
        }else{
            echo 0;
        }
        
    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 
                'db' => '`a`.`id`',                
                'dt' => 0,
                'field' => 'id',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 1,
                'field' => 'kegiatan',
                'formatter' => function ($a) {                        
                        $pelatihan  = dbGetRow('kegiatan'," id = '".$a."'",'','')['title'];
                        return $pelatihan;
                    }
            ),        
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 2,
                'field' => 'kegiatan',
                'formatter' => function ($a, $row) use ($instruktur,$domain) {      
                        $materi = dbGetAll('kegiatan_materi',"instruktur = '".$instruktur."' AND kegiatan = '".$a."'",'','','');
                        $no=1;
                        foreach($materi as $materiVal){
                            $arrMateri[] = '<a href="'.$domain.'berkas/syarat/'.$berkasVal['berkas'].'" target="_blank">'.$no.'. '.$materiVal['title'].'</a>';
                            $no++;
                        }
                        return $arrMateri;                 
                    }
            ),            
            //array( 'db' => '`a`.`title`', 'dt' => 2, 'field' => 'title'),
            array( 
                'db' => '`a`.`waktu_mulai`',                
                'dt' => 3,
                'field' => 'waktu_mulai',
                'formatter' => function ($a) {                        
                        return date('h:i', strtotime($a));
                    }
            ),            
            array( 
                'db' => '`a`.`waktu_selesai`',                
                'dt' => 4,
                'field' => 'waktu_selesai',
                'formatter' => function ($c) {                        
                        return date('h:i', strtotime($c));
                    }
            ),            
            array( 
                'db' => '`a`.`jadwal`',                
                'dt' => 5,
                'field' => 'jadwal',
                'formatter' => function ($b) {
                        return tgl_indo(date('Y-m-d', strtotime($b)));
                    }
            ),
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 6,
                'field' => 'kegiatan',
                'formatter' => function ($a) {
                        $id  = dbGetRow('kegiatan'," id = '".$a."'",'','')['id'];
                        return $id;
                    }
            ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 7, 'field' => 'insertDate'),
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }
}
?>