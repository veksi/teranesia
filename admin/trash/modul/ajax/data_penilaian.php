<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";

if(empty($_SESSION['instruktur'])){

    header('Location:'.$instrukturPath);

}else{
    
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan';
    $instruktur = $_SESSION['instruktur'];
    $id         = decrypt(@$_GET['id']);
    $title      = addslashes($_POST['title']);
    $konten     = addslashes($_POST['konten']);
    $syarat     = addslashes($_POST['syarat']);
    $lokasi     = addslashes($_POST['lokasi']);

    $jadwal     = explode("|",$_POST['Dates']);
    sort($jadwal,SORT_STRING);
    $jadwal     = implode("|",$jadwal);

    $time           = date("his");

    if($_POST['mode']=='add'){
        $folderPath 	= "../../../images/banner/";
        $ext            = explode(".",str_replace(" ","_",$_FILES['photo1']['name']));
        $fotoname       = $ext[0].'_'.$time.'.'.end($ext);
        compress($_FILES['photo1']["tmp_name"], $folderPath.$fotoname);

        $conn                   = db_conn_sepro();
        $ins                    = mysqli_query($conn,"INSERT INTO $table SET                    
            banner              = '$fotoname',
            title               = '$title',
            konten              = '$konten',
            syarat              = '$syarat',
            lokasi              = '$lokasi',
            jadwal              = '$jadwal',
            editor              = '$editor'
        ");  
          
        mysqli_close($conn);

        header('Location:'.$instrukturPath.'pelatihan');
    }else{
        
        /*$joinQuery  = " FROM `{$table}` AS `a` 
        LEFT JOIN `evaluasi_ujian` AS `b` ON (`a`.`id` = `b`.`kegiatan`)
        LEFT JOIN `evaluasi_instruktur` AS `c` ON (`a`.`id` = `c`.`kegiatan`)
        LEFT JOIN `essay` AS `d` ON (`a`.`id` = `d`.`kegiatan`) ";
        $extraWhere = " b.user != '' OR c.instruktur != '' OR d.user != '' ";
        $groupBy    = 'a.id';*/
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` LEFT JOIN kegiatan_materi AS `b` ON `a`.id = `b`.kegiatan ";
        $extraWhere = " instruktur = $instruktur ";
        $groupBy    = "id";
        $columns = array(
            array( 
                'db' => '`a`.`id`',                
                'dt' => 0,
                'field' => 'id',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
            array( 
                'db' => '`a`.`id`',                
                'dt' => 2,
                'field' => 'id',
                'formatter' => function ($a) {
                        $peserta = dbGetNum('kegiatan_peserta'," kegiatan = '".$a."'",'','');
                        return $peserta;
                    }
            ),    
            array( 
                'db' => '`a`.`id`',                
                'dt' => 3,
                'field' => 'id',
                'formatter' => function ($a) {
                        $conn   = db_conn_sepro();
                        $query  = mysqli_query($conn,"SELECT * FROM kegiatan_materi WHERE instruktur > 0 AND kegiatan = $a GROUP BY instruktur ");
                        $num    = mysqli_num_rows($query);
                        return $num;
                    }
            ),    
            array( 'db' => '`a`.`insertDate`', 'dt' => 4,'field' => 'insertDate'),
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }
}
?>