<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'essay';
    $id         = ($_GET['id']);
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $peserta    = urlencode(decrypt(@$_GET['peserta']));
    $editor     = $_SESSION['instruktur'];

    $extraWhere = " user = $peserta AND kegiatan = $kegiatan ";
    $groupBy    = " kegiatan_materi";

    if(isset($_GET['nilai'])){
        $conn       = db_conn_sepro();
        $msg        = json_decode(stripslashes($_GET['msg']));
        mysqli_query($conn,"UPDATE $table SET nilai = '$msg', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 
                'db' => '`a`.`kegiatan_materi`',                
                'dt' => 1,
                'field' => 'kegiatan_materi',
                'formatter' => function ($a) {
                        $jadwalSesi     = dbGetRow('kegiatan_materi',"id = '".$a."'",'','','');
                        $jadwalKegiatan = dbGetRow('kegiatan',"id = '".$jadwalSesi['kegiatan']."'",'','','')['jadwal'];
                        $no=1;
                        foreach(explode("|",$jadwalKegiatan) as $val ){
                            if($val==$jadwalSesi['jadwal']){
                                $hari = $no;
                                break;
                            }else{
                                $hari = "";
                            }
                            $no++;
                        }
                        
                        return "Hari ".$hari." - ".$jadwalSesi['title'];
                    }
            ),
            array( 
                'db' => '`a`.`id`',
                'dt' => 2,
                'field' => 'id',
                'formatter' => function ($x, $row) use ($table) {
                        $data       = dbGetRow($table,'id = '.$x,'','');
                        $namaBerkas = explode("|",$data['namaBerkas']);
                        $berkas     = explode("|",$data['berkas']);
                        $no=1;
                        $urutan=0;
                        if(is_array($namaBerkas)){
                            foreach($namaBerkas as $namaBerkasVal){
                                $arrBerkas[] = '<a href="'.$domain.'berkas/essay/'.$berkas[$urutan].'" target="_blank"> '.$no.'. '.$namaBerkasVal.'</a>';
                                $no++;
                                $urutan++;
                            }
                        }
                        return implode("<br>",$arrBerkas);                                                
                    }
            ),
            array( 'db' => '`a`.`nilai`', 'dt' => 3,'field' => 'nilai'),
            array( 
                'db' => '`a`.`insertDate`',                
                'dt' => 4,
                'field' => 'insertDate',
                'formatter' => function ($b) {
                        return tgl_indo($b,true,true);
                    }
            ),
            
        );


        require( 'ssp.class.php' );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>