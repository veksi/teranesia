<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";

if(empty($_SESSION['instruktur'])){
    
}else{

    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan_peserta';
    $id         = @$_GET['id'];
    $editor     = $_SESSION['instruktur'];

    $kegiatan   = $_GET['kegiatan'];

    $extraWhere = " kegiatan = $kegiatan ";

    if(isset($_GET['add'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
            title           = '$_POST[group]',
            akses           = '$menu',
            editor          = '$editor'
        ");  
          
        mysqli_close($conn);

    }else if(isset($_GET['upd'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            title           = '$_POST[group]',
            akses           = '$menu',
            editor          = '$editor' WHERE
            id              = '$_POST[id]'
            
        ");  
            
        mysqli_close($conn);

    }else if(isset($_GET['view'])){
        $conn     = db_conn_sepro();
        $data     = dbGetRow('user'," id = '".$id."'",'','');
    ?>
        <div class="row">
            <div class="col text-center">
                <?php if(empty($data['foto'])){ 
                    $src= "../../../images/nofoto.png";
                }else{
                    $src = $domain."images/peserta/".$data['foto'];
                }?>

                <img class="avatar rounded-circle img-thumbnail" style="width:250px;height:250px;"  src=<?=$src?>>

               <!-- </div>
            <div class="col">-->
                <div class="clearfix">
                <br>
                <div class="box-body table-responsive">
                <table class="table table-striped table-borderless">
                    <tbody>
                        <tr><td>Nama</td><td><?=$data['first_name'].' '.$data['last_name'];?></td></tr>                                     
                        <tr><td>Telp</td><td><?=$data['telp'];?></td></tr>                            
                        <tr><td>Email</td><td><?=$data['email'];?></td></tr>
                        <tr><td>Tempat/Tanggal Lahir</td><td><?=$data['bornPlace'].', '.tgl_indo($data['bornDate'],false);?></td></tr>
                        <tr><td>Perusahaan</td><td><?=$data['company'];?></td></tr>
                        <tr><td>Status</td><td><?php if($data['active']==1){ echo "Aktif"; }else{ echo "Nonaktif"; } ?></td></tr>
                        <tr><td>Tanggal Reg</td><td><?=tgl_indo($data['insertDate'],true,true);?></td></tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    <?php 

    mysqli_close($conn);

    }else if(isset($_GET['on'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET status = '1', keterangan = 'Dokumen Lengkap', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['off'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET status = '0', keterangan = '', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 
                'db' => '`a`.`user`',
                'dt' => 1,
                'field' => 'user',
                'formatter' => function ($a) {
                        $user = dbGetRow('user'," id = '".$a."'",'','');
                        return $user['first_name'].' '.$user['last_name'];
                    }
            ),
            array( 
                'db' => '`a`.`user`',
                'dt' => 2,
                'field' => 'user',
                'formatter' => function ($b, $row) use ($kegiatan) {
                        $ujian      = dbGetAll('kegiatan_materi','(jenis_materi = 3 OR jenis_materi = 4 ) AND kegiatan = '.$kegiatan,'','','');
                        $sum        = 0;
                        $num        = 0;
                        foreach($ujian as $ujianVal){
                            $nilai  = dbGetRow('evaluasi_ujian',' user = '.$b.' AND kegiatan_materi = '.$ujianVal['id'],'nilai','DESC')['nilai'];
                            $sum    = $sum + $nilai;
                            $num++;
                        }
                        return $sum/$num;
                    }
            ),
            array( 
                'db' => '`a`.`user`',                
                'dt' => 3,
                'field' => 'user',
                'formatter' => function ($c, $row) use ($kegiatan) {
                        $essay      = dbGetAll('kegiatan_materi','(jenis_materi = 5 OR jenis_materi = 6 ) AND kegiatan = '.$kegiatan,'','','');
                        $sum        = 0;
                        $num        = 0;
                        foreach($essay as $essayVal){
                            $nilai  = dbGetRow('essay',' user = '.$c.' AND kegiatan_materi = '.$essayVal['id'],'nilai','DESC')['nilai'];
                            $sum    = $sum + $nilai;
                            $num++;
                        }
                        return $sum/$num;
                    }
            ),
            array( 
                'db' => '`a`.`user`',                
                'dt' => 4,
                'field' => 'user',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 5,
                'field' => 'kegiatan',
                'formatter' => function ($a) {
                    $title = dbGetRow('kegiatan','id = '.$a,'','')['title'];
                    return $title;
                }
            ),    
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>