<?php
session_name("sepro_admin");
session_start();

if(empty($_SESSION['admin'])){
    
}else{

    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'user';
    $id         = @$_GET['id'];
    $editor     = $_SESSION['admin'];

    //$menu 		= @$_POST['menu'];
    //$menu		= implode("|",$menu);

    if(isset($_GET['add'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
            title           = '$_POST[group]',
            akses           = '$menu',
            editor          = '$editor'
        ");  
          
        mysqli_close($conn);

    }else if(isset($_GET['upd'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            title           = '$_POST[group]',
            akses           = '$menu',
            editor          = '$editor' WHERE
            id              = '$_POST[id]'
            
        ");  
            
        mysqli_close($conn);

    }else if(isset($_GET['view'])){
        $conn     = db_conn_sepro();
        $data     = dbGetRow('user'," id = '".$id."'",'','');
    ?>
        <div class="row">
            <div class="col text-center">
                <?php if(empty($data['foto'])){ 
                    $src= "../../../images/nofoto.png";
                }else{
                    $src = $domain."images/peserta/".$data['foto'];
                }?>

                <img class="avatar rounded-circle img-thumbnail" style="width:250px;height:250px;"  src=<?=$src?>>

               <!-- </div>
            <div class="col">-->
                <div class="clearfix">
                <br>
                
                <table class="table table-striped table-borderless">
                    <tbody>
                        <tr><td width="150">Nama</td><td><?=$data['first_name'].' '.$data['last_name'];?></td></tr>                                     
                        <tr><td>Telp</td><td><?=$data['telp'];?></td></tr>                            
                        <tr><td>Email</td><td><?=$data['email'];?></td></tr>
                        <tr><td>Tgl Input</td><td><?=$data['insertDate'];?></td></tr>
                        <tr><td>Alamat</td><td><?=$data['alamat'];?></td></tr>
                        <tr><td>Status</td><td><?php if($data['active']==1){ echo "Aktif"; }else{ echo "Nonaktif"; } ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php 

    mysqli_close($conn);

    }else if(isset($_GET['on'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET active = '1', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['off'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET active = '0', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 'db' => '`a`.`first_name`', 'dt' => 1,'field' => 'first_name'),
            array( 'db' => '`a`.`last_name`', 'dt' => 2,'field' => 'last_name'),
            array( 'db' => '`a`.`email`', 'dt' => 3,'field' => 'email'),
            array( 'db' => '`a`.`telp`', 'dt' => 4,'field' => 'telp'),
            array( 'db' => '`a`.`company`', 'dt' => 5,'field' => 'company'),
            array( 'db' => '`a`.`insertDate`', 'dt' => 6,'field' => 'insertDate'),
            array( 
                'db' => '`a`.`editor`',                
                'dt' => 7,
                'field' => 'editor',
                'formatter' => function ($a) {
                        $admin = dbGetRow('admin'," id = '".$a."'",'','')['fullName'];
                        return explode(" ",$admin)[0];
                    }
            ),     
            array( 'db' => '`a`.`active`', 'dt' => 8,'field' => 'active'),           
            
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>