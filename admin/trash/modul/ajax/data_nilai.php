<?php
if(empty($_SESSION['instruktur']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_instruktur");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'evaluasi_instruktur';
    //$id         = decrypt(@$_GET['id']);
    //$kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " instruktur = $instruktur ";
    $groupBy    = "kegiatan";

    $primaryKey = 'id';
    $joinQuery  = " FROM `{$table}` AS `a` ";
    $columns = array(
        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 1,
            'field' => 'kegiatan',
            'formatter' => function ($a) {
                    $pelatihan  = dbGetRow('kegiatan'," id = '".$a."'",'','')['title'];
                    return $pelatihan;
                }
        ),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 2,
            'field' => 'kegiatan',
            'formatter' => function ($b, $row) use ($instruktur,$table) {

                    $conn   = db_conn_sepro();
                    $query  = mysqli_query($conn,"SELECT AVG(nilai) AS rata FROM $table WHERE instruktur = '$instruktur' AND kegiatan = '$b' ");
                    $row    = mysqli_fetch_assoc($query);
                    return number_format($row['rata'],2);
                }
        ),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 3,
            'field' => 'kegiatan',
            'formatter' => function ($b) {
                    return encrypt($b);
                }
        ),
        
    );

    require( 'ssp.class.php' );

    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
    );
   
}
?>