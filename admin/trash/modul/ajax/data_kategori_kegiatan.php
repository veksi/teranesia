<?php
session_name("sepro_admin");
session_start();

if(empty($_SESSION['admin'])){
    
}else{

    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kategori_kegiatan';
    $id         = @$_GET['id'];
    $editor     = $_SESSION['admin'];

    if(isset($_GET['add'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
            title           = '$_POST[cat]',
            editor          = '$editor'
        ");  
          
        mysqli_close($conn);

    }else if(isset($_GET['upd'])){
        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            title           = '$_POST[cat]',
            editor          = '$editor' WHERE
            id              = '$_POST[id]'
            
        ");  
            
        mysqli_close($conn);

    }else if(isset($_GET['on'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET publish = '1', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['off'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET publish = '0', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
            array( 'db' => '`a`.`insertDate`', 'dt' => 2,'field' => 'insertDate'),
            array( 'db' => '`a`.`publish`', 'dt' => 3,'field' => 'publish'),
            array( 
                'db' => '`a`.`editor`',                
                'dt' => 4,
                'field' => 'editor',
                'formatter' => function ($a) {
                        $admin = dbGetRow('admin'," id = '".$a."'",'','')['fullName'];
                        return explode(" ",$admin)[0];
                    }
            ),    
            
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>