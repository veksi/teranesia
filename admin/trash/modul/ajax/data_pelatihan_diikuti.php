<?php
if(empty($_SESSION['member']) && empty($_SERVER['HTTP_AUTHORIZATION'])){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    session_name("sepro_member");
    session_start();

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan_peserta';
    $id         = decrypt(@$_GET['id']);
    $kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $user       = $_SESSION['member'];

    $extraWhere = " user = $user ";

    if(isset($_GET['join'])){
        $result                 = dbGetNum($table,"user = '".$user."' AND kegiatan = '".$kegiatan."'",'','');
        $userData               = dbGetRow('user',"id = '".$user."'",'','');

        if( 
            empty($userData['first_name'])||
            empty($userData['bornPlace'])||
            empty($userData['hp'])||
            ($userData['bornDate']=='0000-00-00')||
            empty($userData['alamat'])||
            empty($userData['foto'])||
            empty($userData['company'])||
            empty($userData['company_address'])||
            empty($userData['jabatan'])||
            empty($userData['telp'])
        ){
            echo 2;
        }else{
            if($result==0){
                $conn               = db_conn_sepro();
                $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
                    kegiatan        = '$kegiatan',
                    user            = '$user'
                ");  
                mysqli_close($conn);
                echo 1;
            }else{
                echo 0;
            }
        }
        
    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 
                'db' => '`a`.`id`',                
                'dt' => 0,
                'field' => 'id',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 1,
                'field' => 'kegiatan',
                'formatter' => function ($a) {
                        $kegiatan = dbGetRow('kegiatan'," id = '".$a."'",'','')['title'];
                        return $kegiatan;
                    }
            ),
            array( 'db' => '`a`.`status`', 'dt' => 2,'field' => 'status'),
            array( 'db' => '`a`.`insertDate`', 'dt' => 3,'field' => 'insertDate'),
            array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 4,
                'field' => 'kegiatan',
                'formatter' => function ($b) {
                        return encrypt($b);
                    }
            ),
            array( 'db' => '`a`.`keterangan`', 'dt' => 5,'field' => 'keterangan'),
            /*array( 
                'db' => '`a`.`kegiatan`',                
                'dt' => 5,
                'field' => 'kegiatan',
                'formatter' => function ($c) {
                        $syarat = dbGetRow('kegiatan'," id = '".$c."'",'','')['syarat'];
                        return $syarat;
                    }
            ),*/
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>