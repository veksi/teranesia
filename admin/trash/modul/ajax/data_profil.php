<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";

if(empty($_SESSION['instruktur'])){

    header('Location:'.$instrukturPath);

}else{
    
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table          = 'instruktur';
    $nama_d         = addslashes($_POST['name_d']);
    $nama_b         = addslashes($_POST['name_b']);
    $alamat         = addslashes($_POST['alamat']);    
    $bornDate       = date("Y-m-d", strtotime($_POST['Dates']));

    if($_POST['mode']=='upd'){

        $time       = date("his");

        if(empty($_POST['password'])){
            $password       = "";
        }else{
            $password       = md5($_POST['password']);
            $password       = "password        = '$password',";
        }

        if(empty($_FILES['photo1']['name'])){
            $fotoname       = $_POST['imgDB1'];
        }else if( (empty($_FILES['photo1']['name'])) && empty($_POST['imgDB1']) ){
            $fotoname       = "";
        }else{
            $folderPath 	= "../../../images/instruktur/";
            $foto           = $_FILES['photo1'];
            $ext            = explode(".",$_FILES['photo1']['name']);
            $ext_img        = end($ext);
            $fotoname       = $_SESSION['instruktur'].".".$ext_img;
            compress($foto["tmp_name"], $folderPath.$fotoname);
        }


        // file pendukung
        if(empty($_FILES['cv']['name'])){
            $CVname             = $_POST['cv_DB'];
        }else if( (empty($_FILES['cv']['name'])) && empty($_POST['cv_DB']) ){
            $CVname             = "";
        }else{
            $berkasPath 	    = $_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/';
            $CVext              = explode(".",str_replace(" ","_",$_FILES['cv']['name']));
            //$CVname             = $CVext[0].'_'.$_POST['pelatihan'].$_SESSION['instruktur'].$time.'.'.end($CVext);
            $CVname             = $CVext[0].'_'.$_SESSION['instruktur'].'.'.end($CVext);
            move_uploaded_file($_FILES['cv']["tmp_name"], $berkasPath.$CVname);
        }
        
        if(empty($_FILES['ktp']['name'])){
            $KTPname             = $_POST['ktp_DB'];
        }else if( (empty($_FILES['ktp']['name'])) && empty($_POST['ktp_DB']) ){
            $KTPname             = "";
        }else{
            $berkasPath 	    = $_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/';
            $KTPext             = explode(".",str_replace(" ","_",$_FILES['ktp']['name']));
            //$KTPname            = $KTPext[0].'_'.$_POST['pelatihan'].$_SESSION['instruktur'].$time.'.'.end($KTPext);
            $KTPname            = $KTPext[0].'_'.$_SESSION['instruktur'].'.'.end($KTPext);
            move_uploaded_file($_FILES['ktp']["tmp_name"], $berkasPath.$KTPname);
        }

        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            first_name      = '$nama_d',
            last_name       = '$nama_b',
            $password            
            email           = '$_POST[email]',
            bornPlace       = '$_POST[born]',
            bornDate        = '$bornDate',
            telp            = '$_POST[telp]',
            foto            = '$fotoname',
            alamat          = '$alamat',
            instansi        = '$_POST[instansi]',
            bank            = '$_POST[bank]',
            rek_bank        = '$_POST[rek]',
            no_npwp         = '$_POST[npwp]',
            cv_file         = '$CVname',
            ktp_file        = '$KTPname' WHERE
            id              = '$_SESSION[instruktur]'
            
        ");  
            
        mysqli_close($conn);
        header('Location:'.$instrukturPath);

    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        $berkas     = dbGetRow($table,'id = '.$_SESSION['instruktur'],'','');
        
        if($_GET['dokumen']=='ktp'){
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/'.$berkas['ktp_file'])){
                unlink($_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/'.$berkas['ktp_file']);
            }
            
            mysqli_query($conn,"UPDATE $table SET ktp_file ='' WHERE id = '$_SESSION[instruktur]' ");
        }else{
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/'.$berkas['cv_file'])){
                unlink($_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/'.$berkas['cv_file']);
            }
            
            mysqli_query($conn,"UPDATE $table SET cv_file ='' WHERE id = '$_SESSION[instruktur]' ");
        }
        
        mysqli_close($conn);

    }else if(isset($_GET['file'])){

        $conn       = db_conn_sepro();

        if($_GET['dokumen']=='ktp'){
            $berkasPath 	    = $_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/';
            $KTPext             = explode(".",str_replace(" ","_",$_FILES['ktp']['name']));
            $KTPname            = $KTPext[0].'_'.$_SESSION['instruktur'].'.'.end($KTPext);
            move_uploaded_file($_FILES['ktp']["tmp_name"], $berkasPath.$KTPname);
        
            mysqli_query($conn," UPDATE $table SET ktp_file = '$KTPname' WHERE id = '$_SESSION[instruktur]' ");
        }else{
            $berkasPath 	    = $_SERVER['DOCUMENT_ROOT'].'/berkas/instruktur/';
            $CVext              = explode(".",str_replace(" ","_",$_FILES['cv']['name']));
            $CVname             = $CVext[0].'_'.$_SESSION['instruktur'].'.'.end($CVext);
            move_uploaded_file($_FILES['cv']["tmp_name"], $berkasPath.$CVname);

            mysqli_query($conn," UPDATE $table SET cv_file = '$CVname' WHERE id = '$_SESSION[instruktur]' ");
        }  

        mysqli_close($conn);
    }
}
?>