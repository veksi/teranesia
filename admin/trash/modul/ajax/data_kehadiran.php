<?php
session_name("sepro_instruktur");
session_start();

require "../../setting/global.php";
include "../../setting/connection.php";   
include "../../library/function.php";
include "../../library/query.php";

$bypass = dbGetNum('instruktur',"token = '".$_SERVER['HTTP_AUTHORIZATION']."'",'','');

if(empty($_SESSION['instruktur']) && $bypass==0){

    defined("BASEPATH") or exit("No direct access allowed");

}else{

    $table      = 'absensi_instruktur';
    //$id         = decrypt(@$_GET['id']);
    //$kegiatan   = urlencode(decrypt(@$_GET['kegiatan']));
    $instruktur = $_SESSION['instruktur'];

    $extraWhere = " instruktur = $instruktur ";
    $groupBy    = "kegiatan";

    $primaryKey = 'id';
    $joinQuery  = " FROM `{$table}` AS `a` ";
    $columns = array(
        array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 1,
            'field' => 'kegiatan',
            'formatter' => function ($a) {
                    $pelatihan  = dbGetRow('kegiatan'," id = '".$a."'",'','')['title'];
                    return $pelatihan;
                }
        ),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 2,
            'field' => 'kegiatan',
            'formatter' => function ($b, $row) use ($instruktur,$table) {
                    $result = dbGetNum($table,"kegiatan = '".$b."' AND instruktur = ".$instruktur,'','');
                    return $result;
                }
        ),
        array( 
            'db' => '`a`.`kegiatan`',                
            'dt' => 3,
            'field' => 'kegiatan',
            'formatter' => function ($c) {
                    return encrypt($c);
                }
        ),
        
    );

    require( 'ssp.class.php' );

    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
    );

}
?>