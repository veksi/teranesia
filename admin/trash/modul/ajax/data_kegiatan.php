<?php
session_name("sepro_admin");
session_start();

if(empty($_SESSION['admin'])){
    
}else{

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'kegiatan';
    $editor     = $_SESSION['admin'];
    $id         = @$_GET['id'];
    $title      = addslashes($_POST['title']);
    $konten     = addslashes($_POST['konten']);

    if($_POST['mode']=='add'){

        $folderPath 	= "../../../images/banner/";
        $foto           = $_FILES['photo1'];
        $fotoname       = str_replace(" ","_",$foto['name']);
        compress($foto["tmp_name"], $folderPath.$fotoname);

        $conn                   = db_conn_sepro();
        $ins                    = mysqli_query($conn,"INSERT INTO $table SET                    
            banner              = '$fotoname',
            title               = '$title',
            konten              = '$konten',
            jadwal              = '$_POST[Dates]',
            editor              = '$editor'
        ");  
          
        mysqli_close($conn);

        header('Location:'.$adminPath.'pelatihan');

    }else if($_POST['mode']=='upd'){

        if(empty($_FILES['photo1']['name'])){
            $fotoname       = $_POST['imgDB1'];
        }else if( (empty($_FILES['photo1']['name'])) && empty($_POST['imgDB1']) ){
            $fotoname       = "";
        }else{
            $folderPath 	= "../../../images/banner/";
            $foto           = $_FILES['photo1'];
            $fotoname       = str_replace(" ","_",$foto['name']);
            compress($foto["tmp_name"], $folderPath.$fotoname);
        }

        $conn                   = db_conn_sepro();
        $ins                    = mysqli_query($conn,"UPDATE $table SET     
            banner              = '$fotoname',               
            title               = '$title',
            kategori_kegiatan   = '$_POST[category]',
            konten              = '$konten',
            jadwal              = '$_POST[Dates]',
            editor              = '$editor' WHERE
            id                  = '$_POST[id]'
            
        ");  
            
        mysqli_close($conn);

        header('Location:'.$adminPath.'pelatihan');

    }else if(isset($_GET['on'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET publish = '1', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['off'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET publish = '0', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 'db' => '`a`.`title`', 'dt' => 1,'field' => 'title'),
            array( 'db' => '`a`.`insertDate`', 'dt' => 2,'field' => 'insertDate'),
            array( 'db' => '`a`.`publish`', 'dt' => 3,'field' => 'publish'),
            array( 
                'db' => '`a`.`editor`',                
                'dt' => 4,
                'field' => 'editor',
                'formatter' => function ($a) {
                        $admin = dbGetRow('admin'," id = '".$a."'",'','')['fullName'];
                        return explode(" ",$admin)[0];
                    }
            ),    
            
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>