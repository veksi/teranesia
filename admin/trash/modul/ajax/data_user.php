<?php
session_name("sepro_admin");
session_start();

if(empty($_SESSION['admin'])){
    
}else{

    require "../../setting/global.php";
    include "../../setting/connection.php";   
    include "../../library/function.php";
    include "../../library/query.php";

    $table      = 'admin';
    $id         = decrypt(@$_GET['id']);
    $editor     = $_SESSION['admin'];
    $nama       = addslashes($_POST['nama']);
    $alamat     = addslashes($_POST['alamat']);

    if($_POST['mode']=='add'){

        $password       = md5($_POST['password']);
        $folderPath 	= "../../../images/admin/";
        $foto           = $_FILES['photo1'];
        $fotoname       = str_replace(" ","_",$foto['name']);
        compress($foto["tmp_name"], $folderPath.$fotoname);

        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"INSERT INTO $table SET                    
            fullName        = '$nama',
            password        = '$password',
            phone           = '$_POST[phone]',
            email           = '$_POST[email]',
            foto            = '$fotoname',
            akses           = '$_POST[akses]',
            editor          = '$editor'
        ");  
          
        mysqli_close($conn);

        header('Location:'.$adminPath.'daftar-admin');

    }else if($_POST['mode']=='upd'){

        if(empty($_POST['password'])){
            $password       = "";
        }else{
            $password       = md5($_POST['password']);
            $password       = "password        = '$password',";
        }

        if(empty($_FILES['photo1']['name'])){
            $fotoname       = $_POST['imgDB1'];
        }else if( (empty($_FILES['photo1']['name'])) && empty($_POST['imgDB1']) ){
            $fotoname       = "";
        }else{
            $folderPath 	= "../../../images/admin/";
            $foto           = $_FILES['photo1'];
            $fotoname       = str_replace(" ","_",$foto['name']);
            compress($foto["tmp_name"], $folderPath.$fotoname);
        }

        $conn               = db_conn_sepro();
        $ins                = mysqli_query($conn,"UPDATE $table SET                    
            fullName        = '$nama',
            $password
            phone           = '$_POST[phone]',
            email           = '$_POST[email]',
            foto            = '$fotoname',
            akses           = '$_POST[akses]',
            editor          = '$editor' WHERE
            id              = '$_POST[id]'
            
        ");  
            
        mysqli_close($conn);
        header('Location:'.$adminPath.'daftar-admin');

    }else if(isset($_GET['view'])){
        $conn     = db_conn_sepro();
        $data     = dbGetRow('admin'," id = '".$id."'",'','');
    ?>
        <div class="row">
            <div class="col text-center">
                <?php if(empty($data['foto'])){ 
                    $src= "../../../images/nofoto.png";
                }else{
                    $src = $domain."images/admin/".$data['foto'];
                }?>

                <img class="avatar rounded-circle img-thumbnail" style="width:250px;height:250px;" src=<?=$src?>>

            <!--</div>
            <div class="col">-->
                <div class="clearfix">
                <br>

                <table class="table table-striped table-borderless">
                    <tbody>
                        <tr><td width="150">Nama</td><td><?=$data['fullName'];?></td></tr>                                     
                        <tr><td>Telp</td><td><?=$data['phone'];?></td></tr>                            
                        <tr><td>Email</td><td><?=$data['email'];?></td></tr>
                        <tr><td>Tgl Input</td><td><?=$data['insertDate'];?></td></tr>
                        <tr><td>Akses</td><td><?=dbGetRow('admin_group'," id = '".$data['akses']."'",'','')['title'];?></td></tr>
                        <tr><td>Status</td><td><?php if($data['active']==1){ echo "Aktif"; }else{ echo "Nonaktif"; } ?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php 

    mysqli_close($conn);

    }else if(isset($_GET['on'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET active = '1', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['off'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"UPDATE $table SET active = '0', editor = '$editor' WHERE id = '$id'");
        mysqli_close($conn);

    }else if(isset($_GET['del'])){
        $conn       = db_conn_sepro();
        mysqli_query($conn,"DELETE FROM $table WHERE id = '$id' ");
        mysqli_close($conn);

    }else{
        $primaryKey = 'id';
        $joinQuery  = " FROM `{$table}` AS `a` ";
        $columns = array(
            //array( 'db' => '`a`.`id`', 'dt' => 0,'field' => 'id'),
            array( 
                'db' => '`a`.`id`',                
                'dt' => 0,
                'field' => 'id',
                'formatter' => function ($id) {
                        return encrypt($id);
                    }
            ),
            array( 'db' => '`a`.`fullName`', 'dt' => 1,'field' => 'fullName'),
            array( 'db' => '`a`.`email`', 'dt' => 2,'field' => 'email'),
            array( 'db' => '`a`.`phone`', 'dt' => 3,'field' => 'phone'),
            array( 
                'db' => '`a`.`akses`',                
                'dt' => 4,
                'field' => 'akses',
                'formatter' => function ($a) {
                        $admin = dbGetRow('admin_group'," id = '".$a."'",'','')['title'];
                        return $admin;
                    }
            ),
            array( 'db' => '`a`.`insertDate`', 'dt' => 5,'field' => 'insertDate'),
            array( 'db' => '`a`.`active`', 'dt' => 6,'field' => 'active'),
            array( 
                'db' => '`a`.`editor`',                
                'dt' => 7,
                'field' => 'editor',
                'formatter' => function ($a) {
                        $admin = dbGetRow('admin'," id = '".$a."'",'','')['fullName'];
                        return explode(" ",$admin)[0];
                    }
            ),
        );
    
        require( 'ssp.class.php' );
    
        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
        );
    }
}
?>