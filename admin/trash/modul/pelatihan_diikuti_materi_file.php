<?php
$instruktur = dbGetRow('instruktur','id = '.$sesi['instruktur'],'','');
if(!empty(@$instruktur['foto'])){ 
    $ppIns = $domain."images/instruktur/".$instruktur['foto'].'?'.$rand;
}else{ 
    $ppIns = "https://image.flaticon.com/icons/svg/660/660611.svg";
}
?>
<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
    <div class="jumbotron p-2 mb-4 bg-white">
        <div class="row justify-content-md-center">

            <div class="col-sm-3">
              <div class="form-group sticky-top" style="top:80">
                  <div class="text-center">
                      <img id="pp" data-type="editable1" class="avatar img-thumbnail rounded-circle" alt="avatar"  src=<?=$ppIns;?> >
                      <div class="clearfix"></div>
                      <br>
                  </div>
              </div>
            </div>

            <div class="col-sm-9">

              <div class="form-group">
                <label><h5><?=$sesi['title']?></h5></label>
                <hr class="my-4">
                <?=$sesi['konten'];?>
              </div>
            </div>
            
        </div>
    </div>
</div>