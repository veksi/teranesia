<?php 
if(isset($_POST['register'])){

    $reg = dbGetNum('instruktur',"email = '".$_POST['email']."'",'','');
    if($reg==1){ ?>
        <script>swal("Gagal","Email sudah terdaftar","warning",).then(function(){ window.history.back(); });</script>
    <?php }else{
        
        reg($_POST['name_d'],$_POST['name_b'],$_POST['email'],$_POST['password']); 
        include "library/mail/register.php";
    ?>

        <body class="bg-primary">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <main>
                        <div class="container text-center">
                            <div class="row justify-content-center">
                                <div class="col-lg-7">
                                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                                        <div class="card-header"><h3 class="text-center font-weight-light my-4">Terima Kasih</h3></div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <i class="fa fa-check-square-o fa-5x" style="color: #DCC000"></i>
                                            </div>
                                            <div class="form-group">
                                                Akun anda akan aktif setelah diverifikasi oleh tim kami maksimal 1 x 24 Jam.
                                            </div>
                                            <div class="form-group">
                                                <a href="<?=$instrukturPath?>" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Oke">Oke</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
                <br>
                <br>
                <div id="layoutAuthentication_footer">
                    <?php require "modul/copy.php"; ?>
                </div>
            </div>
        </body>
        
    <?php } ?>

<?php }else{ ?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Daftar Sekarang</h3></div>
                                    <div class="card-body">
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="register">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputFirstName">Nama Depan</label>
                                                        <input class="form-control py-4" id="name_d" name="name_d" type="text" placeholder="Nama depan" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputLastName">Nama Belakang</label>
                                                        <input class="form-control py-4" id="name_b" name="name_b" type="text" placeholder="Nama belakang" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" id="email" name="email" type="text" aria-describedby="emailHelp" placeholder="Email " />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputPassword">Kata Sandi</label>
                                                <input class="form-control py-4" id="pwd" name="password" type="password" placeholder="Kata sandi" />
                                            </div>
                                                
                                            <!--<div class="form-group mt-4 mb-0"><input type="submit" id="loginBtn" value="Daftar" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Daftar"></div>-->
                                            <button type="submit" value="Daftar" class="btn btn-primary btn-block loadingBtn" >Daftar</button>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?=$instrukturPath?>">Sudah punya akun? Masuk</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

<?php } ?>

<script>
$("#name_d").focus();
function validasi(){

    var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var name_d      = $("#name_d").val();
    //var name_b      = $("#name_b").val();
    var email       = $("#email").val();
    var password    = $("#pwd").val();
    
    if (name_d==""){

        swal("","Lengkapi data dengan benar","warning",).then(function(){ $("#name_d").focus();$(".loadingBtn").html('Daftar'); });
        return false;
    
    //}else if (name_b==""){

      //  swal("","Lengkapi data dengan benar","warning",).then(function(){ $("#name_b").focus();$(".loadingBtn").html('Daftar'); });
      //  return false;

    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $("#email").focus();$(".loadingBtn").html('Daftar'); });
        return false;

    }else if (password.length<5){

        swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $("#pwd").focus();$(".loadingBtn").html('Daftar'); });
        return false;

    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus();$(".loadingBtn").html('Daftar'); });
        return false;

    }else{

        return true;

    }

}
</script>