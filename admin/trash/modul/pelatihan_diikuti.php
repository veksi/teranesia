<div class="container-fluid">
    <div class="box-body table-responsive px-2">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Judul Pelatihan</th>
                    <th style="border-bottom:none;font-weight:600">Tanggal Reg</th>
                    <th style="border-bottom:none;font-weight:600">Status</th>
                    <th style="border-bottom:none;font-weight:600"></th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="<?=$adminPath;?>plugin/dataTable/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
   
   $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({
        //"processing": true,
        //"language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        //"oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax":{
            url: '<?=$domain;?>modul/ajax/data_pelatihan_diikuti.php',
            headers: { Authorization: '<?=$_SESSION['member']?>' },
        },
        "order": [[ 2, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": titleCol,
                "targets": 1 
            },
            {
                "data": 3,
                "targets": 2
            },
            {
                "render": status,
                "targets": 3
            },
            {
                "data": 5,
                "targets": 4
            },
            {
                "render": actionCol,
                "targets": 5
            }
        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function actionCol(data, type, full) {

        if(full[2]==1){
            link = '<a class="dropdown-item" href=<?=$domain?>pelatihan-saya/'+full[4]+' >Detail</a>';
        }else{
            link = '';
        }

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li>'+link+'</li><li><a class="dropdown-item" href=# onclick=deleteRecord("'+full[0]+'")>Hapus</a></li></ul></div>';
    }

    function titleCol(data, type, full) {

        if(full[2]==1){
            link = "<a href=<?=$domain?>pelatihan-saya/"+slugify(full[1])+"-"+full[4]+" >"+full[1]+"</a>";            
        }else{
            link = full[1];
        }

        return ''+link+'';

    }

    function status(data, type, full) {

        if(full[5]==''){
            if(full[2]==1){
                status = "<span class='badge badge-pill badge-success font-weight-normal'>Verified</span>";
            }else{
                status = "<span class='badge badge-pill badge-info font-weight-normal'>Menunggu</span>";
            }
        }else{
            if(full[2]==1){
                status = "<span class='badge badge-pill badge-success font-weight-normal '>Verified</span>";
            }else{
                status = '<a class="btn btn-primary btn-sm" href=<?=$domain?>pelatihan-saya/berkas-'+full[4]+' >Upload Berkas <i class="fa fa-upload" aria-hidden="true"></i></a>';
                //status = '<input type="file" class="form-control-file" id="exampleFormControlFile1" multiple>';
            }
        }
        

        return ''+status+'';

    }

    function deleteRecord(id){
        swal({
            title: "Data akan dihapus?",
            text: "Setelah dihapus, data tidak bisa dikembalikan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: '<?=$domain;?>modul/ajax/data_pelatihan_diikuti.php?del&id='+id,
                    headers: { Authorization: '<?=$_SESSION['member']?>' },
                    success: function(respon)
                    {
                        reload();
                        swal("Done","Data telah dihapus!", {
                            icon: "success",
                        });
                    }       
                });
                
            } else {
                //swal("Your data is safe!");
            }
        });
    }

    function success(msg){
        swal("Done", msg, "success");
    }

    function reload(){
        otable.ajax.reload();
    }

</script>