<div class="container-fluid">
    <div class="box-body table-responsive">
        <table id="tabelAuthor" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th style="border-bottom:none;font-weight:600">No</th>
                    <th style="border-bottom:none;font-weight:600">Peserta</th>                    
                    <th style="border-bottom:none;font-weight:600">Nilai Ujian (Rata-rata)</th>
                    <th style="border-bottom:none;font-weight:600">Nilai Tugas (Rata-rata)</th>
                    <th style="border-bottom:none;font-weight:600"></th>
                </tr>
            </thead>                                            
        </table>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">

    var kegiatan = '<?=$id?>';
   
   $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var otable = $('#tabelAuthor').DataTable({
        "processing": true,
        //"language": { processing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "oLanguage": {sProcessing: '<div class="lds-ripple"><div></div><div></div></div>'},
        "serverSide": true,
        "ajax":{
            url: '<?=$instrukturPath;?>modul/ajax/data_nilai_peserta.php?kegiatan='+kegiatan,
            headers: { Authorization: '<?=$_SESSION['token']?>' },
        },
        "order": [[ 0, "desc" ]],
        "language": { 
            "infoFiltered" : ""
        },
        "stateSave": false, //tetap pada current page apapun yg terjadi
        "stateSaveParams": function (settings, data) {
            data.search.search = "";
            // table.order = [[ 5, "desc" ]];
        },
        "fnCreatedRow": function (row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        },
        "columnDefs": [
            {
                "render": ujianCol,
                "targets": 2
            },
            {
                "render": tugasCol,
                "targets": 3
            },
            {
                "render": actionCol,
                "targets": 4 
            }
        ],

    });

    $('#tabelAuthor_filter input').unbind();
    $('#tabelAuthor_filter input').bind('keyup', function(e) {
        if(e.keyCode === 13) {
            otable.search( this.value ).draw();
        }
    });

    function ujianCol(data, type, full) {
        var standar = <?=$standar_ujian?>;
        if(full[2]>=standar){
            status = full[2]+" <span class='badge badge-pill badge-success font-weight-normal'>Lulus</span>";
        }else{
            status = full[2]+" <span class='badge badge-pill badge-danger font-weight-normal'>Tidak Lulus</span>";
        }
        return ''+status+'';

    }

    function tugasCol(data, type, full) {
        var standar = <?=$standar_tugas?>;
        if(full[3]>=standar){
            status = full[3]+" <span class='badge badge-pill badge-success font-weight-normal'>Lulus</span>";
        }else{
            status = full[3]+" <span class='badge badge-pill badge-danger font-weight-normal'>Tidak Lulus</span>";
        }
        return ''+status+'';

    }

    function actionCol(data, type, full) {

        nilai   = "<a class='dropdown-item' href=<?=$instrukturPath?>nilai-peserta/"+slugify(full[5])+"-<?=encrypt($id)?>/ujian-"+full[4]+">Nilai Ujian</a>";
        tugas   = "<a class='dropdown-item' href=<?=$instrukturPath?>nilai-peserta/"+slugify(full[5])+"-<?=encrypt($id)?>/tugas-"+full[4]+">Nilai Tugas</a>";

        return '<div class="btn-group pull-right"><button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu"><li>'+nilai+'</li><li>'+tugas+'</li></ul></div>';    
    }
</script>

<div class="modal fade" id="viewModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="result"></div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form>
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>-->
                    <div class="form-group">
                        <h4 class="modal-title pull-left">Tambah Group</h4>
                        <div class="pull-right">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white;opacity:unset;font-weight:unset;">
                            <span style="font-weight:unset;-webkit-text-stroke: 0.1px white;">X</span></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <script type="text/javascript">                 
                        $('#myModal').on('shown.bs.modal', function () {                                        
                            $('#cat').focus();     
                        })
                    </script> 
                   
                    <div class="form-group">
                        <input type="hidden" id="mode" name="mode" value="add">
                        <input type="hidden" id="id" name="id">
                        <label>Nama Group</label>
                        <input type="text" id="group" name="group" class="form-control" placeholder="Nama Group">
                    </div>
                
                    <div class="form-group">
                        <label>Akses Menu</label>
                        <?php
                        //$arrFas = explode('|',$data['menu']);            
                        $menu = dbGetAll('menu','','','','');
                        foreach($menu as $menuVal):?> 
                            <div class="checkbox">
                                <label><input type="checkbox"  value="<?=$menuVal['id']?>" name="menu[]" class="menu"><?=$menuVal['menu']?></label>
                            </div>             
                        <?php endforeach; ?>  
                    </div>
                   
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" id="save" class="btn btn-success"> Save</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->