<?php
  //$userPaid = dbGetRow('kegiatan_peserta',"kegiatan = '".$data['id']."' AND user = '".$_SESSION['member']."'" ,'','');

  if(!empty(@$data['banner'])){ 
      $pp = "src=".$domain."images/banner/".$data['banner']; 
  }else{ 
      $pp="src=".$domain."images/nofoto.png";
  }
?>

<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
    <div class="jumbotron p-2 mb-4 bg-white">
        <div class="row justify-content-md-center">

            <div class="col-sm-4">
              <div class="form-group sticky-top" style="top:80">
                  <div class="text-center">
                      <img id="pp" data-type="editable1" class="avatar img-thumbnail" alt="avatar"  <?=$pp;?> >
                      <div class="clearfix"></div>
                      <br>
                  </div>
              </div>
            </div>
            
            <div class="col-sm-8">

              <?php if(!empty($data['syarat'])){ ?>
                <div class="form-group">
                  <label><h5>Persyaratan</h5></label>
                  <p><?=$data['syarat']?></p>
                  <br>
                </div>
              <?php } ?>

              <div class="form-group">
                <label><h5>Deskripsi</h5></label>
                <p>
                  <?php 
                    $paragraph        = explode('<p>',$data['konten']);
                    $count_paragraph  = count($paragraph);
            
                    if($count_paragraph>5){
                      for($a=0;$a<=5;$a++){
                        echo $paragraph[$a];
                      }
                      echo "<div id=more style=display:none>";
                      for($b=6;$b<=($count_paragraph);$b++){
                        echo "$paragraph[$b]";
                      }
                      echo "</div>";
                    }else{
                      echo $data['konten'];
                    }
                    
                  ?>
                  <br>
                  <a class="btn btn-info btn-block" id="toggleButton" onclick="toggleText();" href="javascript:void(0);">Tampilkan lebih banyak</a>
                  <br>
                </p>
              </div>

              <div class="form-group">
                <label><h5>Materi</h5></label>
                <p>
                <div id="accordion">
                  <?php 
                  $jadwal = explode("|",$data['jadwal']);
                  $day    = 1; 
                  foreach($jadwal as $hariVal){ 
                      $hari   = date("Y-m-d", strtotime($hariVal));
                      $materi = dbGetAll('kegiatan_materi',"jadwal = '".$hariVal."' AND kegiatan=".$id,'urutan','ASC','');                       

                      $isMine = dbGetNum('kegiatan_materi',"instruktur = '".$_SESSION['instruktur']."' AND jadwal = '".$hariVal."' AND kegiatan=".$id,'','','');                       
                  ?>

                      <div class="card mb-0">
                          <div class="card-header bg-white p-2 " id="heading<?=$day?>">
                              <h5 class="mb-0 border-secondary">
                                  <button id="<?=$day?>" onClick="reply_click(this.id)" class="btn btn-link collapsed text-dark" data-toggle="collapse" data-target="#collapse<?=$day?>" aria-expanded="false" aria-controls="collapse<?=$day?>">
                                  <span id="arrow<?=$day?>"><i class="fa fa-chevron-down"></i></span> &nbsp;<b>Hari <?=$day?>, <?=tgl_indo($hari,false)?></b>
                                  </button>
                              </h5>
                          </div>

                          <div id="collapse<?=$day?>" class="collapse" aria-labelledby="heading<?=$day?>" >
                            <div class="card-body p-0">

                              <div class="box-body table-responsive-sm">
                                <table class="table">
                                <tbody>  

                                  <?php if($serverDate==$hari && $isMine > 0){ // jika sesi saya ?> 
                                    <tr>
                                      <td class="py-1 px-4">
                                        <a href="<?=$instrukturPath?>materi-saya/<?=seo($data['title']).'-'.encrypt($data['id'])?>/absensi-<?=encrypt($hariVal)?>" >Absensi</a>
                                      </td>
                                      <td class="py-1 px-4 align-middle" width="110px" style="padding:0 5 !important">-</td><td class="py-1 px-4 align-middle">-</td>
                                    </tr>
                                  <?php }else{ ?>
                                    <tr>
                                      <td class="py-1 px-4">
                                        Absensi
                                      </td>
                                      <td class="py-1 px-4 align-middle" width="110px" style="padding:0 5 !important">-</td><td class="py-1 px-4 align-middle">-</td>
                                    </tr>
                                  <?php } ?>   

                                  <?php 
                                  if(is_array($materi)){
                                    foreach($materi as $materiVal){ 
                                      if($materiVal['jenis_materi']!=1){
                                    ?>
                                        <tr>
                                          <td class="py-1 px-4">
                                            <?php                                             
                                            if($serverDate==$hari && $materiVal['instruktur']==$_SESSION['instruktur']){ ?> 
                                              <a href="<?=$instrukturPath?>materi-saya/<?=seo($data['title']).'-'.encrypt($data['id'])?>/<?=seo($materiVal['title']).'-'.encrypt($materiVal['id'])?>" ><?=$materiVal['title']?></a>
                                            <?php }else{ echo $materiVal['title'];}?>
                                          </td>
                                          <td class="py-1 px-4 align-middle" width="110px" style="padding:0 5 !important">
                                            <?php
                                              if($materiVal['jenis_materi']==2){
                                                echo date('H:i', strtotime($materiVal['waktu_mulai'])).' - '.date('H:i', strtotime($materiVal['waktu_selesai']));
                                              }else{
                                                echo "-";
                                              }
                                            ?>
                                          </td>
                                          <td class="py-1 px-4 align-middle">
                                            <?php
                                              if($materiVal['jenis_materi']==2){
                                                $instruktur = dbGetRow('instruktur','id = '.$materiVal['instruktur'],'','');
                                                echo $instruktur['first_name'].' '.$instruktur['last_name'];
                                              }else{
                                                echo "-";
                                              }
                                            ?>
                                          </td>
                                        </tr>
                                          
                                    <?php } 
                                      } 
                                  }
                                  ?>
                                </tbody>
                                </table>
                              </div>

                            </div>
                          </div>

                      </div>

                  <?php $day++; } ?>

                  </div>
                </p>
              </div>
              
            </div>
        </div>
    </div>
</div>

<script>
var status = "less";
function toggleText()
{
    if (status == "less") {
        document.getElementById("more").style.display = "block";
        document.getElementById("toggleButton").innerText = "Tampilkan lebih sedikit";
        status = "more";
    } else if (status == "more") {
        document.getElementById("more").style.display = "none";
        document.getElementById("toggleButton").innerText = "Tampilkan lebih banyak";
        status = "less"
    }
}

var arrow = "down";
function reply_click(clicked_id)
{ 
    if (arrow == "down") {
        document.getElementById("arrow"+clicked_id).innerHTML = "<i class='arrow fa fa-chevron-up'></i>";
        arrow = "up";
    } else if (arrow == "up") {
        document.getElementById("arrow"+clicked_id).innerHTML = "<i class='arrow fa fa-chevron-down'></i>";
        arrow = "down"
    }
}

function join(kegiatan){
  $.ajax({
      url: '<?=$domain;?>modul/ajax/data_pelatihan_diikuti.php?join&kegiatan='+kegiatan,
      headers: { Authorization: '<?=$_SESSION['token']?>' },
      success: function(respon)
      {
        if(respon==1){
          swal("Berhasil","Pendaftaran berhasil","success");      
        }else{
          swal("Info","Anda sudah terdaftar","info");      
        }
          
      },
      error: function(err) {
          console.log(err);
      }          
  });
}

</script>