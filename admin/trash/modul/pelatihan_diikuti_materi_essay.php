<label class="px-4"><h4><?=$data['title']?></h5></label>

<div class="container-fluid">
  <div class="jumbotron p-2 mb-4 bg-white">
    <div class="row justify-content-md-center">
      <div class="col-sm-8">
                
        <label><h5><?=$sesi['title']?></h5></label>
        
        <form action="<?=$domain?>modul/ajax/data_pelatihan_diikuti_materi_essay.php" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">  
          <input type="hidden" name='mode' value="ins">
          <input type="hidden" name='sesi' value="<?=$sesi_ID?>">
          <div class="row">
            <div class="col-sm">
              <div class="form-group">
                <div class="custom-file">
                  <input type="file" name="essay[]" class="custom-file-input" id="customFile" multiple>
                  <label class="custom-file-label" for="customFile" style="height:38px;overflow:hidden;">Choose file</label>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <!--<input type="submit" id="loginBtn" value="Upload" class="btn btn-primary btn-block"  data-loading-text="<i class='fa fa-spinner fa-spin '></i> Upload">-->
              <button type="submit" value="Upload" class="btn btn-primary btn-block loadingBtn">Upload</button>
            </div>
          </div>
        </form>

        <hr class="my-4">

        <div class="row">
          <?php $essay       = dbGetAll('essay',"kegiatan_materi = '".$sesi_ID."' AND user = '".$_SESSION['member']."'",'insertDate','DESC',''); ?>
          <?php foreach($essay as $essayVal){?>
            <div class="col-sm-3 mb-4">
              <a class="shortLink">
                <div class="card text-dark">
                  <div class="card-body">
                    <h5 class="card-title" style="height:50px;overflow:hidden;"><?=cekBerkasExt($essayVal['berkas'])?></h5>
                    <p class="card-text" style="height:50px;overflow:hidden;"><?=$essayVal['berkas']?></p>
                  </div>
                </div>
              </a>
            </div>
          <?php } ?>
        </div>

      </div>
    </div>          
  </div>        
</div>

<script>
$(document).ready(function() {
  $('input[type="file"]').on("change", function() {
    let filenames = [];
    let files = document.getElementById("customFile").files;
    if (files.length > 1) {
      filenames.push("Total Files (" + files.length + ")");
    } else {
      for (let i in files) {
        if (files.hasOwnProperty(i)) {
          filenames.push(files[i].name);
        }
      }
    }
    $(this)
      .next(".custom-file-label")
      .html(filenames.join(","));
  });
});

function validasi(){
  var file      = $("#customFile").val();
  if (file==""){
      swal("","Lengkapi data dengan benar","warning",).then(function(){ $("#customFile").focus();$(".loadingBtn").html('Upload'); });
      return false;
  }else{
      return true;
  }
}
</script>