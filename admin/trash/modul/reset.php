<?php 
if(isset($_POST['upd'])){ 
    newpwd($_POST['newpwd'],$_POST['email']); 
?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container text-center">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Terima Kasih</h3></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <i class="fa fa-check-square-o fa-5x" style="color: #DCC000"></i>
                                        </div>
                                        <div class="form-group">
                                            Kata sandi anda berhasil diganti, silahkan masuk untuk melanjutkan.
                                        </div>
                                        <div class="form-group">
                                            <a href="<?=$instrukturPath?>" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Oke">Oke</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

<?php }else if(isset($_POST['otp_cek'])){ ?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Kata Sandi Baru</h3></div>
                                    <div class="card-body">
                                        <div class="small mb-3 text-muted">Silahkan atur ulang kata sandi anda yang baru.
                                        </div>
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="upd">
                                            <input type="hidden" name="email" value="<?=$_POST['email']?>">
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputPassword">Kata Sandi</label>
                                                <input class="form-control py-4" id="pwd" name="password" type="password" placeholder="Kata sandi" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputPassword">Ulangi Kata Sandi</label>
                                                <input class="form-control py-4" id="newpwd" name="newpwd" type="password" placeholder="Ulangi Kata sandi" />
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0">
                                                <!--<input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim">-->
                                                <button type="submit" value="Kirim" class="btn btn-primary btn-block loadingBtn" >Kirim</button>
                                            </div>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

    <script>
    $("#pwd").focus();
    function validasi(){
        var pwd       = $("#pwd").val();
        var newpwd    = $("#newpwd").val();
        
        if(pwd.length<5){
            swal("Gagal", "Kata sandi minimal 5 karakter", "error").then(function(){ $("#pwd").focus();$(".loadingBtn").html('Kirim'); });
            return false;
        }else if(newpwd.length<5){
            swal("Gagal", "Kata sandi minimal 5 karakter", "error").then(function(){ $("#newpwd").focus();$(".loadingBtn").html('Kirim'); });
            return false;
        }else if(pwd!=newpwd){
            swal("Gagal", "Kata sandi tidak sama", "error").then(function(){ $("#newpwd").focus();$(".loadingBtn").html('Kirim'); });
            return false;
        }else{
            return true;
        }
        
    }

    </script>

<?php }else if(isset($_POST['email_cek'])){ ?>
    
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Kode OTP</h3></div>
                                    <div class="card-body">
                                        <div class="small mb-3 text-muted">Masukkan kode OTP yang anda terima.
                                        </div>
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="otp_cek">
                                            <input type="hidden" id="email" name="email" value="<?=$_POST['email']?>">      
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="one" name="one" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="two" name="two" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="three" name="three" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="four" name="four" type="text" />
                                                    </div>
                                                    <div class="col p-2">
                                                        <input class="form-control py-4 text-center" id="five" name="five" type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0">
                                                <!--<input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim">-->
                                                <button type="submit" value="Kirim" class="btn btn-primary btn-block loadingBtn" >Kirim</button>
                                            </div>
                                        </form>                                        
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?=$instrukturPath?>reset">Tidak menerima OTP? Kirim ulang</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

    <script>
    $("#one").focus();
    $("#one").keypress(function () {$("#two").focus();});
    $("#two").keypress(function () {$("#three").focus();});
    $("#three").keypress(function () {$("#four").focus();});
    $("#four").keypress(function () {$("#five").focus();});

    function otp_cek(m,code){
        var result="";
        $.ajax({
            url: '<?=$instrukturPath;?>modul/ajax/data_reset.php?b&m='+m+'&code='+code,
            async: false,
            success: function(data)
            {   
                result = data;
            }    
        });
        return result;
    }

    function validasi(){
        var m       = $("#email").val();
        var one     = $("#one").val();
        var two     = $("#two").val();
        var three   = $("#three").val();
        var four    = $("#four").val();
        var five    = $("#five").val();
        var code    = one+two+three+four+five;
        if(otp_cek(m,code)=='1'){
            return true;
        }else{
            swal("Gagal", "OTP tidak valid", "error").then(function(){ $("input:text").val(""); $("#one").focus();$(".loadingBtn").html('Kirim'); });
            return false;
        }
        
    }

    </script>
        
<?php }else{ ?>

    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Lupa Kata Sandi</h3></div>
                                    <div class="card-body">
                                        <div class="small mb-3 text-muted">Masukkan e-mail yang terdaftar. Kami akan mengirimkan kode verifikasi untuk atur ulang kata sandi.
                                        </div>
                                        <form action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
                                            <input type="hidden" name="email_cek">
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" id="email" name="email" type="text" aria-describedby="emailHelp" placeholder="Email " />
                                            </div>
                                            
                                            <div class="form-group mt-4 mb-0">
                                                <!--<input type="submit" id="loginBtn" value="Kirim" class="btn btn-primary btn-block" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Kirim">-->
                                                <button type="submit" value="Kirim" class="btn btn-primary btn-block loadingBtn" >Kirim</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="<?=$instrukturPath?>">Sudah punya akun? Masuk</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <br>
            <br>
            <div id="layoutAuthentication_footer">
                <?php require "modul/copy.php"; ?>
            </div>
        </div>
    </body>

    <script>
    $("#email").focus();
    function mailing(m){
        var result = "";
        $.ajax({
            url: '<?=$instrukturPath;?>modul/ajax/data_reset.php?a&m='+m,
            async: false,
            success: function(data)
            {   
                result = data; 
            }    
        });
        return result;
    }

    function validasi(){
        var m = $("#email").val();
        if(mailing(m)=='1'){
            return true;
        }else{
            swal("Gagal", "Email tidak terdaftar", "error").then(function(){ $("#email").focus();$(".loadingBtn").html('Kirim'); });
            return false;
        }
        
    }

    </script>

<?php } ?>