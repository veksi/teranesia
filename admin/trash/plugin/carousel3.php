<!--<body>
	<div id="slider-wrapper">
		<div class="inner-wrapper">
			<input checked type="radio" name="slide" class="control" id="Slide1"/>
				<label for="Slide1" id="s1"></label>
			<input type="radio" name="slide" class="control" id="Slide2"/>
				<label for="Slide2" id="s2"></label>
			<input type="radio" name="slide" class="control" id="Slide3"/>
				<label for="Slide3" id="s3"></label>
			<input type="radio" name="slide" class="control" id="Slide4"/>
				<label for="Slide4" id="s4"></label>
			<div class="overflow-wrapper">
				<a class="slide" href=""><img src="images/slide/1.jpg"/></a>
				<a class="slide" href=""><img src="images/slide/2.jpg"/></a>
				<a class="slide" href=""><img src="images/slide/3.jpg"/></a>
				<a class="slide" href=""><img src="images/slide/4.jpg"/></a>
			</div>
		</div>
	</div>
	</body>-->

    <style>/*
    #slider-wrapper{
			width: 100%;
			height: 350px;
			margin: 50px auto;
			position: relative;
			margin-bottom: 0px;
			background: rgba(0,0,0,0.5);
			overflow: hidden;
		}
		
				#s1{
					padding: 6px;
					background: #FFFFFF;
					position: absolute;
					left: 50%;
					bottom: 25px;
					margin-left: -36px;
					border-radius: 20px;
					opacity: 0.3;
					cursor: pointer;
					z-index: 999;
				}
				
				#s2{
					padding: 6px;
					background: #FFFFFF;
					position: absolute;
					left: 50%;
					bottom: 25px;
					margin-left: -12px;
					border-radius: 20px;
					opacity: 0.3;
					cursor: pointer;
					z-index: 999;
				}
				
				#s3{
					padding: 6px;
					background: #FFFFFF;
					position: absolute;
					left: 50%;
					bottom: 25px;
					margin-left: 12px;
					border-radius: 20px;
					opacity: 0.3;
					cursor: pointer;
					z-index: 999;
				}
				
				#s4{
					padding: 6px;
					background: #FFFFFF;
					position: absolute;
					left: 50%;
					bottom: 25px;
					margin-left: 36px;
					border-radius: 20px;
					opacity: 0.3;
					cursor: pointer;
					z-index: 999;
				}
				
				#s1:hover, #s2:hover, #s3:hover, #s4:hover{ opacity: 1;}
				
			.inner-wrapper{
				width: 100%;
				height: 350px;
				position: absolute;
				top: 0;
				left: 0;
				margin-bottom: 0px;
				overflow: hidden;
			}
				.control{ display: none;}
				
				#Slide1:checked ~ .overflow-wrapper{ margin-left: 0%; }
				#Slide2:checked ~ .overflow-wrapper{ margin-left: -100%; }
				#Slide3:checked ~ .overflow-wrapper{ margin-left: -200%; }
				#Slide4:checked ~ .overflow-wrapper{ margin-left: -300%; }
				
				#Slide1:checked + #s1 { opacity: 1; }
				#Slide2:checked + #s2 { opacity: 1; }
				#Slide3:checked + #s3 { opacity: 1; }
				#Slide4:checked + #s4 { opacity: 1; }
				
			.overflow-wrapper{
				width: 400%;
				height: 100%;
				position: absolute;
				top: 0;
				left: 0;
				overflow-y: hidden;
				z-index: 1;
				-webkit-transition: all 0.3s ease-in-out;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			
				.slide img{
					width: 25%;
					float: left;
				}
                
                */

                #slider {
                    width: 100%;
  height: 500px;
  overflow: hidden;
  border: 1px solid #c69;
}

.slide {
  width: 100%;
  height: 350px;
  float: left;
  position: relative;
}

#slide-holder {
  width: 400%;
  position: relative;
  left: 0;
  -webkit-animation: scroller 10s infinite;
          animation: scroller 10s infinite;
}

@-webkit-keyframes scroller {
  0% {
    -webkit-transform: translateX(0);
            transform: translateX(0);
  }
  33% {
    -webkit-transform: translateX(-400px);
            transform: translateX(-400px);
  }
  66% {
    -webkit-transform: translateX(-800px);
            transform: translateX(-800px);
  }
  100% {
    -webkit-transform: translateX(0);
            transform: translateX(0);
  }
}

@keyframes scroller {
  0% {
    -webkit-transform: translateX(0);
            transform: translateX(0);
  }
  33% {
    -webkit-transform: translateX(-400px);
            transform: translateX(-400px);
  }
  66% {
    -webkit-transform: translateX(-800px);
            transform: translateX(-800px);
  }
  100% {
    -webkit-transform: translateX(0);
            transform: translateX(0);
  }
}
body {
  font-family: sans-serif;
}

#slider {
  margin: 0 auto;
}

.slide:nth-child(1) {
  background: #c69;
}

.slide:nth-child(2) {
  background: wheat;
}

.slide:nth-child(3) {
  background: #eee;
}
    </style>


<div id="slider">
	<div id="slide-holder">
	  	<div class="slide"><img src="images/slide/1.jpg" alt="" /></div>
	    <div class="slide"><img src="images/slide/2.jpg" alt="" /></div>
	    <div class="slide"><img src="images/slide/3.jpg" alt="" /></div>
	</div>
</div>



