

<div class="w3-content w3-display-container">
  <img class="mySlides" src="images/slide/1.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/2.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/3.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/4.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/5.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/6.jpg" style="width:100%">

  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>

<script>
var slideIndex1 = 1;
showDivs(slideIndex1);

function plusDivs(m) {
  showDivs(slideIndex1 += m);
}

function showDivs(m) {
  var j;
  var y = document.getElementsByClassName("mySlides");
  if (m > y.length) {slideIndex1 = 1}    
  if (m < 1) {slideIndex1 = y.length}
  for (j = 0; j < y.length; j++) {
     y[j].style.display = "none";  
  }
  y[slideIndex1-1].style.display = "block";  
}
</script>
