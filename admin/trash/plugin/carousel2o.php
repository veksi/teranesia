<!--<html>
<head>
 <title>CSS Slider Technique</title>
 <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="container">
 <input type="radio" name="slide" class="radio-nav" id="nav-1" checked/>
 <input type="radio" name="slide" class="radio-nav" id="nav-2"/>
 <input type="radio" name="slide" class="radio-nav" id="nav-3"/>

 <ul class="slide">
  <li class="slide-1">
   <img src="images/slide/1.jpg"/>
   <div class="caption">Lorem Ipsum Dolor Sit Amet 1</div>
  </li>
  <li class="slide-2">
   <img src="images/slide/2.jpg"/>
   <div class="caption">Lorem Ipsum Dolor Sit Amet 2</div>
  </li>
  <li class="slide-3">
   <img src="images/slide/3.jpg"/>
   <div class="caption">Lorem Ipsum Dolor Sit Amet 3</div>
  </li>
 </ul>

 <div class="nav-arrow nav-next">
  <label class="nav-1" for="nav-1">></label>
  <label class="nav-2" for="nav-2">></label>
  <label class="nav-3" for="nav-3">></label>
 </div>
 <div class="nav-arrow nav-prev">
  <label class="nav-1" for="nav-1"><</label>
  <label class="nav-2" for="nav-2"><</label>
  <label class="nav-3" for="nav-3"><</label>
 </div>
</div>

</body>
</html>

<style>
/* CSS Resets */

img {
 max-width: 100%;
 height: auto;
}
ul,ol {
 list-style-type: none;
}
/* end css reset */

.container { /* posisikan letak slidernya */
 margin:10% auto;
 position: relative;
 overflow: hidden;
}
.container, ul.slide li img{
 width:100%; /* Sesuaikan sendiri */
 height: 350px;  /* Sesuaikan sendiri */
}


ul.slide {
 position: absolute;
 display: block;
 width:100%;  /* <-- Angka 3 Bergantung pada jumlah slide */
}

.caption { /* styling untuk deskripsi setiap slide */
 position: absolute;
 background-color: rgba(0,0,0,0.5);
 bottom:0;
 padding:10px;
 color:#fff;
 left: 0;
 right: 0;
}

/* Yang membuatnya jadi slider */
 ul.slide li {
    display: inline-block;
    float: left;
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    ox-sizing:border-box;
    -webkit-transition: -webkit-transform 2000ms;
    -moz-transition: -moz-transform 2000ms;
    transition: -webkit-transform 2000ms, transform 2000ms;
 }
 ul.slide li.slide-1 {
  left: 0%;
 }
 ul.slide li.slide-2 {
  left: 100%;
 }
 ul.slide li.slide-3 {
  left: 200%;
 }
 #nav-1:checked ~ ul.slide li{
    -webkit-transform: translateX(0%);
  -moz-transform: translateX(0%);
    transform: translateX(0%);
 }
 #nav-2:checked ~ ul.slide li{
    -webkit-transform: translateX(-100%);
    -moz-transform: translateX(-100%);
   transform: translateX(-100%);
 }
 #nav-3:checked ~ ul.slide li {
    -webkit-transform: translateX(-200%);
    -moz-transform: translateX(-200%);
    transform: translateX(-200%);
 }
/* End, yang membuatnya jadi slider */


/* Navigator */

.radio-nav { /* menghilangkan radio button */
 display: none;
}
 
 /* styling untuk tombol next dan previous slide */
 .nav-arrow {
  position: absolute;
  top:45%;
  width:50px;
  height: 50px;
 }
 .nav-next {
  right:10px;
 }
 .nav-prev {
  left:10px;
 }
 .nav-arrow label {
  -webkit-transition:all 0.3s;
  -moz-transition:all 0.3s;
  transition:all 0.3s;
  background-color: rgba(0,0,0,0.3);
  color: #fff;
  border-radius: 50%;
  display: block;
  position: absolute;
  padding:15px 20px;
  cursor: pointer;
  z-index: 1;
  opacity: 0;
  font-weight: bold;
  line-height: 1;
 }
 .container:hover .nav-arrow label{
  background-color: rgba(0,0,0,0.7);
 }
 /* end styling untuk tombol next dan previous slide */
 /*Setiap slide mempunya tombol prev dan next-nya masing-masing. Nah, tampilkan tombol yang tepat dengan kode dibawah ini*/ 
 #nav-1:checked ~ .nav-prev label.nav-3,
 #nav-1:checked ~ .nav-next label.nav-2,
 #nav-2:checked ~ .nav-prev label.nav-1,
 #nav-2:checked ~ .nav-next label.nav-3,
 #nav-3:checked ~ .nav-prev label.nav-2,
 #nav-3:checked ~ .nav-next label.nav-1 {
  z-index: 2;
  opacity: 1;
 }
 /* end */

/* Navigator */
</style>-->

<!--
<style>

.slide img {
    width:50%;
    position: absolute;
  z-index: 3;
  animation: slideshow 9s linear 0s infinite;
}
.slide img:nth-child(2) {
  z-index: 2;
  animation: slideshow 9s linear 3s infinite;
}
.slide img:nth-child(3) {
   z-index: 1;
  animation: slideshow 9s linear 6s infinite;
}
@keyframes slideshow {
   25% { opacity: 1;}
   33.33% { opacity: 0;}
   91.66% { opacity: 0;}
   100% { opacity: 1;}
}
</style>
<div class="slide">
	<img src="images/slide/1.jpg" alt="Gambar 1"/>
	<img src="images/slide/2.jpg" alt="Gambar 2"/>
	<img src="images/slide/3.jpg" alt="Gambar 3"/>
</div>-->


<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<div class="w3-content w3-display-container">
  <img class="mySlides" src="images/slide/1.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/2.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/3.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/4.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/5.jpg" style="width:100%">
  <img class="mySlides" src="images/slide/6.jpg" style="width:100%">

  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>
