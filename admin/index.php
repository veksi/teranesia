<?php
ob_start();
session_name("teranesia_admin");
session_start();

require __DIR__.'/setting/global.php';
require __DIR__.'/public/function.php';

?>


<html>
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="<?=$imgserver;?>images/fav.ico" />
    <title><?php include "views/title.php";?></title>
    <link href="<?=$base_url;?>public/css/styles.css" rel="stylesheet" />
    <link href="<?=$base_url;?>public/css/custom.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" />

    <link href="<?=$base_url;?>public/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/b034c3c242.js" crossorigin="anonymous"></script>
    <!--<link rel="stylesheet" href="<?=$base_url;?>vendor/font-awesome-4.7.0/css/font-awesome.min.css">-->
    <!-- sweet alert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <!-- button loading -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?=$base_url;?>public/js/function.js"></script>    

    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">

    <script src="<?=$base_url;?>public/js/highcharts.js"></script><!-- highchart -->    

    <!-- date time picker -->    
    <link rel="stylesheet" href="<?=$base_url?>vendor/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css">
    
</head>
<body>

<?php if(empty($_SESSION)){ 

    if(@$URL[3]=="register"){
        require "modul/register.php";
    }else if(@$URL[3]=="reset"){
        require "modul/reset.php";
    }else{
        require_once "models/login.php";
        require_once "views/login.php";
    }

}else{ 

?>
        
    <body class="sb-nav-fixed">
        <?php require "views/header.php"; ?>
        <div id="layoutSidenav">
            <?php require "views/sidebar.php"; ?>
            <div id="layoutSidenav_content">
                <main>
                    <?php require "views/breadcrumb.php"; ?>
                    <?php 
                    switch (@$URL[3]){
                        case "customer":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once "models/customer.php";
                                    require_once "views/customer/form.php";
                                break;
                                default:
                                    require_once "models/customer.php";
                                    require_once "views/customer/list.php";
                                break;            
                            }
                        break;
                        case "product":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/product.php';
                                    require_once 'views/product/form.php';
                                break;
                                default:
                                    require_once 'models/product.php';
                                    require_once 'views/product/list.php';
                                break;            
                            }
                        break;
                        case "news":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/news.php';
                                    require_once 'views/news/form.php';
                                break;
                                default:
                                    require_once 'models/news.php';
                                    require_once 'views/news/list.php';
                                break;            
                            }
                        break;
                        case "post":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/post.php';
                                    require_once 'views/post/form.php';
                                break;
                                default:
                                    require_once 'models/post.php';
                                    require_once 'views/post/list.php';
                                break;            
                            }
                        break;
                        case "menu":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/menu.php';
                                    require_once 'views/menu/form.php';
                                break;
                                default:
                                    require_once 'models/menu.php';
                                    require_once 'views/menu/list.php';
                                break;            
                            }
                        break;
                        case "testimoni":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/testimoni.php';
                                    require_once 'views/testimoni/form.php';
                                break;
                                default:
                                    require_once 'models/testimoni.php';
                                    require_once 'views/testimoni/list.php';
                                break;            
                            }
                        break;
                        case "promo":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/promo.php';
                                    require_once 'views/promo/form.php';
                                break;
                                default:
                                    require_once 'models/promo.php';
                                    require_once 'views/promo/list.php';
                                break;            
                            }
                        break;
                        case "partnership":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/partnership.php';
                                    require_once 'views/partnership/form.php';
                                break;
                                default:
                                    require_once 'models/partnership.php';
                                    require_once 'views/partnership/list.php';
                                break;            
                            }
                        break;
                        case "banner":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/banner.php';
                                    require_once 'views/banner/form.php';
                                break;
                                default:
                                    require_once 'models/banner.php';
                                    require_once 'views/banner/list.php';
                                break;            
                            }
                        break;
                        case "category":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/category.php';
                                    require_once 'views/category/form.php';
                                break;
                                default:
                                    require_once 'models/category.php';
                                    require_once 'views/category/list.php';
                                break;            
                            }
                        break;
                        case "transaction":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/transaction.php';
                                    require_once 'views/transaction/form.php';
                                break;
                                default:
                                    require_once 'models/transaction.php';
                                    require_once 'views/transaction/list.php';
                                break;            
                            }
                        break;
                        case "administrator":
                            switch (@$URL[4]){
                                case "add":
                                case "edit":
                                    require_once 'models/administrator.php';
                                    require_once 'views/administrator/form.php';
                                break;
                                default:
                                    require_once 'models/administrator.php';
                                    require_once 'views/administrator/list.php';
                                break;            
                            }
                        break;

                        case "partnership":
                           
                        break;
                        default:
                            require_once "models/dashboard.php";
                            require_once "views/dashboard.php";
                        break;
                    }
                    ?>
                   
                </main>                
            </div>
        </div>

        
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?=$base_url?>public/js/scripts.js"></script>
            
    </body>

<?php }?>

<script>
$(".loadingBtn").click(function() {    
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
});
</script>

    