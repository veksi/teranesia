<?php
ob_start();
session_name("teranesia");
session_start();

require __DIR__.'/config/global.php';
require __DIR__.'/config/connection.php';
require __DIR__.'/public/function.php';

require_once "models/meta.php";
?>

<html>
<head>
    
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />    
    <title><?php include "views/meta_title.php"; ?></title>
    <link rel="canonical" href="<?=$curr_url?>" data-rh="true">
    <meta name="title" content="<?php include "views/meta_title.php"; ?>">
    <meta name="description" content="<?php include "views/meta_desc.php"; ?>">
    <meta name="keywords" content="<?php include "views/meta_keyword.php"; ?>">
    <meta property="og:url" content="<?=$curr_url;?>" />
    <meta property="og:locale" content="id_ID" />    
    <meta property="og:title" content="<?php include "views/meta_title.php"; ?>" />
    <meta property="og:image" content="<?php include "views/meta_image.php"; ?>"/>
    <meta property="og:site_name" content="Teranesia" />
    <meta property="og:description" content="<?php include "views/meta_desc.php"; ?>" />
    <meta name="Rating" content="general" />
    <meta name="Robots" content="all" />
    
    <link rel="shortcut icon" href="<?=$imgurl;?>favicon.png" />
    <link href="<?=$base_url;?>public/css/custom.css?v=<?=get_token(5)?>" rel="stylesheet" />
    <link href="<?=$base_url;?>public/css/styles.css" rel="stylesheet" />
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">-->

    <!-- fancybox -->
    <link rel="stylesheet" type="text/css" href="<?=$base_url?>vendor/fancybox-master/dist/jquery.fancybox.min.css">
    
    <!--<link rel="stylesheet" href="<?=$base_url;?>vendor/font-awesome-4.7.0/css/font-awesome.min.css">-->
    <script src="https://kit.fontawesome.com/b034c3c242.js" crossorigin="anonymous"></script>
    <!-- sweet alert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <!-- button loading -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?=$base_url;?>public/js/function.js"></script>
    
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=60555430a784de0012cc7a86&product=inline-share-buttons' async='async'></script>

    <!-- particle 
    <script src="<?=$base_url;?>public/js/particles.min.js"></script>-->

    <!-- cube -->
    <link rel="stylesheet" href="<?=$base_url;?>public/cube/style.css?v=<?=get_token(5)?>">
    
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">    
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?=$base_url?>vendor/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?=$base_url?>vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    <script src="<?=$base_url?>vendor/OwlCarousel2-2.3.4/dist/owl.carousel.js"></script>
    <script src="<?=$base_url?>public/js/owl.carousel.thumb.js"></script>

</head>

<?php require_once "models/mandatory.php"; ?>

<body class="sb-nav-fixed bg-white">
    <?php require "views/header.php"; ?>    
    <div id="layoutSidenav">        
        
        <div id="layoutSidenav_content">
          <div class="top-body"></div>
            <?php            
            require_once "views/menu_mobile.php";

            $URL[2] = explode("?",@$URL[2])[0];                        
            if(is_array($mandatory->is_menu(@$URL[2]))){ // data exist                                          
              if(isset($URL[3])){ // level 2
                  $id = id($URL[3]);
                  if($mandatory->is_menu(@$URL[2])['position']=='1'){ // top menu                    
                    require_once "models/multi_post_detail.php";
                    require_once "views/multi_post_detail.php";
                  }else{ // side menu                    
                    require_once "models/page.php";
                    require_once "views/page.php";           
                  }
              }else{ // level 1
                if($mandatory->is_menu(@$URL[2])['position']=='1'){ // top menu
                  if($mandatory->is_menu(@$URL[2])['model']=='1'){ // single post
                    require_once "models/single_post.php";
                    require_once "views/single_post.php";     
                  }elseif($mandatory->is_menu(@$URL[2])['model']=='3'){ // email form
                    require_once "views/help.php";
                  }elseif($mandatory->is_menu(@$URL[2])['model']=='4'){ // testimonial
                    require_once "models/testimoni.php";
                    require_once "views/testimoni.php";
                  }elseif($mandatory->is_menu(@$URL[2])['model']=='5'){ // home
                    header("Location: ".$base_url);
                  }else{ // multi post                    
                    require_once "models/multi_post.php";
                    require_once "views/multi_post.php";
                  }
                }else{ // side menu                
                  require_once "models/listing.php";                  
                  require_once "views/listing.php";             
                }
              }
            }elseif(@$URL[2]=='confirm'){  
                require_once "models/confirm.php";
                require_once "views/confirm.php";
            }elseif(@$URL[2]=='search'){  
              require_once "models/search.php";
              require_once "views/search.php";   
            }elseif(@$URL[2]=='forget'){
              require_once "models/reset.php"; 
              require_once "views/reset.php";       
            }elseif(@$URL[2]=='newpassword'){
              require_once "models/newpassword.php"; 
              require_once "views/newpassword.php";  
            /*}elseif(@$URL[2]=='partnership'){  
              require_once "models/partnership.php";
              require_once "views/partnership.php";                       
            }elseif(@$URL[2]=='help'){  
                require_once "views/help.php";
            }elseif(@$URL[2]=='contact'){
              require_once "views/contact.php";                           
            }elseif(@$URL[2]=='news'){  
              if(isset($URL[3])){
                  $id = id($URL[3]);
                  //require_once "views/breadcrumb.php";
                  require_once "models/newspage.php";
                  require_once "views/newspage.php";                       
              }else{
                  require_once "models/news.php";
                  require_once "views/news.php";                                     
              }

            }elseif(@$URL[2]=='promo'){  
              if(isset($URL[3])){
                  $id = id($URL[3]);
                  //require_once "views/breadcrumb.php";
                  require_once "models/promopage.php";
                  require_once "views/promopage.php";                       
              }else{
                  require_once "models/promo.php";
                  require_once "views/promo.php";                                     
              }

            }elseif(@$URL[2]=='testimoni'){  
              if(isset($URL[3])){
                  $id = id($URL[3]);
                  //require_once "views/breadcrumb.php";
                  require_once "models/testimonipage.php";
                  require_once "views/testimonipage.php";                       
              }else{
                  require_once "models/testimoni.php";
                  require_once "views/testimoni.php";                                     
              }
                */
            }elseif(@$URL[2]=='beli-sekarang' && !empty($_SESSION['customer'])){
                if(!empty($_POST['id'])){
                    require_once "models/beli.php";
                    require_once "views/beli.php";
                    require __DIR__.'/vendor/midtrans/examples/snap/checkout-process.php';
                }else{
                    require_once "views/404_page.php";
                }
            }elseif(@$URL[2]=='payment' && !empty($_SESSION['customer'])){
                if(!empty($URL[3])){
                    require_once "models/payment.php";
                    require_once "views/payment.php";               
                }else{
                    require_once "views/404_page.php";
                }
            }elseif(@$URL[2]=='invoice' && !empty($_SESSION['customer'])){
                if(!empty($URL[3])){
                    require_once "models/invoice.php";
                    require_once "views/invoice.php";               
                }else{
                    require_once "views/404_page.php";
                }
            }elseif(@$URL[2]=='profil' && !empty($_SESSION['customer'])){
                if(@$URL[3]=='edit'){
                    require_once "models/profil.php";
                    require_once "views/profil_edit.php";                 
                }else{
                    require_once "models/profil.php";
                    require_once "views/profil.php";                        
                }
            }elseif(@$URL[2]=='menunggu-pembayaran' && !empty($_SESSION['customer'])){
                require_once "models/waiting-list.php";
                require_once "views/waiting-list.php";
            }elseif(@$URL[2]=='pembelian' && !empty($_SESSION['customer'])){
                require_once "models/pembelian.php";
                require_once "views/pembelian.php";
            }elseif(@$URL[2]=='notifikasi'){
                require_once "models/notifikasi.php";
                require __DIR__.'/vendor/midtrans/examples/notification-handler.php';
              }elseif(@$URL[2]=='bg'){                                
                require_once "views/bg.php";
            }else{
                require_once "models/home.php";
                require_once "views/home.php";       
            }            
            require_once "views/footer.php";

            if(empty($_SESSION['customer'])){
              require_once "models/login.php"; 
              require_once "views/login.php";              
            }      

            // alert
            if(@$resp[0]==1){ 
              $succ   = @$resp[1]; 
              $err    = '';
            }else{ 
              $succ   = '';
              $err    = @$resp[1];
            } 

            ?>

        </div>
    </div>
    
</body>

<!-- fancybox -->
<script src="<?=$base_url?>vendor/fancybox-master/dist/jquery.fancybox.min.js"></script>
<!-- fancybox -->

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?=$base_url?>public/js/scripts.js"></script>
<script>
$(".loadingBtn").click(function() {    
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
});

var err = '<?=$err?>';
var succ = '<?=$succ?>';

if(err!=''){swal("Gagal",err,"warning");}
if(succ!=''){swal("Berhasil",succ,"success");}

var sidebar = "close";
$('#sidebarToggle').click(function(e) {  
    if (sidebar == "close") {
        $('body').css('overflow', 'hidden');        
        sidebar = "open";
    } else if (sidebar == "open") {
        $('body').css('overflow', 'auto');
        sidebar = "close"
    }
});

</script>