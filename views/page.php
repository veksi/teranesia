<script>

$(function(){
	$('.mhn-slide').owlCarousel({
		nav:true,   
    mouseDrag:true, 
		//loop:true,
    margin:5,
		slideBy:'page',
		rewind:false,
		responsive:{
			0:{items:1},
			325:{items:2},
			600:{items:3},
			1000:{items:6,mouseDrag:false}
		},
		smartSpeed:70,
		onInitialized:function(e){
			$(e.target).find('img').each(function(){
				if(this.complete){
					$(this).closest('.mhn-inner').find('.loader-circle').hide();                              
				  $(this).closest('.mhn-inner').find('.mhn-img').css('background-image','url('+$(e.target).attr('src')+')');
				}else{
					$(this).bind('load',function(e){
						$(e.target).closest('.mhn-inner').find('.loader-circle').hide();
						$(e.target).closest('.mhn-inner').find('.mhn-img').css('background-image','url('+$(e.target).attr('src')+')');
					});
				}
			});
		},
    navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]		
	});
});

</script>

<?php $data = $page->index($id); ?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-6 bg-transparent mb-3">
      <div class="bg-white border shadow-sm pb-3 rounded-top-6 rounded-bottom-6">
        <div class="outer">
          <div id="big" class="owl-carousel owl-theme">
            <!--<div class="item"><img class="rounded-top" style="max-height:450px" src="<?=$imgurl?>product/<?=$data['picture']?>"></div>-->
            <div class="item"><img class="rounded-top-6" src="<?=$imgurl?>product/<?=$data['picture']?>"></div>
          </div>
          <!--<div id="thumbs" class="owl-carousel owl-theme">
            <div class="item"><img src="<?=$imgurl?>banner/educational_banner-98bc37b496aa04ec760fcace7a6f9cb740957a0049d6d7f59c4d0f4d42ab9bc6.jpg"></div>
            <div class="item"><img src="<?=$imgurl?>banner/jualo_blog_banner-e9288363d19e8bf8dd399d6e07bcb1457ee469e16d8aaaf2d03b23d2c764e311.jpg"></div>
            <div class="item"><img src="<?=$imgurl?>banner/carro_automall-1bee652e8f9a2a917e440389e6bd7fd0eed41a1e9a4b3850c0e76580d93e8a31.jpg"></div>
          </div>-->
        </div>
        <div class="container px-4">
          <br>
          <b>Deskripsi</b>
          <hr>
          <?=$data['descriptions']?>
        </div>
      </div>
    </div>
    <div class="col-sm-4 col-md col-lg-4 bg-transparent">
      <div class="sticks">
        <div class="container bg-white border shadow-sm p-3 rounded-top-6 border-bottom-0">
          <h1 style="font-size:20px !important"><p><b><?=$data['title']?></b></p></h1>               
          <!--<h2 style="font-size:18px !important" class="text-success"><p><?=(!empty($_SESSION['customer']))? ''.Rp(num($data['price'])).'':'Rp * *** *** <a href="#" class="float-right link" data-toggle="modal" data-target="#masukModal"><small>Tampilkan harga</small></a>'?></p></h2>-->
          <h2 style="font-size:18px !important" class="text-success"><p><?=Rp(num($data['price']))?></p></h2>
          <p>
            <?php if($data['price']==0){ ?>
              <a class="btn btn-success btn-block text-white" href="https://wa.me/<?=$callcenter?>?text=Saya tertarik dengan produk <?=$data['title']?>. <?=$base_url.seo($page->get_menu($data['category'])['title']).'/'.seo($data['title']).'-'.$data['id']?>, mohon untuk info selanjutnya"><i class="fa fa-whatsapp fa-fw"></i> &nbsp;Hubungi Admin</a>
            <?php }else{ ?>
              <form method="post" action="<?=$base_url?>beli-sekarang">
                <input type="hidden" name="id" value="<?=$data['id']?>">
                <?=(!empty($_SESSION['customer']))? '<input type="submit" class="btn btn-success btn-block" value="Beli sekarang"></submit>':'<a class="btn btn-success btn-block text-white" href="#" data-toggle="modal" data-target="#masukModal">Beli Sekarang</a>'?>
              </form>  
            <?php } ?>
          </p>
          
        </div>
        <div class="container bg-white border shadow-sm p-3 rounded-bottom-6">
          <span class="float-left">Share : </span> &nbsp;<div class="sharethis-inline-share-buttons"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container mt-5 mb-2 font-weight-bold px-4">
  Produk lainnya<span class="float-right text-success"><a href="<?=$base_url.seo($page->get_slug($URL[2])['title'])?>">Lihat semua </a></span>
</div>

<div class="container px-3">
	<div class="mhn-slide owl-carousel">    
    <?php foreach($page->others($page->get_slug($URL[2])['id']) as $data){ ?>
      
      <?php if($data['attachment']==''){?>

        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">
          <a href="<?=$base_url.seo($page->get_menu($data['category'])['title']).'/'.seo($data['title']).'-'.$data['id']?>">
            <div class="mhn-inner pb-3">
              <div style="height:175px !important;overflow:hidden;">
                <img class="rounded-top-6" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" width=100% height=100%  style="object-fit:cover;"> 
              </div>
              <div class="mhn-img">
                <div class="loader-circle">
                  <div class="loader-stroke-left"></div>
                  <div class="loader-stroke-right"></div>
                </div>
              </div>         
              <div style="height:55px !important;overflow:hidden;">
                <div class="card-text px-3 py-2"><?=$data['title']?></div>
              </div>
              <div style="height:40px !important;overflow:hidden;">
                <p class="card-text px-3 py-2"><b><?=Rp(num($data['price']))?></b></p>
              </div>              
            </div>
          </a>
        </div>
      
      <?php }else{ ?>

        <div class="card shadow-sm rounded-0">
          <a href="<?=$base_url.seo($page->get_menu($data['category'])['title']).'/'.seo($data['title']).'-'.$data['id']?>">
            <div class="mhn-inner">
              <div style="height:230px">
                <img src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" width=100% height=100%  style="object-fit:cover;"> 
              </div>
              <div class="" style="height:40px !important;overflow:hidden;">
                <p class="card-text px-3 py-2"><b><?=Rp(num($data['price']))?></b></p>
              </div>
            </div>
          </a>
        </div>

      <?php } ?>
      
    <?php } ?>

  </div>
</div>