  <?php 
if(isset($_POST['submit'])){
    $upd = $profil->update($_POST['nama'],$_POST['password'],$_POST['passDB'],$_POST['hp'],$_POST['born'],$_POST['Dates'],$_POST['alamat'],$_FILES['photo1'],$_POST['imgDB1']);
}else{
    $edit = $profil->index();
}
?>

<form action="<?=$curr_url?>" name='forminput' method="post" enctype="multipart/form-data" onSubmit="return validasi();">    
    <div class="container my-4 px-3">
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md col-lg-3 mb-3">
                <div class="sticks">
                    <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm p-3 text-center">
                        <img id="pp" data-type="editable1" class="img-thumbnail m-2 rounded-circle" alt="avatar" style="width:200px;height:200px;object-fit:cover" src="<?=(!empty($edit['foto'])) ? $imgurl.'customer/'.$edit['foto'] : $imgurl."noprofile.png";?>" >
                        <a class="btn btn-light" style="position:absolute;margin-left:-40" onclick="removeMyImage('pp','1')"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <input type=hidden name='imgDB1' value="<?=@$edit['foto']?>">
                        <div class="clearfix"></div>
                        <small id="emailHelp" class="form-text text-muted">* klik foto untuk upload</small>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md col-lg-6">
                <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm p-3">                    
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Nama</small>
                        <input type="text" id="nama" name="nama" value="<?=htmlentities(@$edit['fullname'])?>" class="form-control" placeholder="Nama" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Email</small>
                        <input type="text" id="email" name="email" value="<?=htmlentities(@$edit['email'])?>" class="form-control" placeholder="Email" autocomplete="off" readonly>
                    </div>
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Password</small>
                        <input type=hidden name='passDB' value="<?=@$edit['pass']?>">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <small id="emailHelp" class="pull-left form-text text-muted">Nomor Handphone</small>
                        <input type="text" id="hp" name="hp" value="<?=htmlentities(@$edit['hp'])?>" class="form-control" placeholder="Nomor Handphone" autocomplete="off" >
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Tempat Lahir</label>
                                <input type="text" id="born" name="born" value="<?=htmlentities(@$edit['bornPlace'])?>" class="form-control" placeholder="Tempat Lahir" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Tanggal Lahir</label>
                                <div class="input-group date form-group" id="datepicker">
                                    <input type="text" class="form-control" id="Dates" name="Dates" value="<?=date("d-m-Y", strtotime(@$edit['bornDate']))?>" placeholder="Pilih Tanggal" autocomplete="off"  />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <span class="count"></span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <small id="emailHelp" class="form-text text-muted">Alamat</small>                   
                        <textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="Alamat"><?=@$edit['alamat'];?></textarea>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-center">                                 
                        <button type="submit" name="submit" class="btn btn-grad-1 text-white" >Simpan Perubahan</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {           
        var file = files[i];
        var imageType = /image.*/;     
        if (!file.type.match(imageType)) {
            continue;
        }           
        var img=document.getElementById("thumbnil");            
        img.file = file;    
        var reader = new FileReader();
        reader.onload = (function(aImg) { 
            return function(e) { 
                aImg.src = e.target.result; 
            }; 
        })(img);
        reader.readAsDataURL(file);
    }    
}

function removeMyImage(id,val) {        
    document.getElementById(id).src = "<?=$imgadmin?>noprofile.png";                
    document.getElementsByName("photo"+val)[0].value = '';
    document.getElementsByName("imgDB"+val)[0].value = '';
} 

function init1() {
    $("img[data-type=editable1]").each(function (i, e) {
        var _inputFile = $('<input/>')
            .attr('name', 'photo1')
            .css('display', 'none')
            .attr('type', 'file')
            .attr('hidden', 'hidden')
            .attr('onchange', 'readImage()')
            .attr('data-image-placeholder', e.id);
        $(e.parentElement).append(_inputFile);
        $(e).on("click", _inputFile, triggerClick);
    });
}

function triggerClick(e) {
    e.data.click();
}

Element.prototype.readImage = function () {
    var _inputFile = this;
    if (_inputFile && _inputFile.files && _inputFile.files[0]) {
        var _fileReader = new FileReader();
        _fileReader.onload = function (e) {
            var _imagePlaceholder = _inputFile.attributes.getNamedItem("data-image-placeholder").value;
            var _img = $("#" + _imagePlaceholder);
            _img.attr("src", e.target.result);
        };
        _fileReader.readAsDataURL(_inputFile.files[0]);
    }
};


(function (yourcode) {
    "use strict";
    yourcode(window.jQuery, window, document);
}(function ($, window, document) {
    "use strict";
    $(function () {
        init1();
    });
}));

function validasi(){

    var nama    = $('#nama').val();
    var email    = $('#email').val();
    var pwd       = $('#password').val();
    var akses      = $('#akses').val();

    if (nama==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#nama').focus(); });
        return false;
    }else if (email==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#email').focus(); });
        return false;
    }else if (akses==""){
        swal("","Lengkapi data dengan benar","warning",).then(function(){ $('#akses').focus(); });
        return false;
    }else if(pwd!=''){
        if (pwd.length<5){
            swal("","Kata sandi minimal 5 karakter","warning",).then(function(){ $('#password').focus(); });
            return false;
        }else{
            return true;
        }
    }else if (!filter.test(email)) {

        swal("","Email anda tidak valid","warning",).then(function(){ $("#email").focus(); });
        return false;

    }else{
        return true;
    }
    //tinyMCE.get('description').focus();

}
</script>

<!--multi datepicker -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function() {
    $('#datepicker').datepicker({
        numberOfMonths: 4,
        startDate: -Infinity,
        multidate: false,
        //multidateSeparator:'|',
        format: "dd-mm-yyyy",
        daysOfWeekHighlighted: "5,6",
        //datesDisabled: ['31/08/2017'],
        language: 'en',
        clearBtn:true,
        toggleActive:true,
        forceParse:true,
        todayHighlight:true,
    });
});
</script>