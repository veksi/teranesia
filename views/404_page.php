<div class="container h-100 d-flex">
    <div class="jumbotron bg-white my-auto rounded-top-6 rounded-bottom-6 text-center w-100">
      <h1 class="display-1 text-grey"><i class="fa fa-ban fa-5x" aria-hidden="true" style="color:#dde0e4"></i></h1>
      <div style="font-size:50px" class="font-weight-bold text-grey"><b>OOPS!</b></div>
      <p class="lead text-grey font-weight-bold">Halaman tidak ditemukan!</p>
    </div>
</div>