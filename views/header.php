<!--
<nav class="navbar sb-topnav navbar-expand-lg navbar-light bg-white mb-0 px-4 shadow-sm fixed-top">
    <div class="container pl-md-5 pr-md-5">
        <a class="navbar-brand text-black-50 logo" href="<?=$base_url?>">
            <i class="fas fa-spa"></i> <b>teranesia</b>
        </a>
        
    
        <ul class="navbar-nav search w-100">
            <li class="nav-item bg-light search w-100 rounded">
            <form class="form-inline my-2 my-lg-0">
                <div class="form-group has-icon w-100 py-0 my-0 ">
                <span class="fa fa-search form-control-feedback "></span>
                <input class="form-control py-2 bg-transparent border-0 w-100" id="name_d" name="name_d" type="text" placeholder="Cari produk" autocomplete="off"/>
                </div>
            </form>
            </li>
        </ul>
     
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item pl-2 pr-2">
            <a class="nav-link text-dark " href="<?=$base_url?>"><b>Home</b> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item pl-2 pr-2">
            <a class="nav-link text-dark" href="<?=$base_url?>partnership"><b>Partnership</b></a>
            </li>        
            <li class="nav-item pl-2 pr-2">
            <a class="nav-link text-dark" href=""><b>News</b></a>
            </li>
            <li class="nav-item pl-2 pr-2">
            <a class="nav-link text-dark" href=""><b>Contact</b></a>
            </li>  
            <li class="nav-item pl-2 pr-2 py-1">
                <div class="dropdown show">
                    <?php if(empty($_SESSION['customer'])){?>
                    <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#masukModal">Masuk</a>            
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarModal">Daftar</a>
                    <?php }else{ ?>
                    <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                        <a class="dropdown-item" href="<?=$base_url?>profil">Profil</a>
                        <a class="dropdown-item" href="<?=$base_url?>pembelian">Pembelian</a>                
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" onclick="window.location.href='<?=$base_url;?>config/logout.php'">Keluar</a>
                    <?php } ?>
                    </div>
                </div>
            </li>       
        </ul>
        <div class="d-block d-md-none">
            <button class="btn btn-link btn-sm ml-auto" id="sidebarToggle" href="#"><i class="fa fa-bars text-muted"></i></button>
        </div>
    </div>
</nav>
                    -->    



<!--
<nav class="sb-topnav navbar navbar-expand-lg navbar-dark bg-white shadow-sm">
    <div class="container pl-md-5 pr-md-5">
        <a class="navbar-brand text-black-50 logo" href="<?=$base_url?>">
            <i class="fas fa-spa"></i> <b>teranesia</b>
        </a>
        
        <ul class="navbar-nav search w-75 d-none d-lg-block">
            <li class="nav-item bg-light search w-75 rounded">
            <form class="form-inline my-2 my-lg-0">
                <div class="form-group has-icon w-75 py-0 my-0 ">
                <span class="fa fa-search form-control-feedback "></span>
                <input class="form-control py-2 bg-transparent border-0 w-75" id="name_d" name="name_d" type="text" placeholder="Cari produk" autocomplete="off"/>
                </div>
            </form>
            </li>
        </ul>                
        
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark " href="<?=$base_url?>"><b>Home</b> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href="<?=$base_url?>partnership"><b>Partnership</b></a>
            </li>        
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href=""><b>News</b></a>
            </li>
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href=""><b>Contact</b></a>
            </li>  
            <li class="nav-item pl-2 pr-2 py-1 d-none d-lg-block">
                <div class="dropdown show">
                    <?php if(empty($_SESSION['customer'])){?>
                    <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#masukModal">Masuk</a>            
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarModal">Daftar</a>
                    <?php }else{ ?>
                    <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                        <a class="dropdown-item" href="<?=$base_url?>profil">Profil</a>
                        <a class="dropdown-item" href="<?=$base_url?>pembelian">Pembelian</a>                
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" onclick="window.location.href='<?=$base_url;?>config/logout.php'">Keluar</a>
                    <?php } ?>
                    </div>
                </div>            
            </li>
        </ul>

        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="#">Activity Log</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </li>
        </ul>

        <button class="btn btn-link btn-sm order-1 order-lg-0 text-black-50 d-block d-lg-none" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>

    </div>
</nav>
-->

<div class="open-sans mt-5 pt-3 bg-grad-3 mx-0 d-none d-md-block">
    <div class="container">
        <ul class="nav justify-content-center">
            <?php foreach($mandatory->top_menu() as $data){ ?>   
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$base_url.seo($data['title'])?>"><small><?=$data['title']?></small></a>
                </li>
            <?php } ?>     
            <!--<li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>promo"><small>Promo</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>partnership"><small>Partnership</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>news"><small>News</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>help"><small>Help</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>contact"><small>Contact</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>testimoni"><small>Testimoni</small></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?=$base_url?>training-sertifikasi"><small>Training & Sertifikasi</small></a>
            </li>-->
        </ul>
    </div>
</div>

<nav class="sb-topnav navbar navbar-expand bg-transparent bg-grad-3" style="height:70px">  
    
    <div class="container-lg pl-md-5 pr-md-5">

        <button class="btn btn-link btn-sm text-white d-block d-lg-none ml-4" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>

        <a class="navbar-brand text-white logo text-center" href="<?=$base_url?>">
            <!--<i class="fas fa-spa fa-fw fa-lg"></i> <b>teranesia</b>
            <img src="<?=$imgurl?>logo.png" height="40px"> <b>teranesia</b>-->
            <img src="<?=$imgurl?>logo-desktop.png" height="45px">
        </a>
        
        <ul class="navbar-nav search w-100 d-none d-md-block">
            <li class="nav-item bg-light search w-100 rounded">
                <form class="form-inline my-2 my-lg-0" action="/search/">
                    <div class="form-group w-100 py-0 my-0 ">
                        <div class="inner-addon right-addon w-100">
                            <i class="glyphicon fa fa-search"></i>
                            <input type="text" name="q" class="form-control py-2 bg-transparent border-0 w-100" placeholder="Cari di Teranesia"/>
                        </div>
                    </div>
                </form>
            </li>
        </ul>       

        <ul class="navbar-nav ml-auto ml-md-0">
            <!--<li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark " href="<?=$base_url?>"><b>Home</b> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href="<?=$base_url?>partnership"><b>Partnership</b></a>
            </li>        
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href="<?=$base_url?>news"><b>News</b></a>
            </li>
            <li class="nav-item pl-2 pr-2 d-none d-lg-block">
                <a class="nav-link text-dark" href="<?=$base_url?>contact"><b>Contact</b></a>
            </li>-->
            <?php if(empty($_SESSION['customer'])){?>
                <li class="nav-item dropdown pl-2 pr-2 py-1 text-white d-block d-lg-none "> 
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#masukModal">Masuk</a>            
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarModal">Daftar</a>
                    </div>
                </li>
                <li class="nav-link d-none d-lg-block ml-2">                
                    <a class="btn btn-default text-white border" href="#" data-toggle="modal" data-target="#masukModal">Masuk</a>
                </li>
                <li class="nav-link d-none d-lg-block">
                    <a class="btn btn-default text-white border" href="#" data-toggle="modal" data-target="#daftarModal">Daftar</a>
                </li>
            <?php }else{ ?>
                <li class="nav-item dropdown pl-2 pr-2 py-1 text-white"> 
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php if(!empty($mandatory->profile($_SESSION['customer'])['foto'])){ ?>
                            <img class="rounded-circle" style="height:30px;width:30px;object-fit:cover;border:2px solid white" src="<?=$imgurl?>customer/<?=$mandatory->profile($_SESSION['customer'])['foto']?>">
                        <?php }else{ ?>
                            <i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i>
                        <?php } ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="<?=$base_url?>profil">Profil</a>
                        <a class="dropdown-item" href="<?=$base_url?>menunggu-pembayaran">Menunggu Pembayaran <?php if($mandatory->waiting($_SESSION['customer'])>0){ ?> <span class="badge badge-primary badge-pill"><?=$mandatory->waiting($_SESSION['customer'])?></span> <?php }?></a>
                        <a class="dropdown-item" href="<?=$base_url?>pembelian">Pembelian</a>      
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" onclick="window.location.href='<?=$base_url;?>config/logout.php'">Keluar</a>
                    </div>
                </li>
            <?php } ?>
            
        </ul>
        
    </div>
</nav>

<script>
var activeurl = window.location;
$('a[href="'+activeurl+'"]').parent('.nav-item').addClass('sorot');
</script>