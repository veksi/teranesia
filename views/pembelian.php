<?php
  $page_url       = explode('?',$curr_url)[0];
  $item_per_page  = 10;
  if(isset($_GET["page"])){ //Get page number from $_GET["page"]
      $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
      if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
  }else{
      $page_number = 1; //if there's no page number, set it to 1
  }

  $get_total_rows = $pembelian->count($URL[2]); //hold total records in variable
  $total_pages    = ceil($get_total_rows/$item_per_page); //break records into pages
  $page_position  = (($page_number-1) * $item_per_page); //get starting position to fetch the records
  $allItem        = $pembelian->index($page_position,$item_per_page);

  $customer       = $pembelian->profile();
?>
<div class="container my-4 px-4">
  <div class="row justify-content-center">
    <div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks">
        <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm text-center p-3">
            <img class="img-thumbnail m-2 rounded-circle" alt="avatar" style="width:130px;height:130px;object-fit:cover" src="<?=(!empty($customer['foto'])) ? $imgurl.'customer/'.$customer['foto'] : $imgurl."noprofile.png";?>" >
            <div>
              <b><?=$customer['fullname']?></b>
            </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-8 bg-transparent">
      <div class="row">
        <div class="col-sm bg-transparent">
          <div class="row">
            <?php 
            if(is_array($allItem)){ 
              foreach($allItem as $data){
                $midtrans = unserialize($data['midtrans_respon']);
                $detail   = unserialize($data['transactions']);
            ?>
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow-sm border rounded-top-6 rounded-bottom-6 mb-3">
                  
                  <ul class="list-group">
                    <li class="list-group-item rounded-top-6 border-left-0 border-top-0 border-right-0 py-1"><small><?=tgl_indo($midtrans['transaction_time'],false,false)?></small></li>
                    <li class="list-group-item border-left-0 border-top-0 border-right-0">
                      <div class="row">
                        <div class="col-sm-4">
                          <div><small>Invoice</small></div>
                          <b>#<?=$data['order_id']?></b>
                        </div>
                        <div class="col-sm-4">
                          <div><small>Status</small></div>
                          <b><?=sts_trx($data['status_trx'])?></b>
                        </div>
                        <div class="col-sm-4">
                          <div><small>Total</small></div>
                          <b><?=Rp(num($data['harga']))?></b>
                        </div>
                      </div>
                    </li>
                    <li class="list-group-item border-left-0 border-top-0 border-right-0 rounded-bottom-6">
                      <div class="row middleVertical">
                        <div class="col-md-6 mb-2">
                          <div class="row middleVertical">
                            <div class="col-2 col-sm-2 text-center">
                              <i class="fa fa-shopping-bag fa-2x <?=($data['status_trx']=='settlement')?'text-success':'text-info'?> "></i>
                              <!--<span class="fa-stack fa-lg">
                                <i class="fa fa-circle-o fa-stack-2x text-success"></i>
                                <i class="fa fa-shopping-bag fa-stack-1x fa-inverse text-success"></i>
                              </span>-->
                            </div>
                            <div class="col-10 col-sm-10">                            
                              <div class="font-weight-bold">
                                <span class='openTitle<?=$data['id']?>'><?=trimString($detail['item_details'][0]['name'],5)?></span>
                                <script>
                                  jQuery.fn.extend({
                                      toggleText: function (a, b){
                                          var isClicked = false;
                                          var that = this;
                                          this.click(function (){
                                              if (isClicked) { that.text(a); isClicked = false; }
                                              else { that.text(b); isClicked = true; }
                                          });
                                          return this;
                                      }
                                  });
                                  $('.openTitle<?=$data['id']?>').toggleText("<?=trimString($detail['item_details'][0]['name'],5)?>", "<?=$detail['item_details'][0]['name']?>");
                                </script>
                              </div>
                              <small><?=$detail['item_details'][0]['quantity']?> Item <?=Rp(num($data['harga']))?></small>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row middleVertical">
                            <div class="col-6 col-sm-6">
                              <div><small>Total Harga</small></div>
                              <b><?=Rp(num($data['harga']))?></b>                                      
                            </div>
                            <div class="col-6 col-sm-6 text-center">       
                              <?php if($pembelian->get_product($detail['item_details'][0]['id'])['attachment']!='' && $data['status_trx']=='settlement' ){ ?>                                                                                                                          
                                <button class="btn btn-grad-1 btn-sm text-white" onclick="document.getElementById('link').click()">Download</button>
                                <a id="link" href="<?=$base_url;?>/files/e-book/<?=$pembelian->get_product($detail['item_details'][0]['id'])['attachment']?>" download hidden></a>                                
                              <?php }elseif($pembelian->get_product($detail['item_details'][0]['id'])['attachment']=='' && $data['status_trx']=='settlement' ){ ?>                                                                                                                                                                                          
                                <a class="btn btn-grad-1 btn-sm text-white" href="https://wa.me/<?=$callcenter?>?text=Saya%20telah%20melakukan%20pembayaran%20terkait%20transaksi%20berikut%20%3A%0AKode%20%3A%20%23<?=$data['order_id']?>%0AProduk%20%3A%20<?=$detail['item_details'][0]['name']?>%0AHarga%20%3A%20<?=Rp(num($data['harga']))?>%0AMohon%20untuk%20info%20selanjutnya.">Konfirmasi</a>                                
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    
                    </li>
                    
                  </ul>
                </div>
              </div>

            <?php 
              }
            }else{
              require_once ('404_trx.php');
            } ?>
          </div>
        </div>
      </div>  
      <nav aria-label="Page navigation example">    
        <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
      </nav>
    </div>
  </div>
</div>
