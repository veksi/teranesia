<?php
if(isset($_POST['set_password'])){ $resp = $newPwd->setNewPwd($_POST['password'],$URL[3]); }
if(empty($newPwd->index(($URL[3]))['reset_key'])){ header("Location: ..");} 
?>

<div class="container my-4 px-4">
    <div class="row justify-content-center">  
        <div class="col-sm-12 col-md-5">  
            <div class="card shadow-sm border">                        
                <div class="card-header bg-transparent">
                    <h5 class="modal-title fw-700" id="exampleModalLabel">New Password</h5>
                </div>    
                <div class="card-body px-2">
                    <form class="form-signin" method="post" enctype="multipart/form-data" onSubmit="return setPwd();">
                        <input type="hidden" name="set_password">
                        <div class="modal-body">
                            <div class="form-group">               
                                <label class="form-check-label" for="agree">
                                    <small>Masukkan password baru yang anda inginkan.</small>
                                </label>
                            </div>
                            <div class="form-group">               
                                <small id="emailHelp" class="form-text text-muted">Password</small>    
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" maxlength="100" onkeyup="long('password')">
                                <div class="invalid-feedback">Min 5 karakter</div>
                            </div>

                            <div class="clear"></div>
                        </div>
                        
                        <div class="card-footer text-right bg-transparent">                                        
                            <button type="submit" class="btn btn-grad-1 text-white"> Simpan</button>
                        </div>
                    
                    </form>
                </div>                      
            </div>
        </div>
    </div>
</div>

<script>

function long(id){
    var pwd   = $('#'+id).val();
    if(pwd.length>=5) { 
        $("#"+id).removeClass('is-invalid');
    }
}

function setPwd(){

    var password    = $("#password").val();

    if (password="" || password.length<5){
        $("#password").focus();$("#password").addClass('is-invalid');//$(".loadingBtn").html('Save');
        return false;
    }else{
        return true;
    }
}
</script>