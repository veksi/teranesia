<div class="container my-4 px-4">
  <div class="row justify-content-center">
    
    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

      <?php 
      $singlepost = $singlepost->index($URL[2]);
      if(is_array($singlepost)){ ?>
                
        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
          <img class="card-img-top rounded-top-6" src="<?=$imgurl?>post/<?=$singlepost['picture']?>" alt="<?=$singlepost['title']?>"> 
          <div class="card-body">
            <h1 class="card-text" style="font-size:20px !important"><b><?=$singlepost['title']?></b></h1>
            <br>
            <p class="card-text"><?=$singlepost['descriptions']?></p>
            <div class="sharethis-inline-share-buttons"></div>
          </div>                
        </div>
      
      <?php }else{ require_once ('404_page.php'); } ?>

    </div>
  </div>
</div>