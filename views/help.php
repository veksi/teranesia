<?php 
if(isset($_POST['ask'])){
  require __DIR__.'/email/contact_email.php';  
}
?>

<div class="container my-4 px-4">
  <div class="row justify-content-center">
    <div class="col-sm-12 col-md-6">
      <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
        <div class="p-4">
          <form class="form-signin" action="#" method="post" enctype="multipart/form-data" onSubmit="return validasi();">
            <input type="hidden" name="ask">
            
            <div class="text-black-50 text-center">
                <i class="fa fa-question-circle fa-5x" aria-hidden="true"></i>
                <br><b>Helpdesk</b>              
            </div>

            <div class="form-group">               
                <small id="emailHelp" class="form-text text-muted">Nama</small>    
                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" maxlength="100" >
            </div>
            <div class="form-group">               
                <small id="emailHelp" class="form-text text-muted">No Telp</small>    
                <input type="text" name="hp" id="hp" class="form-control" placeholder="No Telp" maxlength="100" >
            </div>
            <div class="form-group">               
                <small id="emailHelp" class="form-text text-muted">Email</small>    
                <input type="text" name="email" id="email" class="form-control" placeholder="Email" maxlength="100" >
            </div>
            <div class="form-group">               
                <small id="emailHelp" class="form-text text-muted">Judul</small>    
                <input type="text" name="judul" id="judul" class="form-control" placeholder="Judul" maxlength="100" >
            </div>
            <div class="form-group">               
                <small id="emailHelp" class="form-text text-muted">Pertanyaan</small>    
                <textarea id="soal" name="soal" class="form-control" rows="3" placeholder="Pertanyaan"></textarea>
            </div>
            <div class="clear"></div>
            
            <div class="text-center">                                        
                <button type="submit" id="login" class="btn btn-grad-1 text-white"> Kirim</button>
            </div>
            <div class="clear"></div>

          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>