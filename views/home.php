<script>
$(document).ready(function(){
  
  $('.banner').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    dots:true,
    autoHeight:true,
    lazyLoad:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })

  $('.ads').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    dots:false,
    autoHeight:true,
    lazyLoad:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })

});


$(function(){
	$('.mhn-slide').owlCarousel({
		nav:true,       
		//loop:true,   
    margin:5,
		slideBy:'page',
		rewind:false,        
		responsive:{
			0:{items:1},
			325:{items:2},
			600:{items:3},
			1000:{items:6,mouseDrag:false}
		},
		smartSpeed:70,
		onInitialized:function(e){
			$(e.target).find('img').each(function(){
				if(this.complete){
					$(this).closest('.mhn-inner').find('.loader-circle').hide();                              
				  $(this).closest('.mhn-inner').find('.mhn-img').css('background-image','url('+$(e.target).attr('src')+')');
				}else{
					$(this).bind('load',function(e){
						$(e.target).closest('.mhn-inner').find('.loader-circle').hide();
						$(e.target).closest('.mhn-inner').find('.mhn-img').css('background-image','url('+$(e.target).attr('src')+')');
					});
				}
			});
		},
    navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]		
	});
});

</script>

<div class="container">
  <div class="alert alert-light alert-dismissible fade show py-2 text-uppercase text-success" role="alert" style="color:#8B008B !important">
    <marquee scrolldelay="100"><h1><b>Marketplace digital terapi pertama di dunia - kunjungi dan rasakan sensasi dan khasiatnya!</b></h1></marquee>
    <button type="button" class="close p-2" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  
  <div class="row">
    <div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks rounded">
        <?php require __DIR__.'/menu_sidebar.php'; ?>
      </div>
    </div>
    
    <div class="col-sm-12 col-md col-lg-9 bg-transparent">
      <div class="row">        
        <div class="col-sm-8 col-md-8 col-lg-8 order-1 mb-3">
          <div class="banner owl-carousel owl-theme">
            <?php foreach($home->tbl_banner(1) as $data){ ?>
              <div class="item"><a href="<?=$imgurl?>banner/<?=$data['picture']?>" data-fancybox ><img class="rounded-top-6 rounded-bottom-6" src="<?=$imgurl?>banner/<?=$data['picture']?>" style="max-height:280px;width:100%;object-fit:cover"></a></div>
            <?php } ?>
          </div>          
        </div>    
        <div class="col-sm-4 col-md-4 col-lg-4 order-3 order-sm-2 mb-3 text-center text-info">
          <div class="ads owl-carousel owl-theme">
            <?php foreach($home->tbl_banner(2) as $data){ ?>
              <div class="item"><a href="<?=$imgurl?>banner/<?=$data['picture']?>" data-fancybox><img class="rounded-top-6 rounded-bottom-6" src="<?=$imgurl?>banner/<?=$data['picture']?>" style="max-height:280px;width:100%;object-fit:cover"></a></div>
            <?php } ?>            
          </div>      
          <b>Advertising zone</b>
        </div>
        <div class="col d-md-inline d-lg-none order-2">
          <div class="row justify-content-center">  
            <?php //require __DIR__.'/menu_mobile.php'; ?>
          </div>
        </div>

      </div>     
    </div>
  </div>
</div>


<?php foreach($mandatory->side_menu() as $menu){
  foreach($mandatory->child_sm($menu['id']) as $cat){ ?>

  <div class="container font-weight-bold px-4 pt-4">
    <h2><div class="text-capitalize font-weight-bold"><?=$cat['title']?></div></h2><span class="float-right text-success"><a href="<?=$base_url.seo($cat['title'])?>">Lihat semua </a></span>
  </div>

  <div class="container px-3">
    <div class="mhn-slide owl-carousel">
    
      <?php foreach($home->index($cat['id'],6) as $item){ ?>
      
        <?php if($item['attachment']!=''){ ?>

          <div class="card shadow-sm rounded-0">
            <a href="<?=$base_url.seo($home->get_menu($item['category'])['title']).'/'.seo($item['title']).'-'.$item['id']?>">
              <div class="mhn-inner">
                <div style="height:230px">
                  <img src="<?=$imgurl?>product/<?=$item['picture']?>" alt="<?=$item['title']?>" width=100% height=100%  style="object-fit:cover;"> 
                </div>
                <div class="" style="height:40px !important;overflow:hidden;">
                  <p class="card-text px-3 py-2"><b><?=Rp(num($item['price']))?></b></p>
                </div>
              </div>
            </a>
          </div>

        <?php }else{ ?>

          <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">
            <a href="<?=$base_url.seo($home->get_menu($item['category'])['title']).'/'.seo($item['title']).'-'.$item['id']?>">    
              <div class="mhn-inner pb-3">
                <div style="height:175px !important;overflow:hidden;">
                  <img class="rounded-top-6" src="<?=$imgurl?>product/<?=$item['picture']?>" alt="<?=$item['title']?>" width=100% height=100%  style="object-fit:cover;"> 
                </div>               
                <div style="height:55px !important;overflow:hidden;">
                  <div class="card-text px-3 py-2"><?=$item['title']?></div>
                </div>
                <div style="height:40px !important;overflow:hidden;">
                  <p class="card-text px-3 py-2"><b><?=Rp(num($item['price']))?></b></p>
                </div>              
              </div>
            </a>  
          </div>
          
        <?php } ?>
          
      <?php } ?>

    </div>
  </div>

<?php } } ?>