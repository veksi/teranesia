<nav class="navbar navbar-expand-lg navbar-light bg-grad-1 mb-0 px-4 shadow-none position-static">
  <div class="container-fluid pl-md-5 pr-md-5">
    <a class="navbar-brand text-black-50" href="<?=$base_url?>">
      <img class="d-none d-md-block float-left" src="<?=$imgurl?>tera.png" height="40px"> 
      <img class="d-block d-md-none" src="<?=$imgurl?>tera.png" height="40px">
    </a>
    
    <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav search">
        <li class="nav-item bg-light search">
        <form class="form-inline my-2 my-lg-0">
            <div class="form-group has-icon w-100 py-0 my-0">
              <span class="fa fa-search form-control-feedback"></span>
              <input class="form-control py-2 rounded-0 bg-transparent border-0 w-100" id="name_d" name="name_d" type="text" placeholder="Search" autocomplete="off"/>
            </div>
          </form>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark " href="<?=$base_url?>"><b>Home</b> <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href="<?=$base_url?>partnership"><b>Partnership</b></a>
        </li>
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href=""><b>News</b></a>
        </li>
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href=""><b>Contact</b></a>
        </li>        
      </ul>
    </div>
  </div>
</nav>