<div class="list-group" style="z-index:9999">
    <?php foreach($mandatory->side_menu() as $sidemenu){ ?>
    <div class="list-group-item list-group-horizontal-sm border-0 p-0"><b><?=$sidemenu['title']?></b></div>
        <?php foreach($mandatory->child_sm($sidemenu['id']) as $data){ ?>
        <a href="<?=$base_url.seo($data['title'])?>" class="list-group-item list-group-item-action border-0 px-2 open-sans" style="font-size:14px">
            <span>
                <?=(empty($data['icon'])) ? '<img src="'.$imgurl.'star.png" height="30">' : '<img src='.$imgurl.$data['icon'].' height="30">'?>                
            </span>
            &nbsp;<?=$data['title']?>
        </a>
        <?php } ?>
    <?php } ?>
</div>        
<div class="list-group-item border-0"></div>