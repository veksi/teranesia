<?php 
if(isset($_POST['login'])){
    $resp = $login->auth($_POST['u'],$_POST['p']);
}else if(isset($_POST['reg'])){
    $resp = $login->store($_POST['nama'],$_POST['hp'],$_POST['email'],$_POST['password']);    
    if($resp[0]==1){ require __DIR__.'/email/register.php'; }    
}
?>

<!-- Modal -->
<div class="modal fade" id="masukModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title fw-700" id="exampleModalLabel">Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2">            
            <form class="form-signin" method="post" enctype="multipart/form-data" onSubmit="return loginCek();">
                    <input type="hidden" name="login">
                    <div class="modal-body">
                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Email</small>    
                            <input type="text" name="u" id="u" class="form-control" placeholder="Email" maxlength="100" onkeyup="validEmail('u')">
                            <div class="invalid-feedback">Email tidak valid</div>
                        </div>
                        <div class="clear"></div>

                        <div class="form-group"> 
                            <small id="emailHelp" class="form-text text-muted">Password</small>
                            <div class="input-group mb-3">
                                <input type="password" name="p" id="p" class="form-control pwd" placeholder="Password" aria-label="Recipient's username" aria-describedby="basic-addon2">                                
                                <div class="input-group-append">
                                    <span class="input-group-text reveal" id="basic-addon2"><span class='fa fa-eye-slash fa-fw' id="eye"></span></span>
                                </div>
                            </div>                         
                        </div>
                        
                        <div class="form-group text-right"> 
                            <small><a href="/forget" class="text-info"> Lupa Password?</a></small>
                        </div>
                    </div>
            
                    <div class="modal-footer">                                        
                        <button type="submit" id="login" class="btn btn-grad-1 btn-block text-white"> Masuk</button>
                    </div>
                    <div class="clear"></div>
            
                    <div class="form-group text-center"> 
                        <small>Belum memiliki akun?<a class="text-success" href="#" data-dismiss="modal" data-toggle="modal" data-target="#daftarModal"> <b>Daftar Sekarang</b></a></small>                        
                    </div>
                </form>
  
            </div>          
        </div>
    </div>
</div>

<div class="modal fade" id="daftarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title fw-700" id="exampleModalLabel">Daftar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2">
                <form class="form-signin" method="post" enctype="multipart/form-data" onSubmit="return regCek();">
                    <input type="hidden" name="reg">
                    <div class="modal-body">
                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Nama</small>    
                            <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" maxlength="100" >
                        </div>
                        <div class="clear"></div>

                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Nomor Handphone</small>    
                            <input type="text" name="hp" id="hp" class="form-control" placeholder="Nomor Handphone" maxlength="100" >
                        </div>
                        <div class="clear"></div>

                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Email</small>    
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" maxlength="100" onkeyup="validEmail('email')">
                            <div class="invalid-feedback">Email tidak valid</div>
                        </div>
                        <div class="clear"></div>

                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Password</small>    
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" maxlength="100" onkeyup="long('password')">
                            <div class="invalid-feedback">Min 5 karakter</div>
                        </div>
                        <div class="clear"></div>

                        <div class="form-group px-3">
                            <input class="form-check-input" type="checkbox" name="agree" value="" id="agree">
                            <label class="form-check-label" for="agree">
                              <small>Saya setuju untuk memproses data pribadi dengan Syarat & Ketentuan Teranesia.id sesuai dengan ketentuan hukum.</small>
                            </label>
                            <div class="invalid-feedback">Anda belum menyetujui kebijakan</div>
                        </div>

                    </div>
                    
                    <div class="modal-footer">                                        
                        <button type="submit" id="reg" class="btn btn-grad-1 btn-block text-white"> Daftar</button>
                    </div>
                    <div class="clear"></div>
            
                    <div class="form-group text-center"> 
                        <small>Sudah memiliki akun?<a class="text-success" href="#" data-dismiss="modal" data-toggle="modal" data-target="#masukModal"> <b>Masuk</b></a></small>
                    </div>
                    
                </form>
            </div>          
        </div>
    </div>
</div>

<script>
$(".reveal").on('click',function() {
  var $pwd = $(".pwd");
  var $eye = $("#eye");

  if ($pwd.attr('type') === 'password') {
      $pwd.attr('type', 'text');
      $eye.removeClass('fa fa-eye-slash');
      $eye.addClass('fa fa-eye'); 
  }else{
      $pwd.attr('type', 'password');
      $eye.removeClass('fa fa-eye');
      $eye.addClass('fa fa-eye-slash'); 
  }
});

function validEmail(id){
    var email   = $('#'+id).val();
    var at      = email.search("@");
    var dot     = email.match(/\./g).length;
    if( (at<=0) || (dot<1) ) { 
        $("#"+id).addClass('valid-tooltip');
    }else{
        $("#"+id).removeClass('is-invalid');
    }
}

function long(id){
    var pwd   = $('#'+id).val();
    if(pwd.length>=5) { 
        $("#"+id).removeClass('is-invalid');
    }
}

function loginCek(){
   
   var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   var email       = $("#u").val();
   var password    = $("#p").val();

   if (email==""){
       $("#u").focus();$("#u").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
   }else if (!filter.test(email)) {
       $("#u").focus();$("#u").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
   }else if (password="" || password.length<5){
       $("#p").focus();$("#p").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
   }else{
       return true;
   }

}


function regCek(){
    
    var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var nama        = $("#nama").val();
    var hp          = $("#hp").val();
    var email       = $("#email").val();
    var password    = $("#password").val();
    var agree       = $('#agree')[0].checked;

    if (nama==""){
       $("#nama").focus();$("#nama").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
    }else if (hp==""){
       $("#hp").focus();$("#hp").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
    }else if (email==""){
       $("#email").focus();$("#email").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
    }else if (!filter.test(email)) {
       $("#email").focus();$("#email").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
    }else if (password="" || password.length<5){
       $("#password").focus();$("#password").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
    }else if (agree==false){
        $("#agree").focus();$("#agree").addClass('is-invalid');
        return false;
   }else{
       return true;
   }

}
</script>