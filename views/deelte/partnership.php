<div class="container my-4 px-4">
  <div class="row justify-content-center">
    
    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

      <?php 
      $partnership = $partnership->index();
      if(is_array($partnership)){ ?>
                
        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
          <img class="card-img-top rounded-top-6" src="<?=$imgurl?>partnership/<?=$partnership['foto']?>" alt="<?=$partnership['title']?>"> 
          <div class="card-body">
            <h1 class="card-text" style="font-size:20px !important"><b><?=$partnership['title']?></b></h1>
            <br>
            <p class="card-text"><?=$partnership['content']?></p>
            <div class="sharethis-inline-share-buttons"></div>
          </div>                
        </div>
      
      <?php }else{ require_once ('not_found.php'); } ?>

    </div>
  </div>
</div>