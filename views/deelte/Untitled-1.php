<div class="col-6 col-md-4 col-lg-2 mb-3 px-2 item">
          <div class="card shadow-custom-3 border-0 rounded-top-6 rounded-bottom-6">
            <a href="<?=$base_url.seo($home->get_category($data['category'])['title']).'/'.seo($data['title']).'-'.$data['id']?>">
              <div class="rounded-top-6" style="height:150px !important;overflow:hidden">
                <img class="thumbnails" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" width=100% height=100% style="object-fit:cover"> 
              </div>
              <div style="height:55px !important;overflow:hidden;">
                <div class="card-text px-3 py-2"><?=$data['title']?></div>
              </div>
              <div style="height:40px !important;overflow:hidden;">
                <p class="card-text px-3 py-2"><b><?=Rp(num($data['price']))?></b></p>
              </div>
              <br>
            </a>
          </div>
        </div>


        <!--
<div class="container my-4 px-4">
  <div class="row">
    <div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks">
      <?php require __DIR__.'/menu_sidebar.php'; ?>
      </div>
    </div>
    <div class="col-sm-9 col-md col-lg-9 bg-transparent">
      
      <?php if(is_array($allItem)){ ?>
      <div class="row">
        <div class="col-sm bg-transparent">
          <div class="row">
            <?php foreach($allItem as $data){ ?>
              
                <?php if($URL[2]!='e-book'){?>

                  <div class="col col-md-4 col-lg-3 mb-3 px-2">
                    <div class="card shadow-custom-1 border-0 rounded">
                      <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
                        <div style="height:150px !important;overflow:hidden">
                          <img class="thumbnails rounded-top" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" height=150px> 
                        </div>
                        <div class="card-body" style="height:140px !important;overflow:hidden">
                          <p class="card-text"><?=$data['title']?></p>
                        </div>
                        <br>
                      </a>
                    </div>
                  </div>

                <?php }else{ ?>

                  <div class="col col-md-4 col-lg-3 mb-3 px-2">

                    <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
                      <div class="box-book2">
                        <div class="book2" style="margin:unset" >
                          <div class="front2">
                            <div class="cover2">
                              <img class="thumbnails" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" height="100%"> 
                            </div>
                          </div>
                          <div class="left-side2" style="background-color:<?=getColor($imgurl.'product/'.$data['picture'])[0]?>">
                            <h2>
                              <span style="opacity:0.8;color:<?=getColor($imgurl.'product/'.$data['picture'])[1]?>"><?=explode("-",$data['title'])[0]?></span>
                            </h2>
                          </div>
                        </div>
                      </div>
                    </a>

                  </div>

                <?php } ?>
              
            <?php } ?>
          </div>
        </div>
      </div>
      <?php }else{ require_once ('not_found.php'); } ?>

      <nav aria-label="Page navigation example">    
        <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
      </nav>

    </div>
  </div>
</div>
                -->