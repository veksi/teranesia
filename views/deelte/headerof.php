<nav class="navbar navbar-expand-lg navbar-light bg-white mb-0 px-4 shadow-sm fixed-top">
  <div class="container pl-md-5 pr-md-5">
    <a class="navbar-brand text-black-50 logo" href="<?=$base_url?>">
        <i class="fas fa-spa"></i> <b>teranesia</b>
    </a>
    
    <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav search w-100">
        <li class="nav-item bg-light search w-100 rounded">
        <form class="form-inline my-2 my-lg-0">
            <div class="form-group has-icon w-100 py-0 my-0 ">
              <span class="fa fa-search form-control-feedback "></span>
              <input class="form-control py-2 bg-transparent border-0 w-100" id="name_d" name="name_d" type="text" placeholder="Cari produk" autocomplete="off"/>
            </div>
          </form>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark " href="<?=$base_url?>"><b>Home</b> <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href="<?=$base_url?>partnership"><b>Partnership</b></a>
        </li>        
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href=""><b>News</b></a>
        </li>
        <li class="nav-item pl-2 pr-2">
          <a class="nav-link text-dark" href=""><b>Contact</b></a>
        </li>        
        <li class="nav-item pl-2 pr-2 py-1">
          <div class="dropdown show">
            <?php if(empty($_SESSION['customer'])){?>
              <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#masukModal">Masuk</a>            
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarModal">Daftar</a>
            <?php }else{ ?>
              <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">              
                <a class="dropdown-item" href="<?=$base_url?>profil">Profil</a>
                <a class="dropdown-item" href="<?=$base_url?>pembelian">Pembelian</a>                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void(0)" onclick="window.location.href='<?=$base_url;?>config/logout.php'">Keluar</a>
            <?php } ?>
            </div>
          </div>
        </li>       
      </ul>      
    </div>
    <!--<div class="d-block d-md-none">
            <button class="btn btn-link btn-sm ml-auto" id="sidebarToggle" href="#"><i class="fa fa-bars text-muted"></i></button>
        </div>-->
  </div>
</nav>