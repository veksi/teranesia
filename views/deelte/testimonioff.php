<?php
  $page_url       = explode('?',$curr_url)[0];
  $item_per_page  = 8;
  if(isset($_GET["page"])){ //Get page number from $_GET["page"]
      $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
      if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
  }else{
      $page_number = 1; //if there's no page number, set it to 1
  }

  $get_total_rows = $testimoni->count(); //hold total records in variable
  $total_pages    = ceil($get_total_rows/$item_per_page); //break records into pages
  $page_position  = (($page_number-1) * $item_per_page); //get starting position to fetch the records
  $allItem        = $testimoni->index($URL[2],$page_position,$item_per_page);
?>
<div class="container my-4 px-4">
  <div class="row">
    <!--<div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks shadow-sm">
        <?php //require __DIR__.'/menu_sidebar.php'; ?>
      </div>
    </div>-->
    <!--<div class="col-sm-12 col-md col-lg-9 bg-transparent">-->
    <div class="col-sm-12 col-md-12 col-lg-12 bg-transparent">
      <?php if(is_array($allItem)){ ?>
        
        <div class="card-columns">
          <?php foreach($allItem as $data){ ?>
            <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
              <div class="card shadow-custom-1 border rounded-top-6 rounded-bottom-6">
                <img class="card-img-top rounded-top-6 rounded-bottom-6" src="<?=$imgurl?>testimoni/<?=$data['foto']?>" alt="<?=$data['title']?>"> 
                <div class="card-body">
                  <p class="card-text"><b><?=$data['title']?></b></p>
                </div>                
              </div>
            </a>
          <?php } ?>
        </div>
      
      <?php }else{ require_once ('404_page.php'); } ?>

      <nav aria-label="Page navigation example">    
        <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
      </nav>

    </div>
  </div>
</div>