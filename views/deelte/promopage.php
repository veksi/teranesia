<div class="container my-4 px-4">
  <div class="row justify-content-center">
  
    <?php if(!empty($promopage->othernews($id))){ ?>
      <div class="col-sm-12 col-md-3 bg-transparent order-2 order-sm-2 order-md-1">
        
        <?php foreach($promopage->othernews($id) as $data){ ?>
          <div class="mb-3">
            <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
              <div class="card shadow-custom-1 border rounded-top-6 rounded-bottom-6">
                <img class="card-img-top rounded-top-6" src="<?=$imgurl?>promo/<?=$data['foto']?>" alt="<?=$data['title']?>"> 
                <div class="card-body">
                  <p class="card-text"><?=$data['title']?></p>
                </div>                
              </div>
            </a>          
          </div>
        <?php } ?>

      </div>
    <?php } ?>

    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

      <?php 
      $promopage = $promopage->index($id);
      if(is_array($promopage)){ ?>
                
        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
          <img class="card-img-top rounded-top-6" src="<?=$imgurl?>promo/<?=$promopage['foto']?>" alt="<?=$promopage['title']?>"> 
          <div class="card-body">            
            <h1 class="card-text" style="font-size:20px !important"><b><?=$promopage['title']?></b></h1>            
            <br>
            <p class="card-text"><?=$promopage['content']?></p>
            <div class="sharethis-inline-share-buttons"></div>
          </div>                
        </div>
      
      <?php }else{ require_once ('404_page.php'); } ?>

    </div>
  </div>
</div>