<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-white bg-white" id="sidenavAccordion">
        <div class="sb-sidenav-menu border-right">
            <div class="nav pt-3">
                <!--<div class="list-group-item list-group-horizontal-sm border-0 px-4"><b>Kategori</b></div>
                <?php foreach($mandatory->tbl_category() as $data){ ?>              
                    <a class="nav-link text-muted px-4" href="<?=$base_url.seo($data['title'])?>">
                        <div class="sb-nav-link-icon">
                            <span>
                                <img src="<?=$imgurl.$data['icon']?>" height="30">                                
                            </span>                                    
                        </div>
                        <?=$data['title']?>                        
                    </a>                    
                <?php } ?>-->

                <!--
                <form class="form-inline px-4 border-bottom nav-link" action="/search/">
                    <div class="form-group w-100 py-0 my-0 ">
                        <div class="inner-addon right-addon w-100">
                            <i class="glyphicon fa fa-search"></i>
                            <input type="text" name="q" class="form-control bg-transparent border-0 w-100" placeholder="Cari di Teranesia"/>
                        </div>
                    </div>
                </form>

                <div class="nav-link px-4 border-bottom" href="<?=$base_url?>">
                    <form action="/search/">                        
                            <div class="inner-addon right-addon w-100">
                                <i class="glyphicon fa fa-search"></i>
                                <input type="text" name="q" class="form-control bg-transparent border-0 w-100" placeholder="Cari di Teranesia"/>
                            </div>                        
                    </form>
                </div>-->

                <a class="nav-link px-4 border-bottom" href="<?=$base_url?>">
                    <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                    <b>Home</b>
                </a>

                <a class="nav-link px-4 border-bottom" href="<?=$base_url?>promo">
                    <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                    <b>Promo</b>
                </a>

                <div class="accordion border-bottom" id="accordionExample">                    
                    <a class="nav-link px-4" id="headingOne" href="javascipt:void(0)" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
                        <!--<div class="sb-nav-link-icon"><i class="fa  fa-th-large"></i></div>-->
                        <b>Kategori</b>
                        <span class="col text-right"><i class="fa fa-chevron-down"></i></span>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body p-0">
                            <div class="list-group" id="list-tab" role="tablist">
                                <?php foreach($mandatory->tbl_category() as $data){ ?>              
                                    <li class="list-group-item py-0 border-bottom-0 border-top border-left-0 border-right-0 rounded-0">
                                        <a class="nav-link" href="<?=$base_url.seo($data['title'])?>">                                            
                                            <?=$data['title']?>                        
                                        </a>
                                    </li>
                                <?php } ?>                                
                            </div>                                    
                        </div>
                    </div>                    
                </div>

                <a class="nav-link px-4 border-bottom" href="<?=$base_url?>partnership">
                    <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                    <b>Partnership</b>
                </a>

                <a class="nav-link px-4 border-bottom" href="<?=$base_url?>news">
                    <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                    <b>News</b>
                </a>

                <a class="nav-link px-4 border-bottom" href="<?=$base_url?>contact">
                    <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                    <b>Contact</b>
                </a>

            </div>
        </div>
       
    </nav>
</div>
