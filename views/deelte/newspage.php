<div class="container my-4 px-4">
  <div class="row justify-content-center">

    <?php if(!empty($newspage->othernews($id))){ ?>
      <div class="col-sm-12 col-md-3 bg-transparent order-2 order-sm-2 order-md-1">
        
        <?php foreach($newspage->othernews($id) as $data){ ?>
          <div class="mb-3">
            <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
              <div class="card shadow-custom-1 border rounded-top-6 rounded-bottom-6">
                <img class="card-img-top rounded-top-6" src="<?=$imgurl?>news/<?=$data['foto']?>" alt="<?=$data['title']?>"> 
                <div class="card-body">
                  <p class="card-text"><b><?=$data['title']?></b></p>
                </div>                
              </div>
            </a>          
          </div>
        <?php } ?>

      </div>
    <?php } ?>

    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

      <?php 
      $newspage = $newspage->index($id);
      if(is_array($newspage)){ ?>
                
        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
          <img class="card-img-top rounded-top-6" src="<?=$imgurl?>news/<?=$newspage['foto']?>" alt="<?=$newspage['title']?>"> 
          <div class="card-body">
            <h1 class="card-text" style="font-size:20px !important"><b><?=$newspage['title']?></b></h1>
            <br>
            <p class="card-text"><?=$newspage['content']?></p>
            <div class="sharethis-inline-share-buttons"></div>
          </div>                
        </div>
      
      <?php }else{ require_once ('404_page.php'); } ?>

    </div>
  </div>
</div>