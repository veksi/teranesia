<div class="container my-4 px-4">
  <div class="row justify-content-center">
    
    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">     
          <img class="card-img-top rounded-top-6" src="<?=$imgurl?>94170.jpg">                  
          <div class="card-body">
            <div class="row middleVertical open-sans" >
              <div class="col-sm-6 text-center">
                <p class="card-text">
                  <a class="text-dark logo" href="<?=$base_url?>">            
                    <img src="<?=$imgurl?>logo.png" height="70px"> <b>teranesia</b>
                  </a>
                </p>
              </div>
              <div class="col-sm-6">
                <p class="card-text">
                  <ul class="list-group mb-4">                
                    <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="mailto:<?=$mailcenter?>" target="_top"><i class="fa fa-envelope fa-fw"></i> &nbsp; <?=$mailcenter?></a></li>
                    <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="https://wa.me/<?=$callcenter?>?text=Halo teranesia.id" target="_top"><i class="fa fa-whatsapp fa-fw"></i> &nbsp; +<?=$callcenter?></a></li>     
                    <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="tel:+<?=$callcenter?>" target="_top"><i class="fa fa-phone-square fa-flip-horizontal fa-fw"></i> &nbsp; +<?=$callcenter?></a></li>
                  </ul>   
                </p>
              </div>
            </div>
          </div>                
        </div>

    </div>
  </div>
</div>