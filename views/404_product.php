<div class="container h-100 d-flex">
    <div class="jumbotron bg-white shadow-sm my-auto text-center w-100 border">
      <h1 class="display-3 text-muted"><i class="fa fa-dropbox fa-5x" aria-hidden="true"></i></h1>
      <p class="lead text-muted">Produk yang dicari tidak ditemukan!</p>
    </div>
</div>