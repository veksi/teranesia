<?php $data = $profil->index(); ?>
<div class="container my-4 px-3">
    <div class="row justify-content-center">
        <div class="col-sm-6 col-md col-lg-3 mb-3">
            <div class="sticks">
                <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm text-center p-3">
                    <img class="img-thumbnail m-2 rounded-circle" alt="avatar" style="width:200px;height:200px;object-fit:cover" src="<?=(!empty($data['foto'])) ? $imgurl.'customer/'.$data['foto'] : $imgurl."noprofile.png";?>" >
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md col-lg-6">
            <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm p-3">
                <div class="text-right">                                 
                    <button onclick="location.href='<?=$base_url?>profil/edit'" type="button" class="btn btn-grad-1 text-white"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i> Edit Profil</button>
                </div>
                <div class="clearfix"></div>

                <div class="form-group row">
                    <label for="staticEmail" class="col col-form-label text-muted">Nama</label>
                    <div class="col">
                    <input type="text" readonly class="form-control-plaintext text-sm-left text-md-right" id="staticEmail" value="<?=$data['fullname']?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col col-form-label text-muted">Email</label>
                    <div class="col">
                    <input type="text" readonly class="form-control-plaintext text-sm-left text-md-right" id="staticEmail" value="<?=$data['email']?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col col-form-label text-muted">Nomor Handphone</label>
                    <div class="col">
                    <input type="text" readonly class="form-control-plaintext text-sm-left text-md-right" id="staticEmail" value="<?=$data['hp']?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col col-form-label text-muted">Tempat Tanggal Lahir</label>
                    <div class="col">
                    <input type="text" readonly class="form-control-plaintext text-sm-left text-md-right" id="staticEmail" value="<?=$data['bornPlace']?> <?=date("d-m-Y", strtotime(@$data['bornDate']))?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="staticEmail" class="col col-form-label text-muted">Alamat</label>
                    <div class="col">
                    <input type="text" readonly class="form-control-plaintext text-sm-left text-md-right" id="staticEmail" value="<?=$data['alamat']?>">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
