<?php
  $page_url       = explode('?',$curr_url)[0];
  $item_per_page  = 16;
  if(isset($_GET["page"])){ //Get page number from $_GET["page"]
      $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
      if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
  }else{
      $page_number = 1; //if there's no page number, set it to 1
  }

  $get_total_rows = $listing->count($URL[2]); //hold total records in variable
  $total_pages    = ceil($get_total_rows/$item_per_page); //break records into pages
  $page_position  = (($page_number-1) * $item_per_page); //get starting position to fetch the records
  $allItem        = $listing->index($URL[2],$page_position,$item_per_page);
?>


<div class="container px-4">
  <div class="row">
    <div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks">
        <?php require __DIR__.'/menu_sidebar.php'; ?>
        <br>
        <div id="shape">
          <div class="one bg-white">
              <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
          <div class="two bg-white">
            <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
          <div class="three bg-white">
            <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
          <div class="four bg-white">
            <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
          <div class="five bg-white">
            <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
          <div class="six bg-white">
            <a href="<?=$base_url?>"><img src="<?=$base_url?>public/cube/logo-cubex.png"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-9 col-md col-lg-9 bg-transparent">
      
      <?php if(is_array($allItem)){ ?>
      <div class="row">
        <div class="col-sm bg-transparent">
          <div class="row">
            <?php foreach($allItem as $data){ ?>
              
                <?php if($data['attachment']==''){?>

                  <div class="col-6 col-md-4 col-lg-3 mb-3 px-2">
                    <!--
                    <div class="card shadow-custom-1 border-0 rounded">
                      <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
                        <div style="height:150px !important;overflow:hidden">
                          <img class="thumbnails rounded-top" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" height=150px> 
                        </div>
                        <div class="card-body" style="height:140px !important;overflow:hidden">
                          <p class="card-text"><?=$data['title']?></p>
                        </div>
                        <br>
                      </a>
                    </div>-->
                      <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">
                        <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
                          <div class="mhn-inner pb-3">
                            <div style="height:175px !important;overflow:hidden;">
                              <img class="rounded-top-6" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" width=100% height=100%  style="object-fit:cover;"> 
                            </div>
                            <div style="height:55px !important;overflow:hidden;">
                              <div class="card-text px-3 py-2"><?=$data['title']?></div>
                            </div>
                            <div style="height:40px !important;overflow:hidden;">
                              <p class="card-text px-3 py-2"><b><?=Rp(num($data['price']))?></b></p>
                            </div>              
                          </div>
                        </a>
                      </div>
                  </div>

                <?php }else{ ?>

                  <div class="col-6 col-md-4 col-lg-3 mb-3 px-2">

                      <div class="card shadow-sm border rounded-0">
                        <a href="<?=$base_url.$URL[2].'/'.seo($data['title']).'-'.$data['id']?>">
                          <div class="mhn-inner pb-3">
                            <div style="height:230px">
                              <img src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" width=100% height=100%  style="object-fit:cover;"> 
                            </div>
                            <div class="" style="height:40px !important;overflow:hidden;">
                              <p class="card-text px-3 py-2"><b><?=Rp(num($data['price']))?></b></p>
                            </div>   
                          </div>
                        </a>
                      </div>
<!--
                      <div class="box-book2">
                        <div class="book2" style="margin:unset" >
                          <div class="front2">
                            <div class="cover2">
                              <img class="thumbnails" src="<?=$imgurl?>product/<?=$data['picture']?>" alt="<?=$data['title']?>" height="100%"> 
                            </div>
                          </div>
                          <div class="left-side2" style="background-color:<?=getColor($imgurl.'product/'.$data['picture'])[0]?>">
                            <h2>
                              <span style="opacity:0.8;color:<?=getColor($imgurl.'product/'.$data['picture'])[1]?>"><?=explode("-",$data['title'])[0]?></span>
                            </h2>
                          </div>
                        </div>
                      </div>-->                    

                  </div>

                <?php } ?>
              
            <?php } ?>
          </div>
        </div>
      </div>
      <?php }else{ require_once ('404_page.php'); } ?>

      <nav aria-label="Page navigation example">    
        <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
      </nav>

    </div>
  </div>
</div>