<?php 
$detail     = $beli->index($_POST['id']); 
$customer   = $beli->get_customer(); 
?>
<div class="container my-4">
  <div class="row">
    <div class="col-lg-8 bg-transparent mb-3">    
      <div class="bg-white shadow-sm py-3 border rounded-top-6 rounded-bottom-6">
        <div class="container">
          <div class="row align-items-center">          
            <div class="col-sm-12 col-md-3 text-center mb-sm-3">
              <img src="<?=$imgurl?>product/<?=$detail['picture']?>" alt="..." class="img-thumbnail">
            </div>
            <div class="col align-middle">
              <h1 style="font-size:20px !important"><p><b><?=$detail['title']?></b></p></h1>               
              <?=Rp(num($detail['price']))?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm col-md col-lg-4 bg-transparent">
      <div class="sticks">
        <div class="container bg-white shadow-sm p-3 border rounded-top-6 rounded-bottom-6">
          <h1 style="font-size:20px !important"><p><b>Total Tagihan</b></p></h1>               
          <h2 style="font-size:18px !important" class="text-success"><p><?=(!empty($_SESSION['customer']))? ''.Rp(num($detail['price'])).'':'Rp * *** *** <a href="#" class="float-right link" data-toggle="modal" data-target="#masukModal"><small>Tampilkan harga</small></a>'?></p></h2>
          <p><button type="button" id="pay-button" class="btn btn-success btn-block">Bayar</button></p>
        </div>
      </div>
    </div>
  </div>
</div>