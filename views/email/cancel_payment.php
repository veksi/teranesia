<?php
    include $base_path."vendor/PHPMailer-5.1/class.phpmailer.php";

    $bank               = getBank($respon);

    $subject            = "[Teranesia.id] Pembayaran ".strtoupper($bank[0])." ".$bank[2]." Anda telah kami batalkan - #".$order_id;
    $email              = $data['customer_details']['email'];
    $nama               = explode(" ",$data['customer_details']['first_name'])[0];
    $price              = num($data['transaction_details']['gross_amount']);
    $product            = $data['item_details'][0]['name'];
    $pay                = tgl_indo(date("Y-m-d H:i:s", strtotime($pay_date)),false,true);
    
    $message            = file_get_contents(__DIR__.'/template/cancel_payment.html');

    $message            = str_replace('%order_id%', $order_id, $message);
    $message            = str_replace('%customer%', $nama, $message);
    $message            = str_replace('%price%', $price, $message);
    $message            = str_replace('%bank%', strtoupper($bank[0]), $message);
    $message            = str_replace('%product%', $product, $message);
    $message            = str_replace('%pay_date%', $pay, $message);
    $message            = str_replace('%payment_method%', $bank[2], $message);

    $mail               = new PHPMailer; 
    $mail->IsSMTP();
    $mail->SMTPSecure   = 'ssl';     
    $mail->SMTPDebug    = false;
    $mail->SMTPAuth     = true;

    $mail->Host         = $mailHost;
    $mail->Port         = $mailPort;
    $mail->Username     = $mailUsername;
    $mail->Password     = $mailPassword;
    $mail->SetFrom($mailSender[0],$mailSender[1]); //set email pengirim
    
    $mail->Subject      = $subject; //subyek email
    $mail->AddAddress($email);  //tujuan email

    $mail->MsgHTML($message);
    $mail->IsHTML(true); 
    $mail->CharSet="utf-8";
    $mail->Send();

?>
