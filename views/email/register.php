<?php
    include $base_path."vendor/PHPMailer-5.1/class.phpmailer.php";

    $customer           = explode(" ",$_POST['nama'])[0];
    $subject            = "[Teranesia.id] Selamat Datang ".ucfirst($customer);
    $email              = $_POST['email'];
    $password           = $_POST['password'];
    $reg_date           = tgl_indo(date('Y-m-d'));
    $link               = $base_url.'confirm/'.encrypt($email);

    $message            = file_get_contents(__DIR__.'/template/register.html');    

    $message            = str_replace('%customer%', $customer, $message);
    $message            = str_replace('%reg_date%', $reg_date, $message);
    $message            = str_replace('%username%', $email, $message);
    $message            = str_replace('%password%', $password, $message);
    $message            = str_replace('%link%', $link, $message);
    
    $mail               = new PHPMailer; 
    $mail->IsSMTP();
    $mail->SMTPSecure   = 'ssl';     
    $mail->SMTPDebug    = false;
    $mail->SMTPAuth     = true;

    $mail->Host         = $mailHost;
    $mail->Port         = $mailPort;
    $mail->Username     = $mailUsername;
    $mail->Password     = $mailPassword;
    $mail->SetFrom($mailSender[0],$mailSender[1]); //set email pengirim
    
    $mail->Subject      = $subject; //subyek email
    $mail->AddAddress($email);  //tujuan email

    $mail->MsgHTML($message);
    $mail->IsHTML(true); 
    $mail->CharSet="utf-8";
    $mail->Send();
/*

$bank               = getBank($midtrans);
echo $bank[1];*/
?>
