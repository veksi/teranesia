<?php
if($bank[0]=='bca'){
    $guide = '<p><b>PERHATIAN!</b></p>
    <p>Mohon selesaikan pembayaran sebelum %expired%. Apabila melewati batas waktu, pesanan Anda akan otomatis dibatalkan.</p>
    <p><b>Cara pembayaran via ATM BCA</b></p>
    <ol>
        <li>Pada menu utama, pilih <b>Transaksi Lainnya</b></li>
        <li>Pilih <b>Transfer</b></li>
        <li>Pilih ke Rek <b>BCA Virtual Account</b></li>
        <li>Masukkan nomor <b>%va%</b> lalu tekan <b>Benar</b></li>
        <li>Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan <b>Ya</b></li>
    </ol>
    <p><b>Cara pembayaran via Klik BCA</b></p>
    <ol>
        <li>Pilih menu <b>Transfer Dana</b></li>
        <li>Pilih Transfer ke <b>BCA Virtual Account</b></li>
        <li>Masukkan nomor BCA Virtual Account <b>%va%</b></li>
        <li>Jumlah yang akan ditransfer, nomor rekening dan nama merchant akan muncul di halaman konfirmasi pembayaran, jika informasi benar klik <b>Lanjutkan</b></li>
        <li>Masukkan respon KEYBCA APPLI 1 yang muncul pada Token BCA Anda, lalu klik tombol <b>Kirim</b></li>
        <li>Transaksi Anda selesai</li>
    </ol>
    <p><b>Cara pembayaran via m-BCA</b></p>
    <ol>
        <li>Pilih <b>m-Transfer</b></li>
        <li>Pilih <b>Transfer</b></li>
        <li>Pilih <b>BCA Virtual Account</b></li>
        <li>Pilih nomor rekening yang akan digunakan untuk pembayaran</li>
        <li>Masukkan nomor BCA Virtual Account <b>%va%</b>, lalu pilih <b>OK</b></li>
        <li>Nomor BCA Virtual Account dan nomor Rekening Anda akan terlihat di halaman konfirmasi rekening</li>
        <li>Pilih <b>OK</b> pada halaman konfirmasi pembayaran</li>
        <li>Masukkan PIN BCA untuk mengotorisasi pembayaran</li>
        <li>Transaksi Anda selesai</li>
    </ol>';
}else if($bank[0]=='mandiri'){
    $guide = '<p><b>PERHATIAN!</b></p>
    <p>Mohon selesaikan pembayaran sebelum %expired%. Apabila melewati batas waktu, pesanan Anda akan otomatis dibatalkan.</p>
    <p><b>Informasi</b></p>
    <ul>
        <li>Pembayaran dapat dilakukan melalui ATM Mandiri atau Internet Banking Mandiri</li>
    </ul>
    <p><b>Pembayaran melalui ATM Mandiri:</b></p>
    <ol>
        <li>Masukkan PIN Anda</li>
        <li>Pada menu utama pilih menu <b>Pembayaran</b> kemudian pilih menu <b>Multi Payment</b></li>
        <li>Masukan <b>Kode Perusahaan 70012</b></li>
        <li>Masukan <b>Kode Pembayaran %va%</b></li>
        <li>Konfirmasi pembayaran Anda</li>
    </ol>
    <p><b>Cara membayar melalui Internet Banking Mandiri:</b></p>
    <ol>
        <li>Login ke <a href="https://ibank.bankmandiri.co.id/">Mandiri Internet Banking</a></li>
        <li>Di Menu Utama silakan pilih <b>Bayar</b> kemudian pilih <b>Multi Payment</b></li>
        <li>Pilih akun anda di <b>Dari Rekening</b>, kemudian di <b>Penyedia Jasa</b> pilih <b>midtrans</b></li>
        <li>Masukkan <b>Kode Pembayaran %va%</b> dan klik <b>Lanjutkan</b></li>
        <li>Konfirmasi pembayaran anda menggunakan Mandiri Token</li>

    </ol>';
}else if($bank[0]=='bni'){
    $guide = '<p><b>PERHATIAN!</b></p>
    <p>Mohon selesaikan pembayaran sebelum %expired%. Apabila melewati batas waktu, pesanan Anda akan otomatis dibatalkan.</p>
    <p><b>Cara pembayaran via ATM BNI</b></p>
    <ol>
        <li>Pada menu utama, pilih <b>Menu Lainnya</b></li>
        <li>Pilih <b>Transfer</b></li>
        <li>Pilih <b>Rekening Tabungan</b></li>
        <li>Pilih <b>Ke Rekening BNI</b></li>
        <li>Masukkan Nomor Rekening Pembayaran Anda <b>%va%</b> lalu tekan <b>Benar</b></li>
        <li><b>Masukkan jumlah tagihan yang akan Anda bayar secara lengkap.</b> Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak</li>
        <li>Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening dan nama Merchant. Jika informasi telah sesuai tekan <b>Ya</b></li>
    </ol>';
}else if($bank[0]=='permata'){
    $guide = '<p><b>PERHATIAN!</b></p>
    <p>Mohon selesaikan pembayaran sebelum %expired%. Apabila melewati batas waktu, pesanan Anda akan otomatis dibatalkan.</p>
    <p><b>Cara pembayaran via ATM Mandiri / Bersama</b></p>
    <ol>
        <li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
        <li>Pilih <b>Transfer</b></li>
        <li>Pilih <b>Antar Bank Online</b></li>
        <li>Masukkan nomor Masukkan nomor <b>013 %va%</b> (kode 013 dan 16 angka Virtual Account)</li>
        <li>Masukkan jumlah harga yang akan Anda bayar secara lengkap (tanpa pembulatan). Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</li>
        <li>Kosongkan nomor referensi dan tekan <b>Benar</b></li>
        <li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no.rekening tujuan. Jika informasinya telah cocok tekan <b>Benar</b></li>
    </ol>
    <p><b>Cara pembayaran via ATM BCA / Prima</b></p>
    <ol>
        <li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
        <li>Pilih <b>Transfer</b></li>
        <li>Pilih <b>Ke Rek Bank Lain</b></li>
        <li>Masukkan <b>013</b> (Kode Bank Permata) lalu tekan <b>Benar</b></li>
        <li>Masukkan jumlah harga yang akan Anda bayar secara lengkap (tanpa pembulatan), lalu tekan Benar. Penting: Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</li>
        <li>Masukkan <b>%va%</b> (16 digit no. virtual account pembayaran) lalu tekan Benar</li>
        <li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no.rekening tujuan. Jika informasinya telah cocok tekan <b>Benar</b></li>
    </ol>
    <p><b>Cara pembayaran via ATM Permata / Alto</b></p>
    <ol>
        <li>Di menu utama, Pilih <b>Transaksi Lainnya</b></li>
        <li>Pilih <b>Pembayaran</b></li>
        <li>Pilih <b>Pembayaran Lainnya</b></li>
        <li>Pilih <b>Virtual Account</b></li>
        <li>Masukkan 16 digit no. Virtual Account <b>%va%</b> dan tekan <b>Benar</b></li>
        <li>Di halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, no. Virtual Account, dan nama toko. Jika informasinya telah cocok tekan <b>Benar</b></li>
        <li>Pilih rekening pembayaran Anda dan tekan <b>Benar</b></li>
    </ol>';
}

if($bank[0]=='mandiri'){
    $office = '<tr><td>Kode Perusahaan</td><td>70012</td></tr>';
}else{
    $office = '';
}
?>