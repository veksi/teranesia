<?php
    $subject            = $_POST['judul'];
    $email              = $_POST['email'];
    $nama               = $_POST['nama'];
    $hp                 = $_POST['hp'];
    $message            = $_POST['soal'];

    include $base_path."vendor/PHPMailer-5.1/class.phpmailer.php";

    $mail               = new PHPMailer; 
    $mail->IsSMTP();
    $mail->SMTPSecure   = 'ssl';     
    $mail->SMTPDebug    = false;
    $mail->SMTPAuth     = true;

    $mail->Host         = $mailHost;
    $mail->Port         = $mailPort;
    $mail->Username     = $mailUsername;
    $mail->Password     = $mailPassword;
    $mail->SetFrom($email); //set email pengirim

    $mail->Subject      = $subject; //subyek email    
    $mail->AddAddress($mailCenter);  //tujuan email

    $mail->MsgHTML($message);
    if($mail->Send()){        
        return $resp = array(1,"Email terkirim");
    }else{        
        return $resp = array(0,"Email error, silahkan ulangi beberapa saat lagi");
    }
        
?>