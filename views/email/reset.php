<?php
    include $base_path."vendor/PHPMailer-5.1/class.phpmailer.php";

    $customer           = explode(" ",$data['fullname'])[0];
    $subject            = "[Teranesia.id] Reset Password ";
    $email              = $_POST['email'];    
    $link               = $base_url.'newpassword/'.$data['reset_key'];

    $message            = file_get_contents(__DIR__.'/template/reset.html');    

    $message            = str_replace('%customer%', $customer, $message);
    $message            = str_replace('%link%', $link, $message);
    
    $mail               = new PHPMailer; 
    $mail->IsSMTP();
    $mail->SMTPSecure   = 'ssl';     
    $mail->SMTPDebug    = false;
    $mail->SMTPAuth     = true;

    $mail->Host         = $mailHost;
    $mail->Port         = $mailPort;
    $mail->Username     = $mailUsername;
    $mail->Password     = $mailPassword;
    $mail->SetFrom($mailSender[0],$mailSender[1]); //set email pengirim
    
    $mail->Subject      = $subject; //subyek email
    $mail->AddAddress($email);  //tujuan email

    $mail->MsgHTML($message);
    $mail->IsHTML(true); 
    $mail->CharSet="utf-8";
    $mail->Send();
/*

$bank               = getBank($midtrans);
echo $bank[1];*/
?>
