<?php 
$order_id     = $URL[3];
$respon       = trx_status($order_id);
$payment_type = array('bank_transfer','echannel');
//print_r($respon);
if($respon['status_code']!='404'){

  if(!in_array($respon['payment_type'],$payment_type)){ header("Location: ".$base_url."pembelian"); } // jika bukan bank transfer/echannel
  
  $midtrans = serialize($respon);
  
  $exp      = date('d-m-Y H:i:s', strtotime($respon['transaction_time']. ' +1 days'));
  $exp      = date("Y-m-d H:i:s", strtotime($exp));

  $bank     = getBank($respon);
  $hrg      = $respon['gross_amount'];
  $status   = $respon['transaction_status'];

  if(!empty($status)){ $payment->update($midtrans,$exp,$bank[1],$hrg,$status,$order_id); }
  
  if($status=='pending'){
    $getpay   = $payment->index($order_id);
    if(is_array($getpay)){
      $exp_time = date("F j, Y, g:i a",strtotime($getpay['expired_va']));    
      ?>

      <script>      
        var countDownDate = new Date("<?=$exp_time?>").getTime();
        var x = setInterval(function() {
          var now = new Date().getTime();
          var distance = countDownDate - now;        
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          if(hours<10){ hours_time = "0"+hours; }else{ hours_time = hours; }
          if(minutes<10){ minutes_time = "0"+minutes; }else{ minutes_time = minutes; }
          if(seconds<10){ second_time = "0"+seconds; }else{ second_time = seconds; }
                
          document.getElementById("demo").innerHTML = "<i class='fa fa-clock-o fa-fw'></i>  "+ hours_time + ":" +minutes_time + ":" + second_time + "";                
                    
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
          }
        }, 1000);
      </script>

      <div class="alerts" style="position: fixed; left: 50%;bottom:10px;display:none;z-index:99999">
        <div style="position: relative; left: -50%; border: dotted red 1px;color:white;background:rgb(0,0,0,0.7);padding:10;z-index:9999;">
        Link berhasil disalin
        </div>
      </div>

      <div class="container my-4 px-4">
        <div class="row justify-content-center">
          <div class="col-sm-12 col-md-6">
            <div class="card shadow-sm border rounded-top-6 rounded-bottom-6">            
              <div class="p-4">
                
                <div class="text-center fs-18 mt-3 mb-5">                  
                  <p>
                    <b>Selesaikan pembayaran dalam</b><br>
                    <b><span id="demo" class="text-success"><i class='fa fa-clock-o fa-fw'></i> 00:00:00</span></b>
                  </p>
                  <div class="text-muted">Batas Akhir Pembayaran</div>
                  <b><?=tgl_indo($getpay['expired_va'],true,true)?></b>
                </div>

                <ul class="list-group">
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?=strtoupper($bank[0])?> <?=$bank[2]?>
                  </li>
                  <?php if($bank[0]=='mandiri'){ ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                      <div>
                        <span class="text-muted">Kode Perusahaan</span><br>
                        <b>70012</b>
                      </div>                                    
                    </li>
                  <?php } ?>                                
                  <li class="list-group-item d-flex justify-content-between align-items-center">
                    <div>
                      <span class="text-muted">Nomor <?=$bank[2]?></span><br>
                      <input type="hidden" name="rek" value="<?=$getpay['rekening']?>">
                      <b><?=$getpay['rekening']?></b>
                    </div>
                    <span class="badge badge-success badge-pill fs-14 font-weight-normal">
                      <a href=javascript:void(0) onclick="copyFunc('rek')" style="color:#0571CB"><b><span class="pull-right">Salin &nbsp;<i class="fa fa-clone fa-1x"></i></span></b></a>
                    </span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center">                
                    <div>
                      <span class="text-muted">Total Pembayaran</span><br>
                      <input type="hidden" name="price" value="<?=$getpay['harga']?>">
                      <div><b><?=Rp(num($getpay['harga']))?></b></div>
                    </div>
                    <span class="badge badge-success badge-pill fs-14 font-weight-normal">
                      <a href=javascript:void(0) onclick="copyFunc('price')" style="color:#0571CB"><b><span class="pull-right">Salin &nbsp;<i class="fa fa-clone fa-1x"></i></span></b></a>
                    </span>
                  </li>
                </ul>

                <div class="text-center fs-18 mt-5">                  
                  <p>
                    <a href="<?=$base_url?>pembelian" class="btn btn-grad-1 text-white">Saya sudah bayar</a>
                  </p>
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>

<?php 
    }else{
      require_once "views/404_page.php";  
    }
  }else{
    require_once "views/404_page.php";
  }
}else{ 
  require_once "views/404_page.php";
}?>

<script>

// copy clipboard
function copyFunc(val) {
  var copyText = document.getElementsByName(val)[0];
  copyText.type = 'text';
  copyText.select();
  document.execCommand("copy"); //this function copies the text of the input with ID "copyInp"
  copyText.type = 'hidden';
  $(".alerts").show();
  setTimeout(function(){
    $(".alerts").hide(); 
  }, 2000);
}
</script>
