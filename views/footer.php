<div class="jumbotron m-0 mt-5 footer-text bg-grad-3 text-white open-sans" style="border-radius:unset;">
  <div id="background"></div>
  <div class="container">
    <footer class="pt-4">
      <div class="row">
        <div class="col-md-4 px-0">
          <ul class="list-group mb-4" >
            <?php foreach($mandatory->side_menu() as $sidemenu){ ?>
              <li class="list-group-item bg-transparent border-0"><h5><b><?=$sidemenu['title']?></b></h5></li>
                <?php foreach($mandatory->child_sm($sidemenu['id']) as $data){ ?>
                  <li class="list-group-item bg-transparent border-0 pb-0">
                    <a href="<?=$base_url.seo($data['title'])?>"><small><h3><?=$data['title']?></h3></small></a>
                  </li>
                <?php } ?>
            <?php } ?>
          </ul>
          <ul class="list-group mb-2" >
            <li class="list-group-item bg-transparent border-0">
              <h5><b>Ikuti Kami</b></h5>
              <a target="_blank" href="https://www.facebook.com/">
                <span class="fa-stack fa-lg fb my-2">
                  <i class="fa fa-square fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
              </a>&nbsp;
              <a target="_blank" href="https://twitter.com/infoteranesia">
                <span class="fa-stack fa-lg twitter my-2">
                  <i class="fa fa-square fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>&nbsp;
              <a target="_blank" href="https://www.instagram.com/tera.nesia/">
                <span class="fa-stack fa-lg ig my-2">
                  <i class="fa fa-square fa-stack-2x"></i>
                  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                </span>
              </a>&nbsp;
              <a target="_blank" href="https://youtube.com/channel/UCM2euFPYi5eWaNXOUfmD1tQ">
                <span class="fa-stack fa-lg youtube my-2">
                  <i class="fa fa-square fa-stack-2x"></i>
                  <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>                  
                </span>
              </a>&nbsp;
              <a target="_blank" href="https://www.linkedin.com/in/info-teranesia-24bb2820b/">
                <span class="fa-stack fa-lg linkedin my-2">
                  <i class="fa fa-square fa-stack-2x"></i>
                  <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                </span>
              </a>&nbsp;
            </li>
          </ul>
        </div>
        <div class="col-md-4 px-0">
          <ul class="list-group mb-4" >
            <li class="list-group-item bg-transparent border-0"><h5><b>Link</b></h5></li>
            <?php foreach($mandatory->top_menu() as $data){ ?>
              <li class="list-group-item bg-transparent border-0 pb-0"><a href="<?=$base_url.seo($data['title'])?>" class="fw-500"><small><h3><?=$data['title']?></h3></small></a></li>
            <?php } ?>
          </ul>
        </div>
        <div class="col-md-4 px-0">          
          <ul class="list-group mb-4">
            <li class="list-group-item bg-transparent border-0 pb-0"><h5><b>Contact</b></h5></li>
            <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="mailto:<?=$mailcenter?>" target="_top"><small><i class="fa fa-envelope fa-fw"></i> &nbsp; <?=$mailcenter?></small></a></li>
            <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="https://wa.me/<?=$callcenter?>?text=Halo teranesia.id" target="_top"><small><i class="fa fa-whatsapp fa-fw"></i> &nbsp; +<?=$callcenter?></small></a></li>     
            <li class="list-group-item bg-transparent border-0 pb-0"><a class="fw-500" href="tel:+<?=$callcenter?>" target="_top"><small><i class="fa fa-phone-square fa-flip-horizontal fa-fw"></i> &nbsp; +<?=$callcenter?></small></a></li>
          </ul>    
          <ul class="list-group mb-4">
            <li class="list-group-item bg-transparent border-0 pb-0"><h5><b>Metode Pembayaran</b></h5></li>
            <li class="list-group-item bg-transparent border-0 pb-0"><small>Digital Payment</small></li>
            <li class="list-group-item bg-transparent border-0 pb-0">              
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>inacash.jpg" style="max-width:100px;max-height:35px">
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>gopay_logo.png" style="max-width:100px;max-height:35px">
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>ovo.png" style="max-width:100px;max-height:35px">
            </li>
            <li class="list-group-item bg-transparent border-0 pb-0"><small>Bank Transfer</small></li>
            <li class="list-group-item bg-transparent border-0 pb-0">                          
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>bni.png" style="max-width:100px;max-height:35px">
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>permata.png" style="max-width:100px;max-height:35px">
              <img class="rounded p-2 mb-1 bg-white" src="<?=$imgurl?>mandiri.jpg" style="max-width:100px;max-height:35px">
            </li>
          </ul>                    
        </div>
      </div>
    </footer>
  </div>
  <br><br>
  <div class="container text-left fw-500">
    <small>Powered by teranesia © 2020 RI</small>
  </div>
</div>