<div class="container my-4 px-4">
  <div class="row justify-content-center">
    
    <div class="col-sm-12 col-md col-lg-7 bg-transparent order-1 order-sm-1 order-md-2 mb-3">

      <?php
      $data = $confirm->index(decrypt($URL[3]));      
      if(is_array($data) && $data['active']==0){ 
        $confirm->verify(decrypt($URL[3]));
      ?>
                
        <div class="card shadow-sm border rounded-top-6 rounded-bottom-6 text-center p-5">                      
          <div class="success-checkmark text-center">
            <div class="check-icon">
              <span class="icon-line line-tip"></span>
              <span class="icon-line line-long"></span>
              <div class="icon-circle"></div>
              <div class="icon-fix"></div>
            </div>
          </div>
          <div class="card-body">
            <h1 class="card-text" style="font-size:20px !important"><b>Selamat bergabung</b></h1>
            <br>
            <p class="card-text">Terima kasih telah melakukan verifikasi email</p>            
          </div>
        </div>
      
      <?php }else{ header("Location: .."); } ?>

    </div>
  </div>
</div>