<?php
  $page_url       = explode('?',$curr_url)[0];
  $item_per_page  = 10;
  if(isset($_GET["page"])){ //Get page number from $_GET["page"]
      $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
      if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
  }else{
      $page_number = 1; //if there's no page number, set it to 1
  }

  $get_total_rows = $pembelian->count($URL[2]); //hold total records in variable
  $total_pages    = ceil($get_total_rows/$item_per_page); //break records into pages
  $page_position  = (($page_number-1) * $item_per_page); //get starting position to fetch the records
  $allItem        = $pembelian->index($page_position,$item_per_page);

  $customer       = $pembelian->profile();
?>
<div class="container my-4 px-4">
  <div class="row justify-content-center">
    <div class="col-sm-3 col-md-3 bg-transparent d-none d-lg-block">
      <div class="sticks">
        <div class="bg-white border rounded-top-6 rounded-bottom-6 shadow-sm text-center p-3">
            <img class="img-thumbnail m-2 rounded-circle" alt="avatar" style="width:130px;height:130px;object-fit:cover" src="<?=(!empty($customer['foto'])) ? $imgurl.'customer/'.$customer['foto'] : $imgurl."noprofile.png";?>" >
            <div>
              <b><?=$customer['fullname']?></b>
            </div>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-8 bg-transparent">
      <div class="row">
        <div class="col-sm bg-transparent">
          <div class="row">
            <?php 
            if(is_array($allItem)){ 
              foreach($allItem as $data){ 
                $detail = unserialize($data['midtrans_respon']);
                $bank   = getBank($detail);
            ?>
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card shadow-sm border rounded-top-6 rounded-bottom-6 mb-3">
                  <div class="row p-3 middleVertical">
                    <div class="col-sm-2 text-center">
                      <i class="fa fa-shopping-bag fa-2x text-success"></i>
                      <!--<span class="fa-stack fa-lg">
                        <i class="fa fa-circle-o fa-stack-2x text-success"></i>
                        <i class="fa fa-shopping-bag fa-stack-1x fa-inverse text-success"></i>
                      </span>-->
                    </div>
                    <div class="col-sm-10">
                      <h3><b>Belanja</b> | #<?=$data['order_id']?><!--<a target="_blank" href="<?=$base_url?>invoice/<?=$data['order_id']?>" class="link">Invoice</a>--></h3>                      
                      <div class="form-group row my-0">
                        <label for="staticEmail" class="col-sm-6 col-form-label py-0">Total :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext font-weight-bold py-0 text-success" id="staticEmail" value="<?=Rp(num($data['harga']))?>">
                        </div>
                      </div>
                      <div class="form-group row my-0">
                        <label for="staticEmail" class="col-sm-6 col-form-label py-0">Tanggal Pembelian :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext font-weight-bold py-0" id="staticEmail" value="<?=tgl_indo($detail['transaction_time'],false)?>">
                        </div>
                      </div>
                      <div class="alert alert-info open-sans my-0" role="alert">
                        <small><i class="fa fa-bell fa-fw text-warning" aria-hidden="true"></i> Bayar sebelum <?=tgl_indo($data['expired_va'],true,true)?> WIB</small>
                      </div>         
                      <div class="form-group row my-0">
                        <label for="staticEmail" class="col-sm-6 col-form-label py-0">Metode Pembayaran :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext font-weight-bold py-0" id="staticEmail" value="<?=strtoupper($bank[0])?> Virtual Account">
                        </div>
                      </div>
                      <div class="form-group row my-0">
                        <label for="staticEmail" class="col-sm-6 col-form-label py-0">Nomer Virtual Account :</label>
                        <div class="col-sm-6">
                          <input type="text" readonly class="form-control-plaintext font-weight-bold py-0" id="staticEmail" value="<?=$bank[1]?>">
                        </div>
                      </div>                      
                    </div>                   
                  </div>
                </div>
              </div>

            <?php 
              }
            }else{
              require_once ('404_trx.php');
            } ?>
          </div>
        </div>
      </div>  
      <nav aria-label="Page navigation example">    
        <?=paginate($item_per_page, $page_number, $get_total_rows, $total_pages, $page_url);?>
      </nav>
    </div>
  </div>
</div>