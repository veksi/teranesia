<?php
if(isset($_POST['request_link'])){
    $resp = $reset->verify($_POST['email']);
    if($resp[0]==1){ 
        $data = $reset->get_data($_POST['email']);
        require __DIR__.'/email/reset.php';
    }
}
?>

<div>
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title fw-700" id="exampleModalLabel">Lupa Password</h5>
            </div>
            <div class="modal-body px-2">
                <form class="form-signin" method="post" enctype="multipart/form-data" onSubmit="return resetCek();">
                    <input type="hidden" name="request_link">
                    <div class="modal-body">
                        <div class="form-group">               
                            <label class="form-check-label" for="agree">
                                <small>Masukkan e-mail yang terdaftar. Kami akan mengirimkan link verifikasi untuk atur ulang kata sandi.</small>
                            </label>
                        </div>
                        <div class="form-group">               
                            <small id="emailHelp" class="form-text text-muted">Email</small>    
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" maxlength="100" onkeyup="validEmail('email')">
                            <div class="invalid-feedback">Email tidak valid</div>
                        </div>

                        <div class="clear"></div>
                    </div>
                    
                    <div class="modal-footer text-center">                                        
                        <button type="submit" class="btn btn-grad-1 text-white"> Kirim</button>
                    </div>
                
                </form>
            </div>          
        </div>
    </div>
</div>

<script>
function resetCek(){
   
   var filter      = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   var email       = $("#email").val();

   if (email==""){
       $("#email").focus();$("#email").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
   }else if (!filter.test(email)) {
       $("#email").focus();$("#email").addClass('is-invalid');//$(".loadingBtn").html('Save');
       return false;
   }else{
       return true;
   }

}
</script>