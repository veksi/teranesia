<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-white bg-white" id="sidenavAccordion">
        <div class="sb-sidenav-menu border-right">
            <div class="nav pt-3">
                
                <?php foreach($mandatory->menu() as $rootmenu_side){ ?>
                    <?php if($mandatory->have_cm($rootmenu_side['id'])>0){ ?>
                        <div class="accordion border-bottom" id="accordionExample">                    
                            <a class="nav-link px-4" id="heading<?=$rootmenu_side['id']?>" href="javascipt:void(0)" data-toggle="collapse" data-target="#collapse<?=$rootmenu_side['id']?>" aria-controls="collapse<?=$rootmenu_side['id']?>">                        
                                <b><?=$rootmenu_side['title'];?></b>
                                <span class="col text-right"><i class="fa fa-chevron-down"></i></span>
                            </a>
                            <div id="collapse<?=$rootmenu_side['id']?>" class="panel-collapse collapse" aria-labelledby="heading<?=$rootmenu_side['id']?>" data-parent="#accordionExample">
                                <div class="card-body p-0">
                                    <div class="list-group" id="list-tab" role="tablist">
                                        <?php foreach($mandatory->child_sm($rootmenu_side['id']) as $childmenu_side){ ?>           
                                            <li class="list-group-item py-0 border-bottom-0 border-top border-left-0 border-right-0 rounded-0">
                                                <a class="nav-link" href="<?=$base_url.seo($childmenu_side['title'])?>">                                            
                                                    <?=$childmenu_side['title']?>                        
                                                </a>
                                            </li>
                                        <?php } ?>                                
                                    </div>                                    
                                </div>
                            </div>                    
                        </div>                             
                    <?php }else { ?>
                        <a class="nav-link px-4 border-bottom" href="<?=$base_url.seo($rootmenu_side['title'])?>">
                            <!--<div class="sb-nav-link-icon"><i class="fa fa-question-circle"></i></div>-->
                            <b><?=$rootmenu_side['title']?></b>
                        </a>
                    <?php } ?>
                <?php }?>


            </div>
        </div>
       
    </nav>
</div>
