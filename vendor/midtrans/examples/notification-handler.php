<?php

namespace Midtrans;

require_once dirname(__FILE__) . '/../Midtrans.php';
Config::$isProduction = false;
Config::$serverKey = $serverAPI;
$notif = new Notification();

$transaction = $notif->transaction_status;
$type = $notif->payment_type;
$order_id = $notif->order_id;
$fraud = $notif->fraud_status;

if ($transaction == 'capture') {
    // For credit card transaction, we need to check whether transaction is challenge by FDS or not
    if ($type == 'credit_card') {
        if ($fraud == 'challenge') {
            // TODO set payment status in merchant's database to 'Challenge by FDS'
            // TODO merchant should decide whether this transaction is authorized or not in MAP
            echo "Transaction order_id: " . $order_id ." is challenged by FDS";
        } else {
            // TODO set payment status in merchant's database to 'Success'
            echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
        }
    }
} else if ($transaction == 'settlement') {
    
    $get                = $notification->index($order_id);
    $respon             = trx_status($order_id);
    $data               = unserialize($get['transactions']);    
    $pay_date           = $respon['settlement_time'];
    require $base_path.'views/email/success_payment.php';
    $notification->settlement($transaction,$pay_date,$order_id);

} else if ($transaction == 'pending') {    
    
    $get                = $notification->index($order_id);
    $respon             = trx_status($order_id);
    $data               = unserialize($get['transactions']);    
    require $base_path.'views/email/pending_payment.php';
    $notification->pending($transaction,$order_id);

} else if ($transaction == 'deny') {
    $notification->pending($transaction,$order_id);
} else if ($transaction == 'expire') {

    $get                = $notification->index($order_id);
    $respon             = trx_status($order_id);
    $data               = unserialize($get['transactions']);    
    $pay_date           = date('Y-m-d H:i:s');
    require $base_path.'views/email/expired_payment.php';
    $notification->modified($transaction,$pay_date,$order_id);

} else if ($transaction == 'cancel') {

    $get                = $notification->index($order_id);
    $respon             = trx_status($order_id);
    $data               = unserialize($get['transactions']);  
    $pay_date           = date('Y-m-d H:i:s');
    require $base_path.'views/email/cancel_payment.php';
    $notification->modified($transaction,$pay_date,$order_id);

}

?>