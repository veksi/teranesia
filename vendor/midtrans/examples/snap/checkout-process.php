<?php
namespace Midtrans;

require_once dirname(__FILE__) . '/../../Midtrans.php';

//Set Your server key
Config::$serverKey = $serverAPI;

// Uncomment for production environment
// Config::$isProduction = true;

// Enable sanitization
Config::$isSanitized = true;

// Enable 3D-Secure
Config::$is3ds = true;

// Required
$transaction_details = array(
    'order_id' => rand(),
    'gross_amount' => $detail['price'], // no decimal allowed for creditcard
);

// Optional
$item1_details = array( 
    'id' => $detail['id'],
    'price' => $detail['price'],
    'quantity' => 1,
    'name' => $detail['title']
);

// Optional
/*$item2_details = array(
    'id' => 'a2',
    'price' => 20000,
    'quantity' => 2,
    'name' => "Orange"
);*/

// Optional
$item_details = array ($item1_details);

// Optional
$billing_address = array(
    'first_name'    => "Andri",
    'last_name'     => "Litani",
    'address'       => "Mangga 20",
    'city'          => "Jakarta",
    'postal_code'   => "16602",
    'phone'         => "081122334455",
    'country_code'  => 'IDN'
);

// Optional
$shipping_address = array(
    'first_name'    => "Obet",
    'last_name'     => "Supriadi",
    'address'       => "Manggis 90",
    'city'          => "Jakarta",
    'postal_code'   => "16601",
    'phone'         => "08113366345",
    'country_code'  => 'IDN'
);

$shipping_address = array(
    'address'       => $customer['alamat'],
);

// Optional
$customer_details = array(
    'first_name'    => $customer['fullname'],
    //'last_name'     => @explode(" ",$customer['fullname'])[1],
    'email'         => $customer['email'],
    'phone'         => $customer['hp'],
    //'billing_address'  => $billing_address,
    'shipping_address' => $shipping_address
);

// Optional, remove this to display all available payment methods
//$enable_payments = array('credit_card','cimb_clicks','mandiri_clickpay','echannel');

// Fill transaction details
$transaction = array(
    //'enabled_payments' => $enable_payments,
    'transaction_details' => $transaction_details,
    'customer_details' => $customer_details,
    'item_details' => $item_details,
);

$snapToken = Snap::getSnapToken($transaction);
//echo "snapToken = ".$snapToken;

//print_r(serialize($item1_details));
?>

<!--<button id="pay-button">Pay!</button>-->
<div id="result-json"></div>

<!-- TODO: Remove ".sandbox" from script src URL for production environment. Also input your client key in "data-client-key" -->
<script src="https://app<?=$link_MID?>.midtrans.com/snap/snap.js" data-client-key="<?=$clientAPI?>"></script>
<script type="text/javascript">
    document.getElementById('pay-button').onclick = function(){
        // SnapToken acquired from previous step
        snap.pay('<?php echo $snapToken?>', {
            // Optional
            onSuccess: function(result){
                window.location.href = '<?=$base_url?>payment/'+result.order_id;
            },
            // Optional
            onPending: function(result){
                $.ajax({
                    type:'POST',
                    url: '<?=$base_url;?>models/pay.php',
                    data: {
                        csrf:'<?=createToken()?>',
                        t:'<?=encrypt(serialize($transaction))?>',              
                        o:result.order_id,
                    },
                    success: function(res)
                    {
                        window.location.href = '<?=$base_url?>payment/'+result.order_id;                        
                    }       
                });

            },
            // Optional
            onError: function(result){
                window.location.href = '<?=$base_url?>payment/'+result.order_id;
            }
        });
    };
</script>
