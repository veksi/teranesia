<?php
ob_start();
session_name("teranesia");
session_start();

require __DIR__.'/config/global.php';
require __DIR__.'/config/connection.php';
require __DIR__.'/public/function.php';

$notif = '';

?>

<html>
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content=""/>
    <meta name="author" content="" />
    <link rel="shortcut icon" href="<?=$imgurl;?>favicon.png" />
    <title><?php include "views/title.php";?></title>

    <link href="<?=$base_url;?>public/css/custom.css" rel="stylesheet" />
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <!--<link rel="stylesheet" href="<?=$base_url;?>vendor/font-awesome-4.7.0/css/font-awesome.min.css">-->
    <script src="https://kit.fontawesome.com/b034c3c242.js" crossorigin="anonymous"></script>
    <!-- sweet alert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <!-- button loading -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?=$base_url;?>public/js/function.js"></script>
    <!-- google font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">    
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
</head>
<body class="bg-light">
    <?php require "views/header.php"; ?>
    <div class="py-4"></div>

    <?php

    require_once "models/mandatory.php";

    if($mandatory->is_category(@$URL[2])==1){
        if(isset($URL[3])){
            $id = id($URL[3]);
            //require_once "views/breadcrumb.php";
            require_once "models/page.php";
            require_once "views/page.php";                            
        }else{
            require_once "models/listing.php";
            require_once "views/listing.php";                        
        }
    }elseif(@$URL[2]=='partnership'){  
        echo $URL[2];
    }elseif(@$URL[2]=='beli-sekarang' && !empty($_SESSION['customer'])){
        if(!empty($_POST['id'])){
            require_once "models/beli.php";
            require_once "views/beli.php";
            require __DIR__.'/vendor/midtrans/examples/snap/checkout-process.php';
        }else{
            require_once "views/not_found.php";
        }
    }elseif(@$URL[2]=='payment' && !empty($_SESSION['customer'])){
        if(!empty($URL[3])){
            require_once "models/payment.php";
            require_once "views/payment.php";
            require __DIR__.'/vendor/midtrans/examples/snap/checkout-process.php';
        }else{
            require_once "views/not_found.php";
        }
    }elseif(@$URL[2]=='profil' && !empty($_SESSION['customer'])){
        if(@$URL[3]=='edit'){
            require_once "models/profil.php";
            require_once "views/profil_edit.php";                 
        }else{
            require_once "models/profil.php";
            require_once "views/profil.php";                        
        }
    }elseif(@$URL[2]=='pembelian' && !empty($_SESSION['customer'])){
        require_once "models/pembelian.php";
        require_once "views/pembelian.php";
    }else{
        require_once "models/home.php";
        require_once "views/home.php";        
    }

    /*if(isset($URL[2])){
        if(isset($URL[3])){
            $id = id($URL[3]);
            //require_once "views/breadcrumb.php";
            require_once "models/page.php";
            require_once "views/page.php";                        
        }else{
            //require_once "views/breadcrumb.php";            
            if($URL[2]=='beli-sekarang' && !empty($_SESSION['customer'])){                                                
                require_once "models/beli.php";
                require_once "views/beli.php";             
                require __DIR__.'/vendor/midtrans/examples/snap/checkout-process.php';
            }elseif($URL[2]=='profil' && !empty($_SESSION['customer'])){
                if(@$URL[3]=='edit'){
                    require_once "models/profil.php";
                    require_once "views/profil_edit.php";                        
                    echo "dsa";
                }else{
                    require_once "models/profil.php";
                    require_once "views/profil.php";                        
                }
                
            }elseif($URL[2]=='transaksi' && !empty($_SESSION['customer'])){
                require_once "models/transaksi.php";
                require_once "views/transaksi.php";                        
            }else{
                require_once "models/listing.php";
                require_once "views/listing.php";                        
            }
            
        }
    }else{
        require_once "models/home.php";
        require_once "views/home.php";        
    }*/
        
    require_once "views/footer.php";

    if(empty($_SESSION['customer'])){
        require_once "models/login.php"; 
        require_once "views/login.php"; 
    }
    ?>
</body>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?=$base_url?>public/js/scripts.js"></script>
<script>
$(".loadingBtn").click(function() {    
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
});

var msg = '<?=$notif?>';
if(msg!=''){swal("Gagal",msg,"warning");}
</script>


<?php

function testing($id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.sandbox.midtrans.com/v2/".$id."/status",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ".base64_encode('SB-Mid-server-MrD7pgZEik9Z909eE5mVqF0e')
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
      //echo "cURL Error #:" . $err;
    } else {
      //echo $response;
    }
  
    return json_decode($response, true);
  
  }

  //print_r(testing('341990863'));

?>