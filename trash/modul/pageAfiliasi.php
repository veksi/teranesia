<div id="myCarousel" class="carousel slide mb-0 bg-main" data-ride="carousel">

  <div class="carousel-inner" role="listbox">
    
    <div class="carousel-item active">
      <div class="container">
        <div class="row align-items-center" >
          <div class="col-sm">
            <div class="container text-center">
              <img class="img-fluid" alt="Program Afiliasi" src="<?=$imgserver?>money.png" >
            </div>
          </div>
          <div class="col-sm">
            <div class="text-center text-white">
              <h1>Program Afiliasi</h1>
              <p class="px-4 text-white">Dapatkan <span class="badge badge-pill badge-danger text-white font-weight-normal" style="font-size:20px">Komisi 50%</span> langsung untuk Anda dari Program Afiliasi Planet Hosting</p>
              <p><a href="<?=$clientPath?>" class="btn btn-custom text-light shadow" role="button" aria-pressed="true"><b>GABUNG SEKARANG</b></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> 
</div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 190">
  <defs>
    <linearGradient id="lgrad" x1="0%" y1="50%" x2="100%" y2="50%" >
      <stop offset="0%" style="stop-color:rgba(83, 1, 117, 1);stop-opacity:1" />
      <stop offset="100%" style="stop-color:rgba(148, 0, 211, 1);stop-opacity:1" />
    </linearGradient>
  </defs>
  <path d="M0,64L80,53.3C160,43,320,21,480,53.3C640,85,800,171,960,181.3C1120,192,1280,128,1360,96L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z" fill="url(#lgrad)" fill-opacity="1"/>
</svg>

<div class="container">
  <div class="text-center">
    <h2 class="display-5 text-purple mt-0 pt-0"><b>Dapatkan Penghasilan dari Planet Hosting</b></h2>
    <p class="mb-5">Program afiliasi planet hosting yang terpercaya dan pasti untung. Dapatkan komisi jutaan rupiah setiap bulan. Mulai bisnis afiliasi Anda sekarang, GRATIS!</p>
  </div>
  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Keuntungan yang Anda Dapatkan</b></h2></div>
  <div class="d-flex flex-wrap justify-content-center mb-5">
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <p class="card-text">Bisnis afiliasi dengan komisi besar tanpa batas</p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-money fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <p class="card-text">Pembayaran ditransfer ke rekening Bank Anda</p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-clock-o fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <p class="card-text">Komisi langsung cair saat transaksi, ga pake lama</p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-users fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <p class="card-text">Ajak partner anda sebanyak mungkin</p>
      </div>
    </div>
  </div>

  <div class="container marketing mb-5">
    <div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Cara Kerja</b></h2></div>
    <div class="container text-center">    
      <div class="row justify-content-md-center">
        <div class="col-md-2 p-0">
          <div class="card border-0 align-top">
            <div class="card-body text-center">
              <p class="card-text"><i class="fas fa-id-badge fa-3x text-info"></i></p>
            </div>
            <div class="card-footer text-center bg-transparent">
              <p class="card-text">Bergabung dengan program afiliasi planet hosting</p>
            </div>
          </div>
        </div>
        <div class="col-md-1 p-0">  
          <div class="jumbotron h-100 bg-transparent">
            <div class="d-none d-md-block text-muted"><i class="fa fa-angle-right fa-2x" aria-hidden="true"></i></div>
            <div class="d-block d-md-none text-muted"><i class="fa fa-angle-down fa-2x" aria-hidden="true"></i></div>
          </div>
        </div>
        <div class="col-md-2 p-0">   
          <div class="card border-0">
            <div class="card-body text-center">
              <p class="card-text"><i class="fas fa-share-alt fa-3x text-info"></i></p>
            </div>
            <div class="card-footer text-center bg-transparent">
              <p class="card-text">Bagikan link referral ke setiap orang</p>
            </div>
          </div>  
        </div>
        <div class="col-md-1 p-0">  
          <div class="jumbotron h-100 bg-transparent">
            <div class="d-none d-md-block text-muted"><i class="fa fa-angle-right fa-2x" aria-hidden="true"></i></div>
            <div class="d-block d-md-none text-muted"><i class="fa fa-angle-down fa-2x" aria-hidden="true"></i></div>
          </div>
        </div>
        <div class="col-md-2 p-0">         
          <div class="card border-0">
            <div class="card-body text-center">
              <p class="card-text"><i class="fas fa-users fa-3x text-info"></i></p>
            </div>
            <div class="card-footer text-center bg-transparent">
              <p class="card-text">Pelanggan baru mendaftar melalui link referral anda dan melakukan transaksi</p>
            </div>
          </div>   
        </div>
        <div class="col-md-1 p-0">  
          <div class="jumbotron h-100 bg-transparent">
            <div class="d-none d-md-block text-muted"><i class="fa fa-angle-right fa-2x" aria-hidden="true"></i></div>
            <div class="d-block d-md-none text-muted"><i class="fa fa-angle-down fa-2x" aria-hidden="true"></i></div>
          </div>
        </div>
        <div class="col-md-2 p-0">      
          <div class="card border-0">
            <div class="card-body text-center">
              <p class="card-text"><i class="fa fa-money fa-3x text-info"></i></p>
            </div>
            <div class="card-footer text-center bg-transparent">
              <p class="card-text"><span class="badge badge-pill badge-primary" style="font-size:20px">Komisi 50%</span> langsung jadi milik anda detik itu juga, ga pake lama</p>
            </div>
          </div>         
        </div>
      </div>
    </div>
  </div>
</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Ketentuan Program Afiliasi</b></h2></div>

<div class="jumbotron rounded-0 border-0 bg-transparent mt-0 pt-0">
  <div class="row justify-content-md-center">
    <div class="col-sm-12 col-md-8">
      
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)" id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Apa keuntungan yang bisa saya dapatkan?</small></h3></div>
                <div class="col-1 text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseOne" class="panel-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Anda dapat memperoleh penghasilan yang tidak terbatas. Cukup dengan mereferensikan penjualan ke Planet Hosting. Semudah chatting dengan teman-teman Anda. </p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Produk apa yang bisa direferensikan?</small></h3></div>
                <div class="col-1 text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseThree" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Saat ini planet hosting hanya menyediakan program afiliasi pada produk berupa hosting.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Kapan saya dibayar?</small></h3></div>
                <div class="col-1 text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseTwo" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Pencairan otomatis akan terkirim ke rekening anda ketika transaksi dari pelanggan yang anda referensikan terjadi. Transaksi artinya pelanggan mendaftar, melakukan order paket hosting di planet hosting dan melakukan pelunasan. Maksimum lama pencarian 1 x 24 Jam pada Hari Kerja (Senin - Jumat)</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseFour" aria-controls="collapseFour">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Bagaimana cara memulainya?</small></h3></div>
                <div class="col-1 text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseFour" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Setelah terdaftar sebagai member planet hosting anda wajib like dan follow sosial media kami (Instagram, Facebook, Youtube,Twitter, Linkedin) Atau yang tertera saat ini.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="jumbotron text-white mb-0" style="background: #9400D3;border-radius:unset;">
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col p-4">
        <h3>Dapatkan penghasilan tanpa batas di Planet Hosting!</h3>
      </div>
      <div class="col p-4">
        <a href="<?=$clientPath?>" class="btn btn-custom btn-lg text-light shadow" role="button" aria-pressed="true"><b>GABUNG SEKARANG</b></a>
      </div>
    </div>
  </div>
</div>