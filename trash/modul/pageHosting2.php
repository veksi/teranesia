<div id="myCarousel" class="carousel slide mb-0" data-ride="carousel" style="background: #9400D3;">
  <div class="carousel-inner" role="listbox" >
    
    <div class="carousel-item active" style="height:400px">
      <div class="container">
        <div class="carousel-caption">
          <div class="row middleVertical">
            <div class="col-sm">
              <div class="container">
                <img class="img-circle center-block d-none d-md-inline" alt="avatar" src="<?=$imgserver?>server.png" style="width:100%">
              </div>
            </div>
            <div class="col-sm">
              <h1>Unlimited Web Hosting</h1>
              <p><span class="text-white" >Dapatkan penawaran menarik dengan menggunakan domain & hosting di planet hosting</span></p>
              <p><a href="<?=$clientPath?>" class="btn btn-warning btn-lg text-light" role="button" aria-pressed="true">Pindah Hosting</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<svg style="width:100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 190"><path fill="#9400D3" fill-opacity="1" d="M0,64L80,53.3C160,43,320,21,480,53.3C640,85,800,171,960,181.3C1120,192,1280,128,1360,96L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z"></path></svg>
<!--
<svg style="margin-top:-150;width:100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#9400D3" fill-opacity="1" d="M0,288L60,261.3C120,235,240,181,360,181.3C480,181,600,235,720,240C840,245,960,203,1080,160C1200,117,1320,75,1380,53.3L1440,32L1440,0L1380,0C1320,0,1200,0,1080,0C960,0,840,0,720,0C600,0,480,0,360,0C240,0,120,0,60,0L0,0Z"></path></svg>
-->
<div class="container">

  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Paket Koloni</b></h2></div>

  <div class="card-deck mb-5 text-center">
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Merkurius</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem"><small>IDR</small> 30K</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Mars</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem"><small>IDR</small> 50K</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Bumi</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem"><small>IDR</small> 90K</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
  </div>

  <div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Paket Enterprise</b></h2></div>

  <div class="card-deck mb-5 text-center">
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Uranus</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem">10 Gb</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Neptunus</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem">15 Gb</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
    <div class="card mb-4 shadow mx-3 rounded border-0">
      <div class="card-body">
        <h5 class="card-title">Saturnus</h5>
        <p class="card-text font-weight-bold text-purple" style="font-size:3rem">25 Gb</p>
        <button type="button" class="btn btn-lg btn-block btn-outline-primary">BELI</button>
      </div>
    </div>
  </div>

  <div class="container marketing">
    <div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Fitur Unggulan</b></h2></div>
    <div class="container text-center">    
      <div class="row">
        <div class="col-md-4">            
          <i class="fas fa-tachometer-alt fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h3>Unlimited Bandwidth</h3>
          <p>Tidak ada lagi trafik pengunjung yang hilang karena over limit bandwidth, disini kami sudah tidak lagi membatasi penggunaan banwidth.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hdd fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h3>Unlimited Disk Space</h3>
          <p>Masih cemas dengan kapasitas storage? kami tidak membatasi berapa ukuran file yang akan anda upload.</p>            
        </div>
        <div class="col-md-4">            
          <i class="fab fa-cpanel fa-5x text-info"></i>          
          <h3>cPanel</h3>
          <p>Hemat waktu dalam konfigurasi dengan menggunakan cPanel yang sangat powerfull dan user friendly.</p>            
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4">            
          <i class="fab fa-cloudversify fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h3>CloudLinux</h3>
          <p>Akses monitoring penuh atas CPU, Memory, dan batasan koneksi secara simultan.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hourglass fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h3>99,8% Uptime</h3>
          <p>Bisnis anda dapat terus bertumbuh karena server UP terus sepanjang tahun, tanpa jeda, tanpa lambat.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hand-pointer fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h3>Website Builder</h3>
          <p>Cukup 1 kali klik, bangun website anda dan dapatkan customer dari seluruh dunia.</p>            
        </div>
      </div>
    </div>
  </div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 290"><path fill="#9400D3" fill-opacity="1" d="M0,288L48,277.3C96,267,192,245,288,202.7C384,160,480,96,576,106.7C672,117,768,203,864,208C960,213,1056,139,1152,101.3C1248,64,1344,64,1392,64L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>

<div class="jumbotron text-white mb-5" style="background: #9400D3;border-radius:unset;">
    <div class="text-center">
        <h3><span class="badge badge-secondary p-4 bg-warning"><i class="fas fa-mouse-pointer"></i></span> SOFTACULOUS </h3>
        <h5> Buat Website Dengan Sekali Klik </h5>
    </div>
    
    <div class="developer mb-5 mt-5">
        <div style="width:200px" class="text-center"><i class="fab fa-wordpress fa-3x"></i><br>Wordpress</div>
        <div style="width:200px" class="text-center"><i class="fab fa-joomla fa-3x"></i><br>Joomla</div>
        <div style="width:200px" class="text-center"><i class="fab fa-magento fa-3x"></i><br>Magento</div>
        <div style="width:200px" class="text-center"><i class="fab fa-opencart fa-3x"></i><br>OpenCart</div>
        <div style="width:200px" class="text-center"><i class="fab fa-drupal fa-3x"></i><br>Drupal</div>
    </div>
</div>


<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Kata Pelanggan</b></h2></div>


<div class="container marketing">
  <div class="testimoni mb-5 mt-5">
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/pixelinia.png" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Pixelinia.com</b>
              <br>
              IT Konsultan
              <br>
              Indonesia
              <br><br>
              Pilihan tepat memilih hosting di planethosting.co.id, fitur yang diberikan beragam, teknologi terkini tinggal setup. Plus, spesifikasi servernya oke & real seperti yang tertera.
          </div>
      </div>
    </div>
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/c.jpg" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Roy / c-dynamicsinc.com</b>
              <br>
              Marketing Communication
              <br>
              Singapore
              <br><br>
              Our collaboration works because of their ability to understand and translate what we require into tangible results. They are good listeners as much as they are receptive to new ideas and willing to explore and push the boundaries or innovations with us. Another important factor is the speed and efficiency of execution from start to completion.
          </div>
      </div>
    </div>
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/sh.jpg" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Fahmi / starhome.co.id</b>
              <br>
              IT Manager 
              <br>
              Indonesia
              <br><br>
              Kami butuh vendor yang profesional, bertanggung jawab dan fast respon. Pilihan kami jatuh pada planethosting.co.id. Penawaran fitur yang beragam dan pastinya benar-benar bisa diandalkan.
          </div>
      </div>
    </div>
  </div>
</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Dukungan Teknologi Terkini</b></h2></div>

<div class="developer mb-5 mt-5">
    <div style="width:200px" class="text-center"><i class="fab fa-python fa-3x"></i><br>Phyton</div>
    <div style="width:200px" class="text-center"><i class="fab fa-php fa-3x"></i><br>PHP</div>
    <div style="width:200px" class="text-center"><i class="fab fa-node-js fa-3x"></i><br>Node.js</div>
    <div style="width:200px" class="text-center"><i class="fab fa-laravel fa-3x"></i><br>Laravel</div>
</div>

<script>
$(document).ready(function(){
    $('.galeri').slick({
        slidesToShow: 5,
        dots:false,
        centerMode: false,
        autoplay: true,
        variableWidth: true,
        arrows: false,
        responsive: [
        {
        breakpoint: 768,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
        }
        },
        {
        breakpoint: 480,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1,
            variableWidth: false,
        }
        }
    ]
    });
    $('.testimoni').slick({
        slidesToShow: 1,
        dots:false,
        autoplay: true,
        variableWidth: true,
        arrows: false,
        centerMode: true,
        responsive: [
        {
        breakpoint: 768,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            variableWidth: false,
        }
        },
        {
        breakpoint: 480,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            variableWidth: false,
        }
        }
    ]
    });
    $('.developer').slick({
        slidesToShow: 2,
        dots:false,
        centerMode: true,
        autoplay: true,
        variableWidth: true,
        arrows: false,
        centerPadding: '0px',
        autoplaySpeed: 2000,
        responsive: [
        {
        breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 2
            }
        },
        {
        breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
            }
        }
    ]
    });
    $('.manajer').slick({
        centerMode: true,
        autoplay: true,
        arrows: true,
        variableWidth: false,
        slidesToShow: 3,
        centerPadding: '0px',
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                centerPadding: '0px',
                slidesToShow: 3,
            }
            },
            {
            breakpoint: 768,
            settings: {
                centerPadding: '0px',
                slidesToShow: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                centerPadding: '0px',
                slidesToShow: 1
            }
            }
        ]
    });
});
</script>

<style>
  .galeri {
      width: 80%;
      margin: 0 auto;
  }

  .manajer {
      width: 80%;
      margin: 0 auto;
  }

  .testimoni {
      width: 100%;
      margin: 0 auto;      
  }

  .developer {
      width: 80%;
      margin: 0 auto;
  }

  .slick-slide {
      margin: 0 20;
      height:auto
  }

  .slick-slide img {
    width: 100%;
  }

  .slick-prev:before,
  .slick-next:before {
      color:rgba(0, 0, 0, 0.5)
  }

  .slick-slide {
    transition: all ease-in-out .3s;
    outline:none
  }

  .border {
      cursor: pointer;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      -webkit-box-sizing: border-box;
      border-radius: 10px;
      border: 2px solid #dcdcdc;
      outline: 0;
  }
</style>