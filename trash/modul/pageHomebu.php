<div id="myCarousel" class="carousel slide mb-0" data-ride="carousel" style="background: #9400D3;"  >
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1" class=""></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    
    <div class="carousel-item active">
      <div class="container">
        <div class="row align-items-center" >
          <div class="col-sm d-none d-md-inline">
            <div class="container text-center">
              <img class="center-block" alt="avatar" src="<?=$imgserver?>server.png" style="height:100%;">
            </div>
          </div>
          <div class="col-sm">
            <div class="container p-4 text-center text-white">
              <h1>Web Hosting Indonesia</h1>
              <p class="px-4 text-white">Dapatkan penawaran menarik dengan menggunakan domain & hosting di planet hosting</p>
              <p><a href="<?=$clientPath?>" class="btn btn-warning btn-lg text-light" role="button" aria-pressed="true"><b>PINDAH HOSTING</b></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="carousel-item">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-sm d-none d-md-inline">
            <div class="container text-center">
              <img class="center-block" alt="avatar" src="<?=$imgserver?>money.png" style="height:100%">
            </div>
          </div>
          <div class="col-sm">
            <div class="container p-4 text-center text-white">
              <h1>Unlimited Cash</h1>
              <p class="px-4 text-white">Ajak pelanggan sebanyak mungkin untuk menggunakan layanan di planet hosting</p>
              <p><a href="#" class="btn btn-warning btn-lg text-light" role="button" aria-pressed="true"><b>GABUNG SEKARANG</b></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>

  </div> 
</div>

<svg style="width:100%;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 190"><path fill="#9400D3" fill-opacity="1" d="M0,64L80,53.3C160,43,320,21,480,53.3C640,85,800,171,960,181.3C1120,192,1280,128,1360,96L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z"></path></svg>
<!--
<svg style="margin-top:-150;width:100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#9400D3" fill-opacity="1" d="M0,288L60,261.3C120,235,240,181,360,181.3C480,181,600,235,720,240C840,245,960,203,1080,160C1200,117,1320,75,1380,53.3L1440,32L1440,0L1380,0C1320,0,1200,0,1080,0C960,0,840,0,720,0C600,0,480,0,360,0C240,0,120,0,60,0L0,0Z"></path></svg>
-->
<div class="container">

  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Layanan Planet Hosting</b></h2></div>
  <div class="card-deck mb-5">
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-server fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <h5 class="card-title"><b>Unlimited Hosting</b></h5>
        <p class="card-text">Direkomendasikan untuk website skala kecil dan menengah</p>
        <p class="mb-0"><small class="text-muted">Mulai dari</small>
        <p class="card-text font-weight-bold mb-0 text-purple" style="font-size:1.5rem"><?=kurs('20000',false,false)?></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-globe fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <h5 class="card-title"><b>Domain</b></h5>
        <p class="card-text">Amankan domain anda sekarang juga, sebelum terlambat</p>
        <p class="mb-0"><small class="text-muted">Mulai dari</small>
        <p class="card-text font-weight-bold mb-0 text-purple" style="font-size:1.5rem"><?=kurs(minValue('domain_bucket','register'),false,true)?></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-money-check fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <h5 class="card-title"><b>Pembuatan Website</b></h5>
        <p class="card-text">Percayakan pembuatan website anda pada planet hosting</p>
        <p class="mb-0"><small class="text-muted">Mulai dari</small>
        <p class="card-text font-weight-bold mb-0 text-purple" style="font-size:1.5rem"><?=kurs('490000',false,false)?></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0">
      <div class="card-body text-center">
        <p>
          <span class="fa-stack fa-2x text-purple">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-bullhorn fa-stack-1x fa-inverse"></i>
          </span>
        </p>
        <h5 class="card-title"><b>Iklan Sosmed</b></h5>
        <p class="card-text">Jangkau lebih banyak audience dengan beriklan di sosial media</p>
        <p class="mb-0"><small class="text-muted">Mulai dari</small>
        <p class="card-text font-weight-bold mb-0 text-purple" style="font-size:1.5rem"><?=kurs('190000',false,false)?></p>
      </div>
    </div>
  </div>

  <div class="container marketing">
    <div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Fitur Unggulan</b></h2></div>
    <div class="container text-center">    
      <div class="row">
        <div class="col-md-4">            
          <i class="fas fa-tachometer-alt fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h4><b>Unlimited Bandwidth</b></h4>
          <p>Tidak ada lagi trafik pengunjung yang hilang karena over limit bandwidth, disini kami sudah tidak lagi membatasi penggunaan banwidth.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hdd fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h4><b>Unlimited Disk Space</b></h4>
          <p>Masih cemas dengan kapasitas storage? kami tidak membatasi berapa ukuran file yang akan anda upload.</p>            
        </div>
        <div class="col-md-4">            
          <i class="fab fa-cpanel fa-5x text-info"></i>          
          <h4><b>cPanel</b></h4>
          <p>Hemat waktu dalam konfigurasi dengan menggunakan cPanel yang sangat powerfull dan user friendly.</p>            
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-4">            
          <i class="fab fa-cloudversify fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h4><b>CloudLinux</b></h4>
          <p>Akses monitoring penuh atas CPU, Memory, dan batasan koneksi secara simultan.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hourglass fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h4><b>99,8% Uptime</b></h4>
          <p>Bisnis anda dapat terus bertumbuh karena server UP terus sepanjang tahun, tanpa jeda, tanpa lambat.</p>            
        </div>
        <div class="col-md-4">            
          <i class="far fa-hand-pointer fa-3x text-info"></i>
          <div class="clearfix"></div>
          <br>
          <h4><b>Website Builder</b></h4>
          <p>Cukup 1 kali klik, bangun website anda dan dapatkan customer dari seluruh dunia.</p>            
        </div>
      </div>
    </div>
  </div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 290"><path fill="#9400D3" fill-opacity="1" d="M0,288L48,277.3C96,267,192,245,288,202.7C384,160,480,96,576,106.7C672,117,768,203,864,208C960,213,1056,139,1152,101.3C1248,64,1344,64,1392,64L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>

<div class="jumbotron text-white mb-5" style="background: #9400D3;border-radius:unset;">
    <div class="text-center">
        <h2><span class="badge badge-secondary p-4 bg-warning"><i class="fas fa-mouse-pointer"></i></span> <b>SOFTACULOUS</b> </h2>
        <h3> Buat Website Dengan Sekali Klik </h3>
    </div>
    
    <div class="developer mb-5 mt-5">
        <div style="width:200px" class="text-center"><i class="fab fa-wordpress fa-3x"></i><br>Wordpress</div>
        <div style="width:200px" class="text-center"><i class="fab fa-joomla fa-3x"></i><br>Joomla</div>
        <div style="width:200px" class="text-center"><i class="fab fa-magento fa-3x"></i><br>Magento</div>
        <div style="width:200px" class="text-center"><i class="fab fa-opencart fa-3x"></i><br>OpenCart</div>
        <div style="width:200px" class="text-center"><i class="fab fa-drupal fa-3x"></i><br>Drupal</div>
    </div>
</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Kata Pelanggan</b></h2></div>

<div class="container marketing">
  <div class="testimoni mb-5 mt-5">
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/pixelinia.png" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Pixelinia.com</b>
              <br>
              IT Konsultan
              <br>
              Indonesia
              <br><br>
              Pilihan tepat memilih hosting di planethosting.co.id, fitur yang diberikan beragam, teknologi terkini tinggal setup. Plus, spesifikasi servernya oke & real seperti yang tertera.
          </div>
      </div>
    </div>
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/c.jpg" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Roy / c-dynamicsinc.com</b>
              <br>
              Marketing Communication
              <br>
              Singapore
              <br><br>
              Our collaboration works because of their ability to understand and translate what we require into tangible results. They are good listeners as much as they are receptive to new ideas and willing to explore and push the boundaries or innovations with us. Another important factor is the speed and efficiency of execution from start to completion.
          </div>
      </div>
    </div>
    <div style="width:390px;">
      <div class="col-sm-12" style="padding:30 20;" >
          <div class="col-sm-4 text-center">
              <img class="img-circle img-thumbnail center-block" alt="avatar" src="<?=$imgserver?>expert/sh.jpg" style="width:90px">
          </div>
          <div class="col-sm-12">
              <b>Fahmi / starhome.co.id</b>
              <br>
              IT Manager 
              <br>
              Indonesia
              <br><br>
              Kami butuh vendor yang profesional, bertanggung jawab dan fast respon. Pilihan kami jatuh pada planethosting.co.id. Penawaran fitur yang beragam dan pastinya benar-benar bisa diandalkan.
          </div>
      </div>
    </div>
  </div>
</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Dukungan Teknologi Terkini</b></h2></div>

<div class="developer mb-5 mt-5">
    <div style="width:200px" class="text-center text-info"><i class="fab fa-python fa-3x"></i><br>Phyton</div>
    <div style="width:200px" class="text-center text-info"><i class="fab fa-php fa-3x"></i><br>PHP</div>
    <div style="width:200px" class="text-center text-info"><i class="fab fa-node-js fa-3x"></i><br>Node.js</div>
    <div style="width:200px" class="text-center text-info"><i class="fab fa-laravel fa-3x"></i><br>Laravel</div>
    <div style="width:200px" class="text-center text-info"><i class="fa fa-diamond fa-3x"></i><br>Ruby</div>
</div>

<div class="jumbotron text-white mb-0" style="background: #9400D3;border-radius:unset;">
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col p-4">
        <h3>Kesuksesan anda dimulai dari Planet Hosting!</h3>
      </div>
      <div class="col p-4">
        <a href="<?=$clientPath?>" class="btn btn-warning btn-lg text-light" role="button" aria-pressed="true"><b>MULAI SEKARANG</b></a>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function(){
  $('.carousel').on('slide.bs.carousel', function (event) {
    var height = $(event.relatedTarget).height();
    var $innerCarousel = $(event.target).find('.carousel-inner');
    
    $innerCarousel.animate({
      height: height
    });
  });

  $('.galeri').slick({
      slidesToShow: 5,
      dots:false,
      centerMode: false,
      autoplay: true,
      variableWidth: true,
      arrows: false,
      responsive: [
      {
      breakpoint: 768,
      settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
      }
      },
      {
      breakpoint: 480,
      settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1,
          variableWidth: false,
      }
      }
  ]
  });
  $('.testimoni').slick({
      slidesToShow: 1,
      dots:false,
      autoplay: true,
      variableWidth: true,
      arrows: false,
      centerMode: true,
      responsive: [
      {
      breakpoint: 768,
      settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 1,
          variableWidth: false,
      }
      },
      {
      breakpoint: 480,
      settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '0px',
          slidesToShow: 1,
          variableWidth: false,
      }
      }
  ]
  });
  $('.developer').slick({
      slidesToShow: 2,
      dots:false,
      centerMode: true,
      autoplay: false,
      variableWidth: true,
      arrows: false,
      centerPadding: '0px',
      autoplaySpeed: 2000,
      responsive: [
      {
      breakpoint: 768,
          settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '0px',
              slidesToShow: 2
          }
      },
      {
      breakpoint: 480,
          settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '0px',
              slidesToShow: 2
          }
      }
  ]
  });
  $('.manajer').slick({
      centerMode: true,
      autoplay: true,
      arrows: true,
      variableWidth: false,
      slidesToShow: 3,
      centerPadding: '0px',
      responsive: [
          {
          breakpoint: 1024,
          settings: {
              centerPadding: '0px',
              slidesToShow: 3,
          }
          },
          {
          breakpoint: 768,
          settings: {
              centerPadding: '0px',
              slidesToShow: 2
          }
          },
          {
          breakpoint: 480,
          settings: {
              centerPadding: '0px',
              slidesToShow: 1
          }
          }
      ]
  });
});
</script>

<style>
  .galeri {
      width: 80%;
      margin: 0 auto;
  }

  .manajer {
      width: 80%;
      margin: 0 auto;
  }

  .testimoni {
      width: 100%;
      margin: 0 auto;      
  }

  .developer {
      width: 80%;
      margin: 0 auto;
  }

  .slick-slide {
      margin: 0 20;
      height:auto
  }

  .slick-slide img {
    width: 100%;
  }

  .slick-prev:before,
  .slick-next:before {
      color:rgba(0, 0, 0, 0.5)
  }

  .slick-slide {
    transition: all ease-in-out .3s;
    outline:none
  }

  .border {
      cursor: pointer;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      -webkit-box-sizing: border-box;
      border-radius: 10px;
      border: 2px solid #dcdcdc;
      outline: 0;
  }
</style>