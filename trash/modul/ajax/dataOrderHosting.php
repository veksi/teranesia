<?php
session_name("Planet Hosting");
session_start();

include "../../config/global.php";
include "../../config/connection.php";   
include "../../library/function.php";
include "../../library/query.php";

if(isset($_GET['paket'])){
    
    $id = $_GET['id'];
    echo json_encode(dbGetRow('product','id ='.$id,'',''));

}else if(isset($_GET['domainCheck'])){
    $domain = $_POST['search_domain'].$_POST['search_extension'];
    $api    = http_request('https://planethosting.co.id/library/liquid-php-master/domain.php?domain='.$domain);
    $respon = (json_decode($api, true));

    if($respon[0][$domain]['status']=='available'){
        $price = kurs(dbGetRow('domain_bucket'," extension = '".$_POST['search_extension']."'",'','')['register'],false,true);
        echo 
            '<div class="row align-items-center">
                <div class="col">
                    '.$domain.' <i class="fa fa-check text-warning"></i>
                </div>
                <div class="col text-center">
                    <span class="text-primary font-weight-bold"> '.Rp(num($price)).' ( 1 tahun )</span>
                </div>
                <div class="col text-right">
                    <button type="button" class="btn btn-sm btn-custom text-white" onclick="setDomain('.$price.')">Gunakan Domain</button>
                </div>
            </div>
            ';
    }else{
        echo '<p class="clearfix text-dark">
                <span class="float-left font-weight-bold"><del><b>'.$domain.'</b></del></span>
                <span class="float-right"><i class="fa fa-times text-danger"></i> Mohon maaf, domain tidak tersedia!</span>
            </p>';
    }

}

?>
