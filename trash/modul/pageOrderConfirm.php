<body style="background-color:#F0F8FF !important"> 
  <div class="container">
    <div class="row justify-content-md-center my-5 ">

      <div class="col-sm-8">
        
        <div class="bg-white p-3 shadow-sm rounded">
          <div class="form-group mx-2">
            <div class="container text-center p-4">
              <h5>Terimakasih! Kami akan aktifkan pesanan Anda segera setelah Anda melakukan <b>konfirmasi pembayaran!</b></h5>
            </div>
            <div class="container text-center p-4">
              <a class="btn btn-custom my-2 my-lg-0 text-white shadow rounded-pill" href="<?=$clientPath?>register"><b>Konfirmasi Pembayaran di Member Area</b></a>
            </div>

            <hr>

Invoice ID

Kode Pembayaran

Nominal Tagihan

NH1319512

113661319512

Rp 300.475,00

Payment Gateway
Langkah - Langkah Pembayaran Melalui ATM :
Masukkan Kartu ATM BCA & PIN
Pilih menu Transaksi Lainnya > Transfer > ke Rekening BCA Virtual Account
Masukkan kode pembayaran atau nomor BCA Virtual Account Anda
Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan
Ikuti instruksi untuk menyelesaikan transaksi
Transaksi selesai
Langkah - Langkah Pembayaran Melalui m-Banking :
Lakukan log in pada aplikasi BCA Mobile
Pilih menu m-BCA, kemudian masukkan kode akses m-BCA
Pilih m-Transfer > BCA Virtual Account
Masukkan kode pembayaran atau nomor BCA Virtual Account Anda
Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan
Masukkan pin m-BCA
Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran
Langkah - Langkah Pembayaran Melalui IBanking
Login pada alamat Internet Banking BCA (https://klikbca.com)
Pilih menu Pembayaran Tagihan > Pembayaran > BCA Virtual Account
Masukkan kode pembayaran atau nomor BCA Virtual Account Anda
Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti Nomor BCA Virtual Account, Nama Pelanggan dan Jumlah Pembayaran.
Masukkan password dan mToken.
Transaksi selesai

          </div>
        </div>

    </div>
    </div>
  </div>

</body>
