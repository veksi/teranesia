<form method="post" action="<?=$currentUrl;?>/verifikasi">
    <div class="form-group">
        <label for="exampleInputEmail1">No Tujuan</label>
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">+62</div>
            </div>
            <input type="text" name="phone" class="form-control" id="phone" placeholder="" onkeypress="return hanyaAngka(event)" autocomplete="off">
        </div>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Pesan</label>
        <textarea class="form-control" name="msg" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Generate</button>
</form>

<script>
function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}

var dengan_rupiah = document.getElementById('phone');
dengan_rupiah.addEventListener('keyup', function(e)
{
    dengan_rupiah.value = formatRupiah(this.value, '');
});

dengan_rupiah.addEventListener('keydown', function(event)
{
    limitCharacter(event);
});

function formatRupiah(bilangan, prefix)
{
    var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 4,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,4}/gi);
    if (ribuan) {
        separator = sisa ? '-' : '';
        rupiah += separator + ribuan.join('-');
    }
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
}

function limitCharacter(event)
{
    key = event.which || event.keyCode;
    if ( key != 188 // Comma
            && key != 8 // Backspace
            && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
            && (key < 48 || key > 57) // Non digit
            // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
        ) 
    {
        event.preventDefault();
        return false;
    }
}
</script>