<nav class="navbar navbar-expand-lg navbar-light bg-white mb-0 px-4 ">
  <div class="container">
    <a class="navbar-brand text-light" href="<?=$domain?>">
      <img class="d-none d-md-block" src="<?=$imgserver?>logo_header.png" >
      <img class="d-block d-md-none" src="<?=$imgserver?>logo.png" > 
    </a>
    <ul class="navbar-nav ml-auto">
      <?php if(empty($_SESSION)){ ?>
        <li class="nav-item pl-2 pr-2">
          <a class="btn btn-outline-dark my-2 my-lg-0" href="<?=$clientPath?>"><b>LOGIN</b></a>
        </li>
      <?php }else{ ?>
        <li class="nav-item pl-2 pr-2">
          <a class="m-2 my-lg-0" href="<?=$clientPath?>"><b><i class="fa fa-user-o" aria-hidden="true"></i> <span class="d-none d-md-inline">Member Area</span></b></a>
          <a class="m-2 my-lg-0" href="<?=$domain;?>/config/logout.php"><b><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="d-none d-md-inline">Logout</span></b></a>
        </li>
      <?php } ?>
      
    </ul>
  </div>
</nav>