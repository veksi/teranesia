<div id="myCarousel" class="carousel slide mb-0 bg-main" data-ride="carousel">

  <div class="carousel-inner" role="listbox">
    
    <div class="carousel-item active">
      <div class="container">
        <div class="row align-items-center" >
          <div class="col-sm">
            <div class="container text-center">
              <img class="img-fluid" alt="Web Hosting Terpercaya" src="<?=$imgserver?>server.png" >
            </div>
          </div>
          <div class="col-sm">
            <div class="container py-4 text-center text-white">
              <h1>Web Hosting Terpercaya </h1>
              <p class="text-white">Layanan web hosting indonesia dengan fitur super lengkap serta kemudahan lainnya</p>
              <p><button type="button" class="btn btn-custom text-white toMid shadow"><b>PILIH HOSTING</b></button></p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> 
</div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 190">
  <defs>
    <linearGradient id="lgrad" x1="0%" y1="50%" x2="100%" y2="50%" >
      <stop offset="0%" style="stop-color:rgba(83, 1, 117, 1);stop-opacity:1" />
      <stop offset="100%" style="stop-color:rgba(148, 0, 211, 1);stop-opacity:1" />
    </linearGradient>
  </defs>
  <path d="M0,64L80,53.3C160,43,320,21,480,53.3C640,85,800,171,960,181.3C1120,192,1280,128,1360,96L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z" fill="url(#lgrad)" fill-opacity="1"/>
</svg>

<div class="container mb-5">

  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Little Planet</b></h2></div>
  <div class="d-flex flex-wrap justify-content-center mb-5">
    <?php 
    $koloni = dbGetAll('product','category = 1 AND publish = 1','urutan','ASC','');
    foreach($koloni as $koloniVal):
    ?>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b><?=$koloniVal['title']?></b></h4>
        <p class="card-text font-weight-bold mb-0 text-info mt-4">
          <span style="font-size:1.5rem"><?=Rp(num($koloniVal['price']))?></span><small>/bln</small>
        </p>
        <p class="card-text py-4"><a href="<?=$domain?>order/<?=$koloniVal['id']?>" class="btn btn-outline-success px-5" >Order</a></p>
        <hr>
        <ul class="fa-ul text-left ml-4">
          <?php foreach(explode("|",$koloniVal['deskripsi']) as $item):?>
          <li class="py-1"><i class="fa-li fa fa-check text-info"></i> <?=$item?></li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <?php endforeach; ?>
    
  </div>

  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Big Planet</b></h2></div>
  <div class="d-flex flex-wrap justify-content-center mb-5">
    <?php 
    $bintang = dbGetAll('product','category = 2 AND publish = 1','urutan','ASC','');
    foreach($bintang as $bintangVal):
    ?>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b><?=$bintangVal['title']?></b></h4>
        <p class="card-text font-weight-bold mb-0 text-info mt-4">
          <span style="font-size:1.5rem"><?=Rp(num($bintangVal['price']))?></span><small>/bln</small>
        </p>
        <p class="card-text py-4"><a href="<?=$domain?>order/<?=$bintangVal['id']?>" class="btn btn-outline-success px-5" >Order</a></p>
        <hr>
        <ul class="fa-ul text-left ml-4">
          <?php foreach(explode("|",$bintangVal['deskripsi']) as $item):?>
          <li class="py-1"><i class="fa-li fa fa-check text-info"></i> <?=$item?></li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <?php endforeach; ?>
    
  </div>

  
  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Universe</b></h2></div>
  <div class="d-flex flex-wrap justify-content-center mb-5">
    <?php 
    $universe = dbGetRow('product','category = 3 AND publish = 1','urutan','ASC','');
    ?>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b><?=$universe['title']?></b></h4>
        <p class="card-text font-weight-bold mb-0 text-info mt-4">
          <span style="font-size:1.5rem"><?=Rp(num($universe['price']))?></span><small>/bln</small>
        </p>
        <p class="card-text py-4"><a href="<?=$domain?>order/<?=$universe['id']?>" class="btn btn-outline-success px-5" >Order</a></p>
        <hr>
        <ul class="fa-ul text-left ml-4">
          <?php foreach(explode("|",$universe['deskripsi']) as $item):?>
          <li class="py-1"><i class="fa-li fa fa-check text-info"></i> <?=$item?></li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>

  <div class="jumbotron bg-transparent">
    <div class="">
      <div class="row align-items-center text-center">
        <div class="col p-4">
          <button type="button" class="btn btn-custom text-white toMid shadow"><b>PINDAH HOSTING</b></button>
        </div>
        <div class="col p-4">
          <p>
            <span class="fa-stack fa-2x text-purple">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
            </span>
          </p>
          <h3 class="card-title">Sudah Ada Hosting?</h3>
          <p class="p-4 text-muted">Gampang, tidak harus beli domain baru! cukup lakukan transfer domain dari penyedia layanan hosting lama ke Planet Hosting.</p>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Seputar Web Hosting</b></h2></div>

<div class="jumbotron rounded-0 border-0 bg-transparent mt-0 pt-0">
  <div class="row justify-content-md-center">
    <div class="col-sm-12 col-md-8">
      
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)" id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Pengertian Web Hosting</small></h3></div>
                <div class="col text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseOne" class="panel-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Web hosting adalah layanan untuk menyimpan data seperti website, file, multimedia dan sebagainya agar dapat diakses secara massal. Data tersebut ditempatkan dalam server yang aktif 24 jam setiap hari. Kualitas maupun kecepatan akses data ini bergantung pada Kualitas layanan suatu web hosting. Oleh sebab itu selalu pilih layanan web hosting yang berkualitas dan terpercaya.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Web Hosting Indonesia</small></h3></div>
                <div class="col text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseTwo" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Kualitas akses data dari suatu web hosting juga bergantung pada lokasi tempat penyimpanan data tersebut berada. Analoginya jarak bumi ke bulan lebih dekat daripada jarak bumi ke matahari, sehingga untuk mencapai bulan dengan kendaraan jet yang sama dibutuhkan lebih sedikit bahan bakar dan waktu singkat ketimbang ke matahari.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" onClick="reply_click(this.id)" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Pindah Hosting</small></h3></div>
                <div class="col text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseThree" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Bila layanan web hosting anda saat ini masih diluar expektasi, maka pindah hosting ke planet hosting bisa menjadi solusi untuk menjawab masalah tersebut.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="jumbotron text-white mb-0 bg-main" style="border-radius:unset;">
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col p-4">
        <h3>Jadi pilih paket hosting yang mana?</h3>
      </div>
      <div class="col p-4">
        <button type="button" class="btn btn-custom btn-lg text-white toMid shadow"><b>ORDER SEKARANG</b></button>
      </div>
    </div>
  </div>
</div>

<style>
  .blobmaker {
    background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg viewBox='0 0 200 200' xmlns='http://www.w3.org/2000/svg'%3e%3cpath fill='%239400D3' d='M35.7,-65.3C45.3,-56.3,51.4,-44.7,61.3,-33.4C71.3,-22.1,85.2,-11.1,87,1C88.7,13.1,78.3,26.1,67.5,36C56.7,46,45.5,52.7,34.2,56.8C22.9,60.8,11.4,62.2,-2.5,66.6C-16.5,71,-32.9,78.3,-45,74.6C-57,71,-64.6,56.3,-71.7,42C-78.8,27.8,-85.5,13.9,-83.4,1.2C-81.3,-11.5,-70.4,-22.9,-62.7,-36.1C-55,-49.4,-50.4,-64.4,-40.5,-73.1C-30.5,-81.8,-15.3,-84.3,-1.1,-82.4C13.1,-80.5,26.1,-74.2,35.7,-65.3Z' transform='translate(100 100)' /%3e%3c/svg%3e");
    background-repeat:no-repeat;    
    background-position: 0 50%;
}
</style>

<script>

function setExt(ext){
  $("#ext option").filter(function() {
    return $(this).text() == ext;
  }).prop('selected', true);
}

$('.toMid').on('click', function(e) {
  $('html, body').animate({scrollTop : 500},800, 'linear');
})

</script>