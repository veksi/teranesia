<?php $domainDb = dbGetAll('domain_bucket','','','',''); ?>

<div class="jumbotron rounded-0 border-0 mb-0 text-center bg-main" >
  <h1 class="text-white mt-1">Amankan Domain Anda Sebelum Terlambat!</h1>
  <p><span class="text-white">Sedang mencari nama domain? Temukan domain impian Anda hanya di planet hosting!</span></p>
  <div class="row justify-content-md-center">
    <div class="col-sm-12 col-md-8">
      <div id="cek_domain">
        <form method="post" action="" onSubmit="return validasi();">
          <div class="row mx-2">
              <div class="col px-1 my-2">
                  <input type="text" class="form-control form-control-lg" id="domain" name="domain" placeholder="Temukan domain anda disini" autocomplete="off">
                  <input type="hidden" value="74e1b6d17a204c8a8c8ede0f2ff1470cb060edcc" name="token">
                  <input type="hidden" value="true" name="direct">
              </div>
              <div class="col-4 col-md-2 px-1 my-2">
                  <select class="form-control form-control-lg" id="ext" name="ext">
                  <?php foreach($domainDb as $domainVal): ?>
                      <option value="<?=$domainVal['extension']?>"><?=$domainVal['extension']?></option>
                      <?php endforeach; ?>
                  </select>
              </div>
              <div class="col-sm-12 col-md-2 px-1 my-2">
                  <button class="btn btn-custom btn-lg text-white text-nowrap shadow" name="cek" type="submit"><b>CEK DOMAIN</b></button>
              </div>
          </div>
        </form>        
        <?php
        if (isset($_POST['cek'])) {
          $nama_domain = "$_POST[domain]"."$_POST[ext]";
          $arrHost = @gethostbynamel("$nama_domain");
          if (empty($arrHost)){
            echo "<h2 class='text-white'>Congratulations! <u><b>$nama_domain</b></u> is available.</h2>Continue to register this domain for Rp1.499.000,00";
          }else{
            echo "<div class='text-white hasildomain'>Domain <u><b>$nama_domain</b></u> sudah digunakan, coba dengan domain lain.</div>";
          }
        }
        ?>
      </div>
    </div>
  </div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 190">
  <defs>
    <linearGradient id="lgrad" x1="0%" y1="50%" x2="100%" y2="50%" >
      <stop offset="0%" style="stop-color:rgba(83, 1, 117, 1);stop-opacity:1" />
      <stop offset="100%" style="stop-color:rgba(148, 0, 211, 1);stop-opacity:1" />
    </linearGradient>
  </defs>
  <path d="M0,64L80,53.3C160,43,320,21,480,53.3C640,85,800,171,960,181.3C1120,192,1280,128,1360,96L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z" fill="url(#lgrad)" fill-opacity="1"/>
</svg>

<div class="container">

  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Domain Populer</b></h2></div>
  <div class="d-flex flex-wrap justify-content-center mb-5">
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b>.COM</b></h4>
        <p class="card-text">Ekstensi domain paling populer di dunia</p>
      </div>
      <div class="card-footer text-center bg-transparent border-0 pb-4 py-0">
        <p class="card-text font-weight-bold mb-0 text-info py-4" style="font-size:1.5rem"><?=Rp(num(kurs(dbGetRow('domain_bucket'," extension = '.com' ",'','')['register'],false,true)))?></p>
        <p class="card-text"><button type="button" class="btn btn-outline-success toTop" onclick="setExt('.com')">Order</button></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b>.ID</b></h4>
        <p class="card-text">Ekstensi domain populer saat ini di indonesia</p>
      </div>
      <div class="card-footer text-center bg-transparent border-0 pb-4 py-0">
        <p class="card-text font-weight-bold mb-0 text-info py-4" style="font-size:1.5rem"><?=Rp(num(kurs(dbGetRow('domain_bucket'," extension = '.id' ",'','')['register'],false,true)))?></p>
        <p class="card-text"><button type="button" class="btn btn-outline-success toTop" onclick="setExt('.id')">Order</button></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0" style="width: 15rem;">
      <div class="card-body text-center">
        <h4 class="card-title pt-4 text-purple"><b>.CO.ID</b></h4>
        <p class="card-text">Rekomendasi untuk bidang komersil di indonesia</p>
      </div>
      <div class="card-footer text-center bg-transparent border-0 pb-4 py-0 ">
        <p class="card-text font-weight-bold mb-0 text-info py-4" style="font-size:1.5rem"><?=Rp(num(kurs(dbGetRow('domain_bucket'," extension = '.co.id' ",'','')['register'],false,true)))?></p>
        <p class="card-text"><button type="button" class="btn btn-outline-success toTop" onclick="setExt('.co.id')">Order</button></p>
      </div>
    </div>
    <div class="card mb-4 shadow-arround mx-3 rounded-1-5 border-0 " style="width: 15rem;">
      <div class="card-body text-center">
      <h4 class="card-title pt-4 text-purple"><b>.ORG</b></h4>
        <p class="card-text">Cocok untuk organisasi yang sedang berkembang</p>
      </div>
      <div class="card-footer text-center bg-transparent border-0 pb-4 py-0">
        <p class="card-text font-weight-bold mb-0 text-info py-4" style="font-size:1.5rem"><?=Rp(num(kurs(dbGetRow('domain_bucket'," extension = '.org' ",'','')['register'],false,true)))?></p>
        <p class="card-text"><button type="button" class="btn btn-outline-success toTop" onclick="setExt('.org')">Order</button></p>
      </div>
    </div>
  </div>
  
  <div class="text-center"><h2 class="display-5 mb-5 text-purple mt-0 pt-0"><b>Domain Rekomendasi</b></h2></div>
  
  <div class="row justify-content-md-center">
    <div class="col-md-8 col">
      <div class="table-responsive">
        <table class="table table-lg table-hover border">
          <thead class="bg-main text-white text-left">
            <tr>
              <th class="align-middle px-4">Domain</th>
              <th class="align-middle px-4">Harga</th>
              <th class="align-middle px-4 text-center"><div class="text-warning"><i class="fa fa-star fa-lg"></i><i class="fa fa-star fa-2x"></i><i class="fa fa-star fa-lg"></i></div></th>
            </tr>
          </thead>
          <tbody>

            <?php             
            foreach($domainDb as $domainVal):
              if($domainVal['id']>=5){
            ?>
            <tr>
              <td class="px-4 align-middle text-uppercase"><?=$domainVal['extension']?></td>
              <td class="px-4 align-middle"><?=Rp(num(kurs($domainVal['register'],false,true)))?></td>
              <td class="text-center align-middle"><button type="button" class="btn btn-custom text-white toTop shadow" onclick="setExt('<?=$domainVal['extension']?>')">Order </button></td>
            </tr>
            <?php } endforeach; ?>
          
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="jumbotron bg-transparent ">
    <div class="">
      <div class="row align-items-center text-center">
        <div class="col p-4">
          <button type="button" class="btn btn-custom text-white toTop shadow"><b>TRANSFER DOMAIN</b></button>
        </div>
        <div class="col p-4">
          <p>
            <span class="fa-stack fa-2x text-purple">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
            </span>
          </p>
          <h3 class="card-title">Sudah Ada Domain?</h3>
          <p class="p-4 text-muted">Gampang, tidak harus beli domain baru! cukup lakukan transfer domain dari penyedia layanan hosting lama ke Planet Hosting.</p>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="text-center"><h2 class="display-5 mb-5 text-purple"><b>Seputar Domain Murah</b></h2></div>

<div class="jumbotron rounded-0 border-0 bg-transparent mt-0 pt-0">
  <div class="row justify-content-md-center">
    <div class="col-sm-12 col-md-8">
      
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)" id="arrow"  data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Pengertian Domain</small></h3></div>
                <div class="col text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseOne" class="panel-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Domain adalah sebuah nama yang merujuk pada suatu website. Jika web hosting sebagai rumah maka domain sebagai alamatnya. Domain bersifat unik sehingga tidak terjadi kesalahan pertukaran data saat diakses oleh pengunjung. Sejatinya domain berbentuk angka yaitu IP address, namun agar mudah diingat maka pakar IT dunia sepakat melakukan masking IP tersebut menjadi kata.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      <br>
      <div class="accordion" id="accordionExample">
        <div class="card border rounded">
          <a href="javascipt:void(0)"  id="arrow" data-toggle="collapse" data-target="#collapseFour" aria-controls="collapseFour">
            <div class="card-header bg-transparent" id="headingOne">
              <div class="row align-items-center">
                <div class="col"><h3><small>Kegunaan Domain</small></h3></div>
                <div class="col text-right"><i class="fa fa-chevron-down"></i></div>
              </div>
            </div>
          </a>
          <div id="collapseFour" class="panel-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body p-0">
                  <div class="list-group" id="list-tab" role="tablist">
                      <p class="p-4">Jika web hosting sebagai rumah maka domain sebagai alamatnya. Dengan adanya domain maka website anda akan mudah ditemukan oleh siapapun dari seluruh dunia.</p>
                  </div>                                    
              </div>
          </div>
        </div>
      </div>
      

    </div>
  </div>
</div>

<div class="jumbotron text-white mb-0  bg-main" style="border-radius:unset;">
  <div class="container">
    <div class="row align-items-center text-center">
      <div class="col p-4">
        <h3>Jadi sudah punya domain atau belum?</h3>
      </div>
      <div class="col p-4">
        <button type="button" class="btn btn-custom btn-lg text-white toTop shadow"><b>ORDER SEKARANG </b></button>
      </div>
    </div>
  </div>
</div>

<style>
  .blobmaker {
    background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg viewBox='0 0 200 200' xmlns='http://www.w3.org/2000/svg'%3e%3cpath fill='%239400D3' d='M35.7,-65.3C45.3,-56.3,51.4,-44.7,61.3,-33.4C71.3,-22.1,85.2,-11.1,87,1C88.7,13.1,78.3,26.1,67.5,36C56.7,46,45.5,52.7,34.2,56.8C22.9,60.8,11.4,62.2,-2.5,66.6C-16.5,71,-32.9,78.3,-45,74.6C-57,71,-64.6,56.3,-71.7,42C-78.8,27.8,-85.5,13.9,-83.4,1.2C-81.3,-11.5,-70.4,-22.9,-62.7,-36.1C-55,-49.4,-50.4,-64.4,-40.5,-73.1C-30.5,-81.8,-15.3,-84.3,-1.1,-82.4C13.1,-80.5,26.1,-74.2,35.7,-65.3Z' transform='translate(100 100)' /%3e%3c/svg%3e");
    background-repeat:no-repeat;    
    background-position: 0 50%;
}
</style>

<script>

function setExt(ext){
  $("#ext option").filter(function() {
    return $(this).text() == ext;
  }).prop('selected', true);
}

$('.toTop').on('click', function(e) {
  $('html, body').animate({scrollTop : 0},800, 'linear');
})

function validasi(){

  var domain      = $("#domain").val();

  if (domain==""){
    $("#domain").focus();$("#domain").addClass('is-invalid');
    return false;
  }else{
    return true;
  }
}
</script>