<nav id="sidebar">
    <div class="sidebar-header">
        <a href="<?=$subdomain;?>"><h3><i class="fab fa-whatsapp"></i> </h3></a>
</h3>
    </div>
    <ul class="list-unstyled components">
        <li>
            <a href="<?=$subdomain;?>"><i class="fab fa-windows"></i> Beranda</a>
        </li>
        <li>
            <a href="#kontak" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle right-toggle right-chev"><i class="fas fa-address-book"></i> Kontak</a>
            <ul class="collapse list-unstyled" id="kontak">
                <li>
                    <a href="<?=$subdomain;?>kontak">Daftar Kontak</a>
                </li>
                <li>
                    <a href="<?=$subdomain;?>one">Group Kontak</a>
                </li>
                <li>
                    <a href="<?=$subdomain;?>one">Upload Kontak</a>
                </li>
                <li>
                    <a href="<?=$subdomain;?>one">Grab Kontak</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle right-toggle"><i class="fas fa-paper-plane"></i> Kirim Pesan</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
                <li>
                    <a href="<?=$subdomain;?>one">Kirim Satu</a>
                </li>
                <li>
                    <a href="<?=$subdomain;?>one">Kirim Banyak</a>
                </li>
                <li>
                    <a href="<?=$subdomain;?>one">Kirim Otomatis</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fas fa-envelope"></i> Kotak Keluar</a>
        </li>
        <li>
            <a href="#"><i class="fas fa-trash-alt"></i> Sampah</a>
        </li>
    </ul>
</nav>