<script>
$(document).ready(function() { 
    document.getElementById("hosting").value = '<?=$URL[3];?>';
    getPrice(<?=$URL[3];?>);
});
</script>

<?php 
$hosting  = dbGetAll('product','publish = 1','price','ASC',''); 
$domainDb = dbGetAll('domain_bucket','','','','');
?>

<body style="background-color:#F0F8FF !important"> 
  <div class="container">
    <div class="row justify-content-md-center my-5 ">

      <div class="col-sm-8">
        
        <div class="bg-white p-3 shadow-sm rounded">
          <div class="form-group mx-2">
            <label for="exampleInputEmail1">Paket Hosting</label>
            <select class="form-control form-control-lg" id="hosting" name="hosting" onchange="getPrice(this.value)">
              <?php foreach($hosting as $hostingVal): ?>
              <option class="p-4" value="<?=$hostingVal['id']?>" ><?=$hostingVal['title'].' - '.$hostingVal['ukuran']?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1" class="px-2">Durasi Paket</label>
            <ul class="nav nav-pills nav-fill mb-3 " id="pills-tab" role="tablist">
              <li class="nav-item m-2 border rounded">
                <div class="card border-0">
                  <div class="card-body p-2 bg-primary text-white rounded-top">
                    <b>1 Tahun</b>
                  </div>
                </div>
                <a class="nav-link  border-top-0 rounded-0 active " data-toggle="pill" href="#pills" role="tab" onclick="total(1)">
                  <div>Hemat</div>
                  <span class="years1"></span>/bln
                </a>
              </li>
              <li class="nav-item m-2 border rounded">
                <div class="card border-0">
                  <div class="card-body p-2 bg-primary text-white rounded-top">
                    <b>2 Tahun</b>
                  </div>
                </div>
                <a class="nav-link  border-top-0 rounded-0" data-toggle="pill" href="#pills" role="tab" onclick="total(2)">
                  <div>Hemat 5%</div>
                  <span class="years2"></span>/bln
                </a>
              </li>
              <li class="nav-item m-2 border rounded">
                <div class="card border-0">
                  <div class="card-body p-2 bg-custom text-white rounded-top">
                    <b>3 Tahun</b>
                  </div>
                </div>
                <a class="nav-link border-top-0 rounded-0" data-toggle="pill" href="#pills" role="tab" onclick="total(3)">
                  <div>Hemat 15%</div>
                  <span class="years3"></span>/bln
                </a>
              </li>
            </ul>

          </div>
        </div>
        
        <div class="bg-white my-5 p-3 shadow-sm rounded">
          <div class="form-group mx-2">
            <label for="exampleInputPassword1">Domain</label>
            <ul class="nav nav-pills align-items-center" id="pills-tab" role="tablist">
              <li class="nav-item my-2 border rounded">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><b>Domain Baru</b></a>
              </li>
              <li class="nav-item my-2 border rounded">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><b>Saya Sudah Punya Domain</b></a>
              </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row mx-0 border rounded mb-2">
                  <div class="col px-0 my-0">
                      <input type="text" class="form-control form-control-lg border-0" id="domain" name="domain" placeholder="domainbaru" autocomplete="off">
                  </div>
                  <div class="col col-md-3 px-0 my-0">
                      <select class="form-control form-control-lg border-0" id="ext" name="ext">
                      <?php foreach($domainDb as $domainVal): ?>
                          <option value="<?=$domainVal['extension']?>"><?=$domainVal['extension']?></option>
                          <?php endforeach; ?>
                      </select>
                  </div>
                </div>
                <button class="btn btn-custom text-white text-nowrap shadow" id="btn-search" name="cek"><b>Cek Domain</b></button>
                <br><br>
                <div id="response"></div>
              </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                
                <div class="row mx-0 border rounded mb-2">
                  <div class="col px-0 my-0">
                    <input type="text" class="form-control form-control-lg border-0" id="domainsaya" name="domainsaya" placeholder="domainsaya" autocomplete="off">
                  </div>
                  <div class="col col-md-3 px-0 my-0">
                    <select class="form-control form-control-lg border-0" id="extsaya" name="extsaya">
                    <?php foreach($domainDb as $domainVal): ?>
                        <option value="<?=$domainVal['extension']?>"><?=$domainVal['extension']?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <button class="btn btn-custom text-white text-nowrap shadow" name="cek" onclick="setDomainSaya()"><b>Gunakan Domain</b></button>
                <br><br>
              </div>
            </div>
          </div>
        </div>
<!--
        <div class="bg-white my-5 p-3 shadow-sm rounded">
          <div class="form-group mx-2">
            <label for="exampleInputPassword1">Rekomendasi Fitur Tambahan</label>
            <div class="row align-items-center text-center">
                <div class="col-md-2">
                  <i class="fa fa-wordpress fa-3x text-purple" aria-hidden="true"></i>
                </div>
                <div class="col-md-8 text-md-left">
                  <b>Auto Install Website Wordpress</b><br>
                  <small>Auto Install WordPress dan Optimize WordPress LiteSpeed Cache.</small>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-lg btn-custom text-white"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                </div>
            </div>
          </div>
          <hr>
          <div class="form-group mx-2">
            <div class="row align-items-center text-center">
                <div class="col-md-2">
                  <i class="fa fa-link fa-3x text-purple" aria-hidden="true"></i>
                </div>
                <div class="col-md-8 text-md-left">
                  <b>Backlink Maker</b><br>
                  <small>Submit backlink ke 50+ domain.</small>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-lg btn-custom text-white"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>
                </div>
            </div>
          </div>
        </div>-->

        <div class="bg-white my-5 p-3 shadow-sm rounded">
          <div class="form-group mx-2">
            <label for="exampleInputPassword1">Informasi Anda</label>
            <ul class="nav nav-pillss justify-content-center" id="pills-tab" role="tablist">
              <li class="nav-item my-2">
                <a class="nav-link border active rounded-pill" id="pills-masuk-tab" data-toggle="pill" href="#pills-masuk" role="tab" aria-controls="pills-masuk" aria-selected="true"><b>Masuk</b></a>
              </li>
              <li class="nav-item m-2">
                <a class="nav-link border rounded-pill" id="pills-daftar-tab" data-toggle="pill" href="#pills-daftar" role="tab" aria-controls="pills-daftar" aria-selected="false"><b>Daftar</b></a>
              </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-masuk" role="tabpanel" aria-labelledby="pills-masuk-tab">
                <br>
                <div class="form-row justify-content-md-center">
                  <div class="col-md-6">
                    <form id="loginForm">
                      <input type="hidden" name="login">
                      <div class="form-group">
                        <input class="form-control" id="u" name="u" type="email" placeholder="Enter Email"/>
                      </div>
                      <div class="form-group">
                        <input class="form-control" id="p" name="p" type="password" placeholder="Password"/>
                      </div>
                      <div id="loginRespon"></div>
                      <div class="form-group text-center mt-4 mb-0">
                        <button type="submit" id="loginBtn" class="btn btn-primary loadingBtn font-weight-bold" >Masuk & Checkout</button>
                      </div>
                    </form>
                  </div>
                </div>
                
              </div>
              <div class="tab-pane fade" id="pills-daftar" role="tabpanel" aria-labelledby="pills-daftar-tab">
                
                <form>
                  <input type="hidden" name="register">
                  <div class="d-flex py-2">
                      <div class="flex-fill p-0 m-0"><hr></div>
                      <div class="flex-fill text-center p-0 m-0 text-nowrap">Personal Information</div>
                      <div class="flex-fill p-0 m-0"><hr></div>
                  </div> 
                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-user form-control-feedback"></span>
                              <input class="form-control py-2" id="name_d" name="name_d" type="text" placeholder="First Name" />
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-user form-control-feedback"></span>
                              <input class="form-control py-2" id="name_b" name="name_b" type="text" placeholder="Last Name" />
                          </div>
                      </div>
                  </div>
                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-envelope form-control-feedback"></span>
                              <input class="form-control py-2" id="email" name="email" type="text" placeholder="Email Address" onkeyup="validEmail()"/>
                              <div class="invalid-feedback">Email tidak valid</div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-phone form-control-feedback"></span>
                              <input class="form-control py-2" id="phone" name="phone" type="text" placeholder="Phone Number" />
                          </div>
                      </div>
                  </div>

                  <div class="d-flex py-2">
                      <div class="flex-fill p-0 m-0"><hr></div>
                      <div class="flex-fill text-center p-0 m-0 text-nowrap">Billing Address</div>
                      <div class="flex-fill p-0 m-0"><hr></div>
                  </div>
                  <div class="form-group has-icon">
                      <span class="fa fa-building form-control-feedback"></span>
                      <input class="form-control py-2" id="company" name="company" type="text" placeholder="Company Name (Optional)" />
                  </div>
                  <div class="form-group has-icon">
                      <span class="fa fa-building-o form-control-feedback"></span>
                      <input class="form-control py-2" id="street" name="street" type="text" placeholder="Street Address" />
                  </div>
                  <div class="form-group has-icon">
                      <span class="fa fa-map-marker form-control-feedback"></span>
                      <input class="form-control py-2" id="street2" name="street2" type="text" placeholder="Street Address 2" />
                  </div>
                  <div class="form-row">
                      <div class="col-md-4">
                          <div class="form-group has-icon">
                              <span class="fa fa-building-o form-control-feedback"></span>
                              <input class="form-control py-2" id="city" name="city" type="text" placeholder="City" />
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group has-icon">
                              <span class="fa fa-map-signs form-control-feedback"></span>
                              <input class="form-control py-2" id="state" name="state" type="text" placeholder="State" />
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group has-icon">
                              <span class="fa fa-certificate form-control-feedback"></span>
                              <input class="form-control py-2" id="postcode" name="postcode" type="text" placeholder="Postcode" />
                          </div>
                      </div>
                  </div>
                  <div class="form-group has-icon">
                      <span class="fa fa-globe form-control-feedback"></span>
                      <input class="form-control py-2" id="country" name="country" type="text" placeholder="Country" />
                  </div>
                  <div class="form-group has-icon">
                      <span class="fa fa-building form-control-feedback"></span>
                      <input class="form-control py-2" id="tax" name="tax" type="text" placeholder="Tax ID" />
                  </div>

                  <div class="d-flex py-2">
                      <div class="flex-fill p-0 m-0"><hr></div>
                      <div class="flex-fill text-center p-0 m-0 text-nowrap">Account Security</div>
                      <div class="flex-fill p-0 m-0"><hr></div>
                  </div>
                  <div class="form-row">
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-lock form-control-feedback"></span>
                              <input class="form-control py-2" id="password" name="password" type="password" placeholder="Password" />
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group has-icon">
                              <span class="fa fa-lock form-control-feedback"></span>
                              <input class="form-control py-2" id="repassword" name="repassword" type="password" placeholder="Confirm Password" />
                          </div>
                      </div>
                  </div>

                  <div class="card">
                      <div class="card-header text-muted">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Terms of Service
                      </div>
                      <div class="card-body">
                          <div class="form-group form-check">
                              <input type="checkbox" class="form-check-input" id="agree">
                              <label class="form-check-label text-muted" for="agree">I have read and agree to the Terms of Service</label>
                              <div class="invalid-feedback">You must accept our Terms of Service</div>
                          </div>
                      </div>
                  </div>
                  <div class="form-group text-center mt-4 mb-0">
                      <input type="submit" id="regBtn" value="Register" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Register">
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>

    </div>

    <div class="col-sm-4">

      <div class="bg-transparent sticky-top">
        <div class="bg-white p-3 shadow-sm rounded">
          <!--
          <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
              <div class="stepwizard-step col-xs-3"> 
                <span class="fa-stack text-primary">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
                </span>
                <span class="badge font-weight-normal">Hosting & Domain</span>
              </div>
              <div class="stepwizard-step col-xs-3"> 
                <span class="fa-stack text-grey">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
                </span>
                <span class="badge font-weight-normal">Tambah Fitur</span>
              </div>
              <div class="stepwizard-step col-xs-3"> 
                <span class="fa-stack text-grey">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
                </span>
                <span class="badge font-weight-normal">Metode Pembayaran</span>
              </div>
            </div>
          </div>-->
          
          <br>
          <form>
            <div class="form-group">
              <label for="exampleInputEmail1" class="m-0"><small>Hosting 1 Tahun</small></label>
              <div class="row">
                <div class="col-6"><div class="paket"></div></div> 
                <div class="col-6 text-right"><div class="hostingYears"></div></div>
                <input type="hidden" id="hostingFix" name="hostingFix">
                <input type="hidden" id="yearsFix" name="yearsFix" value="1">
              </div>
              <br>
              <div class="domainRows d-none">
                <label for="exampleInputEmail1" class="m-0"><small>Domain</small></label>
                <div class="row">
                  <div class="col-6"><div class="domain"></div></div> 
                  <div class="col-6 text-right"><div class="hargaDomain"></div></div>
                  <input type="hidden" id="domainFix" name="domainFix">
                </div>
                <br>
              </div>
              <label for="exampleInputEmail1" class="m-0"><small>Pajak & Biaya Tambahan</small></label>
              <div class="row">
                <div class="col-6">PPN 10%</div> 
                <div class="col-6 text-right">Rp. 0</div>
              </div>
              <hr>
              <div class="row">
                <div class="col-6">TOTAL</div> 
                <div class="col-6 text-right"><div class="total"></div></div>
              </div>
            </div>

          </form>
        </div>
        <!--<br>
        <div class="btn-lanjut text-center d-none">
          <button class="btn btn-custom text-white text-nowrap shadow" id="lanjut" name="lanjut" ><b>Lanjutkan</b></button>
        </div>
        -->
      </div>
    </div>
  </div>

</body>

<script>
function getPrice(val){ 
  $.ajax({     
    type: "GET",
    url: "<?=$domain;?>modul/ajax/dataOrderHosting.php?paket&id="+val,
    success: function(respon){
      var data = JSON.parse(respon);
      var twoYearsDisc = ((data.price * 24) - ((5/100) * (data.price * 24)))/24;
      var threeYearsDisc = ((data.price * 36) - ((15/100) * (data.price * 36)))/36;
      
      $('.paket').text(data.title);
      
      $('.years1').text(Rp(num(data.price)));
      $('.years2').text(Rp(num(twoYearsDisc)));
      $('.years3').text(Rp(num(threeYearsDisc)));

      $('#hostingFix').val(val);
      total($('#yearsFix').val());
    }     
  });
}

$(function(){
  $('#loginBtn').on('click', function(e){
    e.preventDefault();
    var u = $('#u').val(),
        p = $('#p').val()

    if(u==''){
      $("#u").focus();$("#u").addClass('is-invalid');$(".loadingBtn").html('Masuk & Checkout');
    }else if(p==''){
      $("#p").focus();$("#p").addClass('is-invalid');$(".loadingBtn").html('Masuk & Checkout');
      return false;
    }else{
      $(".loadingBtn").attr('disabled', true);
      $("#u").removeClass('is-invalid');
      $("#p").removeClass('is-invalid');

      $.ajax({
          url: "<?=$domain;?>library/auth.php",
          type: "POST",
          data: $('form').serialize(),
          success: function(data)
          {
            if(data==0){ 
              $("#loginRespon").html('Silakan periksa kembali detil login Anda!');$(".loadingBtn").html('Masuk & Checkout');$(".loadingBtn").attr('disabled', false); 
            }else{
              
              window.location.href="<?=$domain?>thankyou?"; 
            }
          }
      });
    }
  });
});

$('#btn-search').on('click',function(e){
  //e.preventDefault(); 
  var search_extension  = $('#ext option:selected').val(),
      search_domain     = $('#domain').val(),
      $sresult          = $('#response')
  
  if(search_domain==''){
    $("#domain").focus();$("#domain").addClass('is-invalid');
    return false;
  }else{
    $("#domain").removeClass('is-invalid');

    if(search_domain && search_extension){
      var formData = {
        'search_domain' : search_domain,
        'search_extension' :search_extension
      };
      $sresult.html('<p><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Please wait....</p>');
      $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : "<?=$domain;?>modul/ajax/dataOrderHosting.php?domainCheck",
        data        : formData, // our data object
      }).done(function(data) {
        $sresult.html(data);
      });
      //e.preventDefault();
    }
  }
});

function setDomainSaya(){
  var domain        = $('#domainsaya').val();
  var ext           = $('#extsaya').val();
  if(domain==''){
    $("#domainsaya").focus();$("#domainsaya").addClass('is-invalid');
    return false;
  }else{
    $("#domainsaya").removeClass('is-invalid');

    $('.domain').text(domain+ext);
    $('#domainFix').val(domain+ext);
    $('.hargaDomain').text(Rp(num(0)));
    $('.domainRows').addClass('d-block');
    //$('.btn-lanjut').addClass('d-block');
    total($('#yearsFix').val());
  }
}

function setDomain(price){
  var domain        = $('#domain').val();
  var ext           = $('#ext').val();
  $('.domain').text(domain+ext);
  $('#domainFix').val(domain+ext);
  $('.hargaDomain').text(Rp(num(price)));
  $('.domainRows').addClass('d-block');
  //$('.btn-lanjut').addClass('d-block');
  total($('#yearsFix').val());
}

function total(years){
  var paket       = $('.years'+years).text().replace("Rp. ", "").replace(/,/g, "");
  var paketYears  = Number(paket) * (years * 12);
  var domain      = Number($('.hargaDomain').text().replace("Rp. ", "").replace(/,/g, ""));
  $('.hostingYears').text(Rp(num(paketYears)));
  $('.total').text(Rp(num(paketYears + domain)));
  $('#yearsFix').val(years);
}

$(".loadingBtn").click(function() {    
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
});
</script>