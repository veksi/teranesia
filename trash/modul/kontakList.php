<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Telp</th>
      <th scope="col">Nama</th>
      <th scope="col">#</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>1</td>
        <td scope="row">+62812-7774-3487</td>
        <td>Budi</td>
        <td><div class="btn-group">
              <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pilihan
              </button>
              <div class="dropdown-menu dropdown-menu-right">
                <button class="dropdown-item" type="button">Edit</button>
                <button class="dropdown-item" type="button">Hapus</button>                
              </div>
            </div>
        </td>        
    </tr>
    <tr>
        <td>2</td>
        <td scope="row">+62812-7774-3488</td>
        <td>Edi</td>
        <td><div class="btn-group">
              <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pilihan
              </button>
              <div class="dropdown-menu dropdown-menu-right">
                <button class="dropdown-item" type="button">Edit</button>
                <button class="dropdown-item" type="button">Hapus</button>                
              </div>
            </div>
        </td>
    </tr>
  </tbody>
</table>