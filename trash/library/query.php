<?php 
function dbGetNum($table,$where,$order,$sort){
    if(empty($where)){
        $where = '';
    }else{
        $where = ' WHERE '.$where;
    }
    if(empty($order)){
        $order = '';
    }else{
        $order = ' ORDER BY '.$order.' '.$sort;
    }
    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT * FROM $table $where $order ");
    $num    = mysqli_num_rows($query);
    return $num;
}

function dbGetRow($table,$where,$order,$sort){
    if(empty($where)){
        $where = "";
    }else{
        $where = " WHERE ".$where;
    }
    if(empty($order)){
        $order = '';
    }else{
        $order = ' ORDER BY '.$order.' '.$sort;
    }
    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT * FROM $table $where $order");
    if($result = $query){
        $row    = mysqli_fetch_assoc($query);
    }
    return $row;
}

function dbGetAll($table,$where,$order,$sort,$limit){
    if(empty($where)){
        $where = '';
    }else{
        $where = ' WHERE '.$where;
    }
    if(empty($order)){
        $order = '';
    }else{
        $order = ' ORDER BY '.$order.' '.$sort;
    }
    if(empty($limit)){
        $limit = '';
    }else{
        $limit = ' LIMIT '.$limit;
    }

    $db = new PDO('mysql:host=localhost;dbname=teranesia_db', "teranesia_user", "a,iHfgAp8}FI");
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $query = $db->prepare("SELECT * FROM client");
    $result = $query->fetchAll();

    //$conn   = db_conn();
    //$query = mysqli_query($conn,"SELECT * FROM $table $where $order $limit");
    //if($query==true){
    //    while($row = mysqli_fetch_assoc($query)){
    //        $result[] = $row;
    //    }
    //}else{
     //   $result = "404";
    //}
    return $result;
}

function dbGetJoin($table_A,$table_B,$onField,$operation,$where,$model){
    if(empty($where)){
        $where = '';
    }else{
        $where = ' WHERE '.$where;
    }

    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT $operation FROM $table_A AS a JOIN $table_B AS b ON a.$onField = b.$onField $where ");

    if(empty($model)){
        $result = mysqli_num_rows($query);
    }else{
        $result = mysqli_fetch_assoc($query);
    }

    return $result;
}

function reg($nama_d,$nama_b,$email,$password,$phone,$company,$street,$street2,$city,$state,$postcode,$country,$tax){
        
    $conn               = db_conn();
    $password           = password_hash($password, PASSWORD_DEFAULT);

    $ins = mysqli_query($conn,"INSERT INTO client SET
        first_name      = '$nama_d',
        last_name       = '$nama_b',
        email           = '$email',
        password        = '$password',
        phone           = '$phone',
        company         = '$company',
        street          = '$street',
        street_2        = '$street2',
        city            = '$city',
        state           = '$state',
        postcode        = '$postcode',
        country         = '$country',
        tax             = '$tax'
    ");

    //header('Location:'.$domain.'register');
    //exit;

}

function login($u,$p){
        
    $conn       = db_conn();
    //$password   = password_hash($p, PASSWORD_DEFAULT);
    //$query      = mysqli_query($conn,"SELECT * FROM client WHERE email = '$u' AND password = '$password' AND active = 1 ");
    $query      = mysqli_query($conn,"SELECT * FROM client WHERE email = '$u' AND active = 1 ");
    //if (mysqli_num_rows($query) == 1) { 
    $row = mysqli_fetch_assoc($query);
    if(password_verify($p,$row['password'])){
        $_SESSION['authorized_client']  = true;
        $_SESSION['client']             = $row['id'];
        return 1;
    }else{
        return 0;
    }

}

function minValue($table,$col,$where){
    if(empty($where)){
        $where = '';
    }else{
        $where = ' WHERE '.$where;
    }
    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT MIN($col) as minimum FROM $table $where " ); 
    $row    = mysqli_fetch_assoc($query); 
    return $row['minimum'];
}

function maxValue($table,$col,$where){
    if(empty($where)){
        $where = '';
    }else{
        $where = ' WHERE '.$where;
    }
    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT MAX($col) as maximum FROM $table $where " ); 
    $row    = mysqli_fetch_assoc($query); 
    return $row['maximum'];
}

function sumAll($table){
    $conn   = db_conn();
    $query  = mysqli_query($conn,"SELECT SUM(price_ask_order) as total FROM $table WHERE status = 2 " ); 
    $row    = mysqli_fetch_assoc($query); 
    
    if(empty($row['total'])){
        $result = 0;
    }else{
        $result = $row['total'];
    }
    return $result;
}

function graphData($table){
    global $domain;
    $d = strtotime("-1 Months");
    $dateBefore = date("Y-m-d", $d);
    
    $dateNow = date("Y-m-d");
    while (strtotime($dateBefore) <= strtotime($dateNow)) {
        $arrMonth[] = '"'.substr($dateBefore,-2).'"';
        $arrTotal[] = sum($dateBefore,$table);
        $dateBefore = date ("Y-m-d", strtotime("+1 days", strtotime($dateBefore)));
    }

    $result = array($arrMonth,$arrTotal);
    return $result;
}

function sum($date,$table){
    $conn       = db_conn();
    $query      = mysqli_query($conn,"SELECT SUM(price_ask_order) AS total FROM $table WHERE status = 2 AND DATE(insertDate) = '$date' ");
    if($query==true){
        $row        = mysqli_fetch_assoc($query);
        if(empty($row['total'])){
            $result = 0;
        }else{
            $result = $row['total'];
        }
    }else{
        $result = 0;
    }
    return $result;
}

function insertBrand($category,$brand){
    $conn                       = db_conn();
    $part                       = explode("#",$brand);
    foreach($part as $val):
        $resVal                 = trim($val);
        $slug                   = seo($resVal);
        $ins                    = mysqli_query($conn,"INSERT INTO brand SET
            category            = '$category',
            title               = '$resVal',
            slug                = '$slug'
        ");  
    endforeach;       
    exit;
}

function insertBrandVersion($brand,$brandVersion){
    $conn                       = db_conn();
    $part                       = explode("#",$brandVersion);
    foreach($part as $val):
        $resVal                 = trim($val);
        $slug                   = seo($resVal);
        $ins                    = mysqli_query($conn,"INSERT INTO brand_version SET
            brand               = '$brand',
            title               = '$resVal',
            slug                = '$slug'
        ");  
    endforeach;       
    exit;
}
?>